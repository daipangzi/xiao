//province change
$('#province').change(function() {
    $.ajax({
      url: base_url+"common/get_city_list",
      type: "post",
      data: {'province': $(this).val(), "ci_csrf_token": ci_csrf_token()},
      success: function(res){
          $('#city').html(res);
      }   
    });    
});

function show_loading()
{
    $('#loading-mask').show();
}

function hide_loading()
{
    $('#loading-mask').hide();
}
