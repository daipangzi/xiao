<?php
    // Setup our default assets to load.
    Assets::add_js( array('bootstrap.min.js', 'codeigniter-csrf.js', 'custom2.js') );
    Assets::add_css( array('bootstrap.min.css', 'bootstrap-responsive.min.css'));
            
    $inline  = '$(".dropdown-toggle").dropdown();';

    Assets::add_js( $inline, 'inline' );
?>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<title><?php e($this->settings_lib->item('site.title')); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<script src="<?php echo Template::theme_url('js/modernizr-2.5.3.js') ?>"></script>

<?php echo Assets::css(); ?>

<!-- iPhone and Mobile favicon's and touch icons -->
<link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico">
</head>

<body>

<div class="container"> 

<?php $uri = $this->uri->segment(2); ?>
<div class="navbar">  
    <div class="navbar-inner">  
        <div class="container">  
            <ul class="nav">  
                <li class="<?php if($uri==''){echo 'active';}?>"><a href="<?php echo site_url("simulator"); ?>">Home</a></li>  
                <?php $aa = array('schools', 'school_info', 'login', 'logout', 'forgot'); ?>
                <li class="dropdown <?php if(in_array($uri, $aa)){echo 'active';}?>" id="messagemenu">  
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Common<b class="caret"></b></a>  
                    <ul class="dropdown-menu">  
                        <li><a href="<?php echo site_url("simulator/schools"); ?>">School</a></li>  
                        <li><a href="<?php echo site_url("simulator/class_subject_lists"); ?>">Class and Subject Lists</a></li>  
                        <li><a href="<?php echo site_url("simulator/class_lists"); ?>">Class Lists</a></li>  
                        <li><a href="<?php echo site_url("simulator/update_check"); ?>">Update Check</a></li>  
                        <li class="divider"></li>
                        
                        <li><a href="<?php echo site_url("simulator/login"); ?>">Login</a></li>  
                        <li><a href="<?php echo site_url("simulator/logout"); ?>">Logout</a></li>  
                        <li><a href="<?php echo site_url("simulator/forgot"); ?>">Forgot</a></li>  
                    </ul>
                </li>
                
                <?php $aa = array('profile', 'update_profile_general', 'update_profile_photo', 'update_profile_password'); ?>
                <li class="dropdown <?php if(in_array($uri, $aa)){echo 'active';}?>" id="messagemenu">  
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Profile<b class="caret"></b></a>  
                    <ul class="dropdown-menu">  
                        <li><a href="<?php echo site_url("simulator/profile"); ?>">Profile</a></li>  
                        <li class="divider"></li>
                        
                        <li><a href="<?php echo site_url("simulator/update_profile_general"); ?>">Update General</a></li>  
                        <li><a href="<?php echo site_url("simulator/update_profile_photo"); ?>">Update Photo</a></li>  
                        <li><a href="<?php echo site_url("simulator/update_profile_password"); ?>">Update Password</a></li>  
                        <li class="divider"></li>
                        
                        <li><a href="<?php echo site_url("simulator/teacher_update_subject"); ?>">Teacher Update Subject</a></li>  
                        <li><a href="<?php echo site_url("simulator/teacher_update_class"); ?>">Teacher Update Class</a></li>  
                        <li><a href="<?php echo site_url("simulator/teacher_update_school"); ?>">Teacher Update School</a></li>  
                        <li class="divider"></li>
                        
                        <li><a href="<?php echo site_url("simulator/parent_children_update_school"); ?>">Children Class</a></li>  
                        <li><a href="<?php echo site_url("simulator/parent_add_children"); ?>">Add Children</a></li>  
                        <li><a href="<?php echo site_url("simulator/parent_remove_children"); ?>">Remove Children</a></li>  
                    </ul>  
                </li>  
                
                <?php $aa = array('receiver_lists', 'sent_messages', 'received_messages', 'new_message'); ?>
                <li class="dropdown <?php if(in_array($uri, $aa)){echo 'active';}?>" id="messagemenu">  
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Message<b class="caret"></b></a>  
                    <ul class="dropdown-menu">  
                        <li><a href="<?php echo site_url("simulator/receiver_lists"); ?>">Receiver Lists</a></li>  
                        <li><a href="<?php echo site_url("simulator/new_message"); ?>">New Message</a></li>  
                        <li><a href="<?php echo site_url("simulator/new_message2"); ?>">New Message2</a></li>  
                        <li><a href="<?php echo site_url("simulator/chat_sessions"); ?>">Chat Sessions</a></li>  
                        <li><a href="<?php echo site_url("simulator/chat_conversations"); ?>">Chat Conversations</a></li>  
                        <li><a href="<?php echo site_url("simulator/new_chat_conversations"); ?>">New Chat Conversations</a></li>  
                        <li><a href="<?php echo site_url("simulator/count_new_message_nums"); ?>">Count new message nums</a></li>  
                        <li><a href="<?php echo site_url("simulator/remove_chat_session"); ?>">Unsubscribe Chat session</a></li>  
                        <li><a href="<?php echo site_url("simulator/remove_message"); ?>">Unsubscribe Message</a></li>  
                        <li><a href="<?php echo site_url("simulator/search_by_date_type"); ?>">Search</a></li>  
                    </ul>  
                </li>  
                
                <?php $aa = array('register_teacher', 'register_parents', 'register_complete', 'regenerate_sms'); ?>
                <li class="dropdown <?php if(in_array($uri, $aa)){echo 'active';}?>" id="messagemenu">  
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Register<b class="caret"></b></a>  
                    <ul class="dropdown-menu">  
                        <li><a href="<?php echo site_url("simulator/register_teacher"); ?>">Register Teacher</a></li>  
                        <li><a href="<?php echo site_url("simulator/register_parents"); ?>">Register Parent</a></li>  
                        <li class="divider"></li>
                        
                        <li><a href="<?php echo site_url("simulator/register_complete"); ?>">Register Complete</a></li>  
                        <li><a href="<?php echo site_url("simulator/regenerate_sms"); ?>">Regenerate SMS</a></li>  
                    </ul>  
                </li>  
            </ul>
        </div>  
    </div>  
</div>  
<!--end navbar-->

<form action="{server_url}/{controller}/{method}" class='form-horizontal' id='aform' method="post" enctype="multipart/form-data">
    <fieldset><legend>Client Info</legend></fieldset>
    <div id="client_part">
        <div class="control-group">
            <label for="server" class="control-label">Server</label>
            <div class="controls">
                <select id="server" name="server">
                    <option value="http://10.76.15.90/xiao/mobile">Localhost Server(10.76.15.90)</option>
                    <option value="http://218.25.89.223/mobile">Remote Server(218.25.89.223)</option>
                </select>
            </div>
        </div>
        
        <div class="control-group">
            <label for="device" class="control-label">Device Type</label>
            <div class="controls">
                <select id="device" name="param[client][device_type]">
                    <option value="<?php echo DEVICE_ANDROID; ?>">Android</option>
                    <option value="<?php echo DEVICE_IPHONE; ?>">iPhone</option>
                </select>
            </div>
        </div>
        
        <div class="control-group">
            <label for="device_info" class="control-label">Device</label>
            <div class="controls">
                <input type="text" name="param[client][device_info]" value="simulator_device_001">
            </div>
        </div>
        
        <div class="control-group">
            <label for="user_type" class="control-label">User Type</label>
            <div class="controls">
                <select id="user_type" name="param[client][user_type]">
                    <option value="1">Teacher</option>
                    <option value="9">Master</option>
                    <option value="10">Children</option>
                    <option value="100">Parent</option>
                </select>
            </div>
        </div>
        
        <div class="control-group">
            <label for="lang" class="control-label">Language</label>
            <div class="controls">
                <select id="lang" name="param[client][lang]">
                    <option value="en">English</option>
                    <option value="cn">Chinese</option>
                    <option value="ko">Korean</option>
                    <option value="ja">Japanese</option>
                </select>
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label">APK Version</label>
            <div class="controls">
                <input type="text" name="param[client][version]" value="1.35">
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label">Connection UID</label>
            <div class="controls">
                <input type="text" name="param[client][connection_uid]" value="1">
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label">User ID</label>
            <div class="controls">
                <input type="text" name="param[client][user_id]" value="">
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label">School UID(Parent)</label>
            <div class="controls">
                <input type="text" name="param[client][school_uid]" value="">
            </div>
        </div> 
    </div>

    <fieldset><legend>Data Info</legend></fieldset>
    <div id="data_part">
        <input type="hidden" name="param[data][temp]" value=""/>
        <?php echo isset($content) ? $content : Template::yieldContent(); ?>
    </div>
</form>
</div><!--/.container-->

<script>
var base_url = '<?php echo base_url(); ?>';
</script>
<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
<script>window.jQuery || document.write('<script src="<?php echo js_path(); ?>jquery-1.7.2.min.js"><\/script>')</script>

<!-- This would be a good place to use a CDN version of jQueryUI if needed -->
<?php echo Assets::js(); ?>

</body>
</html>