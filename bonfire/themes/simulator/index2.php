<?php
    // Setup our default assets to load.
    Assets::add_css( array('bootstrap.min.css', 'bootstrap-responsive.min.css'));            
?>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<title><?php e($this->settings_lib->item('site.title')); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<?php echo Assets::css(); ?>

<!-- iPhone and Mobile favicon's and touch icons -->
<link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico">
</head>

<body>

<div class="container"> 
    <?php echo isset($content) ? $content : Template::yieldContent(); ?>
</div><!--/.container-->
</body>
</html>