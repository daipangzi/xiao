$('#btn_submit').click(function() {
    form = $('#aform');
    
    server = $('#server').val();
    controller = get_controller_name($('#user_type').val());
    method = $('#method').val();
    if($('#controller').val())
    {
        controller = $('#controller').val();
    }
    
    action = form.attr('action');
    action = action.replace('{server_url}', server);
    action = action.replace('{controller}', controller);
    action = action.replace('{method}', method);
      
    form.attr('action', action);
     
    form.submit();
});

function get_controller_name(type)
{
    name = '';
    switch(type)
    {
        case '1':
            name = 'teacher';
            break;
        case '9':
            name = 'teacher';
            break;
        case '10':
            name = 'children';
            break;
        case '100':
            name = 'parents';
            break;
    }
    return name;
}

////////////////////////////////////////
//new message
$("#is_reserved").click(function(){
    if($(this).is(':checked'))
    {
        $("#dt_waiting_none").attr("disabled", "disabled");
        $("#dt_waiting_date").removeAttr("disabled");
    }
    else
    {
        $("#dt_waiting_date").attr("disabled", "disabled");
        $("#dt_waiting_none").removeAttr("disabled");
    }
});

if($('#master_box').length > 0)
{
    type = $('#user_type').val();
    show_options(type);
    
    $('#user_type').change(function() {
        type = $(this).val();
        show_options(type);
    });
}

function show_options(type)
{
    if(type == '10' || type == '100')
    {
        hide_box('#master_box');
        hide_box('#teacher_box');
    }
    else if(type == '1')
    {
        hide_box('#master_box');
        show_box('#teacher_box');
    }
    else
    {
        show_box('#master_box');
        show_box('#teacher_box');
    }    
}

function hide_box(box_id)
{
    box = $(box_id);
    box.hide();
    $('input', box).attr("disabled", "disabled");
}

function show_box(box_id)
{
    box = $(box_id);
    box.show();
    $('input', box).removeAttr("disabled");
}

///////////////////////////////////
//parent register
if($('#father_box').length > 0)
{
    if($('#father').is(':checked'))
    {
        show_box('#father_box');
    }
    else
    {
        hide_box('#father_box');
    }
    
    if($('#mother').is(':checked'))
    {
        show_box('#mother_box');
    }
    else
    {
        hide_box('#mother_box');
    }
    
    if($('#children1').is(':checked'))
    {
        show_box('#children1_box');
    }
    else
    {
        hide_box('#children1_box');
    }
    
    if($('#children2').is(':checked'))
    {
        show_box('#children2_box');
    }
    else
    {
        hide_box('#children2_box');
    }
    
    if($('#children3').is(':checked'))
    {
        show_box('#children3_box');
    }
    else
    {
        hide_box('#children3_box');
    }
    $('#user_type').val(100);
    
    $("#father, #mother, #children1, #children2, #children3").click(function(){
        id = $(this).attr('id');
        if($(this).is(':checked'))
        {
            show_box('#'+id + '_box');    
        }
        else
        {
            hide_box('#'+id + '_box');    
        }
    });
    
}