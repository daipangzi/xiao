/* Nofication Close Buttons */
$('.notification a.close').click(function(e){
	e.preventDefault();

	$(this).parent('.notification').fadeOut();
});

/*
	Check All Feature
*/
$(".check-all").click(function(){
	$("table input[type=checkbox]").attr('checked', $(this).is(':checked'));
    
    if($(this).is(':checked'))
    {
        $('input[type=submit].select_one').attr('disabled', false);
    }
    else
    {
        $('input[type=submit].select_one').attr('disabled', true);
    }
});
$("table input[type=checkbox]").click(function() {
    if($("table input[type=checkbox]:checked").length > 0)
    {
        $('input[type=submit].select_one').attr('disabled', false);
    }
    else
    {
        $('input[type=submit].select_one').attr('disabled', true);
    }
});

/*
	Dropdowns
*/
$('.dropdown-toggle').dropdown();

/*
	Set focus on the first form field
*/
$(":input:visible:first").focus();

/*
	Responsive Navigation
*/
$('.collapse').collapse();

/*
 Prevent elements classed with "no-link" from linking
*/
//$(".no-link").click(function(e){ e.preventDefault();	});

$('.date').datepicker({ dateFormat: 'yy-mm-dd'});

//$(".mask_card_id").mask("99999999999999999*");
//$('.date').mask("9999-99-99");

//when page refresh, if there is a selected tab, then select it
function select_saved_tab()
{  
    $('a[data-toggle="tab"]').on('show', function (e) {
        href = $(this).attr('href');
        $('#selected_tab').val(href); 
    });
    
    $(document).ready(function() {
        selected_tab = $('#selected_tab').val();
        if(selected_tab != '')
        {        
            $('a[href="'+selected_tab+'"]').trigger('click');
        }
    });
}

//user_photo
function set_user_photo(user_type)
{
    event = $.browser.msie?'click':'change'; 
    $('#user_photo').live(event, function() { 
        show_loading();
        $("#msg_user_photo").html('');
        $("#user-form").ajaxForm({
            url: base_url+'admin/system/' + user_type + '/upload_photo',
            success: function(image) { 
                if(image != '') { 
                    $("#img_user_photo").attr("src", base_url+"media/tmp/photo/" + user_type + "/"+image);
                    $("#msg_user_photo").html("");
                    $('#remover').show();
                    $("#image_name").val(image);
                    $("#image_action").val("changed"); 
                } else {
                    $("#msg_user_photo").html('Error in uploading');
                }
                hide_loading();
                $('#user-form').unbind('submit'); 
            },
        }).submit();
    }); 

    $('#remover').live('click', function() {
        $("#img_user_photo").attr("src", base_url+"media/placeholder/100x100.gif");   
        $("#image_name").val(""); 
        $("#image_action").val("deleted"); 
        $(this).hide();
        
        $('#save_photo, #apply_photo').removeAttr("disabled");
    }); 
}

//when change fields, then remove disable apply and save buttons
function user_tab_changes()
{
    //general tab
    $('#general-box input, #general-box select').change(function() {
        $('#save_general, #apply_general').removeAttr("disabled");
    });  
    
    //photo
    $('#photo-box input, #photo-box select').change(function() {
        $('#save_photo, #apply_photo').removeAttr("disabled");
    });   
    
    //password
    $('#password-box input, #password-box select').change(function() {
        $('#save_password, #apply_password').removeAttr("disabled");
    });  
}