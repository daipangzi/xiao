<?php
    $js = array('bootstrap.min.js', 
        'jwerty.js', 
        'codeigniter-csrf.js', 
        'jquery-ui-1.8.13.min.js',
        //'jquery.maskedinput-1.3.min.js',
        'common.js',
    );
	Assets::add_js($js, 'external', true);
?>
<?php echo theme_view('partials/_header'); ?>

<div class="container-fluid body">
        <?php echo Template::message(); ?>

        <?php echo Template::yieldContent(); ?>
</div>

<?php echo theme_view('partials/_footer'); ?>
