<?php echo theme_view('parts/_header'); ?>

<!-- Start of Main Container -->
<div class="container body narrow-body">

<?php

	echo Template::message();
	echo isset($content) ? $content : Template::yieldContent();
?>

</div>
<!--/.container-->

<?php echo theme_view('parts/_footer'); ?>
