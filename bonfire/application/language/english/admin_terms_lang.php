<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//common
$lang['ct_unknown']     = 'Unknown';
$lang['ct_schools']     = 'Schools';
$lang['ct_all_clients'] = 'All Clients';

//Teacher
$lang['ct_become_master']   = 'Become master';
$lang['ct_become_teacher']  = 'Become teacher';

//Toolkit
$lang['ct_upload_files']    = 'Upload Files';

//Log
$lang['ct_logins']          = 'Logins';
$lang['ct_login_time']      = 'Login Time';
$lang['ct_login_remarks']   = 'Remarks';

$lang['ct_errors']      = 'Errors';
$lang['ct_error_no']    = 'Error No';
$lang['ct_error_message'] = 'Error Message';
$lang['ct_request_params']= 'Request Parameters';

$lang['ct_actions']      = 'Actions';
$lang['ct_action_detail']= 'Action Detail';

$lang['ct_buffers']      = 'Buffers';
$lang['ct_buffer_detail']= 'Buffer Detail';
$lang['ct_buffer_content']= 'Content';
$lang['ct_buffer_code']  = 'Code';

$lang['ct_amnesiacs']    = 'Amnesiacs';
