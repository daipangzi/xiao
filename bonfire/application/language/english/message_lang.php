<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//common
$lang['msg_select'] = 'Select';
$lang['msg_page_status'] = '&nbsp;(Showing:%1$s-%2$s of %3$s)';

//validation
$lang['msg_invalid_date_format'] = "Invalid date format. Please input like '2014-04-04'";
$lang['msg_invalid_id_card']     = "Invalid ID card number.";
$lang['msg_invalid_phone_number']= "Invalid phone number.";
$lang['msg_include_unacceptable_string'] = '%s includes unacceptable characters.';