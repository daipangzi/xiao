<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//common
$lang['msg_select'] = '选择';
$lang['msg_page_status'] = '&nbsp;(%3$s个中显示%1$s到%2$s)';

//validation
$lang['msg_invalid_date_format'] = "Invalid date format. Please input like '2014-04-04'";
$lang['msg_invalid_id_card']     = "Invalid ID card number.";
$lang['msg_invalid_phone_number']= "Invalid phone number.";
$lang['msg_include_unacceptable_string'] = '%s includes unacceptable characters.';