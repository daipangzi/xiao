<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//common
$lang['ct_english'] = 'English';
$lang['ct_korean']  = 'Korean';
$lang['ct_chinese'] = 'Chinese';

$lang['ct_ip_address']  = 'IP Address';
$lang['ct_device_info'] = '设备账号';
$lang['ct_device_type'] = '设备类型';
$lang['ct_android'] = '安桌系统';
$lang['ct_iphone']  = '苹果系统';
$lang['ct_pc']      = '计算机';
$lang['ct_man']     = '男';
$lang['ct_woman']   = '女';
$lang['ct_name']    = '姓名';
$lang['ct_address'] = '住址';
$lang['ct_phone']   = '电话号码';
$lang['ct_email']   = '电子邮箱';
$lang['ct_both']    = 'Both';
$lang['ct_type']    = 'Type';
$lang['ct_city']    = '都市';
$lang['ct_latitude'] = '纬度';
$lang['ct_longitude']= '经度';
$lang['ct_registered']      = '注册时间';
$lang['ct_registered_date'] = '注册日期';
$lang['ct_registered_time'] = '注册时间';

$lang['ct_general'] = '一般';
$lang['ct_new'] = 'New';
$lang['ct_loading'] = 'Loading';
$lang['ct_please_wait'] = 'Please wait...';

$lang['ct_application'] = '程序';
$lang['ct_applications']= '应用程序';
$lang['ct_version']     = '版本';
$lang['ct_release_date']= '更新日期';
$lang['ct_description'] = '说明';
$lang['ct_file']        = 'File';
$lang['ct_path']        = 'Path';
$lang['ct_file_name']   = 'File Name';

$lang['ct_user_type'] = '客户种类';
$lang['ct_master']  = '校长';
$lang['ct_teacher'] = '教师';
$lang['ct_parent']  = '家长';
$lang['ct_father']  = '父亲';
$lang['ct_mother']  = '母亲';
$lang['ct_children']= '学生';
$lang['ct_teachers']= '教师';
$lang['ct_parents'] = '家长';

$lang['ct_status']  = '状态';
$lang['ct_active']  = '激活';
$lang['ct_inactive']= '未激活';
$lang['ct_online']  = 'Login';
$lang['ct_logout']  = 'Logout';
$lang['ct_logout_force'] = 'Logout(Force)';

$lang['ct_user_id']     = '身份证号码';
$lang['ct_user_name']   = '姓名';
$lang['ct_user_sex']    = '性别';
$lang['ct_user_phone']  = '电话号码';
$lang['ct_user_email']  = '电子邮箱';
$lang['ct_user_photo']  = '相册';
$lang['ct_user_birthday'] = '出生年月日';
$lang['ct_user_address']  = '住址';
$lang['ct_user_password'] = '密码';
$lang['ct_user_password_confirm'] = '确认密码';
$lang['ct_user_recent']   = '最近访问的日期';
$lang['ct_user_expire_date'] = 'Expire Date';
$lang['ct_user_school_info'] = '学校信息';

$lang['ct_btn_reset']   = '重启';
$lang['ct_btn_search']  = '查询';
$lang['ct_btn_delete']  = '删除';
$lang['ct_btn_save']    = '保存';
$lang['ct_btn_change']  = '更改';
$lang['ct_btn_apply']   = '应用';
$lang['ct_btn_back']    = '后退';
$lang['ct_btn_view']    = '查询';
$lang['ct_btn_upload']  = '上载';
$lang['ct_btn_download']= '下载';
$lang['ct_btn_truncate']    = '全部删除';
$lang['ct_btn_activate']    = '激活';
$lang['ct_btn_deactivate']  = '未激活';
$lang['ct_btn_new_grade']   = 'Add new grade';
$lang['ct_btn_upgrade_grades'] = 'Upgrade Grades';

$lang['ct_school']      = '学校';
$lang['ct_school_name'] = '学校名';
$lang['ct_school_type'] = '种类';
$lang['ct_school_address'] = '地区';
$lang['ct_school_name_cr'] = '学校名';
$lang['ct_school_type_cr'] = '学校种类';
$lang['ct_school_address_cr']  = '学校地区';
$lang['ct_school_primary']  = '小学';
$lang['ct_school_junior']   = '初级';
$lang['ct_school_senior']   = '高级';
$lang['ct_subject'] = '科目';
$lang['ct_subjects']= '科目';
$lang['ct_class']   = '班级';
$lang['ct_classes'] = '班级';
$lang['ct_class_unit']  = '个';
$lang['ct_grade']       = '年级';
$lang['ct_grade_with']  = '%s年级';
$lang['ct_grade_class'] = '年级班级';
$lang['ct_grade_class_with']= '%s年级%s班';
$lang['ct_live_grades']     = 'Live grades';
$lang['ct_graduated_grades']= '毕业年级';
$lang['ct_entrance'] = '入学';
$lang['ct_graduate'] = 'Graduate';
$lang['ct_graduated']= 'Graduated';

$lang['ct_charge_class']  = '担任班级';
$lang['ct_subject_class'] = '科目担任班级';