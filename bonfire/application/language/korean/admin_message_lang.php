<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//School
$lang['msg_input_grade_class_numers'] = 'Please input number of classes per grade.';
$lang['msg_school_invalid_id']      = 'The school uid is invalid.';
$lang['msg_school_no_records']      = "There is not any registered school on system.";
$lang['msg_school_details']         = "School Information";
$lang['msg_school_create_fail']     = "We can's create school. Maybe there is validation or database error.";
$lang['msg_school_create_success']  = 'New school is registered successfully.';
$lang['msg_school_edit_success']    = "The information of '%s' is updated successfully.";
$lang['msg_school_edit_fail']       = "We can's update the information of '%s'. Maybe there is validation or database error.";
$lang['msg_school_delete_one_confirm']  = 'Do you want to to delete this school from system?';
$lang['msg_school_delete_list_confirm'] = 'Do you want to to delete these schools from system?';
$lang['msg_school_add_grade_success']   = "The new grade added to '%s' successfully.";
$lang['msg_school_add_grade_fail']      = "We can't add new grade to '%s'. Maybe there is a problem on system.";
$lang['msg_school_graduate_grade_success'] = "Grade changes into graduated status successfully.";
$lang['msg_school_graduate_grade_fail'] = "We can't change grade into grduate status. Maybe there is a problem on system.";
$lang['msg_school_activate_already']    = "'%s' is already ativated.";
$lang['msg_school_activate_success']    = "'%s' has been activated.";
$lang['msg_school_activate_fail']       = "There is a problem on activating '%s'.";
$lang['msg_school_activate_already']    = "'%s' is already ativated.";
$lang['msg_school_deactivate_success']  = "'%s' has been deactivated.";
$lang['msg_school_deactivate_fail']     = "There is a problem on deactivating '%s'.";
$lang['msg_school_grade_upgrade_success'] = "Grades are upgraged successfully.";
$lang['msg_school_grade_upgrade_fail']    = "Upgrading is failed. Maybe there is a problem on system.";
$lang['msg_school_no_new_grade']    = "We can't upgrade grades. Because there is no new grade in this school.";
$lang['msg_school_delete_success']  = "'%s' is removed successfully from our system.";
$lang['msg_school_delete_fail']     = "We can't remove '%s' from our system.";
$lang['msg_school_invalid_class_id'] = "Ther class uid is invalid.";

//Teachers
$lang['msg_teacher_delete_one_confirm'] = 'Do you want to to delete this teacher from system?';
$lang['msg_teacher_delete_list_confirm']= 'Do you want to to delete these teachers from system?';
$lang['msg_teacher_invalid_id']         = 'The teacher uid is invalid.';
$lang['msg_teacher_no_records']         = "There is registered teachers on system.";
$lang['msg_teacher_activate_already']   = "'%s' is already ativated.";
$lang['msg_teacher_activate_success']   = "'%s' has been activated.";
$lang['msg_teacher_activate_fail']      = "There is a problem on activating '%s'.";
$lang['msg_teacher_deactivate_already'] = "'%s' is already deativated.";
$lang['msg_teacher_deactivate_success'] = "'%s' has been deactivated.";
$lang['msg_teacher_deactivate_fail']    = "There is a problem on deactivating '%s'.";
$lang['msg_teacher_delete_success'] = "'%s' is removed successfully from our system.";
$lang['msg_teacher_delete_fail']    = "We can't remove '%s' from our system.";
$lang['msg_teacher_update_success']   = "The information of '%s' was updated successfully.";
$lang['msg_teacher_update_fail']      = "There is a problem on updating information of %s.";
$lang['msg_teacher_photo_update_success'] = "The photo of '%s' was updated successfully.";
$lang['msg_teacher_photo_update_fail']    = "There is a problem on updating photo of %s.";
$lang['msg_teacher_pass_update_success'] = "The password of '%s' was updated successfully.";
$lang['msg_teacher_pass_update_fail']    = "There is a problem on updating password of %s.";
$lang['msg_teacher_school_update_success'] = "The school information of '%s' was updated successfully.";
$lang['msg_teacher_school_update_fail']    = "There is a problem on updating school information of %s.";
$lang['msg_teacher_master_exist']       = "The master is already exist in '%s'.";
$lang['msg_teacher_to_master_success']  = "'%s' became master.";
$lang['msg_teacher_to_master_fail']     = "There is a problem on updating '%s' to master.";
$lang['msg_teacher_to_teacher_success'] = "'%s' became teacher.";
$lang['msg_teacher_to_teacher_fail']    = "There is a problem on updating '%s' to teacher.";
$lang['msg_teacher_want_change_school_info'] = "I want to change the relation with school.";

//Parent
$lang['msg_parent_delete_one_confirm'] = 'Do you want to to delete this parent from system?';
$lang['msg_parent_delete_list_confirm']= 'Do you want to to delete these parents from system?';
$lang['msg_parent_invalid_id']         = 'The parent uid is invalid.';
$lang['msg_parent_no_records']         = "There is not any registered teachers on system.";
$lang['msg_parent_activate_already']   = "'%s' is already ativated.";
$lang['msg_parent_activate_success']   = "'%s' has been activated.";
$lang['msg_parent_activate_fail']      = "There is a problem on activating '%s'.";
$lang['msg_parent_deactivate_already'] = "'%s' is already deativated.";
$lang['msg_parent_deactivate_success'] = "'%s' has been deactivated.";
$lang['msg_parent_deactivate_fail']    = "There is a problem on deactivating '%s'.";
$lang['msg_parent_delete_success'] = "'%s' is removed successfully from our system.";
$lang['msg_parent_delete_fail']    = "We can't remove '%s' from our system.";
$lang['msg_parent_update_success']   = "The information of '%s' was updated successfully.";
$lang['msg_parent_update_fail']      = "There is a problem on updating information of %s.";
$lang['msg_parent_photo_update_success'] = "The photo of '%s' was updated successfully.";
$lang['msg_parent_photo_update_fail']    = "There is a problem on updating photo of %s.";
$lang['msg_parent_pass_update_success'] = "The password of '%s' was updated successfully.";
$lang['msg_parent_pass_update_fail']    = "There is a problem on updating password of %s.";

//Children
$lang['msg_children_delete_one_confirm'] = 'Do you want to to delete this children from system?';
$lang['msg_children_delete_list_confirm']= 'Do you want to to delete these childrens from system?';
$lang['msg_children_invalid_id']         = 'The children uid is invalid.';
$lang['msg_children_no_records']         = "There is not any registered children on system.";
$lang['msg_children_activate_already']   = "'%s' is already ativated.";
$lang['msg_children_activate_success']   = "'%s' has been activated.";
$lang['msg_children_activate_fail']      = "There is a problem on activating '%s'.";
$lang['msg_children_deactivate_already'] = "'%s' is already deativated.";
$lang['msg_children_deactivate_success'] = "'%s' has been deactivated.";
$lang['msg_children_deactivate_fail']    = "There is a problem on deactivating '%s'.";
$lang['msg_children_delete_success'] = "'%s' is removed successfully from our system.";
$lang['msg_children_delete_fail']    = "We can't remove '%s' from our system.";
$lang['msg_children_update_success']   = "The information of '%s' was updated successfully.";
$lang['msg_children_update_fail']      = "There is a problem on updating information of %s.";
$lang['msg_children_photo_update_success'] = "The photo of '%s' was updated successfully.";
$lang['msg_children_photo_update_fail']    = "There is a problem on updating photo of %s.";
$lang['msg_children_pass_update_success'] = "The password of '%s' was updated successfully.";
$lang['msg_children_pass_update_fail']    = "There is a problem on updating password of %s.";
$lang['msg_children_want_change_school_info'] = "I want to change the relation with school.";
$lang['msg_children_school_update_success'] = "The school information of '%s' was updated successfully.";
$lang['msg_children_school_update_fail']    = "There is a problem on updating school information of %s.";
$lang['msg_children_school_no_changes']     = "There is not changes for %s.";

//Client
$lang['msg_client_no_records'] = "There is not any registered clients on system.";

//Logins 
$lang['msg_login_no_records']       = 'There is not any online users on system.';
$lang['msg_login_truncate_confirm'] = 'Do you want to delete all records?.';
$lang['msg_login_truncate_success'] = 'All records are deleted successfully.';
$lang['msg_login_truncate_fail']    = 'There is a problem on truncating records.';
$lang['msg_login_logout_success']   = '%s users were logged out.';
$lang['msg_login_delete_list_confirm'] = 'Do you want to delete these records?';
$lang['msg_login_delete_success']   = '%s users were deleted successfully.';
$lang['msg_duplicate_login']        = 'Duplicate login attempts.';
$lang['msg_school_updated']         = 'User\'s school or class is updated.';
$lang['msg_parent_children_updated']= 'One of his children is updated.';
$lang['msg_by_admin']               = 'Logged out by administrator.';

//Errors
$lang['msg_error_invalid_id']       = 'Invalid error id.';
$lang['msg_error_no_records']       = 'There is not any errors yet.';
$lang['msg_error_truncate_confirm'] = 'Do you want to delete all records?';
$lang['msg_error_truncate_success'] = 'All records are deleted successfully.';
$lang['msg_error_truncate_fail']    = 'There is a problem on truncating records.';
$lang['msg_error_delete_list_confirm'] = 'Do you want to delete these records?';
$lang['msg_error_delete_success']   = '%s errors were deleted successfully.';

//Actions
$lang['msg_action_invalid_id']      = 'Invalid action id.';
$lang['msg_action_no_records']      = 'There is not any actions yet.';
$lang['msg_action_truncate_confirm']= 'Do you want to delete all records?';
$lang['msg_action_truncate_success']= 'All records are deleted successfully.';
$lang['msg_action_truncate_fail']   = 'There is a problem on truncating records.';
$lang['msg_action_delete_list_confirm']= 'Do you want to delete these records?';
$lang['msg_action_delete_success']  = '%s actions were deleted successfully.';

//Buffers
$lang['msg_buffer_invalid_id']      = 'Invalid buffer id.';
$lang['msg_buffer_no_records']      = 'There is not any buffer.';
$lang['msg_buffer_truncate_confirm']= 'Do you want to delete all records?';
$lang['msg_buffer_truncate_success']= 'All records are deleted successfully.';
$lang['msg_buffer_truncate_fail']   = 'There is a problem on truncating records.';
$lang['msg_buffer_delete_list_confirm']= 'Do you want to delete these records?';
$lang['msg_buffer_delete_success']  = '%s buffer were deleted successfully.';

//Forgotten
$lang['msg_amnesiac_invalid_id']      = 'Invalid id.';
$lang['msg_amnesiac_no_records']      = 'There is not any record.';
$lang['msg_amnesiac_truncate_confirm']= 'Do you want to delete all records?';
$lang['msg_amnesiac_truncate_success']= 'All records are deleted successfully.';
$lang['msg_amnesiac_truncate_fail']   = 'There is a problem on truncating records.';
$lang['msg_amnesiac_delete_list_confirm']= 'Do you want to delete these records?';
$lang['msg_amnesiac_delete_success']  = '%s record were deleted successfully.';

//Toolkit
$lang['msg_app_invalid_id']   = 'The application uid is invalid.';
$lang['msg_app_edit_success'] = 'The information of application is updated successfully.';
$lang['msg_app_edit_fail']    = 'There is a problem on updating information of application.';
$lang['msg_app_no_records']   = 'There is not any applications on system.';
$lang['msg_toolkit_uploader'] = 'Uploader';
$lang['msg_toolkit_invalid_path']   = 'Target path is invalid.';
$lang['msg_toolkit_upload_success'] = 'File is uploaded successfully.';
$lang['msg_toolkit_upload_fail']    = 'There is a problem on uploading file.';
$lang['msg_toolkit_tester']         = 'Tester';
$lang['msg_toolkit_push_success']   = 'Notification was sent successfully.';
$lang['msg_toolkit_push_fail']      = 'Notification was fail.';
$lang['msg_toolkit_sms_success']    = 'SMS was sent successfully.';
$lang['msg_toolkit_sms_fail']       = 'SMS was fail.';