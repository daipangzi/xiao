<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['msg_new_message_arrived'] = 'New message from %s.';
$lang['msg_sms_verify_code']     = 'Verify Code: %s';
$lang['msg_send_security']       = 'Your Password: %s.';
$lang['msg_send_security_title'] = 'Your security info';