<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//common
$lang['ct_english'] = 'English';
$lang['ct_korean']  = 'Korean';
$lang['ct_chinese'] = 'Chinese';

$lang['ct_ip_address']  = 'IP Address';
$lang['ct_device_info'] = 'Device UID';
$lang['ct_device_type'] = 'Device Type';
$lang['ct_android'] = 'Android';
$lang['ct_iphone']  = 'iPhone';
$lang['ct_pc']      = 'PC';
$lang['ct_man']     = 'Man';
$lang['ct_woman']   = 'Woman';
$lang['ct_name']    = 'Name';
$lang['ct_address'] = 'Address';
$lang['ct_phone']   = 'Phone';
$lang['ct_email']   = 'Email';
$lang['ct_both']    = 'Both';
$lang['ct_type']    = 'Type';
$lang['ct_city']    = 'City';
$lang['ct_latitude'] = 'Latitude';
$lang['ct_longitude']= 'Longitude';
$lang['ct_registered']      = 'Registered';
$lang['ct_registered_date'] = 'Registered';
$lang['ct_registered_time'] = 'Registered';

$lang['ct_general'] = 'General';
$lang['ct_new'] = 'New';
$lang['ct_loading'] = 'Loading';
$lang['ct_please_wait'] = 'Please wait...';

$lang['ct_application'] = 'Application';
$lang['ct_applications']= 'Applications';
$lang['ct_version']     = 'Version';
$lang['ct_release_date']= 'Release Date';
$lang['ct_description'] = 'Description';
$lang['ct_file']        = 'File';
$lang['ct_path']        = 'Path';
$lang['ct_file_name']   = 'File Name';

$lang['ct_user_type'] = 'User Type';
$lang['ct_master']  = 'Master';
$lang['ct_teacher'] = 'Teacher';
$lang['ct_parent']  = 'Parent';
$lang['ct_father']  = 'Father';
$lang['ct_mother']  = 'Mother';
$lang['ct_children']= 'Children';
$lang['ct_teachers']= 'Teachers';
$lang['ct_parents'] = 'Parents';

$lang['ct_status']  = 'Status';
$lang['ct_active']  = 'Active';
$lang['ct_inactive']= 'Inactive';
$lang['ct_online']  = 'Online';
$lang['ct_logout']  = 'Logout';
$lang['ct_logout_force'] = 'Logout(Force)';

$lang['ct_user_id']     = 'User ID';
$lang['ct_user_name']   = 'Name';
$lang['ct_user_sex']    = 'Gender';
$lang['ct_user_phone']  = 'Phone';
$lang['ct_user_email']  = 'Email';
$lang['ct_user_photo']  = 'Photo';
$lang['ct_user_birthday'] = 'Birthday';
$lang['ct_user_address']  = 'Address';
$lang['ct_user_password'] = 'Password';
$lang['ct_user_password_confirm'] = 'Password Confirm';
$lang['ct_user_recent']   = 'Recent Visit Date';
$lang['ct_user_expire_date'] = 'Expire Date';
$lang['ct_user_school_info'] = 'School Info';

$lang['ct_btn_reset']   = 'Reset';
$lang['ct_btn_search']  = 'Search';
$lang['ct_btn_delete']  = 'Delete';
$lang['ct_btn_save']    = 'Save';
$lang['ct_btn_change']  = 'Change';
$lang['ct_btn_apply']   = 'Apply';
$lang['ct_btn_back']    = 'Back';
$lang['ct_btn_view']    = 'View';
$lang['ct_btn_upload']  = 'Upload';
$lang['ct_btn_download']= 'Download';
$lang['ct_btn_truncate']    = 'Truncate';
$lang['ct_btn_activate']    = 'Activate';
$lang['ct_btn_deactivate']  = 'Deactivate';
$lang['ct_btn_new_grade']   = 'Add new grade';
$lang['ct_btn_upgrade_grades'] = 'Upgrade Grades';

$lang['ct_school']      = 'School';
$lang['ct_school_name'] = 'Name';
$lang['ct_school_type'] = 'Type';
$lang['ct_school_address'] = 'Address';
$lang['ct_school_name_cr'] = 'School Name';
$lang['ct_school_type_cr'] = 'School Type';
$lang['ct_school_address_cr']  = 'School Address';
$lang['ct_school_primary']  = 'Primary';
$lang['ct_school_junior']   = 'Junior';
$lang['ct_school_senior']   = 'Senior';
$lang['ct_subject'] = 'Subject';
$lang['ct_subjects']= 'Subjects';
$lang['ct_class']   = 'Class';
$lang['ct_classes'] = 'Classes';
$lang['ct_class_unit']  = 'Classes';
$lang['ct_grade']       = 'Grade';
$lang['ct_grade_with']  = 'Grade %s';
$lang['ct_grade_class'] = 'Grade Class';
$lang['ct_grade_class_with']= '%s-%s';
$lang['ct_live_grades']     = 'Live grades';
$lang['ct_graduated_grades']= 'Graduated grades';
$lang['ct_entrance'] = 'Entrance';
$lang['ct_graduate'] = 'Graduate';
$lang['ct_graduated']= 'Graduated';

$lang['ct_charge_class']  = 'Charge Classes';
$lang['ct_subject_class'] = 'Subject Classes';