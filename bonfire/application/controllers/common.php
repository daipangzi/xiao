<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

class Common extends Front_Controller
{
    public function __construct()
    {
        parent::__construct();  
        
        $this->load->model('region_model');      
        $this->load->model('school_model');
    }
    //--------------------------------------------------------------------
    
    ///////////////////////////////////////////////////////////////////////
    //!AJAX METHODS
    ///////////////////////////////////////////////////////////////////////
    public function get_city_list() {
        $province = $this->input->post('province') ? $this->input->post('province') : '00';        
        $cities = $this->region_model->city_dropdown_list($province, TRUE, lang('msg_select'));
        echo form_dropdown2('city', $cities, set_value('city', ''));
        exit;
    }//end get_city_list()
    
    public function get_subject_list() {
        $school_uid = $this->input->post('school_uid');        
        $subjects = $this->school_model
            ->select('uid, subject_name name')
            ->get_subjects($sch_id);
        $subjects = format_dropdown($subjects, TRUE, lang('msg_select'));
        echo form_dropdown2('subject', $subjects, set_value('subject', ''));
        exit;
    }//end get_subject_list()
}//end class