<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

class simulator extends Web_base_Controller
{
    public function __construct()
    {
        $this->controller = 'simulator';
        
        parent::__construct();  
        
        Template::set_theme('simulator/', 'junk');
    }
    //--------------------------------------------------------------------
    public function index()
    {
        $this->render_page('index', 'index2');    
    }
    
    public function login()
    {
        $this->render_page('login');    
    }
    
    public function logout()
    {
        $this->render_page('logout');    
    }
    
    public function forgot()
    {
        $this->render_page('forgot');    
    }
    
    public function register_teacher()
    {
        $this->render_page('register/register_teacher');    
    }
    
    public function register_parents()
    {
        $this->render_page('register/register_parents');    
    }
    
    public function register_complete()
    {
        $this->render_page('register/register_complete');    
    }
    
    public function regenerate_sms()
    {
        $this->render_page('register/regenerate_sms');    
    }
    
    //////////////////////////////////////
    //LOGGED IN
    //////////////////////////////////////
    
    //profile part
    public function profile()
    {
        $this->render_page('profile/profile');    
    }
    
    public function update_profile_general()
    {
        $this->render_page('profile/update_profile_general');    
    }
    
    public function update_profile_photo()
    {
        $this->render_page('profile/update_profile_photo');    
    }
    
    public function update_profile_password()
    {
        $this->render_page('profile/update_profile_password');    
    }
    
    public function teacher_update_subject()
    {
        $this->render_page('profile/teacher_update_subject');    
    }
    
    public function teacher_update_class()
    {
        $this->render_page('profile/teacher_update_class');    
    }
    
    public function teacher_update_school()
    {
        $this->render_page('profile/teacher_update_school');    
    }
    
    public function parent_children_update_school()
    {
        $this->render_page('profile/parent_children_update_school');    
    }    
    
    public function parent_add_children()
    {
        $this->render_page('profile/parent_add_children');    
    }    
    
    public function parent_remove_children()
    {
        $this->render_page('profile/parent_remove_children');    
    }    
    
    //message part
    public function receiver_lists()
    {
        $this->render_page('message/receiver_lists');    
    }

    public function new_message()
    {
        $this->render_page('message/new_message');    
    }
    
    public function new_message2()
    {
        $this->render_page('message/new_message2');    
    }
    
    public function chat_sessions()
    {
        $this->render_page('message/chat_sessions');    
    }
    
    public function chat_conversations()
    {
        $this->render_page('message/chat_conversations');    
    }
    
    public function new_chat_conversations()
    {
        $this->render_page('message/new_chat_conversations');    
    }
    
    public function count_new_message_nums()
    {
        $this->render_page('message/count_new_message_nums');    
    }
    
    public function remove_chat_session()
    {
        $this->render_page('message/remove_chat_session');    
    }
    
    public function remove_message()
    {
        $this->render_page('message/remove_message');    
    }
    
    public function search_by_date_type()
    {
        $this->render_page('message/search_by_date_type');    
    }
    //////////////////////////////////////
    //COMMON
    //////////////////////////////////////
    public function schools()
    {
        $this->render_page('common/schools');    
    }
    
    public function class_subject_lists()
    {
        $this->render_page('common/class_subject_lists');    
    }
    
    public function class_lists()
    {
        $this->render_page('common/class_lists');    
    }
    
    public function update_check()
    {
        $this->render_page('common/update_check');    
    }
    
    private function render_page($view='index', $template='index')
    {
        Template::set_view("{$this->controller}/{$view}");
        Template::render($template);
    }
}//end class