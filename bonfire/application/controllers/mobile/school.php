<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class School extends Mobile_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
    * get and return all actived school lists
    * 
    */
    public function lists()
    {
        $result = $this->school_model
            ->select("school_uuid school_uid, school_name_cn, school_name_ko, school_name_en, school_address, school_type")
            ->filter_lives()
            ->order_by('school_name_'.$this->short_language)
            ->find_all();
            
        if(empty($result))
        {
            $result = array();
        }
        
        $this->return_result($result);
    }//end index()      
    //--------------------------------------------------------------------
    
    /**
    * get and return grade-class lists and subject lists of school
    * 
    */
    public function class_subject_lists()
    {
        $result = array();
        $params = $this->params;
        $school_uid = $params['data']['school_uid'];

        //check the school is exist
        if(!$this->school_model->valid_school($school_uid))
        {       
            $this->return_error(INVALID_SCHOOL);
        }
        
        //get grade-classes
        $grade_classes = $this->school_model
            ->select('class_uuid class_uid, grade_num, class_num, year_level')
            ->filter_live_grades()
            ->filter_live_classes()
            ->get_classes($school_uid);
        
        if(empty($grade_classes))
        {
            $this->return_error(ERR_NO_GRADE_CLASS);
        }
        
        //get subjects
        $subjects = $this->school_model
            ->select('subject_uuid subject_uid, subject_name')
            ->get_subjects($school_uid);
        
        if(empty($subjects))
        {
            $this->return_error(ERR_NO_SUBJECTS);
        }
        
        $result['classes'] = $grade_classes;  
        $result['subject'] = $subjects;      
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * get and return grade-class lists and subject lists of school
    * 
    */
    public function class_lists()
    {
        $result = array();
        $params = $this->params;
        $school_uid = $params['data']['school_uid'];

        //check the school is exist
        if(!$this->school_model->valid_school($school_uid))
        {       
            $this->return_error(INVALID_SCHOOL);
        }
        
        //get grade-classes
        $grade_classes = $this->school_model
            ->select('class_uuid class_uid, grade_num, class_num, year_level')
            ->filter_live_grades()
            ->filter_live_classes()
            ->get_classes($school_uid);
        
        if(empty($grade_classes))
        {
            $this->return_error(ERR_NO_GRADE_CLASS);
        }
        
        $result['classes'] = $grade_classes;  
        $this->return_result($result);
    }
    //--------------------------------------------------------------------

}//end class