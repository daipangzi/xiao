<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Update extends Mobile_Controller
{
    public function __construct()
    {
        parent::__construct(false);
        
        $this->load->model('apps_model', null, true);
    }
    
    public function check()
    {
        $client = $this->params['client'];
        $user_type = $client['user_type'];
        
        if($user_type == USER_TYPE_MASTER)
        {
            $user_type = USER_TYPE_TEACHER;
        }   
        
        $apk = $this->apps_model->find_by('app_type', $user_type);
        if(empty($apk))
        {
            $this->return_error(APK_NO_FILE);
        }
        
        //get download link
        $link = MEDIA_PATH . 'download/' . $apk->file_name;
        if(file_exists($link))
        {
            $link = site_url($link);
        }
        else
        {
            $link = '';
        }
        
        $result = array();
        $result['version']      = $apk->version;
        $result['release_date'] = $apk->release_date;
        $result['download_path']= $link;
        
        $this->return_result($result);
    }//end index()      
    //--------------------------------------------------------------------

}//end class