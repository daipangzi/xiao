<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

class Children extends Mobile_Controller
{
    public function __construct()
    {
        $this->controller = 'children';
        
        parent::__construct();
        
        if($this->params['client']['user_type'] != USER_TYPE_CHILDREN)
        {
            $this->return_error(ERR_USER_TYPE_NOT_MATCH);        
        }
    }
    //--------------------------------------------------------------------
    
    /**
    * retrieve security password if you forgot it
    *     
    */
    public function forgot()
    {   
        //check if all fields are exist
        $data = $this->params['data'];     
        $fields = array('user_id', 'via_email', 'via_phone');     
        if(!array_keys_exists($fields, $data))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        $result    = array();
        $user_id   = $data['user_id'];
        $via_phone = $data['via_phone'];
        $via_email = $data['via_email'];
        if($via_email == 0 && $via_phone == 0)
        {
            return $this->return_error(FORGOT_NO_CONTACT_PATH); //no retrieve path
        }  
        
        //get user row
        $user = $this->children_model->find_by_userid($user_id);
        if(empty($user))
        {
            $result['user_exist'] = NO_REGISTERED;
            $this->return_result($result);        
        }
        
        //check if this user has already requested
        $path = SECURITY_PATH_BOTH;
        if($via_phone == SELECTED && $via_email == NONE)
        {
            $path = SECURITY_PATH_PHONE;
        }
        else if($via_email == SELECTED && $via_phone == NONE)
        {
            $path = SECURITY_PATH_EMAIL;
        }
        
        //load model
        $this->load->model('amnesiac_model');
        $last_request = $this->amnesiac_model->find_row($user_id, USER_TYPE_CHILDREN, $path);  
        if(!empty($last_request) && isset($last_request->reg_date))
        {           
            $result['has_requested']= STATUS_YES;
            $result['expire_time']  = date(FORMAT_DATETIME, strtotime($last_request->reg_date) + 24 * 60 * 60);
            $this->return_result($result);
        }
        
        //check empty string for phone and email
        if(trim($user->user_email) == '' && trim($user->user_phone) == '')
        {
            $result['phone_email_exist']= NONE;
            $this->return_result($result);
        }
        if($via_phone == SELECTED && $via_email == NONE && trim($user->user_phone) == '')
        {
            $result['phone_exist']= NONE;
            $this->return_result($result);
        }
        if($via_email == SELECTED && $via_phone == NONE && trim($user->user_email) == '')
        {
            $result['email_exist']= NONE;
            $this->return_result($result);
        }
        
        //register this to fogotten_model
        $fdata = array();
        $fdata['user_id']   = $user_id;
        $fdata['user_name'] = $user->user_name;
        $fdata['user_type'] = USER_TYPE_CHILDREN;
        $fdata['path']      = $path;
        $fdata['device_type'] = $this->params['client']['device_type'];
        $fdata['device_info'] = $this->params['client']['device_info'];
        $this->amnesiac_model->insert($fdata);
        if($status === FALSE)
        {
            $this->return_error(ERR_INSERT_TO_DATABASE);
        }
        
        //send info, put process in background
        if(SEND_SMS === TRUE)
        {
            $language_name = get_long_language_name($this->short_language);
            $command = sprintf(COMMAND_SEND_SECURITY, $language_name, USER_TYPE_CHILDREN, $user_id, $via_phone, $via_email);
            $process = new Process;
            $pcs_id  = $process->runInBackground($command);
        }
        
        //return result
        $result['finding_status'] = STATUS_SUCCESS;
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * children login
    * 
    */
    public function login()
    {
        $result = array();
        $client = $this->params['client'];
        $data   = $this->params['data'];
        
        //check valid login data
        $fields = array('user_id', 'user_passwd');
        if(!array_keys_exists($fields, $data)) 
        {
            $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        if(!valid_id_number($data['user_id']) && !valid_passport_number($data['user_id']))
        {
            $this->return_error(INVALID_USER_ID);
        }
        
        if(!valid_password($data['user_passwd']))
        {      
            $this->return_error(INVALID_USER_PASSWORD);
        }

        //check if user is registered on our system
        //if registered get school ids with user_id and user_type
        $user_id     = $data['user_id'];
        $user_passwd = $data['user_passwd'];
        $ip_address  = $this->input->ip_address();
        $children    = $this->children_model->with_class()->find_by_userid($user_id); 
        
        if(empty($children))
        {
            $result['user_exist'] = NO_REGISTERED;
            $this->return_result($result);        
        }
        
        //check login attempt, if attempt is greater than specified count, 
        //return fail
        $attempts = $this->log_login_model->count_login_attempts($user_id, USER_TYPE_CHILDREN, $ip_address, LOGIN_ATTEMPT_CYCLE); 
        if($attempts >= LOGIN_ATTEMPT_LIMIT_NUMS)
        {   
            $result['limit_attempt'] = STATUS_YES;
            $result['refresh_cycle'] = LOGIN_ATTEMPT_CYCLE; //mins
            $this->return_result($result);        
        }
        
        //if status is active, then check password
        if(do_hash($children->salt . $user_passwd) !== $children->password_hash)
        {
            $this->log_login_model->increase_login_attempts($user_id, USER_TYPE_CHILDREN, $ip_address);
        
            $result['verify_passwd'] = VERIFY_FAIL;
            $this->return_result($result);    
        }
        
        //check the school is active status at first
        $school = $this->school_model->find($children->school_uid); 
        if(empty($school)) $this->return_error(LOGIN_INVALID_SCHOOL);
        
        if((int)$school->active_status === STATUS_INACTIVE)
        {
            $this->log_login_model->increase_login_attempts($user_id, USER_TYPE_CHILDREN, $ip_address);
        
            $school_name_field = 'school_name_' . $this->short_language;
            $result['school_uid']    = $school->school_uuid;
            $result['school_name']   = $school->$school_name_field;
            $result['school_status'] = STATUS_INACTIVE;
            $this->return_result($result);
        } 
        
        //check if the class is active
        if((int)$children->active_status === STATUS_INACTIVE)
        {
            $result['class_uid'] = $children->class_uid;
            $result['grade_num'] = $children->grade_num;
            $result['class_num'] = $children->class_num;
            $result['class_status'] = STATUS_INACTIVE;
            $this->return_result($result);
        }
        
        //if school is active, then go to children table of that school
        //check user_status is active
        if((int)$children->user_status === STATUS_INACTIVE)
        {
            $this->log_login_model->increase_login_attempts($user_id, USER_TYPE_CHILDREN, $ip_address);
        
            $result['user_status'] = STATUS_INACTIVE;
            $this->return_result($result);
        }
        
        //set expire status if expired
        $expire_time = strtotime($children->expire_date);
        if($expire_time < time())
        {               
            $result['expire_status']   = CHILDREN_SERVICE_EXPIRED;
            $result['payment_details'] = $this->last_payment_of_children($children->user_id);
            $this->return_result($result);
        }
        
        //check user is already logged in on another device
        //then register new login info and set it as a inactive status and send request a fav song
        //to check right person
        $login_uid = FALSE;
        $device_info = $client['device_info'];
        $device_type = $client['device_type'];
        
        $login_info = array();
        $login_info['user_id']   = $user_id;
        $login_info['user_name'] = $children->user_name;
        $login_info['user_type'] = USER_TYPE_CHILDREN;
        $login_info['device_type'] = $device_type;
        $login_info['device_info'] = $device_info;
        
        $exist_login = $this->log_login_model
            ->filter_userid($user_id)
            ->filter_user_type(USER_TYPE_CHILDREN)
            ->filter_device_type($device_type)
            ->filter_device_info($device_info)
            ->filter_status(STATUS_ACTIVE)
            ->find_row();
        if(!empty($exist_login))
        {                                                          
            //log out other connections                
            $this->log_login_model->log_out_by_userid($user_id, USER_TYPE_CHILDREN, $exist_login->uid);
                               
            $login_uid = $exist_login->uid;
        }
        else
        {
            //log out other connections                
            $this->log_login_model->log_out_by_userid($user_id, USER_TYPE_CHILDREN);
            
            $login_uid = $this->log_login_model->insert($login_info);
        }
        
        if($login_uid === FALSE)
        {
            $this->return_error(LOGIN_NO_LOGIN_UID);
        }
        
        //clear login attempts history
        $this->log_login_model->clear_login_attempts($user_id, USER_TYPE_CHILDREN, $ip_address);
        
        //if everything is ok, then login client to our system
        //make return result
        $result = $this->_connection_data($login_uid, $school, $client['device_type'], $children);
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * user logout
    * 
    */
    public function logout()
    {
        $this->restrict(FALSE);
        
        $client = $this->params['client'];
        $user_id    = $client['user_id'];   
        $connect_uid= $client['connection_uid'];
        
        //set recent_visite_date by current time
        $this->children_model->update_recent_date($this->user->children_uuid);       
                                                                                                  
        //delete from login_monitor
        $this->log_login_model->log_out($connect_uid);
        
        //return result
        $result = array();
        $result['logout_status'] = STATUS_SUCCESS;
        
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
     
    /**
    * retrieve all available receiver lists
    * 
    */
    public function receiver_lists()
    {
        //insert receivers to table
        $this->restrict();
        
        $children = $this->user;
        $school_uid  = $children->school_uid;
        $photo_field1 = $this->user_photo_field(USER_TYPE_TEACHER, 'user_photo');
        $photo_field2 = $this->user_photo_field(USER_TYPE_CHILDREN, 'user_photo');
        $photo_field3 = $this->user_photo_field(USER_TYPE_PARENT, 'user_photo');
            
        //get master row
        $master = $this->teacher_model
            ->select("teacher_uuid, user_id, user_name, user_sex, {$photo_field1}, user_email, user_phone, ". USER_TYPE_MASTER . ' user_type', FALSE)
            ->find_master($school_uid);
            
        //get teachers
        $exclude_teacher_uids = array();
        if(isset($master->teacher_uuid))
        {
            $exclude_teacher_uids[] = $master->teacher_uuid;
            unset($master->teacher_uuid);
        }
        
        $teachers = $this->teacher_model
            ->filter_school($school_uid)
            ->select("teacher_uuid, user_id, user_name, user_sex, {$photo_field1}, user_email, user_phone, ". USER_TYPE_TEACHER . ' user_type, GROUP_CONCAT(teacher_role) roles', FALSE)
            ->filter_out_user_uids($exclude_teacher_uids)
            ->find_all_by_class($children->class_uid); 
            
        if(!empty($teachers))
        {
            foreach($teachers as $teacher)
            {
                //set charge or subject flag
                $roles = explode(',', $teacher->roles);
                if(in_array(ROLE_CHARGE, $roles))
                {
                    $teacher->is_charge = STATUS_YES;
                }
                else
                {
                    $teacher->is_charge = STATUS_NO;
                }
                
                if(in_array(ROLE_SUBJECT, $roles))
                {
                    $teacher->is_subject = STATUS_YES;
                    $teacher->subject_name = $this->teacher_model->get_charge_subject_name($teacher->teacher_uuid);
                }
                else
                {
                    $teacher->is_subject = STATUS_NO;
                }
                
                unset($teacher->teacher_uuid);
                unset($teacher->roles);
            }
        }
        
        //get my parents
        $my_parents_uids = $this->children_model->get_children_parent_uids($children->children_uuid);
        $my_parents = $this->parent_model
            ->select("user_id, user_name, user_sex, {$photo_field3}, user_email, user_phone, " . USER_TYPE_PARENT . ' user_type, parent_type', FALSE)
            ->filter_user_uids($my_parents_uids)
            ->find_all();
        
        //get my class and classmates
        $time       = date(FORMAT_DATE); 
        $s_active   = CHILDREN_SERVICE_AVAILABLE;
        $s_inactive = CHILDREN_SERVICE_EXPIRED;
        $extra      = "IF(expire_date<='{$time}', {$s_inactive}, {$s_active}) expire_status, ";
        
        $my_classmates = $this->children_model
            ->select("user_id, user_name, user_sex, {$photo_field2}, user_email, user_phone, {$extra}" . USER_TYPE_CHILDREN . ' user_type', FALSE)
            ->filter_out_user_uids($children->children_uuid)
            ->filter_class($children->class_uid)
            ->order_by('user_name')
            ->find_all();
          
        $my_class = array();
        $my_class['class_uid'] = $children->class_uid;
        $my_class['grade_num'] = $children->grade_num;
        $my_class['class_num'] = $children->class_num;
        $my_class['year_level']= $children->year_level;
        $my_class['children']  = empty($my_classmates) ? array() : $my_classmates;
        
        $result = array();
        $result['master']     = $master;
        $result['teachers']   = empty($teachers)?array():$teachers;
        $result['my_parents'] = empty($my_parents)?array():$my_parents;
        $result['my_class']   = $my_class;
        
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
   
   /**
   * children's profile
   *  
   */
    public function profile()
    {
        $this->restrict();
        
        $user = $this->user;
        $school_uid = $user->school_uid;
                     
        //get school info
        $school = $this->school_model->select_name($this->short_language)->find($school_uid);
        
        //personal
        $personal = array();
        $personal['user_name']  = $user->user_name;
        $personal['user_sex']   = $user->user_sex;
        $personal['user_email'] = $user->user_email;
        $personal['user_phone'] = $user->user_phone;
        $personal['user_photo'] = $user->user_photo_url;
        $personal['user_birthday'] = $user->user_birthday;
        $personal['user_address']  = $user->user_address;
        $personal['recent_visit_date'] = $user->recent_visit_date;
        $personal['expire_date']= $user->expire_date;
        
        $personal['school_uid']  = $school_uid;
        $personal['school_name'] = $school->school_name;
        
        $personal['class_uid']  = $user->class_uid;
        $personal['grade_num']  = $user->grade_num;
        $personal['class_num']  = $user->class_num;
        $personal['year_level'] = $user->year_level;
        
        //return result
        $result = array();
        $result['personal'] = $personal;
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * update general info of profile
    * 
    */
    public function update_profile_general()
    {
        $this->restrict();
        
        //check if all fields are exist
        $data = get_clean_fields_in($this->params['data']);
        $fields = array('user_name', 'user_sex', 'user_birthday', 'user_address', 'user_email', 'user_phone');     
        if(!array_keys_exists($fields, $data))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        //validate fields
        if(($return = $this->validate_profile_general($data)) !== NO_ERROR)
        {
            $result = array();
            $result['validation_fail'] = STATUS_YES;
            $result['validation_reason']= $return;
            $this->return_result($result);
        }
        
        //update row
        $update = array();
        $update['user_name']    = $data['user_name'];
        $update['user_sex']     = $data['user_sex']==MAN?MAN:WOMAN;
        $update['user_address'] = $data['user_address'];
        $update['user_email']   = $data['user_email'];
        $update['user_phone']   = $data['user_phone'];
        $update['user_birthday']= $data['user_birthday'];
        $status = $this->children_model->update($this->user->children_uuid, $update);
        
        //return result
        $result = array();
        $result['update_status'] = $status !== TRUE ? STATUS_FAIL : STATUS_SUCCESS;
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * update general info of profile
    * 
    */
    public function update_profile_photo()
    {
        $this->restrict();
        
        //check if all fields are exist
        $data = $this->params['data'];
        if(!isset($data['user_photo']))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        //validate and upload photo
        $user_photo = '';
        if($data['user_photo'] != '')
        {
            //validate fields
            $file = $data['user_photo']; 
            if(($return = $this->validate_profile_photo($file)) !== NO_ERROR)
            {
                $result = array();
                $result['validation_fail']  = STATUS_YES;
                $result['validation_reason']= $return;
                $this->return_result($result);
            }
            
            //upload photo
            if(!$this->upload_photo($file, USER_TYPE_CHILDREN))
            {
                $this->return_error(ERR_FILE_UPLOAD_FAIL);
            }
            
            //move photo
            if(!$this->move_photo($file, USER_TYPE_CHILDREN))
            {
                $this->return_error(ERR_FILE_UPLOAD_FAIL);
            }
            
            //delete old photo before new
            delete_user_photo($this->user->user_photo, USER_TYPE_CHILDREN);
            
            $user_photo = $file;
        }
        
        //update row
        $update = array();
        $update['user_photo'] = $user_photo;
        $status = $this->children_model->update($this->user->children_uuid, $update);
        
        //return result
        $result = array();
        $result['update_status'] = $status !== TRUE ? STATUS_FAIL : STATUS_SUCCESS;
        $result['user_photo_url']= get_user_photo_url($user_photo, USER_TYPE_CHILDREN);
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * update password of profile
    * 
    */
    public function update_profile_password()
    {
        $this->restrict();   
        
        //check if all fields are exist
        $data = $this->params['data'];
        $fields = array('old_password', 'new_password');     
        if(!array_keys_exists($fields, $data))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        //validate password
        if(ct_strlen($data['new_password']) > PASSWORD_MAX_LEN || ct_strlen($data['new_password']) < PASSWORD_MIN_LEN)
        {      
            return $this->return_error(INVALID_USER_PASSWORD);
        }
        
        //check old password
        $old_password = $data['old_password'];
        $new_password = $data['new_password'];
        $user = $this->user;
        
        if(do_hash($user->salt . $old_password) !== $user->password_hash)
        {
            $result = array();
            $result['verify_old_password'] = VERIFY_FAIL;
            $this->return_result($result);    
        }
        
        //update row
        $update = array();
        $update['user_passwd'] = $new_password;
        $status   = $this->children_model->update($this->user->children_uuid, $update);
        
        //return result
        $result = array();
        $result['update_status'] = $status!==TRUE?STATUS_FAIL:STATUS_SUCCESS;
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /////////////////////////////////////////////////////////////////////
    //PROTECTED METHODS
    /////////////////////////////////////////////////////////////////////
    /**
    * check login
    * 
    */
    protected function restrict($check_login_status=TRUE)
    {
        parent::restrict($check_login_status);  
        
        //check class status
        if((int)$this->user->active_status === STATUS_INACTIVE)
        {
            $this->return_error(FORCE_CHILDREN_CLASS_INACTIVE);
        }
        
        //check expire status if expired
        $expire_time = strtotime($this->user->expire_date);
        if($expire_time < time())
        {               
            $this->return_error(FORCE_CHILDREN_EXPIRED);
        }      
    } 
    //--------------------------------------------------------------------
    
    /////////////////////////////////////////////////////////////////////
    //PRIVATE METHODS
    /////////////////////////////////////////////////////////////////////
    
    /**
    * prepare return result after login
    * 
    * @param int    $login_uid
    * @param array  $school
    * @param int    $device_type
    * @param array  $children
    */
    private function _connection_data($login_uid, $school, $device_type, $children)
    {        
        $result    = array();
        $login_key = $this->log_login_model->get_connection_key($login_uid);
        $recent_date = $this->children_model->update_recent_date($children->children_uuid);
        
        $client = array();
        $client['connection_uid'] = $login_uid;
        $client['connection_key'] = $login_key;
        $client['device_type']    = $device_type;
        
        $client['user_id']   = $children->user_id;
        $client['user_name'] = $children->user_name;
        $client['user_type'] = USER_TYPE_CHILDREN;
        $client['user_photo']= get_user_photo_url($children->user_photo, USER_TYPE_CHILDREN);   
        
        $client['school_uid']     = $school->school_uuid;
        $client['school_name_cn'] = $school->school_name_cn;
        $client['school_name_ko'] = $school->school_name_ko;
        $client['school_name_en'] = $school->school_name_en;
        //$client['school_name_jp'] = $school->school_name_jp;
         
        $client['class_uid'] = $children->class_uid;
        $client['grade_num'] = $children->grade_num;
        $client['class_num'] = $children->class_num;
        
        $client['expire_date']    = $children->expire_date;
        $client['payment_details']= $this->last_payment_of_children($children->user_id);
        
        $result['login_status']     = STATUS_SUCCESS;
        $result['recent_visit_date']= $recent_date;
        $result['client']           = $client;
        $result['number_of_users']  = $this->get_number_of_users($school->school_uuid);
        
        return $result;
    }
}//end class