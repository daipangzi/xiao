<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

class Parents extends Mobile_Controller
{
    private $childrens = array();
    
    public function __construct()
    {
        $this->controller = 'parents';
        
        parent::__construct();
        
        if($this->params['client']['user_type'] != USER_TYPE_PARENT)
        {
            $this->return_error(ERR_USER_TYPE_NOT_MATCH);        
        }
    }
    //--------------------------------------------------------------------
    
    /**
    * parent register action
    * 
    */
    public function register()
    {
        //check all fields are setted exactly
        if(($error = $this->check_user_fields_in($this->params['data'])) !== NO_ERROR)
        {
            $this->return_error($error);
        }
        
        ////////////////////////////////////////////////
        $result = array();
        $client = $this->params['client'];
        $data   = $this->get_clean_user_fields_in($this->params['data']);
        ////////////////////////////////////////////////
        
        //validate fields  
        if(($return = $this->validate_user_fields_in($data)) !== NO_ERROR)
        {
            $result['validation_fail'] = STATUS_YES;
            $result['validation_reason']= $return;
            $this->return_result($result);
        }
        
        //check duplicate user_id between children and children-parent
        $return = $this->_check_duplicate_userid_in($data);
        if(!empty($return))
        {
            $this->return_result($return);    
        }
         
        //check if children is already registered on our system 
        //and requested school is different with that
        $childs = $data['children'];
        $return = $this->_check_children_different_school($childs);
        if(!empty($return))
        {
            $this->return_result($return);
        }
        
        //check if children is already registered on our system 
        //and requested school is different with that
        $return = $this->_check_children_has_limit_parents($childs);
        if(!empty($return))
        {
            $this->return_result($return);
        }
                  
        //check parents are alredy exist in user_router
        $return = $this->_exist_in_system($data);  
        if(!empty($return))
        {
            $this->return_result($return);
        }
        
        //check duplicate email and phone
        $return = $this->_check_duplicate_email_phone($data);
        if(!empty($return))
        {
            $this->return_result($return);
        }
        
        //detect mass request from same ip_address and device
        $this->load->model('buffer_model');
        $this->buffer_model->remove_expires();
        
        $ip_address  = $this->input->ip_address();
        $device_info = $client['device_info'];
        $device_type = $client['device_type'];
        $user_type   = $client['user_type'];
        $requestes = $this->buffer_model
            ->filter_device_info($device_info)
            ->filter_ip($ip_address)
            ->filter_user_type($user_type)
            ->count_all();;
        if($requestes >= BUFFER_LIMIT_REQUEST_NUMS)
        {
            $result['limit_request'] = STATUS_YES;
            $result['refresh_cycle'] = BUFFER_EXPIRE_TIME; //mins
            $this->return_result($result);
        }
        
        //upload photo to tmp
        $photos = $this->_upload_photos($data);
        $data['photos'] = $photos;
        
        $phones = $emails = '';
        if(!empty($data['father']))
        {
            $phones = $data['father']['user_phone'];  
            $emails = $data['father']['user_email']; 
        }
        if(!empty($data['mother']))
        {
            if($phones != '') $phones .= ',';
            if($emails != '') $emails .= ',';
            
            $phones .= $data['mother']['user_phone'];  
            $emails .= $data['mother']['user_email']; 
        }
        
        //generate sms and send it to phones and emails
        $sms_uid = $this->send_verify_code($phones, $emails);
        $content = ct_json_encode($data);
        $buffer_key = md5($content);
        
        //insert data to buffer table tempereraly
        $info = array();
        $info['key']     = $buffer_key;
        $info['content'] = $content;
        $info['sms_uid'] = $sms_uid;
        $info['device_info'] = $device_info;
        $info['device_type'] = $device_type;
        $info['user_type']   = $user_type;
        $info['ip_address']  = $ip_address;
        $buffer_uid = $this->buffer_model->insert($info);
        if($buffer_uid === FALSE)
        {
            $this->return_error(ERR_INSERT_TO_DATABASE);
        }
        
        //return result
        $result['buffer_uid'] = $buffer_uid;
        $result['buffer_key'] = $buffer_key;
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    public function register_complete()
    {
        $result = array();
        $data   = $this->params['data'];
        $fields = array('buffer_uid', 'buffer_key', 'verify_code');     
        if(!array_keys_exists($fields, $data))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        //load buffer model
        $this->load->model('buffer_model');
        $this->buffer_model->remove_expires();
        
        //get data from buffer
        $buffer_uid = $data['buffer_uid'];
        $buffer_key = $data['buffer_key'];
        $buffer_row = $this->buffer_model->with_sms()->find_buffer($buffer_uid, $buffer_key);
        if(empty($buffer_row))
        {
            $this->return_error(BUFFER_NO_EXIST);
        }        
        
        //check all fields are setted exactly again
        $content = ct_json_decode($buffer_row->content);
        if(($erroro = $this->check_user_fields_in($content)) !== NO_ERROR)
        {
            $this->return_error(BUFFER_NOT_MATCH);
        }
        
        //check buffer is expired
        $buffer_time = strtotime($buffer_row->reg_date);        
        if(time() - $buffer_time > BUFFER_EXPIRE_TIME * 60)
        {
            $result['verify_status'] = VERIFY_EXPIRED;
            $this->return_result($result);
        } 
        
        //check verify code with db
        if($buffer_row->verify_code !== $data['verify_code'])
        {
            $result['verify_status'] = VERIFY_FAIL;
            $this->return_result($result);
        }
            
        //check parents are alredy exist in system
        $return = $this->_exist_in_system($content);
        if(!empty($return))
        {
            $this->return_result($return);
        }
        
        //check duplicate email and phone
        $return = $this->_check_duplicate_email_phone($content);
        if(!empty($return))
        {
            $this->return_result($return);
        }
        
        $photos = $content['photos']; 
        $status = $this->_insert_data($content);
        if($status === FALSE)
        {
            $result['register_status'] = STATUS_FAIL;
        }
        else
        {
            //remove buffer data
            $this->buffer_model->delete($buffer_uid);
            
            //if user_photo is exist, then move it from tmp folder to valid folder
            $this->_move_photos($photos);
            
            $result['register_status'] = STATUS_SUCCESS;    
        }
          
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * resend sms again to phones and emails stored in buffer row
    *    
    */
    public function regenerate_sms()
    {
        $data = $this->params['data'];
        if(!isset($data['buffer_uid']) || !isset($data['buffer_key']))
        {
            $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        //load buffer model
        $this->load->model('buffer_model');
        $this->buffer_model->remove_expires();
        
        //get buffer from db
        $buffer_uid = $data['buffer_uid'];
        $buffer_key = $data['buffer_key'];
        $buffer_row = $this->buffer_model->find_buffer($buffer_uid, $buffer_key);
        if(empty($buffer_row))
        {
            $this->return_error(BUFFER_NO_EXIST);
        }
        
        //check all fields are setted exactly again
        $content = ct_json_decode($buffer_row->content);
        if(($error = $this->check_user_fields_in($content)) !== NO_ERROR)
        {
            $this->return_error(BUFFER_NOT_MATCH);
        }
        
        //TODO
        //check regenerated counts here!!!
                
        //collect phones and emails
        $phones = $emails = '';
        if(!empty($content['father']))
        {
            $phones .= $content['father']['user_phone'];
            $emails .= $content['father']['user_email'];
        }
        
        if(!empty($content['mother']))
        {
            if($phones !== '') $phones .= ',';
            if($emails !== '') $emails .= ',';
            
            $phones .= $content['mother']['user_phone'];
            $emails .= $content['mother']['user_email'];
        }
        
        //send verify code by phone and email
        $verify_uid = $this->send_verify_code($phones, $emails);
        
        //update sms_uid on buffer row
        $update_data = array('sms_uid' => $verify_uid);
        $this->buffer_model->update($buffer_uid, $update_data);
        
        $result['buffer_uid'] = $buffer_uid;
        $result['buffer_key'] = $buffer_key;
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * retrieve security password if you forgot it
    *     
    */
    public function forgot()
    {   
        //check if all fields are exist
        $data = $this->params['data'];     
        $fields = array('user_id', 'via_email', 'via_phone');     
        if(!array_keys_exists($fields, $data))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        $result    = array();
        $user_id   = $data['user_id'];
        $via_phone = $data['via_phone'];
        $via_email = $data['via_email'];
        if($via_email == NONE && $via_phone == NONE)
        {
            return $this->return_error(FORGOT_NO_CONTACT_PATH); //no retrieve path
        }  
        
        //get user row
        $user = $this->parent_model->find_by_userid($user_id);
        if(empty($user))
        {
            $result['user_exist'] = NO_REGISTERED;
            $this->return_result($result);        
        }
                    
        //check if this user has already requested
        $path = SECURITY_PATH_BOTH;
        if($via_phone == SELECTED && $via_email == NONE)
        {
            $path = SECURITY_PATH_PHONE;
        }
        else if($via_email == SELECTED && $via_phone == NONE)
        {
            $path = SECURITY_PATH_EMAIL;
        }
        
        //load model
        $this->load->model('amnesiac_model');
        $last_request = $this->amnesiac_model->find_row($user_id, USER_TYPE_PARENT, $path);  
        if(!empty($last_request) && isset($last_request->reg_date))
        {           
            $result['has_requested']= STATUS_YES;
            $result['expire_time']  = date(FORMAT_DATETIME, strtotime($last_request->reg_date) + 24 * 60 * 60);
            $this->return_result($result);
        }
        
        //check empty string for phone and email
        if(trim($user->user_email) == '' && trim($user->user_phone) == '')
        {
            $result['phone_email_exist']= NONE;
            $this->return_result($result);
        }
        if($via_phone == SELECTED && $via_email == NONE && trim($user->user_phone) == '')
        {
            $result['phone_exist']= NONE;
            $this->return_result($result);
        }
        if($via_email == SELECTED && $via_phone == NONE && trim($user->user_email) == '')
        {
            $result['email_exist']= NONE;
            $this->return_result($result);
        }
        
        //register this to fogotten_model
        $fdata = array();
        $fdata['user_id']   = $user_id;
        $fdata['user_name'] = $user->user_name;
        $fdata['user_type'] = USER_TYPE_PARENT;
        $fdata['path']      = $path;
        $fdata['device_type'] = $this->params['client']['device_type'];
        $fdata['device_info'] = $this->params['client']['device_info'];
        $this->amnesiac_model->insert($fdata);
        if($status === FALSE)
        {
            $this->return_error(ERR_INSERT_TO_DATABASE);
        }
        
        //send info, put process in background
        if(SEND_SMS === TRUE)
        {
            $language_name = get_long_language_name($this->short_language);
            $command = sprintf(COMMAND_SEND_SECURITY, $language_name, USER_TYPE_PARENT, $user_id, $via_phone, $via_email);
            $process = new Process;
            $pcs_id  = $process->runInBackground($command);
        }
        
        //return result
        $result['finding_status'] = STATUS_SUCCESS;
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * parent login
    * 
    */
    public function login()
    {
        $result = array();
        $client = $this->params['client'];
        $data   = $this->params['data'];
        
        //check valid login data
        $fields = array('user_id', 'user_passwd');
        if(!array_keys_exists($fields, $data)) 
        {
            $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        if(!valid_id_number($data['user_id']) && !valid_passport_number($data['user_id']))
        {
            $this->return_error(INVALID_USER_ID);
        }
        
        if(!valid_password($data['user_passwd']))
        {      
            $this->return_error(INVALID_USER_PASSWORD);
        }
        
        //check if user is registered on our system
        //if registered get school ids with user_id and user_type
        $user_id     = $data['user_id'];
        $user_passwd = $data['user_passwd'];
        $ip_address  = $this->input->ip_address();
        $parent      = $this->parent_model->find_by_userid($user_id);
        if(empty($parent))
        {
            $result['user_exist'] = NO_REGISTERED;
            $this->return_result($result);        
        }
        
        //check login attempt, if attempt is greater than specified count, 
        //return fail
        $attempts = $this->log_login_model->count_login_attempts($user_id, USER_TYPE_PARENT, $ip_address, LOGIN_ATTEMPT_CYCLE); 
        if($attempts >= LOGIN_ATTEMPT_LIMIT_NUMS)
        {   
            $result['limit_attempt'] = STATUS_YES;
            $result['refresh_cycle'] = LOGIN_ATTEMPT_CYCLE; //mins
            $this->return_result($result);        
        }
        
        //if status is active, then check password
        if(do_hash($parent->salt . $user_passwd) !== $parent->password_hash)
        {
            $this->log_login_model->increase_login_attempts($user_id, USER_TYPE_PARENT, $ip_address);
            
            $result['verify_passwd'] = VERIFY_FAIL;
            $this->return_result($result);    
        }
        
        //check user status
        if((int)$parent->user_status === STATUS_INACTIVE)
        {
            $this->log_login_model->increase_login_attempts($user_id, USER_TYPE_PARENT, $ip_address);
            
            $result['user_status'] = STATUS_INACTIVE;
            $this->return_result($result);
        }
        
        //check user is already logged in on another device
        //then register new login info and set it as a inactive status and send request a fav song
        //to check right person
        $login_uid  = FALSE;
        $device_info = $client['device_info'];
        $device_type = $client['device_type'];
        
        $login_info = array();
        $login_info['user_id']   = $user_id;
        $login_info['user_name'] = $parent->user_name;
        $login_info['user_type'] = USER_TYPE_PARENT;
        $login_info['device_type'] = $device_type;
        $login_info['device_info'] = $device_info;
        
        $exist_login = $this->log_login_model
            ->filter_userid($user_id)
            ->filter_user_type(USER_TYPE_PARENT)
            ->filter_device_type($device_type)
            ->filter_device_info($device_info)
            ->filter_status(STATUS_ACTIVE)
            ->find_row();
        if(!empty($exist_login))
        {                                                          
            //log out other connections                
            $this->log_login_model->log_out_by_userid($user_id, USER_TYPE_PARENT, $exist_login->uid);
                               
            $login_uid = $exist_login->uid;
        }
        else
        {
            //log out other connections                
            $this->log_login_model->log_out_by_userid($user_id, USER_TYPE_PARENT);
            
            $login_uid = $this->log_login_model->insert($login_info);
        }
        
        if($login_uid === FALSE)
        {
            $this->return_error(LOGIN_NO_LOGIN_UID);
        }
        
        //clear login attempts history
        $this->log_login_model->clear_login_attempts($user_id, USER_TYPE_PARENT, $ip_address);
          
        //if everything is ok, then login client to our system
        //make return result
        $result = $this->_connection_data($login_uid, $parent, $client['device_type']);
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * user logout
    * 
    */
    public function logout()
    {
        $this->restrict(FALSE);
        
        $client     = $this->params['client'];
        $user_id    = $client['user_id'];   
        $connect_uid= $client['connection_uid'];
        
        //set recent_visite_date by current time
        $this->parent_model->update_recent_date($this->user->parent_uuid);       
                                                                                                  
        //delete from login_monitor
        $this->log_login_model->log_out($connect_uid);
        
        //return result
        $result = array();
        $result['logout_status'] = STATUS_SUCCESS;
        
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * get parent receiver lists
    * 
    */
    public function receiver_lists()
    {
        $this->restrict();
        
        //if no children
        if(empty($this->childrens))
        {
            $result = array();
            $result['master'] = array();
            $result['my_children'] = array();
            $result['classes'] = array();
            $this->return_result($result);
        }
        
        $client     = $this->params['client'];
        $school_uid = $client['school_uid'];
        $photo_field1 = $this->user_photo_field(USER_TYPE_TEACHER, 'user_photo');
        $photo_field2 = $this->user_photo_field(USER_TYPE_CHILDREN, 'user_photo');
        $photo_field3 = $this->user_photo_field(USER_TYPE_PARENT, 'user_photo');
        
        //get master row
        $master = $this->teacher_model
            ->select("teacher_uuid, user_id, user_name, user_sex, {$photo_field1}, user_email, user_phone, ". USER_TYPE_MASTER . ' user_type', FALSE)
            ->find_master($school_uid);
        
        $exclude_teacher_uids = array();
        if(isset($master->teacher_uuid))
        {
            $exclude_teacher_uids[] = $master->teacher_uuid;
            unset($master->teacher_uuid);
        }
            
        //get my childrens
        $my_children = array();
        $my_children_classes = array();
        $my_children_uids = array();
        foreach($this->childrens as $children)
        {
            $my_children_uids = $children->children_uuid;
            
            //children personal info
            $item = array();
            $item['user_id']   = $children->user_id;
            $item['user_name'] = $children->user_name;
            $item['user_sex']  = $children->user_sex;
            $item['user_photo']= $children->user_photo_url;
            $item['user_phone']= $children->user_phone;
            $item['user_email']= $children->user_email;
            $item['user_type'] = USER_TYPE_CHILDREN;
            
            $expire_time = strtotime($children->expire_date);
            $item['expire_status'] = ($expire_time <= time())?CHILDREN_SERVICE_EXPIRED:CHILDREN_SERVICE_AVAILABLE;
            
            $my_children[] = $item;
            
            //add class to array
            if(!key_exists($children->class_uid, $my_children_classes))
            {
                $temp = new stdClass();
                $temp->class_uid = $children->class_uid;
                $temp->grade_num = $children->grade_num;
                $temp->class_num = $children->class_num;
                $temp->year_level= $children->year_level;
                $my_children_classes[$children->class_uid] = $temp;
            }
        }
        
        $classes = array();
        foreach($my_children_classes as $class)
        {
            //get teachers
            $teachers = $this->teacher_model
                ->filter_school($school_uid)
                ->select("teacher_uuid, user_id, user_name, user_sex, {$photo_field1}, user_email, user_phone, " . USER_TYPE_TEACHER . ' user_type, GROUP_CONCAT(teacher_role) roles', FALSE)
                ->filter_out_user_uids($exclude_teacher_uids)
                ->find_all_by_class($class->class_uid);
                
            if(empty($teachers)) $teachers = array();
               
            foreach($teachers as $teacher)
            {
                //set charge or subject flag
                $roles = explode(',', $teacher->roles);
                if(in_array(ROLE_CHARGE, $roles))
                {
                    $teacher->is_charge = STATUS_YES;
                }
                else
                {
                    $teacher->is_charge = STATUS_NO;
                }
                
                if(in_array(ROLE_SUBJECT, $roles))
                {
                    $teacher->is_subject = STATUS_YES;
                    $teacher->subject_name = $this->teacher_model->get_charge_subject_name($teacher->teacher_uuid);
                }
                else
                {
                    $teacher->is_subject = STATUS_NO;
                }
                
                unset($teacher->teacher_uuid);
                unset($teacher->roles);
            }
            
            //get classmates
            $classmates = $this->children_model
                //->select("children_uuid, user_id, user_name, user_sex, {$photo_field2}, user_email, user_phone, " . USER_TYPE_CHILDREN . ' user_type, expire_date', FALSE)
                ->filter_class($class->class_uid)
                ->order_by('user_name')
                ->find_all();
            if(empty($classmates)) $classmates = array();
            
            $parents = array();
            foreach($classmates as $k => $children)
            {
                $expire_time = strtotime($children->expire_date);
                $children->expire_status = ($expire_time <= time())?CHILDREN_SERVICE_EXPIRED:CHILDREN_SERVICE_AVAILABLE;
                
                //parents
                $children_parent_uids = $this->children_model->get_children_parent_uids($children->children_uuid);
                $children_parents = $this->parent_model
                    //->select("user_id, user_name, user_sex, {$photo_field3}, user_email, user_phone, " . USER_TYPE_PARENT . " user_type, parent_type", FALSE)
                    ->select("user_id, user_name, user_sex, {$photo_field3}, user_email, 
                        user_phone, " . USER_TYPE_PARENT . " user_type, parent_type, 
                        '{$children->user_id}' children_id, '{$children->user_name}' children_name, '{$children->expire_status}' expire_status", FALSE)
                    ->filter_user_uids($children_parent_uids)
                    ->filter_out_user_uids($this->user->parent_uuid)
                    ->find_all();
                   
                if(!empty($children_parents))
                {
                    $parents = array_merge($parents, $children_parents);
                }
                
                /*if(empty($parents)) 
                {
                    unset($classmates[$k]);
                }
                
                $children->parents = $parents;
                
                unset($children->children_uuid);
                unset($children->expire_date);*/
            }
              
            $class->teachers = $teachers;
            $class->parents  = $parents;
            //$class->classmates = $classmates;
            $classes[] = $class;
        }
       
        //make result
        $result = array();
        $result['master']     = $master;
        $result['my_children']= $my_children;
        $result['classes']    = $classes;
        
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * retrieve all teacher's profile info
    * 
    */
    public function profile()
    {
        $this->restrict();
        
        //personal
        $personal = array();
        $personal['user_name']  = $this->user->user_name;
        $personal['user_sex']   = $this->user->user_sex;
        $personal['user_email'] = $this->user->user_email;
        $personal['user_phone'] = $this->user->user_phone;
        $personal['user_photo'] = $this->user->user_photo_url;
        $personal['user_birthday'] = $this->user->user_birthday;
        $personal['user_address']  = $this->user->user_address;
        $personal['parent_type']   = $this->user->parent_type;
        $personal['recent_visit_date'] = $this->user->recent_visit_date;
        
        //childrens
        $children_uids = $this->parent_model->get_parent_children_uids($this->user->parent_uuid);
        $photo_field = $this->user_photo_field(USER_TYPE_CHILDREN);
        $parent_childrens = $this->children_model
            ->select("*, {$photo_field}", FALSE)
            ->filter_user_uids($children_uids)
            ->with_class()
            ->find_all();
            
        $childrens = array(); 
        foreach($parent_childrens as $children)
        {
            $item = array();
            $item['user_id']        = $children->user_id;
            $item['user_name']      = $children->user_name;
            $item['user_sex']       = $children->user_sex;
            $item['user_birthday']  = $children->user_birthday;
            $item['user_address']   = $children->user_address;
            $item['user_email']     = $children->user_email;
            $item['user_phone']     = $children->user_phone;
            $item['user_photo']     = $children->user_photo_url;
            $item['recent_visit_date'] = $children->recent_visit_date;
            
            $item['school_uid'] = $children->school_uid;
            $item['class_uid']  = $children->class_uid;
            $item['grade_num']  = $children->grade_num;
            $item['class_num']  = $children->class_num;
            
            $item['expire_date'] = $children->expire_date;
            
            $childrens[] = $item;
        }                
                
        //return result
        $result = array();
        $result['personal'] = $personal;
        $result['children'] = $childrens;
        
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * update general info of profile
    * 
    */
    public function update_profile_general()
    {
        $this->restrict();
        
        //check if all fields are exist
        $data = get_clean_fields_in($this->params['data']);
        $fields = array('user_name', 'user_sex', 'user_birthday', 'user_address', 'user_email', 'user_phone');     
        if(!array_keys_exists($fields, $data))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        //validate fields
        if(($return = $this->validate_profile_general($data)) !== NO_ERROR)
        {
            $result = array();
            $result['validation_fail'] = STATUS_YES;
            $result['validation_reason']= $return;
            $this->return_result($result);
        }
        
        //check email and phone is already exist in gus_teachers
        $email_count = $this->parent_model
            ->filter_email($data['user_email'])
            ->filter_out_user_id($this->user->user_id)
            ->count_all();
        if($email_count !== 0)
        {
            $result['exist_email'] = $data['user_email'];
            $this->return_result($result);
        }
        
        $phone_count = $this->parent_model
            ->filter_phone($data['user_phone'])
            ->filter_out_user_id($this->user->user_id)
            ->count_all();
        if($phone_count !== 0)
        {
            $result['exist_phone'] = $data['user_phone'];
            $this->return_result($result);
        }
        
        //update row
        $update = array();
        $update['user_id']      = $this->user->user_id;
        $update['user_name']    = $data['user_name'];
        $update['user_sex']     = $data['user_sex']==MAN?MAN:WOMAN;
        $update['user_address'] = $data['user_address'];
        $update['user_email']   = $data['user_email'];
        $update['user_phone']   = $data['user_phone'];
        $update['parent_type']  = $data['user_sex']==MAN?MAN:WOMAN;
        $update['user_birthday']= $data['user_birthday'];
        $status = $this->parent_model->update($this->user->parent_uuid, $update);
        
        //return result
        $result = array();
        $result['update_status'] = $status !== TRUE ? STATUS_FAIL : STATUS_SUCCESS;
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * update general info of profile
    * 
    */
    public function update_profile_photo()
    {
        $this->restrict();
        
        //check if all fields are exist
        $data = $this->params['data'];
        if(!isset($data['user_photo']))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        //validate and upload photo
        $user_photo = '';
        if($data['user_photo'] != '')
        {
            //validate fields
            //$file = $user_id; 
            $file = $data['user_photo']; 
            if(($return = $this->validate_profile_photo($file)) !== NO_ERROR)
            {
                $result = array();
                $result['validation_fail'] = STATUS_YES;
                $result['validation_reason']= $return;
                $this->return_result($result);
            }
            
            //upload photo
            if(!$this->upload_photo($file, USER_TYPE_PARENT))
            {
                $this->return_error(ERR_FILE_UPLOAD_FAIL);
            }
            
            //move photo
            if(!$this->move_photo($file, USER_TYPE_PARENT))
            {
                $this->return_error(ERR_FILE_UPLOAD_FAIL);
            }
            
            //delete old photo before new
            delete_user_photo($this->user->user_photo, USER_TYPE_PARENT);
            
            $user_photo = $file;
        }
        
        //update row
        $update = array();
        $update['user_photo'] = $user_photo;
        $status = $this->parent_model->update($this->user->parent_uuid, $update);
        
        //return result
        $result = array();
        $result['update_status'] = $status !== TRUE ? STATUS_FAIL : STATUS_SUCCESS;
        $result['user_photo_url']= get_user_photo_url($user_photo, USER_TYPE_PARENT);
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * update password of profile
    * 
    */
    public function update_profile_password()
    {
        $this->restrict();   
        
        //check if all fields are exist
        $data = $this->params['data'];
        $fields = array('old_password', 'new_password');     
        if(!array_keys_exists($fields, $data))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        //validate password
        if(!valid_password($data['new_password']))
        {      
            return INVALID_USER_PASSWORD;
        }
        
        //check old password
        $old_password = $data['old_password'];
        $new_password = $data['new_password'];
        $user = $this->user;
        
        if(do_hash($user->salt . $old_password) !== $user->password_hash)
        {
            $result = array();
            $result['verify_old_password'] = VERIFY_FAIL;
            $this->return_result($result);    
        }
        
        //update row
        $update = array();
        $update['user_passwd'] = $new_password;
        $status = $this->parent_model->update($this->user->parent_uuid, $update);
        
        //return result
        $result = array();
        $result['update_status'] = $status !== TRUE ? STATUS_FAIL : STATUS_SUCCESS;
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * update children's school and class
    * 
    */
    public function update_children_school_class()
    {
        $this->restrict();   
        
        /////////////////////////////////////
        $client = $this->params['client'];
        $data   = $this->params['data'];
        $school_uid = $client['school_uid'];
        /////////////////////////////////////
        
        $fields = array('children_id', 'school_uid', 'class_uid');
        if(!array_keys_exists($fields, $data))
        {
            $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        ///////////////////////////////////////
        $children_id    = $data['children_id'];
        $new_school_uid = $data['school_uid'];
        $new_class_uid  = $data['class_uid'];
        $result = array();
        ///////////////////////////////////////
        
        //get children row
        $children = $this->children_model->with_class()->find_by_userid($children_id);
        if(empty($children))
        {
            $this->return_error(PARENT_INVALID_CHILDREN);
        }       
        
        //check that the children is parent's right children
        $parent_children_uids = $this->parent_model->get_parent_children_uids($this->user->parent_uuid);
        if(!in_array($children->children_uuid, $parent_children_uids))
        {
            $this->return_error(PARENT_INVALID_CHILDREN);
        } 
        
        //check children's school is right        
        /*if($children->school_uid != $school_uid)
        {
            $this->return_error(PARENT_CHILDREN_NOT_IN_SCHOOL);
        } */
        
        //check new_school is valid
        if(!$this->school_model->valid_school($new_school_uid))
        {
            $this->return_error(INVALID_SCHOOL);
        }
        
        //check new_class is valid
        if(!$this->school_model->valid_class($new_school_uid, $new_class_uid))
        {
            $this->return_error(INVALID_SCHOOL_CLASS);
        }
        
        //check if there is really difference with school_uid and class_uid
        if($school_uid === $new_school_uid && $children->class_uid === $new_class_uid)
        {
            $result['update_status'] = NO_CHANGE;
            $this->return_result($result);
        }
        
        //update row
        $update = array();
        $update['class_uid'] = $new_class_uid;
        if($school_uid === $new_school_uid)
        {
            $update['school_date']= date(FORMAT_DATETIME);    
        }
        $status = $this->children_model->update($children->children_uuid, $update);                  
        if($status === TRUE)
        {
            //brefore return result, make user to logout
            $connection_uid = $this->params['client']['connection_uid'];
            $this->log_login_model->log_out($connection_uid, LOGIN_STATUS_OUT);            
            
            //and make updated' children and other parents to logout
            $this->log_login_model->log_out_by_userid($children_id, USER_TYPE_CHILDREN, FALSE, LOGIN_STATUS_YOU_UPDATED);
            
            //get all parents of updated children
            $children_parent_uids = $this->children_model->get_children_parent_uids($children->children_uuid);
            if(!empty($children_parent_uids))
            {
                foreach($children_parent_uids as $parent_uid)
                {
                    if($parent_uid === $this->user->parent_uuid) continue;
                    
                    $parent = $this->parent_model->find($parent_uid);
                    if(empty($parent)) continue;
                                        
                    $this->log_login_model->log_out_by_userid($parent->user_id, USER_TYPE_PARENT, FALSE, LOGIN_STATUS_YOUR_CHILDREN_UPDATED);
                }
            }
            
            $result['update_status'] = STATUS_SUCCESS;
        }
        else
        {
            $result['update_status'] = STATUS_FAIL;
        }
        
        //return result
        $this->return_result($result);
    }
    
    /**
    * add new children to parent
    * 
    */
    public function add_new_children()
    {
        $this->restrict();   
        
        //check user fields to insert
        $data = $this->params['data'];
        if(!parent::check_user_fields_in($data, USER_TYPE_CHILDREN))
        {
            $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        /////////////////////////////////////////
        $school_uid = (int)$data['school_uid'];
        $class_uid  = (int)$data['children_class'];
        $children_id  = $data['user_id'];
        $children_uid = FALSE;        
        $result = array();
        /////////////////////////////////////////
        
        //check school and class is valid
        if(!$this->school_model->valid_school($school_uid))
        {
            $this->return_error(INVALID_SCHOOL);
        }
        
        if(!$this->school_model->valid_class($school_uid, $class_uid))
        {
            $this->return_error(INVALID_SCHOOL_CLASS);
        }
        
        //check user fields are valid
        if(($return = parent::validate_user_fields_in($data)) !== NO_ERROR)
        {
            $result['validation_fail'] = STATUS_YES;
            $result['validation_reason']= $return;
            $this->return_result($result);
        }
        
        //check if children is already registered in system
        //if registered, we didn't touch old info, just add new relation to this parent
        $exist_children = $this->children_model->with_class()->find_by_userid($children_id);  
        if(!empty($exist_children))
        {
            //check children's school is different
            if($school_uid !== (int)$exist_children->school_uid)
            {
                $school = $this->school_model
                    ->select_name($this->short_language)
                    ->find($exist_children->school_uid);
                
                $result['difference_children'] = $children_id;
                $result['school_name'] = $school->school_name;
                $this->return_result($result);
            }
            
            //check if new children have limit parents
            $new_children_parent_nums = $this->children_model->count_children_parents($exist_children->children_uuid);
            if($new_children_parent_nums >= CHILDREN_PARENTS_LIMIT)
            {
                $result['limit_parents'] = $children_id;
                $this->return_error($result);
            }
            
            $children_uid = $exist_children->children_uuid; 
        } 
        else
        {
            $user_photo = '';
            
            if($data['user_photo'] != '')
            {
                $file = $data['user_photo'];                 
                //upload photo
                if(!$this->upload_photo($file, USER_TYPE_CHILDREN))
                {
                    $this->return_error(ERR_FILE_UPLOAD_FAIL);
                }
                
                //move photo
                if(!$this->move_photo($file, USER_TYPE_CHILDREN))
                {
                    $this->return_error(ERR_FILE_UPLOAD_FAIL);
                }
                
                $user_photo = $file;
            }
        
            //expire date
            $expire_duration = get_test_duration();
            
            //insert new children
            $children_data = array();
            $children_data['user_id']   = $children_id;
            $children_data['user_name'] = $data['user_name'];
            $children_data['user_sex']  = $data['user_sex'];
            $children_data['user_phone']= $data['user_phone'];
            $children_data['user_email']= $data['user_email'];
            $children_data['user_passwd']   = $data['user_passwd'];
            $children_data['user_birthday'] = $data['user_birthday'];
            $children_data['user_photo']  = $user_photo;
            $children_data['expire_date'] = date(FORMAT_DATE, time() + strtotime("{$expire_duration}"));
            $children_data['class_uid']   = $data['children_class'];                
            $children_uid = $this->children_model->insert($children_data);
        }
        
        //if children_uid is false, then return false
        if($children_uid === FALSE)
        {
            $result['register_status'] = STATUS_FAIL;
            $this->return_result($result);
        }
        
        //add relation with new children
        $status = $this->parent_model->insert_parent_children_router($this->user->parent_uuid, $children_uid);
        if($status === TRUE)
        {
            //before return result, make user to logout
            $connection_uid = $this->params['client']['connection_uid'];
            $this->log_login_model->log_out($connection_uid, LOGIN_STATUS_OUT);            
            
            $result['register_status'] = STATUS_SUCCESS;
        }
        else
        {
            $result['register_status'] = STATUS_FAIL;
        }
        
        //return result
        $this->return_result($result);
    }
    
    /**
    * remove parent-children relation 
    * 
    */
    public function remove_children()
    {
        $this->restrict();
        
        ///////////////////////////////
        $result = array();
        $data = $this->params['data'];        
        ///////////////////////////////            
        
        //check fields are setted
        $fields = array('children_id');
        if(!array_keys_exists($fields, $data))
        {
            $this->return_error(ERR_MISSING_PARAMETERS);
        }    
        
        //check children
        $children = $this->children_model->find_by_userid($data['children_id']);
        if(empty($children))
        {
            $this->return_error(PARENT_INVALID_CHILDREN);
        }       
        
        //check that the children is parent's right children
        $parent_children_uids = $this->parent_model->get_parent_children_uids($this->user->parent_uuid);
        if(!in_array($children->children_uuid, $parent_children_uids))
        {
            $this->return_error(PARENT_INVALID_CHILDREN);
        } 
        
        //check if this children is last, if last, can't remove relation
        if(count($parent_children_uids) === 1)
        {
            $this->return_error(PARENT_ONE_CHILDREN);
        }
        
        //remove parent-children relation
        $status = $this->parent_model->delete_parent_children_router($this->user->parent_uuid, $children->children_uuid);
        if($status === TRUE)
        {
            //before return result, make user to logout
            $connection_uid = $this->params['client']['connection_uid'];
            $this->log_login_model->log_out($connection_uid, LOGIN_STATUS_OUT);     
            
            //and make updated' children and other parents to logout
            $this->log_login_model->log_out_by_userid($children->children_uuid, USER_TYPE_CHILDREN, FALSE, LOGIN_STATUS_YOU_UPDATED);       
            
            $result['remove_status'] = STATUS_SUCCESS;
        }
        else
        {
            $result['remove_status'] = STATUS_FAIL;
        }
        
        //return result
        $this->return_result($result);         
    }
    
    /////////////////////////////////////////////////////////////////////
    //PROTECTED METHODS
    /////////////////////////////////////////////////////////////////////
    
    /**
    * validate requested parameters
    * 
    * @param array $data
    * @return array
    */
    protected function validate_user_fields_in($data)
    {          
        $father = $data['father'];
        $mother = $data['mother'];
        $childs = $data['children'];
                   
        //check father's fields
        if(!empty($father))
        {
            if(($return = parent::validate_user_fields_in($father)) !== NO_ERROR)
            {
                return $return+100;
            }
        }
        
        //check mother's fields
        if(!empty($mother))
        {
            if(($return = parent::validate_user_fields_in($mother)) !== NO_ERROR)
            {
                return $return+200;
            }
        }
        
        //check children's field per every children
        foreach($childs as $child)
        {
            if(($return = parent::validate_user_fields_in($child)) !== NO_ERROR)
            {
                return $return+300;
            }  
            
            //additional validation for childrens
            if(!$this->school_model->valid_school($child['school_uid']))
            {
                return INVALID_SCHOOL;
            }
            
            if(!$this->school_model->valid_class($child['school_uid'], $child['children_class']))
            {
                return REGISTER_INVALID_CHILDREN_CLASS;
            }
        } 
          
        return NO_ERROR;
    }
    //--------------------------------------------------------------------
    
    /**
    * check user fields is setted
    * 
    * @param array $data
    * @return array
    */
    protected function check_user_fields_in($data)
    {       
        //check father and mother is setted in data
        if(!isset($data['father']) || !isset($data['mother']))
        {
            return REGISTER_MISSING_PARENT_INFO;
        }        
        
        //check children is setted in data
        if(!isset($data['children']) || !is_array($data['children']) || empty($data['children']))
        {
            return REGISTER_MISSING_CHILDREN_INFO;
        } 
        
        $father = $data['father'];
        $mother = $data['mother'];
        $childs = $data['children'];
        //check user fields in father and mother and every children's info 
        if(!empty($father) && !parent::check_user_fields_in($father, USER_TYPE_PARENT))
        {
            return REGISTER_MISSING_FATHER_PARAMETERS;
        }      
        
        if(!empty($mother) && !parent::check_user_fields_in($mother, USER_TYPE_PARENT))
        {
            return REGISTER_MISSING_MOTHER_PARAMETERS;
        }      
        
        foreach($childs as $child)
        {
            if(!parent::check_user_fields_in($child, USER_TYPE_CHILDREN))
            {
                return REGISTER_MISSING_CHILDREN_PARAMETERS;
            }      
        }
        
        return NO_ERROR;
    }
    //--------------------------------------------------------------------
    
    /**
    * trim and explode data fields
    * 
    * @param array $data
    */
    protected function get_clean_user_fields_in($data) 
    {              
        $father = get_clean_fields_in($data['father']);
        $mother = get_clean_fields_in($data['mother']);
        
        $childs = $data['children'];
        foreach($childs as $code => $child)
        {
            $child = get_clean_fields_in($child);
            $childs[$code] = $child;
        }
        
        $result = array();
        $result['father'] = $father;
        $result['mother'] = $mother;
        $result['children'] = $childs;
        
        return $result;
    }
    //--------------------------------------------------------------------
    
    /**
    * check login
    * 
    */
    protected function restrict($check_login_status=TRUE)
    {
        parent::restrict($check_login_status);
        
        //check school_uid is in client
        if(!isset($this->params['client']['school_uid']))
        {
            $this->return_error(ERR_MISSING_CLIENT_PARAMETERS);
        }
        
        $client = $this->params['client'];
        $school_uid = $client['school_uid'];
        
        //check my children in this school
        $my_children_uids = $this->parent_model->get_parent_children_uids($this->user->parent_uuid);
        if(empty($my_children_uids))
        {
            $this->return_error(FORCE_PARENT_NO_CHILDREN);    
        }

        $photo_field = $this->user_photo_field(USER_TYPE_CHILDREN);
        $childrens = $this->children_model
            ->select("*, {$photo_field}", FALSE)
            ->filter_school($school_uid)
            ->filter_user_uids($my_children_uids)
            ->with_class()
            ->find_all();
            
        if(empty($childrens))
        {
            $this->return_error(FORCE_PARENT_NO_CHILDREN);
        }
        
        $this->childrens = $childrens;
    } 
    //--------------------------------------------------------------------
    
    /////////////////////////////////////////////////////////////////////
    //PRIVATE METHODS
    /////////////////////////////////////////////////////////////////////
    
    private function _check_duplicate_userid_in($data)
    {
        $result = array();
        $father = $data['father'];
        $mother = $data['mother'];
        $children = $data['children'];
        
        //check duplicate children_id
        $child_ids = array();
        $parent_ids= array();
        foreach($children as $item)
        {
            if(in_array($item['user_id'], $child_ids))
            {
                $result['duplicate_children'] = $item['user_id'];
                return $result;
            }
            
            $child_ids[] = $item['user_id'];
        }
        
        if(!empty($father))
        {
            if(in_array($father['user_id'], $child_ids))
            {
                $result['duplicate_parent_children'] = $father['user_id'];
                return $result;
            }
            
            $parent_ids[] = $father['user_id'];
        }
        
        if(!empty($mother))
        {
            if(in_array($mother['user_id'], $child_ids))
            {
                $result['duplicate_parent_children'] = $mother['user_id'];
                return $result;
            }
            
            if(in_array($mother['user_id'], $parent_ids))
            {
                $result['duplicate_parent'] = $mother['user_id'];
                return $result;    
            }
        }
        
        return $result;
    }
    
    /**
    * //check if children is already registered on our system and requested school is different with that
    * 
    * @param array $children
    */
    private function _check_children_different_school($children)
    {
        $result = array();
        $diffs  = array();
        foreach($children as $child)
        {
            $user_id = $child['user_id'];
            $school_uid = $child['school_uid'];
            $child_row  = $this->children_model->with_class()->find_by_userid($user_id);  
            
            if(!empty($child_row) && $school_uid != $child_row->school_uid)
            {
                $school = $this->school_model->select_name($this->short_language)->find($child_row->school_uid);
                $diffs[] = array(
                    'children_id' => $user_id,
                    'school_name' => $school->school_name
                );
            }
        }
        
        if(!empty($diffs))
        {
            $result['children_difference'] = $diffs;
        }
        
        return $result;
    }
    
    /**
    * check children can have more parents
    * 
    * @param array $children
    */
    private function _check_children_has_limit_parents($children)
    {
        $result = array();
        $limits = array();
        foreach($children as $child)
        {
            $user_id   = $child['user_id'];
            $school_id = $child['school_uid'];
            $child_row = $this->children_model->find_by_userid($user_id);  
            
            if(!empty($child_row) && $this->children_model->count_children_parents($user_id) >= CHILDREN_PARENTS_LIMIT)
            {
                $limits[] = $user_id;
            }
        }
        
        if(!empty($limits))
        {
            $result['limit_parents'] = $limits;
        }
        
        return $result;
    }
    
    /**
    * check father and mother are exist in our system
    * 
    * @param array $data
    */
    private function _exist_in_system($data)
    {
        $result = array();
        $father = $data['father'];
        $mother = $data['mother'];
        
        //check from db            
        $exists = array();
        if(!empty($father) && $this->parent_model->find_by_userid($father['user_id']))
        {
            $exists[] = $father['user_id'];
        }
        
        if(!empty($mother) && $this->parent_model->find_by_userid($mother['user_id']))
        {
            $exists[] = $mother['user_id'];
        }
        
        if(!empty($exists))
        {
            $result['exist_user'] = $exists;
        }
        
        return $result;
    }
    
    /**
    * check duplicate email and phone of parents
    * 
    * @param array $data
    */
    private function _check_duplicate_email_phone($data)
    {
        $result = array();
        $father = $data['father'];
        $mother = $data['mother'];
        
        $phones = array();
        $emails = array();
        if(!empty($father))
        {
            //check email and phone is already exist in gus_teachers
            $phone_count = $this->parent_model
                ->filter_phone($father['user_phone'])
                ->filter_out_user_id($father['user_id'])
                ->count_all();
            if($phone_count !== 0) 
            {
                $phones[] = $father['user_phone'];
            }
            
            if($father['user_email'] !== '')
            {
                $email_count = $this->parent_model
                    ->filter_email($father['user_email'])
                    ->filter_out_user_id($father['user_id'])
                    ->count_all();
                if($email_count !== 0)
                {
                    $emails[] = $father['user_email'];
                }
            }
        }
        
        if(!empty($mother))
        {
            //check email and phone is already exist in gus_teachers
            $phone_count = $this->parent_model
                ->filter_phone($mother['user_phone'])
                ->filter_out_user_id($mother['user_id'])
                ->count_all();
            if($phone_count !== 0) 
            {
                $phones[] = $mother['user_phone'];
            }       
            
            if($mother['user_email'] !== '')
            {
                $email_count = $this->parent_model
                    ->filter_email($mother['user_email'])
                    ->filter_out_user_id($mother['user_id'])
                    ->count_all();
                if($email_count !== 0)
                {
                    $emails[] = $mother['user_email'];
                }
            }
        }
        
        if(!empty($phones))
        {
            $result['exist_phone'] = $phones;
        }
        if(!empty($emails))
        {
            $result['exist_email'] = $emails;
        }
        
        //check father's and mother's
        if(!empty($father) && !empty($mother))
        {
            if($father['user_phone'] === $mother['user_phone'])
            {
                $result['equal_phone'] = $father['user_phone'];
            }
            
            if(($father['user_email'] !== '') && ($father['user_email'] === $mother['user_email']))
            {
                $result['equal_email'] = $father['user_email'];
            }
        }
        
        return $result;
    }
    
    /**
    * upload photo images
    * 
    * @param array $data
    */
    private function _upload_photos($data)
    {
        $result = array();
        $father = $data['father'];
        $mother = $data['mother'];
        $childs = $data['children'];
        
        if(!empty($father))
        {
            $photo_name = '';
            if($father['user_photo'] !== '')
            {
                $file = $father['user_photo'];
                if($this->upload_photo($file, USER_TYPE_PARENT)) $photo_name = $file;
            }
            
            $result['p'.$father['user_id']] = $photo_name;
        }
        
        if(!empty($mother))
        {
            $photo_name = '';
            if($mother['user_photo'] !== '')
            {
                $file = $mother['user_photo'];
                if($this->upload_photo($file, USER_TYPE_PARENT)) $photo_name = $file;
            }
            
            $result['p'.$mother['user_id']] = $photo_name;
        }
        
        foreach($childs as $child)
        {
            $photo_name = '';
            if($child['user_photo'] !== '')
            {
                $file = $child['user_photo'];
                if($this->upload_photo($file, USER_TYPE_CHILDREN)) $photo_name = $file;
            }
            
            $result['c'.$child['user_id']] = $photo_name;
        }
        
        return $result;
    }
    
    /**
    * move photo images
    * 
    * @param array $photos
    */
    private function _move_photos($photos)
    {
        if(empty($photos)) return;

        foreach($photos as $code=>$photo)
        {
            if($photo == '') continue;
            
            $first_char = substr($code, 0, 1);
            if($first_char == 'p')
            {
                $this->move_photo($photo, USER_TYPE_PARENT);
            }
            else
            {
                $this->move_photo($photo, USER_TYPE_CHILDREN);
            }
        }
        
        return;
    }
    
    /**
    * insert parent and children data to proper tables
    * 
    * @param array $data
    */
    private function _insert_data($data)
    {
        $father = $data['father'];
        $mother = $data['mother'];
        $childs = $data['children'];
        $photos = $data['photos'];
        
        foreach($childs as $child)
        {
            $children_id = $child['user_id'];
            $school_uid  = $child['school_uid'];
            if(($children_uid = $this->children_model->get_uid_by_userid($children_id)) === FALSE)
            {
                $expire_duration = get_test_duration();
                
                $cinfo = array();
                $cinfo['user_id']     = $child['user_id'];
                $cinfo['user_name']   = $child['user_name'];
                $cinfo['user_sex']    = $child['user_sex'];
                $cinfo['user_phone']  = $child['user_phone'];
                $cinfo['user_email']  = $child['user_email'];
                $cinfo['user_passwd'] = $child['user_passwd'];
                $cinfo['user_birthday'] = $child['user_birthday'];
                $cinfo['user_photo']  = $photos['c' . $child['user_id']];
                $cinfo['expire_date'] = date(FORMAT_DATE, strtotime("{$expire_duration} days"));
                $cinfo['class_uid']   = $child['children_class'];                
                $children_uid = $this->children_model->insert($cinfo);
            }  
            
            if(!empty($father))
            {
                $father['user_photo'] = $photos['p' . $father['user_id']];    
                $this->_insert_parent_info($father, PARENT_FATHER, $school_uid, $children_uid);
            } 
            
            if(!empty($mother))
            {
                $mother['user_photo'] = $photos['p' . $mother['user_id']];    
                $this->_insert_parent_info($mother, PARENT_MOTHER, $school_uid, $children_uid);
            } 
        }
                
        return TRUE;    
    }
    
    /**
    * insert parent info
    * 
    * @param array  $data
    * @param int    $type
    * @param int    $school_uid
    * @param int    $child_uid
    */
    private function _insert_parent_info($data, $type, $school_uid, $child_uid)
    {
        $pinfo = array();
        $pinfo['user_id']     = $data['user_id'];
        $pinfo['user_name']   = $data['user_name'];
        $pinfo['user_sex']    = $type;
        $pinfo['user_phone']  = $data['user_phone'];
        $pinfo['user_email']  = $data['user_email'];
        $pinfo['user_passwd'] = $data['user_passwd'];
        $pinfo['user_birthday'] = $data['user_birthday'];
        $pinfo['user_photo']  = $data['user_photo'];    
        $pinfo['parent_type'] = $type; //this will be replaced in the future; ex:father, mother, sister and brother and etc
        
        $puid = FALSE;
        $parent = $this->parent_model->find_by_userid($data['user_id']);
        if(empty($parent))
        {
            $puid = $this->parent_model->insert($pinfo);
        }
        else
        {
            $puid = $parent->parent_uuid;            
        }
        $this->parent_model->insert_parent_children_router($puid, $child_uid);
    }
    
    /**
    * prepare return result after login
    * 
    * @param int    $login_uid
    * @param array  $parent : parent row in any school, because we think, all rows in our system of parent is same
    * @param string $device_type
    */
    private function _connection_data($login_uid, $parent, $device_type)
    {
        $result    = array();             
        $login_key = $this->log_login_model->get_connection_key($login_uid);
        $recent_date = $this->parent_model->update_recent_date($parent->parent_uuid);
        
        $client = array();
        $client['connection_uid'] = $login_uid;
        $client['connection_key'] = $login_key;
        $client['device_type']    = $device_type;
        
        $client['user_id']      = $parent->user_id;
        $client['user_name']    = $parent->user_name;
        $client['user_type']    = USER_TYPE_PARENT;
        $client['user_photo']   = get_user_photo_url($parent->user_photo, USER_TYPE_PARENT);   
         
        //get all childrens
        $my_children_uids = $this->parent_model
            ->get_parent_children_uids($parent->parent_uuid);
        $schools = array();
        if(!empty($my_children_uids))
        {
            foreach($my_children_uids as $children_uid)
            {
                $photo_field = $this->user_photo_field(USER_TYPE_CHILDREN);
                $child = $this->children_model
                    ->select("*, {$photo_field}", FALSE)
                    ->with_class()
                    ->find($children_uid);
                if(empty($child)) continue;
                                                             
                $school = $this->school_model->find($child->school_uid);
                if(empty($school)) continue;
                                                             
                if(!key_exists($school->school_uuid, $schools))
                {
                    $schools[$school->school_uuid] = array();
                    $schools[$school->school_uuid]['school_uid']  = $school->school_uuid;
                    $schools[$school->school_uuid]['school_name_cn'] = $school->school_name_cn;
                    $schools[$school->school_uuid]['school_name_ko'] = $school->school_name_ko;
                    $schools[$school->school_uuid]['school_name_en'] = $school->school_name_en;
                    $schools[$school->school_uuid]['school_status'] = $school->active_status;
                    $schools[$school->school_uuid]['childrens'] = array();
                }
                
                $item = array();
                $item['children_id']    = $child->user_id;
                $item['children_name']  = $child->user_name;
                $item['children_sex']   = $child->user_sex;
                $item['children_photo'] = $child->user_photo_url;
                $item['children_status']= $child->user_status;
                
                $item['class_uid']  = $child->class_uid;
                $item['grade_num']  = $child->grade_num;
                $item['class_num']  = $child->class_num;
                $item['class_status']= $child->active_status;
                
                //check if children's expire date
                $expire_time = strtotime($child->expire_date);
                $pay_info = $this->last_payment_of_children($child->user_id);
    
                if($expire_time <= time())
                {
                    $item['expire_status']  = CHILDREN_SERVICE_EXPIRED;
                    $item['expire_date']    = $child->expire_date;
                    $item['payment_details']= $pay_info;
                }
                else
                {
                    $item['expire_status']  = CHILDREN_SERVICE_AVAILABLE;
                    $item['expire_date']    = $child->expire_date;
                    $item['payment_details']= $pay_info;
                }

                $schools[$school->school_uuid]['childrens'][] = $item;                   
                $schools[$school->school_uuid]['number_of_users'] = $this->get_number_of_users($school->school_uuid);;                   
            }    
        }
        
        $result['login_status']     = STATUS_SUCCESS;
        $result['recent_visit_date']= $recent_date;
        $result['client']  = $client;
        $result['schools'] = array_values($schools);
        return $result;
    }
}//end class