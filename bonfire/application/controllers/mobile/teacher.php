<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

class Teacher extends Mobile_Controller
{
    public function __construct()
    {
        $this->controller = 'teacher';
        
        parent::__construct();
        
        if($this->params['client']['user_type'] != USER_TYPE_TEACHER 
            && $this->params['client']['user_type'] != USER_TYPE_MASTER)
        {
            $this->return_error(ERR_USER_TYPE_NOT_MATCH);        
        }
    }
    //--------------------------------------------------------------------
    
    /**
    * teacher register action
    * 
    */
    public function register()
	{
	    //check all fields are setted exactly
        if(!$this->check_user_fields_in($this->params['data'], USER_TYPE_TEACHER))
        {
            $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        /////////////////////////////////////////
        $result = array();
        $client = $this->params['client'];
        $data   = get_clean_fields_in($this->params['data']);
        /////////////////////////////////////////
        
        //validate fields
        if(($return = $this->validate_user_fields_in($data)) !== NO_ERROR)
        {
            $result['validation_fail'] = STATUS_YES;
            $result['validation_reason']= $return;
            $this->return_result($result);
        }
        
        //check user_id is alredy exist in gus_teachers
        $user_id  = $data['user_id'];
        $user_row = $this->teacher_model->find_by_userid($user_id);        
        if(isset($user_row->teacher_uuid))
        {
            $result['exist_user'] = $user_id;
            $this->return_result($result);
        }
        
        //check email and phone is already exist in gus_teachers
        $email_count = $this->teacher_model
            ->filter_email($data['user_email'])
            ->filter_out_user_id($user_id)
            ->count_all();
        if($email_count !== 0)
        {
            $result['exist_email'] = $data['user_email'];
            $this->return_result($result);
        }
        
        $phone_count = $this->teacher_model
            ->filter_phone($data['user_phone'])
            ->filter_out_user_id($user_id)
            ->count_all();
        if($phone_count !== 0)
        {
            $result['exist_phone'] = $data['user_phone'];
            $this->return_result($result);
        }
        
        //detect mass request from same ip_address and device
        $this->load->model('buffer_model');
        $this->buffer_model->remove_expires();
        
        $ip_address  = $this->input->ip_address();
        $device_info = $client['device_info'];
        $device_type = $client['device_type'];
        $user_type   = $client['user_type'];
        $requestes = $this->buffer_model
            ->filter_device_info($device_info)
            ->filter_ip($ip_address)
            ->filter_user_type($user_type)
            ->count_all();
        if($requestes >= BUFFER_LIMIT_REQUEST_NUMS)
        {
            $result['limit_request'] = STATUS_YES;
            $result['refresh_cycle'] = BUFFER_EXPIRE_TIME; //mins
            $this->return_result($result);
        }
        
        //upload photo to tmp
        $photo_file = '';
        if($data['user_photo'] !== '')
        {
            $file = $data['user_photo'];
            if(!$this->upload_photo($file, USER_TYPE_TEACHER))
            {
                $this->return_error(ERR_FILE_UPLOAD_FAIL);
            }
            $photo_file = $file;
        }
        $data['user_photo'] = $photo_file;
        
        //generate sms and send it to phones and emails
        $sms_uid = $this->send_verify_code($data['user_phone'], $data['user_email']);
        $content = ct_json_encode($data);
        $buffer_key = md5($content);
        
        //insert data to buffer table tempereraly
        $info = array();
        $info['key']     = $buffer_key;
        $info['content'] = $content;
        $info['sms_uid'] = $sms_uid;
        $info['device_info'] = $device_info;
        $info['device_type'] = $device_type;
        $info['user_type']   = $user_type;
        $info['ip_address']  = $ip_address;
        $buffer_uid = $this->buffer_model->insert($info);
        if($buffer_uid === FALSE)
        {
            $this->return_error(ERR_INSERT_TO_DATABASE);
        }
        
        //return result
        $result['buffer_uid'] = $buffer_uid;
        $result['buffer_key'] = $buffer_key;
        $this->return_result($result);
	}
    //--------------------------------------------------------------------
    
    public function register_complete()
    {
        $result = array();
        $data   = $this->params['data'];
        $fields = array('buffer_uid', 'buffer_key', 'verify_code');     
        if(!array_keys_exists($fields, $data))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        //load buffer model
        $this->load->model('buffer_model');
        $this->buffer_model->remove_expires();
        
        //get data from buffer
        $buffer_uid = $data['buffer_uid'];
        $buffer_key = $data['buffer_key'];
        $buffer_row = $this->buffer_model->with_sms()->find_buffer($buffer_uid, $buffer_key);
        if(empty($buffer_row))
        {
            $this->return_error(BUFFER_NO_EXIST);
        }        
        
        //check all fields are setted exactly again
        $content = ct_json_decode($buffer_row->content);
        if(!$this->check_user_fields_in($content, USER_TYPE_TEACHER))
        {
            $this->return_error(BUFFER_NOT_MATCH);
        }
        
        //check buffer is expired
        $buffer_time = strtotime($buffer_row->reg_date);        
        if(time() - $buffer_time > BUFFER_EXPIRE_TIME * 60)
        {
            $result['verify_status'] = VERIFY_EXPIRED;
            $this->return_result($result);
        } 
        
        //check verify code with db
        if($buffer_row->verify_code !== $data['verify_code'])
        {
            $result['verify_status'] = VERIFY_FAIL;
            $this->return_result($result);
        }

        $user_id    = $content['user_id'];
        $user_photo = $content['user_photo']; 
        $user_row   = $this->teacher_model->find_by_userid($user_id);        
        
        //check user_id is alredy exist in user_router as a teacher or master again
        if(isset($user_row->teacher_uuid))
        {
            $result['exist_user'] = $user_id;
            $this->return_result($result);
        }
        
        //check email and phone is already exist in gus_teachers
        $email_count = $this->teacher_model
            ->filter_email($content['user_email'])
            ->filter_out_user_id($user_id)
            ->count_all();
        if($email_count !== 0)
        {
            $result['exist_email'] = $content['user_email'];
            $this->return_result($result);
        }
        
        $phone_count = $this->teacher_model
            ->filter_phone($content['user_phone'])
            ->filter_out_user_id($user_id)
            ->count_all();
        if($phone_count !== 0)
        {
            $result['exist_phone'] = $content['user_phone'];
            $this->return_result($result);
        }
        
        //really insert to db
        $status = $this->teacher_model->insert_with($content);
        if($status === FALSE)
        {
            $result['register_status'] = STATUS_FAIL;
        }
        else
        {
            //remove buffer data
            $this->buffer_model->delete($buffer_uid);
            
            //if user_photo is exist, then move it from tmp folder to valid folder
            $this->move_photo($user_photo, USER_TYPE_TEACHER);
            
            $result['register_status'] = STATUS_SUCCESS;    
        }
        
        $this->return_result($result);
    }
	//--------------------------------------------------------------------
    
    /**
    * resend sms again to phones and emails stored in buffer row
    *    
    */
    public function regenerate_sms()
    {
        $data = $this->params['data'];
        if(!isset($data['buffer_uid']) || !isset($data['buffer_key']))
        {
            $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        //load buffer
        $this->load->model('buffer_model');
        $this->buffer_model->remove_expires();
        
        //get buffer from db
        $buffer_uid = $data['buffer_uid'];
        $buffer_key = $data['buffer_key'];
        $buffer_row = $this->buffer_model->find_buffer($buffer_uid, $buffer_key);
        if(empty($buffer_row))
        {
            $this->return_error(BUFFER_NO_EXIST);
        }
        
        //check all fields are setted exactly again
        $content = ct_json_decode($buffer_row->content);
        if(!$this->check_user_fields_in($content, USER_TYPE_TEACHER))
        {
            $this->return_error(BUFFER_NOT_MATCH);
        }
        
        //TODO
        //check regenerated counts here!!!
        
        $phones = $content['user_phone'];
        $emails = $content['user_email'];
        $verify_uid = $this->send_verify_code($phones, $emails);
        
        //update sms_uid on buffer row
        $update_data = array('sms_uid' => $verify_uid);
        $this->buffer_model->update($buffer_uid, $update_data);
        
        $result['buffer_uid'] = $buffer_uid;
        $result['buffer_key'] = $buffer_key;
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * retrieve password if you forgot it
    *     
    */
    public function forgot()
    {   
        //check if all fields are exist
        $data = $this->params['data']; 
        $fields = array('user_id', 'via_email', 'via_phone');     
        if(!array_keys_exists($fields, $data))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        $result    = array();
        $user_id   = $data['user_id'];
        $via_phone = $data['via_phone'];
        $via_email = $data['via_email'];
        if($via_email == NONE && $via_phone == NONE)
        {
            return $this->return_error(FORGOT_NO_CONTACT_PATH); //no retrieve path
        }  
        
        //get user row
        $user = $this->teacher_model->find_by_userid($user_id);
        if(empty($user))
        {
            $result['user_exist'] = NO_REGISTERED;
            $this->return_result($result);        
        }
                    
        //check if this user has already requested
        $path = SECURITY_PATH_BOTH;
        if($via_phone == SELECTED && $via_email == NONE)
        {
            $path = SECURITY_PATH_PHONE;
        }
        else if($via_email == SELECTED && $via_phone == NONE)
        {
            $path = SECURITY_PATH_EMAIL;
        }
        
        //load model
        $this->load->model('amnesiac_model');
        $last_request = $this->amnesiac_model->find_row($user_id, USER_TYPE_TEACHER, $path);  
        if(!empty($last_request) && isset($last_request->reg_date))
        {           
            $result['has_requested']= STATUS_YES;
            $result['expire_time']  = date(FORMAT_DATETIME, strtotime($last_request->reg_date) + 24 * 60 * 60);
            $this->return_result($result);
        }
        
        //check empty string for phone and email
        if(trim($user->user_email) == '' && trim($user->user_phone) == '')
        {
            $result['phone_email_exist']= NONE;
            $this->return_result($result);
        }
        if($via_phone == SELECTED && $via_email == NONE && trim($user->user_phone) == '')
        {
            $result['phone_exist']= NONE;
            $this->return_result($result);
        }
        if($via_email == SELECTED && $via_phone == NONE && trim($user->user_email) == '')
        {
            $result['email_exist']= NONE;
            $this->return_result($result);
        }
        
        //register this to fogotten_model
        $fdata = array();
        $fdata['user_id']   = $user_id;
        $fdata['user_name'] = $user->user_name;
        $fdata['user_type'] = $user->teacher_type;
        $fdata['path']      = $path;
        $fdata['device_type'] = $this->params['client']['device_type'];
        $fdata['device_info'] = $this->params['client']['device_info'];
        $status = $this->amnesiac_model->insert($fdata);
        if($status === FALSE)
        {
            $this->return_error(ERR_INSERT_TO_DATABASE);
        }
        
        //send info, put process in background
        if(SEND_SMS === TRUE)
        {
            $language_name = get_long_language_name($this->short_language);
            $command = sprintf(COMMAND_SEND_SECURITY, $language_name, USER_TYPE_TEACHER, $user_id, $via_phone, $via_email);   
            $process = new Process;
            $pcs_id  = $process->runInBackground($command);
        }
        
        //return result
        $result['finding_status'] = STATUS_SUCCESS;
        $this->return_result($result);    
    }
    //--------------------------------------------------------------------
    
    /**
    * teacher login
    * 
    */
    public function login()
    {
        $result = array();
        $client = $this->params['client'];
        $data   = $this->params['data'];
        
        //check valid login data
        $fields = array('user_id', 'user_passwd');
        if(!array_keys_exists($fields, $data)) 
        {
            $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        if(!valid_id_number($data['user_id']) && !valid_passport_number($data['user_id']))
        {
            $this->return_error(INVALID_USER_ID);
        }
        
        if(!valid_password($data['user_passwd']))
        {      
            $this->return_error(INVALID_USER_PASSWORD);
        }
        
        //check if user is registered on our system
        //if registered get school ids with user_id and user_type
        $user_id     = $data['user_id'];
        $user_passwd = $data['user_passwd'];
        $ip_address  = $this->input->ip_address();
        $teacher     = $this->teacher_model->find_by_userid($user_id);
        if(empty($teacher))
        {
            $result['user_exist'] = NO_REGISTERED;
            $this->return_result($result);        
        }
        
        //check login attempt, if attempt is greater than specified count, 
        //return fail
        $attempts = $this->log_login_model->count_login_attempts($user_id, $teacher->teacher_type, $ip_address, LOGIN_ATTEMPT_CYCLE); 
        if($attempts >= LOGIN_ATTEMPT_LIMIT_NUMS)
        {   
            $result['limit_attempt'] = STATUS_YES;
            $result['refresh_cycle'] = LOGIN_ATTEMPT_CYCLE; //mins
            $this->return_result($result);        
        }
        
        //if status is active, then check password
        if(do_hash($teacher->salt . $user_passwd) !== $teacher->password_hash)
        {
            $this->log_login_model->increase_login_attempts($user_id, $teacher->teacher_type, $ip_address);
            
            $result['verify_passwd'] = VERIFY_FAIL;
            $this->return_result($result);    
        }
        
        //check the school is active status
        $school = $this->school_model->find($teacher->school_uid); 
        if(empty($school)) $this->return_error(LOGIN_INVALID_SCHOOL);
        
        if((int)$school->active_status === STATUS_INACTIVE)
        {
            $this->log_login_model->increase_login_attempts($user_id, $teacher->teacher_type, $ip_address);
            
            $school_name_field = 'school_name_' . $this->short_language;
            $result['school_uid']    = $school->school_uuid;
            $result['school_name']   = $school->$school_name_field;
            $result['school_status'] = STATUS_INACTIVE;
            $this->return_result($result);
        }
        
        //if school is active, then go to teacher table of that school
        //check user_status is active
        if((int)$teacher->user_status === STATUS_INACTIVE)
        {
            $this->log_login_model->increase_login_attempts($user_id, $teacher->teacher_type, $ip_address);
            
            $result['user_status'] = STATUS_INACTIVE;
            $this->return_result($result);
        }
        
        //check user is already logged in on another device
        //then force logout old user and make new one logged in
        $login_uid = FALSE;
        $device_info = $client['device_info'];
        $device_type = $client['device_type'];
        $utype  = array(USER_TYPE_TEACHER, USER_TYPE_MASTER);
        
        $login_info = array();
        $login_info['user_id']   = $user_id;
        $login_info['user_name'] = $teacher->user_name;
        $login_info['user_type'] = $teacher->teacher_type;
        $login_info['device_type'] = $device_type;
        $login_info['device_info'] = $device_info;
        
        $exist_login = $this->log_login_model
            ->filter_userid($user_id)
            ->filter_user_type($utype)
            ->filter_device_type($device_type)
            ->filter_device_info($device_info)
            ->filter_status(STATUS_ACTIVE)
            ->find_row();
            
        if(!empty($exist_login))
        {                   
            //log out other connections                
            $this->log_login_model->log_out_by_userid($user_id, $utype, $exist_login->uid);
                               
            $login_uid = $exist_login->uid; 
        }
        else
        {
            //log out other connections                
            $this->log_login_model->log_out_by_userid($user_id, $utype);
            
            $login_uid = $this->log_login_model->insert($login_info);
        }
        
        if($login_uid === FALSE)
        {
            $this->return_error(LOGIN_NO_LOGIN_UID);
        }
        
        //clear login attempts history
        $this->log_login_model->clear_login_attempts($user_id, $teacher->teacher_type, $ip_address);
        
        //if everything is ok, then login client to our system
        //make return result
        $result = $this->_connection_data($login_uid, $school, $device_type, $teacher);
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * user logout
    * 
    */
    public function logout()
    {
        $this->restrict(FALSE);
        
        $client = $this->params['client'];
        $connect_uid = $client['connection_uid'];
        
        //set recent_visite_date by current time
        $this->teacher_model->update_recent_date($this->user->teacher_uuid);       
                                                                                                  
        //delete from login_monitor
        $this->log_login_model->log_out($connect_uid);
        
        //return result
        $result = array();
        $result['logout_status'] = STATUS_SUCCESS;
        
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * retrieve all available receiver lists
    * 
    */
    public function receiver_lists()
    {
        $this->restrict();
        
        $client    = $this->params['client'];
        $user_type = $client['user_type'];        
        if($user_type == USER_TYPE_TEACHER)
        {
            $result = $this->_teacher_receiver_lists();
        }
        else
        {
            $result = $this->_master_receiver_lists();
        }
        
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * retrieve all teacher's profile info
    * 
    */
    public function profile()
    {
        $this->restrict();
        
        $client = $this->params['client'];
        $user   = $this->user;
        $result = array();
        
        //get school info
        $school = $this->school_model
            ->select_name($this->short_language)
            ->find($user->school_uid);
        
        //personal
        $personal = array();
        $personal['user_name']  = $user->user_name;
        $personal['user_sex']   = $user->user_sex;
        $personal['user_email'] = $user->user_email;
        $personal['user_phone'] = $user->user_phone;
        $personal['user_photo'] = $user->user_photo_url;
        $personal['user_birthday'] = $user->user_birthday;
        $personal['user_address']  = $user->user_address;
        $personal['recent_visit_date'] = $user->recent_visit_date;
        
        $personal['school_uid']  = $user->school_uid;
        $personal['school_name'] = $school->school_name;
         
        if($this->user->teacher_type == USER_TYPE_TEACHER)
        {        
            //get teacher's info
            $charge_class_uids  = $this->teacher_model->get_class_uids($user->teacher_uuid, ROLE_CHARGE);
            $charge_class       = array();
            $subject_uid        = $this->teacher_model->get_charge_subject($user->teacher_uuid);
            $subject_class_uids = $this->teacher_model->get_class_uids($user->teacher_uuid, ROLE_SUBJECT);
            $subject_class      = array();
            if(!empty($subject_class_uids))
            {
                foreach($subject_class_uids as $class_uid)
                {
                    $subject_class[] = $class_uid; 
                }
            }
            
            if(!empty($charge_class_uids))
            {
                foreach($charge_class_uids as $class_uid)
                {
                    $charge_class[] = $class_uid;
                }
            }
            
            $personal['is_charge']   = empty($charge_class) ? NONE : SELECTED;
            $personal['charge_class']= implode(',', $charge_class);
            $personal['is_subject']  = empty($subject_class) ? NONE : SELECTED;
            $personal['subject_uid'] = $subject_uid===FALSE? '' : $subject_uid;
            $personal['subject_class'] = implode(',', $subject_class);
            
            $classes = $this->school_model
                ->select('class_uuid class_uid, grade_num, class_num, year_level')
                ->filter_live_grades()
                ->filter_live_classes()
                ->get_classes($user->school_uid);
            if(empty($classes)) 
            {
                //log error
                log_error(
                    ERR_NO_GRADE_CLASS,
                    USER_TYPE_TEACHER,
                    'profile',
                    $this->params['client']['device_type'],
                    $this->params
                );
                
                $this->return_error(ERR_NO_GRADE_CLASS);
            }
            
            $subjects = $this->school_model
                ->select('subject_uuid subject_uid, subject_name')
                ->get_subjects($user->school_uid);
            if(empty($classes)) 
            {
                //log error
                log_error(
                    ERR_NO_SUBJECTS,
                    USER_TYPE_TEACHER,
                    'profile',
                    $this->params['client']['device_type'],
                    $this->params
                );
                
                $this->return_error(ERR_NO_SUBJECTS);
            }
            
            $result['personal'] = $personal;
            $result['classes']  = $classes;
            $result['subject']  = $subjects;    
        }
        else
        {
            $result['personal'] = $personal;
        }

        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * update general info of profile
    * 
    */
    public function update_profile_general()
    {
        $this->restrict();
        
        //check if all fields are exist
        $data = get_clean_fields_in($this->params['data']);
        $fields = array('user_name', 'user_sex', 'user_birthday', 'user_address', 'user_email', 'user_phone');     
        if(!array_keys_exists($fields, $data))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        //validate fields
        if(($return = $this->validate_profile_general($data)) !== NO_ERROR)
        {
            $result = array();
            $result['validation_fail'] = STATUS_YES;
            $result['validation_reason']= $return;
            $this->return_result($result);
        }
        
        //check email and phone is already exist in gus_teachers
        $email_count = $this->teacher_model
            ->filter_email($data['user_email'])
            ->filter_out_user_id($this->user->user_id)
            ->count_all();
        if($email_count !== 0)
        {
            $result['exist_email'] = $data['user_email'];
            $this->return_result($result);
        }
        
        $phone_count = $this->teacher_model
            ->filter_phone($data['user_phone'])
            ->filter_out_user_id($this->user->user_id)
            ->count_all();
        if($phone_count !== 0)
        {
            $result['exist_phone'] = $data['user_phone'];
            $this->return_result($result);
        }
        
        //update row
        $update = array();
        $update['user_name']    = $data['user_name'];
        $update['user_sex']     = $data['user_sex']==MAN?MAN:WOMAN;
        $update['user_address'] = $data['user_address'];
        $update['user_email']   = $data['user_email'];
        $update['user_phone']   = $data['user_phone'];
        $update['user_birthday']= $data['user_birthday'];
        $status = $this->teacher_model->update($this->user->teacher_uuid, $update);
        
        //return result
        $result = array();
        $result['update_status'] = $status !== TRUE ? STATUS_FAIL : STATUS_SUCCESS;
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * update general info of profile
    * 
    */
    public function update_profile_photo()
    {
        $this->restrict();
        
        //check if all fields are exist
        $data = $this->params['data'];
        if(!isset($data['user_photo']))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        //validate and upload photo
        $user_photo = '';
        if($data['user_photo'] != '')
        {
            //validate fields
            $file = $data['user_photo']; 
            if(($return = $this->validate_profile_photo($file)) !== NO_ERROR)
            {
                $result = array();
                $result['validation_fail'] = STATUS_YES;
                $result['validation_reason']= $return;
                $this->return_result($result);
            }
            
            //upload photo
            if(!$this->upload_photo($file, USER_TYPE_TEACHER))
            {
                $this->return_error(ERR_FILE_UPLOAD_FAIL);
            }
            
            //move photo
            if(!$this->move_photo($file, USER_TYPE_TEACHER))
            {
                $this->return_error(ERR_FILE_UPLOAD_FAIL);
            } 
             
            //delete old photo before new
            delete_user_photo($this->user->user_photo, USER_TYPE_TEACHER);
            
            $user_photo = $file;
        }
        
        //update row
        $update = array();
        $update['user_photo'] = $user_photo;
        $status = $this->teacher_model->update($this->user->teacher_uuid, $update);
        
        //return result
        $result = array();
        $result['update_status'] = $status !== TRUE ? STATUS_FAIL : STATUS_SUCCESS;
        $result['user_photo_url']= get_user_photo_url($user_photo, USER_TYPE_TEACHER);
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * update password of profile
    * 
    */
    public function update_profile_password()
    {
        $this->restrict();   
        
        //check if all fields are exist
        $data = $this->params['data'];
        $fields = array('old_password', 'new_password');     
        if(!array_keys_exists($fields, $data))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        //validate password
        if(!valid_password($data['new_password']))
        {      
            return $this->return_error(INVALID_USER_PASSWORD);
        }
        
        //check old password
        $old_password = $data['old_password'];
        $new_password = $data['new_password'];
        $user = $this->user;
        
        if(do_hash($user->salt . $old_password) !== $user->password_hash)
        {
            $result = array();
            $result['verify_old_password'] = VERIFY_FAIL;
            $this->return_result($result);    
        }
        
        //update row
        $update = array();
        $update['user_passwd'] = $new_password;
        $status = $this->teacher_model->update($this->user->teacher_uuid, $update);
                              
        //return result
        $result = array();
        $result['update_status'] = $status !== TRUE ? STATUS_FAIL : STATUS_SUCCESS;
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * update class and subject info of profile
    * 
    */
    public function update_subject()
    {
        $this->restrict();   
        
        //check if all fields are exist
        $data = $this->params['data'];
        $fields = array('subject_uid');
        if(!array_keys_exists($fields, $data))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        $result = array();
        $new_subject_uid = (int)$data['subject_uid'];     
        
        //check if subject is valid for this school
        if($new_subject_uid !== 0 && !$this->school_model->valid_subjects($this->user->school_uid, $new_subject_uid))
        {
            return $this->return_error(INVALID_SCHOOL_SUBJECT);
        }
        
        //check if new subject is really different with old one
        $current_subject_uid = (int)$this->teacher_model->get_charge_subject($this->user->teacher_uuid);
        if($current_subject_uid === $new_subject_uid)
        {
            $result['update_status'] = NO_CHANGE;
            $this->return_result($result);
        }
            
        $status = TRUE;
        if($new_subject_uid === 0)
        {
            //if user unselect subject, then remove their subject class links
            $status = $this->teacher_model->delete_class_links($this->user->teacher_uuid, ROLE_SUBJECT);
        }
            
        if($status === TRUE)
        {        
            //remove old subject links
            $status = $this->teacher_model->delete_subject_link($this->user->teacher_uuid);
        } 
        
        if($status === TRUE)
        {
            //add new subject links
            $status = $this->teacher_model->add_subject_link($this->user->teacher_uuid, $new_subject_uid); 
        }                  
        
        if($status === TRUE)
        {
            $result['update_status'] = STATUS_SUCCESS;
        }
        else
        {
            $result['update_status'] = STATUS_FAIL;
        }
        
        //return result
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * update class and subject info of profile
    * 
    */
    public function update_class()
    {
        $this->restrict();   
        
        //check if all fields are exist
        $data = $this->params['data'];
        $fields = array('role_type', 'class');
        if(!array_keys_exists($fields, $data))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        ///////////////////////////////////////////
        $result = array();
        $role_type  = (int)$data['role_type']; 
        $new_classes= explode(',', $data['class']);
        ///////////////////////////////////////////
        
        //check role is valid
        if(!in_array($role_type, array(ROLE_CHARGE, ROLE_SUBJECT)))
        {
            return $this->return_error(TEACHER_INVALID_ROLE);
        }
          
        //check classes are valid
        if(!$this->school_model->valid_classes($this->user->school_uid, $new_classes))
        {
            return $this->return_error(INVALID_SCHOOL_CLASS);
        }
        
        //check new classes are different with old classes
        $current_classes = $this->teacher_model->get_class_uids($this->user->teacher_uuid, $role_type);
        if(array_equal($current_classes, $new_classes))
        {
            $result['update_status'] = NO_CHANGE;
            $this->return_result($result);
        }
        
        $status = TRUE; 
        if(empty($new_classes))
        {
            //if classes is none, then remove current classes by role
            $status = $this->teacher_model->delete_class_links($this->user->teacher_uuid, $role_type);
        }
        
        if($status === TRUE)
        {
            //otherwise, check differences and add/remove classes
            $must_delete_links = array_diff($current_classes, $new_classes);
            $must_add_links    = array_diff($new_classes, $current_classes);
            
            $status1 = $status2 = TRUE;
            if(!empty($must_delete_links))
            {
                $status1 = $this->teacher_model->delete_class_links($this->user->teacher_uuid, $role_type, $must_delete_links);
            }
            
            if(!empty($must_add_links))
            {
                $status2 = $this->teacher_model->add_class_link($this->user->teacher_uuid, $role_type, $must_add_links);
            }
            
            if($status1 === FALSE || $status2 === FALSE)
            {
                $status = FALSE;
            }
        }    
        
        if($status === TRUE)
        {
            //brefore return result, make user to logout
            $connection_uid = $this->params['client']['connection_uid'];
            $this->log_login_model->log_out($connection_uid, LOGIN_STATUS_OUT);            
            
            $result['update_status'] = STATUS_SUCCESS;
        }
        else
        {
            $result['update_status'] = STATUS_FAIL;
        }
        
        //return result
        $this->return_result($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * change school
    * 
    */
    public function update_school()
    {
        $this->restrict();   
        
        //check if all fields are exist
        $data = $this->params['data'];
        $fields = array('school_uid');
        if(!array_keys_exists($fields, $data))
        {
            return $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        $result = array();
        $new_school_uid = (int)$data['school_uid']; 
        //check new school is valid
        if(!$this->school_model->valid_school($new_school_uid))
        {
            return $this->return_error(INVALID_SCHOOL);
        }
        
        //check if new school_uid is same with old
        if($new_school_uid === (int)$this->user->school_uid)
        {
            $result['update_status'] = NO_CHANGE;
            $this->return_result($result);
        }
        
        //update row
        $update = array();
        $update['school_uid'] = $new_school_uid;
        $update['school_date']= date(FORMAT_DATETIME);
        $status = $this->teacher_model->update($this->user->teacher_uuid, $update);
        if($status === TRUE)
        {
            //brefore return result, make user to logout
            $connection_uid = $this->params['client']['connection_uid'];
            $this->log_login_model->log_out($connection_uid, LOGIN_STATUS_OUT);            
            
            $result['update_status'] = STATUS_SUCCESS;
        }
        else
        {
            $result['update_status'] = STATUS_FAIL;
        }
        
        
        //return result
        $this->return_result($result);
    }
    //-------------------------------------------------------------------- 

    /////////////////////////////////////////////////////////////////////
    //PROTECTED METHODS
    ///////////////////////////////////////////////////////////////////// 
    
    /**
    * check login
    * 
    */
    protected function restrict($check_login_status=TRUE)
    {
        parent::restrict($check_login_status);
        
        //check teacher_type in teacher table and user_type in requested client part
        $user   = $this->user;
        $client = $this->params['client'];
        if((int)$user->teacher_type !== (int)$client['user_type'])
        {
            $this->return_error(FORCE_CHANGED_TYPE);
        }
    }
    //--------------------------------------------------------------------
    
    /**
    * validate requested parameters
    * 
    * @param array $data
    * @return int
    */
    protected function validate_user_fields_in($data)
    {
        if(($return = parent::validate_user_fields_in($data)) !== NO_ERROR)
        {
            return $return;
        }
        
        //user_email, email is required for only teacher
        if($data['user_email'] === '')
        {
            return INVALID_USER_EMAIL;
        }
        
        //additional fields for teacher
        //check if school is valid
        if(!$this->school_model->valid_school($data['school_uid']))
        {
            return INVALID_SCHOOL;
        }
        
        //check carged classes
        if((int)$data['is_charge'] === SELECTED)
        {
            if($data['charge_class'] == '')
            {
                return REGISTER_MISSING_CHARGE;
            }
            
            if(!$this->school_model->valid_classes($data['school_uid'], explode(',', $data['charge_class'])))
            {
                return REGISTER_INVALID_CHARGE;
            }
        }
        
        if((int)$data['is_subject'] === SELECTED)
        {
            //check charged subjects
            if($data['subject_uid'] == '')
            {
                return REGISTER_MISSING_SUBJECT;
            }
            
            if($data['subject_class'] != '' && !$this->school_model->valid_classes($data['school_uid'], explode(',', $data['subject_class'])))
            {
                return REGISTER_INVALID_SUBJECT_CLASS;
            }
        }
        
        if($data['subject_uid'] !== '')
        {
            if(!$this->school_model->valid_subject($data['school_uid'], $data['subject_uid']))
            {
                return REGISTER_INVALID_SUBJECT;
            }   
        }   
          
        return NO_ERROR;
    }
    //--------------------------------------------------------------------
    
    /////////////////////////////////////////////////////////////////////
    //PRIVATE METHODS
    /////////////////////////////////////////////////////////////////////
    
    /**
    * prepare return result after login
    * 
    * @param int    $login_uid
    * @param array  $school
    * @param int    $device_type
    * @param array  $teacher
    * @param int    $user_type
    */
    private function _connection_data($login_uid, $school, $device_type, $teacher)
    {
        $result    = array();
        $login_key = $this->log_login_model->get_connection_key($login_uid);
        $recent_date = $this->teacher_model->update_recent_date($teacher->teacher_uuid);
        
        $client = array();
        $client['connection_uid'] = $login_uid;
        $client['connection_key'] = $login_key;
        $client['device_type']    = $device_type;
        
        $client['user_id']      = $teacher->user_id;
        $client['user_name']    = $teacher->user_name;
        $client['user_type']    = $teacher->teacher_type==USER_TYPE_TEACHER?USER_TYPE_TEACHER:USER_TYPE_MASTER;
        $client['user_photo']   = get_user_photo_url($teacher->user_photo, USER_TYPE_TEACHER);   
        
        $client['school_uid']     = $school->school_uuid;
        $client['school_name_cn'] = $school->school_name_cn;
        $client['school_name_ko'] = $school->school_name_ko;
        $client['school_name_en'] = $school->school_name_en;
        //$client['school_name_jp'] = $school->school_name_jp;
        
        $result['login_status']     = STATUS_SUCCESS;
        $result['recent_visit_date']= $recent_date;
        $result['client']           = $client;
        $result['number_of_users']  = $this->get_number_of_users($school->school_uuid);
        
        return $result;
    }
    //--------------------------------------------------------------------
    
    /**
    * get class-children-parent lists for teacher
    * 
    * @param int $school_uid
    * @param int $teacher_uid
    * @param int $teacher_role
    */
    private function _class_children_parent_data($school_uid, $teacher_uid, $teacher_role)
    {
        $class_uids = $this->teacher_model->get_class_uids($teacher_uid, $teacher_role);        
        $classes = $this->school_model
            ->select('class_uuid class_uid, grade_num, class_num, year_level')
            ->get_some_classes($class_uids);
        
        if(empty($classes))
        {
            return array();
        }
            
        $photo_field1 = $this->user_photo_field(USER_TYPE_CHILDREN, 'user_photo'); 
        $photo_field2 = $this->user_photo_field(USER_TYPE_PARENT, 'user_photo'); 
        foreach($classes as $class)
        {                                         
            $class_children = $this->children_model
                ->select("children_uuid, user_id, user_name, user_sex, {$photo_field1}, user_email, user_phone, " . USER_TYPE_CHILDREN . ' user_type, expire_date', FALSE)
                ->filter_school($school_uid)   
                ->with_class()
                ->filter_class($class->class_uid)
                ->order_by('user_name')
                ->find_all();
                  
            if(empty($class_children))
            {
                $class->children = array();
                continue;
            }
                   
            foreach($class_children as $children)
            {
                //exire_status
                $expire_time = strtotime($children->expire_date);
                $children->expire_status = ($expire_time <= time())?CHILDREN_SERVICE_EXPIRED:CHILDREN_SERVICE_AVAILABLE;
                        
                //parents
                $children_parent_uids = $this->children_model->get_children_parent_uids($children->children_uuid);
                $parents = $this->parent_model
                    ->select("user_id, user_name, user_sex, {$photo_field2}, user_email, user_phone, " . USER_TYPE_PARENT . ' user_type, parent_type', FALSE)
                    ->filter_user_uids($children_parent_uids)
                    ->find_all();
                
                $children->parents = empty($parents) ? array() : $parents;
                
                unset($children->children_uuid);
                unset($children->expire_date);                    
            }
            $class->children = $class_children;
        }

        return $classes;   
    }
    
    /**
    * retreive receiver lists for teacher
    * 
    */
    private function _teacher_receiver_lists()
    {
        $school_uid  = $this->user->school_uid;
        $teacher_uid = $this->user->teacher_uuid;
        
        //get master row
        $photo_field = $this->user_photo_field(USER_TYPE_TEACHER, 'user_photo');
        $master = $this->teacher_model
            ->select("teacher_uuid, user_id, user_name, user_sex, {$photo_field}, user_email, user_phone, ". USER_TYPE_MASTER . ' user_type', FALSE)
            ->find_master($school_uid);
        
        //get teachers
        $excl_uids = array($teacher_uid);
        if(isset($master->teacher_uuid))
        {
            $excl_uids[] = $master->teacher_uuid;
            unset($master->teacher_uuid);
        }
        
        $teachers = $this->teacher_model
            ->select("user_id, user_name, user_sex, {$photo_field}, user_email, user_phone, ". USER_TYPE_TEACHER . ' user_type', FALSE)
            ->filter_school($school_uid)
            ->filter_out_user_uids($excl_uids)
            ->find_all();
             
        //get carge classes
        $charge_class = $this->_class_children_parent_data($school_uid, $teacher_uid, ROLE_CHARGE);
                     
        //get subject classes
        $subject_class = $this->_class_children_parent_data($school_uid, $teacher_uid, ROLE_SUBJECT); 
        
        $result = array();
        $result['master']       = $master;
        $result['teachers']     = empty($teachers) ? array() : $teachers;
        $result['charge_class'] = $charge_class;
        $result['subject_class']= $subject_class;
        
        return $result;
    }
    
    /**
    * retreive receiver lists for master
    * 
    * @param int $school_uid
    * @param int $user_uid
    */
    private function _master_receiver_lists()
    {
        $school_uid  = $this->user->school_uid;
        $teacher_uid = $this->user->teacher_uuid;
        $photo_field1 = $this->user_photo_field(USER_TYPE_TEACHER, 'user_photo');
        $photo_field2 = $this->user_photo_field(USER_TYPE_CHILDREN, 'user_photo');
        $photo_field3 = $this->user_photo_field(USER_TYPE_PARENT, 'user_photo');
        
        //get teachers
        $excl_uids = array($teacher_uid);
        $teachers = $this->teacher_model
            ->filter_school($school_uid)
            ->select("user_id, user_name, user_sex, {$photo_field1}, user_email, user_phone, " . USER_TYPE_TEACHER . ' user_type', FALSE)
            ->filter_out_user_uids($excl_uids)
            ->find_all();
                       
        //get class-teacher lists
        $classes = $this->school_model
            ->select('class_uuid class_uid, grade_num, class_num, year_level')
            ->get_classes($school_uid);
        if(empty($classes))
        {
            //log error
            log_error(
                ERR_NO_GRADE_CLASS,
                $this->user->teacher_type,
                '_master_receiver_lists',
                $this->params['client']['device_type'],
                $this->params
            );
            
            $this->return_error(ERR_NO_GRADE_CLASS);
        }
                     
        foreach($classes as $class)
        {
            //get class teachers
            $class_teachers = $this->teacher_model
                ->filter_school($school_uid)
                ->select("user_id, user_name, user_sex, {$photo_field1}, " . USER_TYPE_TEACHER . ' user_type', FALSE)
                ->filter_out_user_uids($excl_uids)
                ->find_all_by_class($class->class_uid, ROLE_CHARGE);
            $class->teachers = empty($class_teachers)? array() : $class_teachers;
                       
            //get children-parents of class
            $class_children = $this->children_model
                ->select("children_uuid, user_id, user_name, user_sex, {$photo_field2}, user_email, user_phone, " . USER_TYPE_CHILDREN . ' user_type, expire_date', FALSE)
                ->filter_class($class->class_uid)
                ->order_by('user_name')
                ->find_all();
                       
            if(empty($class_children))
            {
                $class->children = array();
                continue;
            }
            
            foreach($class_children as $k => $children)
            {
                //exire_status
                $expire_time = strtotime($children->expire_date);
                $children->expire_status = ($expire_time <= time())?CHILDREN_SERVICE_EXPIRED:CHILDREN_SERVICE_AVAILABLE;
                
                //parents
                $children_parent_uids = $this->children_model->get_children_parent_uids($children->children_uuid);
                $parents = $this->parent_model
                    ->select("user_id, user_name, user_sex, {$photo_field3}, " . USER_TYPE_PARENT . ' user_type, parent_type', FALSE)
                    ->filter_user_uids($children_parent_uids)
                    ->find_all();
                
                $children->parents = empty($parents) ? array() : $parents;
                
                unset($children->children_uuid);
                unset($children->expire_date);
            }
            
            $class->children = $class_children;
        }
        
        $result = array();
        $result['teachers']= $teachers;
        $result['classes'] = $classes;
        
        return $result;
    }
}//end class