<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Message extends Mobile_Controller
{
    private $childrens = FALSE; //for parent;
    private $personal_identity = '';
    
    public function __construct()
    {
        $this->controller = 'message';
        
        parent::__construct();
        
        $this->restrict();
        
        $this->load->helper('file');
        $this->load->model('message_model');
    }
    
    /**
    * send new message
    * 
    */
    public function send()
    {
        $client = $this->params['client'];
        $data   = $this->params['data'];
        $user_id    = $client['user_id'];
        $user_type  = (int)$client['user_type'];
        $school_uid = $this->user->school_uid;
                    
        //check message content and attachment
        $resend_file = FALSE;
        if($this->_check_message($data, $user_type) === FALSE)
        {
            $file_key = $data['attachment'];
            $resend_file = $this->message_model
                ->select_school($school_uid)
                ->get_exist_attachment($file_key);
            if(empty($resend_file))
            {
                $this->return_error(MESSAGE_INVALID_SEND_REQUEST);
            }
        }
        
        $parse_channels = FALSE;
        $participants = FALSE;
        $chat_uid = 0;
        $chat_creator = '';
        if(is_array($data['receivers']))
        {
            $extracted_data= $this->_extract_receivers($data['receivers'], $school_uid, $user_type, $user_id, (int)$data['message_type']); 
            $participants  = $extracted_data['participants'];
            $parse_channels= $extracted_data['parse_channels'];
            if(empty($participants) || empty($parse_channels))
            {
                $this->return_error(MESSAGE_NO_PARTICIPANTS);
            }
            
            //create new chat session
            $chat_creator = $this->personal_identity;
            $chat_uid = $this->message_model
                ->select_school($school_uid)
                ->create_chat_session($data['message_type'], $participants, $this->personal_identity);
        }
        else
        {      
            //can't reply for notification and homework
            $message_type = (int)$data['message_type'];
            if(in_array($message_type, array(MSG_NOTIFICATION, MSG_HOMEWORK)))
            {
                $this->return_error(MESSAGE_INVALID_SEND_REQUEST);
            }       
            
            //get chat_uid from chat_session
            $chat_conditions = $this->_get_chat_conditions($user_type);
            $chat_session = $this->message_model
                ->select_school($school_uid)
                ->get_chat_session($data['session_key'], $chat_conditions);
            if(!isset($chat_session->chat_uuid))
            {
                $this->return_error(MESSAGE_INVALID_CHAT_SESSION);
            }
            
            //extract parse channels
            $parse_channels = $this->_extract_parse_channels_from_chat_session($chat_session, $school_uid); 
            if(empty($parse_channels))
            {
                $this->return_error(MESSAGE_NO_PARTICIPANTS);
            }
            
            $chat_creator = $chat_session->creator;
            $chat_uid = $chat_session->chat_uuid; 
            $this->message_model->update_chat_session_change_time($chat_uid);
        }
         
        //check if chat session is exist
        if($chat_uid === 0)
        {
            $this->return_error(MESSAGE_NO_CHAT_ID);
        }
        
        //insert message info to table
        $message = array();
        $message['chat_uid'] = $chat_uid;
        $message['message_type']    = $data['message_type'];
        $message['message_contents']= $data['message_contents'];
        
        $message['sender_id']   = $user_id;
        $message['sender_name'] = $this->user->user_name;
        $message['sender_type'] = $user_type;
        $message['sender_additional'] = $this->_extract_additional_sender_info($client);
        $message['subscriber'] = $this->personal_identity;
         
        if(ct_strlen($message['message_contents']) === 0)
        {
            $file_uid = FALSE;
            if(empty($resend_file))
            {
                $attachment = $this->_upload_attachments($data, $school_uid);
                if(!empty($attachment))
                {
                    $file_uid = $this->message_model
                        ->select_school($school_uid)
                        ->insert_attachment($attachment);
                }
                else
                {
                    $this->return_error(ERR_FILE_UPLOAD_FAIL);
                }
            }
            else
            {
                $file_uid = $resend_file->file_uuid;
            }
            
            if($file_uid !== FALSE)
            {
                $message['file_uid'] = $file_uid;
            }
        }
        
        $msg_uid = $this->message_model
            ->select_school($school_uid)
            ->insert($message);
        if($msg_uid === FALSE)
        {
            $this->return_error(ERR_INSERT_TO_DATABASE);
        }
        
        //send push notification
        $language_name = get_long_language_name($this->short_language);
        $command = sprintf(COMMAND_PUSH_NOTIFICATION, $language_name, CHANNEL_TYPE_MESSAGE, implode(',', $parse_channels), $this->user->user_name);
        $process = new Process;
        $process->runInBackground($command);
        
        //return result
        $result = $this->message_model
            ->select_school($school_uid)
            ->get_sent_message($msg_uid);
        $result->sender_id   = $user_id;
        $result->sender_name = $this->user->user_name;
        $result->sender_type = $user_type;
        $result->sender_photo= $this->_user_photo($user_id, $user_type);
        $result->sender_additional = ct_json_decode($message['sender_additional']);
        $result->temp_id = $data['temp_id'];
        
        $result->session_key = md5($chat_uid . $chat_creator . $data['message_type'] . 'c');
        if(!empty($participants))
        {
            $result->participants= $this->_parse_chat_participants($participants, $school_uid);
        }
               
        $this->return_result($result);    
    }
    
    /**
    * get chat session lists
    * 
    */
    public function chat_sessions()
    {
        $client = $this->params['client'];
        $data   = $this->params['data'];
        $user_type  = $client['user_type'];
        $school_uid = $this->user->school_uid;
        
        $fields = array('show_all', 'last_time');
        if(!array_keys_exists($fields, $data))
        {
            $this->return_error(ERR_MISSING_PARAMETERS);
        }  
        
        $show_all  = (int)$data['show_all'];
        $last_time = ($data['last_time'] === '') ? date(FORMAT_DATETIME) : $data['last_time'];
        $chat_condition = $this->_get_chat_session_conditions($user_type, $show_all, $last_time);
        $chat_sessions  = $this->message_model
            ->select_school($school_uid)
            ->get_chat_sessions($chat_condition, $this->personal_identity);
        
        $result = array();
        if(!empty($chat_sessions))
        {
            foreach($chat_sessions as $row)
            {
                $temp = explode(':', $row->participants);
                $participants = $this->_parse_chat_participants($temp, $school_uid);
                if(empty($participants)) continue; 
                
                $item = array();
                $item['session_key']  = $row->session_key;
                $item['participants'] = $participants;
                $item['chat_type']    = $row->chat_type; 
                $item['created']      = $row->created; 
                $item['last_change']  = $row->last_change; 
                $item['new_message_nums'] = 0;
                
                if($row->is_new === '1')
                {
                    $item['new_message_nums'] = $this->message_model
                        ->select_school($school_uid)
                        ->get_message_nums($row->session_key, $this->personal_identity);
                }
                
                $result[] = $item; 
            }
        }
        
        $this->return_result($result);
    }
    
    /**
    * retrieve new message nums per session
    * 
    */
    public function count_new_message_nums()
    {
        $client = $this->params['client'];
        $data   = $this->params['data'];
        $user_type  = $client['user_type'];   
        $school_uid = $this->user->school_uid;
        
        $chat_condition = $this->_get_chat_conditions($user_type);
        $chat_sessions  = $this->message_model
            ->select_school($school_uid)
            ->get_message_nums_by_chat_session($chat_condition, $this->personal_identity);
        
        $result = array();
        if(!empty($chat_sessions))
        {
            foreach($chat_sessions as $row)
            {
                $temp = explode(':', $row->participants);
                $participants = $this->_parse_chat_participants($temp, $school_uid);
                if(empty($participants)) continue; 
                
                $item = array();
                $item['session_key']  = $row->chat_session;
                $item['participants'] = $participants;
                $item['created']      = $row->created;
                $item['chat_type']    = $row->chat_type;
                $item['last_change']  = $row->last_change; 
                $item['new_message_nums'] = $row->new_message_nums;
                $result[] = $item; 
            }
        }
        
        $this->return_result($result);
    }
    
    /**
    * get contents of chat
    * 
    */
    public function chat_conversations()
    {             
        $fields = array('session_key', 'start_time_option', 'end_time');
        if(!array_keys_exists($fields, $this->params['data']))
        {
            $this->return_error(ERR_MISSING_PARAMETERS);
        }  
        
        /////////////////////////////////////////
        $data = $this->params['data'];
        $school_uid = $this->user->school_uid;
        $start_time_option = (int)$data['start_time_option'];
        $start_time_text = '';
        /////////////////////////////////////////
        
        switch($start_time_option)
        {
            case 0:
            default:
                $init_days = MSG_INIT_DAYS;
                $start_time_text = "-{$init_days} days";
                break;
            case 1:
                $start_time_text = "-2 week";
                break;
            case 2:
                $start_time_text = "-1 month";
                break;
            case 3:
                $start_time_text = "-3 month";
                break;
            case 4:
                $start_time_text = "-6 month";
                break;
            case 5:
                $start_time_text = "-1 year";
                break;
            case 6:
                $start_time_text = "-10 years";
                break;
        }
        
        $start_time = date(FORMAT_DATE, strtotime($start_time_text));
        $end_time   = ($start_time_option === 0 || $data['end_time'] === '') ? date(FORMAT_DATETIME) : $data['end_time'];
        $session_key= $data['session_key'];
        $messages = $this->message_model
            ->select_school($school_uid)
            ->get_chat_conversations($session_key, $start_time, $end_time, $this->personal_identity);
            
        $photos = array();
        foreach($messages as $item)
        {
            $pcid = generate_personal_identity($item->sender_type, $item->sender_id);
            if(!array_key_exists($pcid, $photos))
            {
                $photos[$pcid] = $this->_user_photo($item->sender_id, $item->sender_type);
            }
            
            $item->sender_photo = $photos[$pcid];
            $item->sender_additional = ct_json_decode($item->sender_additional);
        }
        
        if(!empty($messages))
        {
            $this->message_model
                ->select_school($school_uid)
                ->subscribe_me_to_chat_session($session_key, $this->personal_identity, date(FORMAT_DATETIME));
        }
        
        $result = array();
        $result['session_key'] = $session_key;
        $result['messages']    = $messages;      
        $this->return_result($result);
    }
    
    /**
    * get new conversation contents on chat session page
    * 
    */
    public function new_chat_conversations()
    {
        $fields = array('session_key');
        if(!array_keys_exists($fields, $this->params['data']))
        {
            $this->return_error(ERR_MISSING_PARAMETERS);
        }  
        
        ///////////////////////////////////////
        $data = $this->params['data'];
        $school_uid = $this->user->school_uid;    
        $search_time = date(FORMAT_DATETIME); 
        $session_key = $data['session_key'];
        ///////////////////////////////////////

        //get new chat conversations
        $messages = $this->message_model
            ->select_school($school_uid)
            ->get_new_chat_conversations($session_key, $this->personal_identity);
        
        $photos = array();
        foreach($messages as $item)
        {
            $pcid = generate_personal_identity($item->sender_type, $item->sender_id);
            if(!array_key_exists($pcid, $photos))
            {
                $photos[$pcid] = $this->_user_photo($item->sender_id, $item->sender_type);
            }
            
            $item->sender_photo = $photos[$pcid];
            $item->sender_additional = ct_json_decode($item->sender_additional);
        }
        
        if(!empty($messages))
        {
            $this->message_model
                ->select_school($school_uid)
                ->subscribe_me_to_chat_session($session_key, $this->personal_identity, $search_time);
        }
                
        $result = array();
        $result['session_key'] = $session_key;
        $result['messages']    = $messages;      
        $this->return_result($result);
    }
    
    /**
    * remove chat session of my chat sessions
    * 
    */
    public function remove_chat_session()
    {
        $fields = array('session_key');
        if(!array_keys_exists($fields, $this->params['data']))
        {
            $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        ///////////////////////////////////////
        $data = $this->params['data'];
        $user_type   = $this->params['client']['user_type'];
        $school_uid  = $this->user->school_uid;    
        $session_key = $data['session_key'];
        ///////////////////////////////////////         
        
        //get chat session
        $chat_conditions = $this->_get_chat_conditions($user_type);
        $chat_session = $this->message_model
            ->select_school($school_uid)
            ->get_chat_session($session_key, $chat_conditions);
        if(!isset($chat_session->chat_uuid))
        {
            $this->return_error(MESSAGE_INVALID_CHAT_SESSION);
        }
        
        //unsubscribe
        $status = $this->message_model
            ->select_school($school_uid)
            ->unsubscribe_chat_session($chat_session->chat_uuid, $this->personal_identity);
        
        //return result
        $result = array();
        $result['remove_status'] = $status === TRUE ? STATUS_SUCCESS : STATUS_FAIL;
        $this->return_result($result);
    }
    
    /**
    * remove message item of chat session
    * 
    */
    public function remove_message()
    {
        $fields = array('session_key', 'message_key');
        if(!array_keys_exists($fields, $this->params['data']))
        {
            $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        ///////////////////////////////////////
        $data = $this->params['data'];
        $user_type   = $this->params['client']['user_type'];
        $school_uid  = $this->user->school_uid;    
        $session_key = $data['session_key'];
        $message_key = $data['message_key'];
        ///////////////////////////////////////  
        
        //get chat session
        $chat_conditions = $this->_get_chat_conditions($user_type);
        $chat_session = $this->message_model
            ->select_school($school_uid)
            ->get_chat_session($session_key, $chat_conditions);
        if(!isset($chat_session->chat_uuid))
        {
            $this->return_error(MESSAGE_INVALID_CHAT_SESSION);
        }      
        
        //unsubscribe
        $status = $this->message_model
            ->select_school($school_uid)
            ->unsubscribe_message($chat_session->chat_uuid, $message_key, $this->personal_identity);
        
        //return result
        $result = array();
        $result['remove_status'] = ($status === TRUE) ? STATUS_SUCCESS : STATUS_FAIL;
        $result['session_key'] = $data['session_key'];
        $result['message_key'] = $data['message_key'];
        $this->return_result($result);     
    }
    
    /**
    * search notifications and homework by date and type
    * 
    */
    public function search_by_date_type()
    {
        $fields = array('chat_date', 'chat_type');
        if(!array_keys_exists($fields, $this->params['data']))
        {
            $this->return_error(ERR_MISSING_PARAMETERS);
        }
        
        ///////////////////////////////////////
        $data = $this->params['data'];        
        $chat_type = (int)$data['chat_type'];
        $chat_date = $data['chat_date'];
        $user_type = $this->params['client']['user_type'];
        $school_uid= $this->user->school_uid;    
        /////////////////////////////////////// 
        
        //check chat type: 1:notification 2:homework
        if(!in_array($chat_type, array(MSG_NOTIFICATION, MSG_HOMEWORK)))
        {
            $this->return_error(MESSAGE_INVALID_SEARCH_REQUEST);
        }
        
        $search_conditions = $this->_get_search_conditions($user_type);
        $result = $this->message_model
            ->select_school($school_uid)
            ->get_by_date_and_type($chat_type, $chat_date, $search_conditions);
        
        //set sender's photo
        $photos = array();
        foreach($result as $item)
        {
            $pcid = generate_personal_identity($item->sender_type, $item->sender_id);
            if(!array_key_exists($pcid, $photos))
            {
                $photos[$pcid] = $this->_user_photo($item->sender_id, $item->sender_type);
            }
            
            $item->sender_photo = $photos[$pcid];
        }
            
        $this->return_result($result);
    }
    
    /////////////////////////////////////////////////////////////////////
    //PROTECTED METHODS
    /////////////////////////////////////////////////////////////////////
    protected function restrict()
    {
        parent::restrict();
        
        $client = $this->params['client'];
        $user_type = (int)$client['user_type'];        
        $this->personal_identity = generate_personal_identity($user_type, $this->user->user_id);
            
        if($user_type === USER_TYPE_PARENT)
        {
            //check my children in this school
            $my_children_uids = $this->parent_model->get_parent_children_uids($this->user->parent_uuid);
            if(empty($my_children_uids))
            {
                //$this->return_error(MESSAGE_NO_PERMISSION);    
                $this->return_error(FORCE_PARENT_NO_CHILDREN);    
            }

            $childrens = $this->children_model
                ->filter_school($this->user->school_uid)
                ->filter_user_uids($my_children_uids)
                ->with_class()
                ->find_all();
            if(empty($childrens))
            {
                //$this->return_error(MESSAGE_NO_PERMISSION);
                $this->return_error(FORCE_PARENT_NO_CHILDREN);    
            }
            
            //get parent's school_date
            $school_date = date(FORMAT_DATETIME);
            foreach($childrens as $children)
            {
                $school_date = min($school_date, $children->school_date);
            }
            
            $this->childrens = $childrens;
            $this->user->school_date = max($school_date, $this->user->reg_date);
        }
    }
    
    /////////////////////////////////////////////////////////////////////
    //PRIVATE METHODS
    /////////////////////////////////////////////////////////////////////
    
    /**
    * check message data is valid from requested data
    * 
    * @param array  $data
    * @param int    $user_type
    */
    private function _check_message($data, $user_type)
    {
        //check all fields for message is in data
        $fields = array('message_type', 'message_contents', 'attachment', 'attachment_name', 'temp_id');
        if(!array_keys_exists($fields, $data)) 
        {
            return FALSE;
        }
        
        ///check receivers or session_key
        if(!isset($data['receivers']) && !isset($data['session_key']))
        {
            return FALSE;
        }
        
        //check valid message_type
        $message_type = (int)$data['message_type'];
        $valid_types  = array(MSG_NORMAL, MSG_NOTIFICATION, MSG_HOMEWORK);
        if(!in_array($message_type, $valid_types))
        {
            return FALSE;
        }
        
        //children and parent can't send notification and homework
        if(in_array($user_type, array(USER_TYPE_CHILDREN, USER_TYPE_PARENT)) 
            && in_array($message_type, array(MSG_NOTIFICATION, MSG_HOMEWORK)))
        {
            return FALSE;
        }                
        
        //if text message is not, then this is file upload
        if(ct_strlen(trim($data['message_contents'])) === 0)
        {
            //check attachment's size
            $file = $data['attachment']; 
            if(isset($_FILES[$file]) 
                && $_FILES[$file]['size'] > 0 && $_FILES[$file]['name'] != '')
            {
                $attach = $_FILES[$file];
                $name   = $data['attachment_name'];
                $size   = $attach['size']/1024; //KB
                
                if(trim($name) === '')
                {
                    return FALSE;
                }
                
                if($size > MSG_ATTACH_MAX_SIZE)
                {          
                    return FALSE;
                }
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            $data['attachment'] = '';
        }
        
        return TRUE;
    }
    //--------------------------------------------------------------------
    
    /**
    * extract parse_channels and participants array from request
    * 
    * @param array  $data
    * @param int    $school_uid
    * @param int    $user_type
    * @param string $user_id
    */
    private function _extract_receivers($data, $school_uid, $user_type, $user_id, $message_type)
    {          
        $result  = array();
        $participants = array();
        $parse_channels = array();
        
        $to_all = FALSE;
        $to_all_teachers = FALSE;
        $to_all_children = FALSE;
        $to_all_parents  = FALSE;
        
        if($user_type == USER_TYPE_MASTER)
        {
            //to_all
            if(isset($data['to_all']) && $data['to_all'] == '1')
            {
                $participants[] = MSG_CHL_ALL;
                $parse_channels[] = generate_parse_channel($school_uid);
                
                $to_all = TRUE;
            }
            
            if($to_all === FALSE)
            {
                //to_all_teachers
                if(isset($data['to_all_teachers']) && $data['to_all_teachers'] == '1')
                {
                    $participants[] = MSG_CHL_ALL_TEACHER;
                    $parse_channels[] = generate_parse_channel($school_uid, USER_TYPE_TEACHER);
                    
                    $to_all_teachers = TRUE;
                }
                
                //to_all_children
                if(isset($data['to_all_children']) && $data['to_all_children'] == '1')
                {
                    $participants[] = MSG_CHL_ALL_CHILDREN;
                    $parse_channels[] = generate_parse_channel($school_uid, USER_TYPE_CHILDREN); 
                    
                    $to_all_children = TRUE;
                }
                
                //to_all_parents
                if(isset($data['to_all_parents']) && $data['to_all_parents'] == '1')
                {
                    $participants[] = MSG_CHL_ALL_PARENT;   
                    $parse_channels[] = generate_parse_channel($school_uid, USER_TYPE_PARENT);
                    
                    $to_all_parents = TRUE;
                }
            }
        }
        
        if(in_array($user_type, array(USER_TYPE_TEACHER, USER_TYPE_MASTER)) && $message_type === MSG_NOTIFICATION)
        {
            $participants[] = MSG_CHL_ALL;
            $parse_channels[] = generate_parse_channel($school_uid);
            
            $to_all = TRUE;   
        }
        
        if($to_all === FALSE)
        {
            if($user_type == USER_TYPE_MASTER || $user_type == USER_TYPE_TEACHER)
            {   
                $class_uids = $this->teacher_model
                    ->select_school($school_uid)
                    ->get_class_uids($this->user->teacher_uuid);
                  
                //to_all_children_group
                if($to_all_children === FALSE 
                    && isset($data['to_all_children_group']) 
                    && is_array($data['to_all_children_group']) 
                    && !empty($data['to_all_children_group']))
                { 
                    foreach($data['to_all_children_group'] as $class_uid)
                    {
                        //validate class
                        if(trim($class_uid) === '') continue;
                                 
                        //check if teacher can send message to this class
                        if($user_type == USER_TYPE_TEACHER && !in_array($class_uid, $class_uids)) continue; 
                            
                        $class_children = $this->children_model
                            ->select_school($school_uid)
                            ->filter_class($class_uid)
                            ->order_by('user_name')
                            ->find_all();
                            
                        if(empty($class_children)) continue;
                           
                        foreach($class_children as $child)
                        {
                            $participants[] = generate_personal_identity(USER_TYPE_CHILDREN, $child->user_id); 
                        }
                        
                        $parse_channels[] = generate_parse_channel($school_uid, $class_uid, USER_TYPE_CHILDREN);
                    }
                }
                
                //to_all_parents_group
                if($to_all_parents === FALSE && isset($data['to_all_parents_group']) 
                    && is_array($data['to_all_parents_group']) && !empty($data['to_all_parents_group']))
                {
                    foreach($data['to_all_parents_group'] as $class_uid)
                    {
                        //validate class
                        if(trim($class_uid) === '') continue;
                                                     
                        //check if teacher can send message to this class
                        if($user_type == USER_TYPE_TEACHER && !in_array($class_uid, $class_uids)) continue; 
                        
                        $class_children = $this->children_model
                            ->select_school($school_uid)
                            ->filter_class($class_uid)
                            ->order_by('user_name')
                            ->find_all();
                        if(empty($class_children)) continue;
                         
                        foreach($class_children as $child)
                        {
                            $parent_uids = $this->children_model->get_children_parent_uids($child->children_uuid);
                            $parents = $this->parent_model
                                ->select('user_id')
                                ->filter_user_uids($parent_uids)
                                ->find_all();
                            if(empty($parents)) continue;
                            
                            foreach($parents as $parent)
                            {
                                $participants[] = generate_personal_identity(USER_TYPE_PARENT, $parent->user_id);
                            }
                        }
                        
                        $parse_channels[] = generate_parse_channel($school_uid, $class_uid, USER_TYPE_PARENT);
                    }
                }
            }
                     
            //to_custom_group
            if(isset($data['to_custom_group']) && is_array($data['to_custom_group']) && !empty($data['to_custom_group']))
            { 
                foreach($data['to_custom_group'] as $user)
                {                 
                    //validate users
                    if(!isset($user['user_type']) || !isset($user['user_id'])) continue; 
                    if(trim($user['user_type']) === '' || trim($user['user_id']) === '') continue; 
                    if($user['user_id'] === $user_id) continue; //if it's me
                    
                    $u_type = (int)$user['user_type'];
                    if(($u_type === USER_TYPE_TEACHER && $to_all_teachers === TRUE)
                        || ($u_type === USER_TYPE_CHILDREN && $to_all_children === TRUE)
                        || ($u_type === USER_TYPE_PARENT && $to_all_parents === TRUE))
                    {
                        continue;
                    }
                     
                    //get user
                    $user_profile = $this->get_user_record($user['user_id'], $u_type);
                    if(empty($user_profile)) continue;
                    
                    $participants[] = generate_personal_identity($u_type, $user_profile->user_id);               
                    $parse_channels[] = generate_parse_channel($school_uid, $u_type, $user['user_id']); //if message will be global, school_uid will be removed!!!
                }
            }
        }
        
        //add sender's identity
        if($to_all === FALSE && $to_all_teachers === FALSE)
        {
            $participants[] = $this->personal_identity;
        }
        
        $result['participants'] = array_unique($participants); 
        $result['parse_channels'] = array_unique($parse_channels);
        return $result;
    }
    //--------------------------------------------------------------------
    
    /**
    * extract parse_channels from chat_session
    * 
    * @param mixed $chat_session
    * @param mixed $school_uid
    * @return []
    */
    private function _extract_parse_channels_from_chat_session($chat_session, $school_uid)
    {
        $result = array();
                          
        $participants = explode(':', $chat_session->participants);
        foreach($participants as $item)
        {
            if($item === $this->personal_identity) continue;
            
            if($item === MSG_CHL_ALL)
            {
                $result[] = generate_parse_channel($school_uid); 
                break;
            }
            
            if($item === MSG_CHL_ALL_TEACHER)
            {
                $result[] = generate_parse_channel($school_uid, USER_TYPE_TEACHER); 
            }
            else if($item === MSG_CHL_ALL_CHILDREN)
            {
                $result[] = generate_parse_channel($school_uid, MSG_CHL_ALL_CHILDREN); 
            }
            else if($item === MSG_CHL_ALL_PARENT)
            {
                $result[] = generate_parse_channel($school_uid, MSG_CHL_ALL_PARENT); 
            }
            else
            {
                $custom_user = parse_personal_identity($chat_session->creator);
                $result[] = generate_parse_channel($school_uid, $custom_user['user_type'], $custom_user['user_id']);
            }
        }
        
        //add creator's parse_channel 
        $creator = parse_personal_identity($chat_session->creator);
        $result[] = generate_parse_channel($school_uid, $creator['user_type'], $creator['user_id']);
        
        return $result;
    }
    //--------------------------------------------------------------------
    
    /**
    * get additional sender info
    * 
    * @param array $client
    */
    private function _extract_additional_sender_info($client)
    {
        $user_type = (int)$client['user_type'];
        
        $result = array();
        if($user_type === USER_TYPE_CHILDREN)
        {                                                 
            $result['grade_num'] = $this->user->grade_num;
            $result['class_num'] = $this->user->class_num;
        }
        
        return ct_json_encode($result);
    }
    //--------------------------------------------------------------------
    
    /**
    * upload message attachments
    * 
    * @param string $file
    * @param int    $school_uid
    */
    private function _upload_attachments($data, $school_uid)
    {   
        $file = $data['attachment'];
        $attach = $_FILES[$file];
        $attach_name = $data['attachment_name'];
        $attach_size = round($attach['size'] / 1024, 1); //kb
        
        $subp = date('Y/m/d/H') . '/' . time();
        $path = MEDIA_PATH . "attachments/" . $school_uid . '/' . $subp;       
        if(!valid_path($path))
        {
            return array();
        }
        
        $config = array();
        $config['upload_path'] = $path;
        $config['max_size']    = MSG_ATTACH_MAX_SIZE;
        $config['overwrite']   = TRUE;
        $config['file_name']   = md5($attach_name);
        $config['allowed_types'] = '*';
        
        $this->upload_file($file, $config);
        if($file !== '')
        {
            $attachment = array();
            $attachment['file_name'] = $attach_name;
            $attachment['file_size'] = $attach_size;
            $attachment['file_path'] = $subp . '/' . $file;
            
            return $attachment;
        }
        
        return array();        
    }
    //-------------------------------------------------------------------- 
    
    /**
    * get chat conditions
    * 
    * @param int $participant_type
    */
    private function _get_chat_conditions($participant_type)
    {
        $additional = '';
        if($participant_type == USER_TYPE_MASTER || $participant_type == USER_TYPE_TEACHER)
        {
            //add all_teacher_channel
            $additional = " OR (participants LIKE '%" . MSG_CHL_ALL_TEACHER 
                . "%' AND last_change > '{$this->user->school_date}')";
        }
        else if($participant_type == USER_TYPE_CHILDREN)
        {
            //add all_children_channel
            $additional .= " OR (participants LIKE '%" . MSG_CHL_ALL_CHILDREN 
                . "%' AND last_change > '{$this->user->school_date}')";
        }
        else if($participant_type == USER_TYPE_PARENT)
        {
            //add all_parent_channel
            $additional .= " OR (participants LIKE '%". MSG_CHL_ALL_PARENT 
                . "%' AND last_change > '{$this->user->school_date}')";
        }
        
        //add personal conditions
        $result = "( 
                creator = '{$this->personal_identity}' OR
                participants LIKE '%{$this->personal_identity}%' OR
                (participants LIKE '%" . MSG_CHL_ALL . "%' AND last_change > '{$this->user->reg_date}')
                {$additional}
            )
            AND LOCATE('{$this->personal_identity}', IFNULL(chat_unsubscriber, '')) = 0
        ";
        
        return $result;
    }
    //--------------------------------------------------------------------
    
    /**
    * get conditions to search homework and notification
    * 
    * @param int $participant_type
    */
    private function _get_search_conditions($participant_type)
    {
        $additional = '';
        if($participant_type == USER_TYPE_MASTER || $participant_type == USER_TYPE_TEACHER)
        {
            //add all_teacher_channel
            $additional = " OR (participants LIKE '%" . MSG_CHL_ALL_TEACHER 
                . "%' AND last_change > '{$this->user->school_date}')";
        }
        else if($participant_type == USER_TYPE_CHILDREN)
        {
            //add all_children_channel
            $additional .= " OR (participants LIKE '%" . MSG_CHL_ALL_CHILDREN 
                . "%' AND last_change > '{$this->user->school_date}')";
        }
        else if($participant_type == USER_TYPE_PARENT)
        {
            //add all_parent_channel
            $additional .= " OR (participants LIKE '%". MSG_CHL_ALL_PARENT 
                . "%' AND last_change > '{$this->user->school_date}')";
        }
        
        //add personal conditions
        $result = "( 
                participants LIKE '%{$this->personal_identity}%' OR
                (participants LIKE '%" . MSG_CHL_ALL . "%' AND last_change > '{$this->user->reg_date}')
                {$additional}
            )
        ";
        
        return $result;
    }
    //--------------------------------------------------------------------
    
    /**
    * get chat session_conditions
    * 
    * @param int  $participant_type
    * @param int  $show_all : 0: 1 week and new sessions, 1:get all before last time, 2: get all
    * @param time $last_time
    */
    private function _get_chat_session_conditions($participant_type, $show_all=0, $last_time='')
    {
        $result = $this->_get_chat_conditions($participant_type);
                                            
        if($show_all === 0)
        {
            $init_days = MSG_INIT_DAYS;                                                                                
            $start_time = date(FORMAT_DATE, strtotime("-{$init_days} day"));
            $result .= " AND (last_change > '{$start_time}' AND LOCATE('{$this->personal_identity}', subscriber) = 0)";
        }
        else
        {    
            if($show_all === 2)
            {
                $last_time = date(FORMAT_DATETIME);
            }
            
            $result .= " AND (last_change < '{$last_time}')";
        }
        
        return $result; 
    }
    //--------------------------------------------------------------------
    
    /**
    * parse and return message channels for app
    * 
    * @param string $string
    * @param int    $school_uid
    */
    private function _parse_chat_participants($participants, $school_uid)
    {
        $result = array();
        
        foreach($participants as $pp)
        {
            if($pp === $this->personal_identity) continue;
            
            if($pp === MSG_CHL_ALL)
            {
                $result['to_all'] = 1;
            }
            else if($pp === MSG_CHL_ALL_TEACHER)
            {
                $result['to_all_teachers'] = 1;
            }
            else if($pp === MSG_CHL_ALL_CHILDREN)
            {
                $result['to_all_children'] = 1;
            }
            else if($pp === MSG_CHL_ALL_PARENT)
            {
                $result['to_all_parents'] = 1;
            }
            else
            {
                if(!isset($result['to_custom_group']))
                {
                    $result['to_custom_group'] = array();
                } 
                
                $user_item = parse_personal_identity($pp);
                $user_type = (int)$user_item['user_type'];
                $user = $this->get_user_record($user_item['user_id'], $user_type);
                if(empty($user)) continue;
                
                $item = array();
                $item['user_id']   = $user->user_id;
                $item['user_name'] = $user->user_name;
                $item['user_type'] = $user_item['user_type'];
                $item['user_sex']  = $user->user_sex;
                $item['user_photo']= $user->user_photo_url;
                 
                $result['to_custom_group'][] = $item;
            }
        }
        
        return $result;     
    }
    //--------------------------------------------------------------------
    
    /**
    * get user photo
    * 
    * @param int    $school_uid
    * @param string $user_id
    * @param int    $user_type
    */
    private function _user_photo($user_id, $user_type, $empty_text='')
    {
        $user = $this->get_user_record($user_id, $user_type);
        if(empty($user))
        {
            return $empty_text;
        }
        
        return $user->user_photo_url;
    }   
}//end class