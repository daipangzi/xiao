<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

class Downloads extends Front_Controller
{    
	/**
	 * Displays the download page
	 *
	 * @return void
	 */
	public function index()
	{
        $this->load->model('apps_model', null, true);
        
        $records = $this->apps_model->find_all();
        Template::set('records', $records);
        
        Template::render();
	}//end index()
	//--------------------------------------------------------------------
}//end class