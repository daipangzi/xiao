<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

class MY_Upload extends CI_Upload
{
    function is_allowed_filetype($ignore_mime = FALSE)
    {
        parent::is_allowed_filetype($ignore_mime);
        
        if($this->file_ext === 'php' || $this->file_ext === 'exe')
        {    
            return FALSE;
        }
        
        return TRUE;
    }	
}//end class