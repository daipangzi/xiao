<?php

//require_once "pdu.class.php";
require_once "serial.class.php";

class SMS {
	//public $pdu;
	//public $serial;
	public $msg;
	public $used_device;
	
	function __construct() {
		$this->used_device = 0;	
	}
	/**
	* Repeatedly excutes the SendAsPDU() at 8 times.
	* @param  $to
	* @param  $txt
	* @param  $device
	* 
	* @return bool
	*/
	public function StartSend($to, $txt, $device=0){
		$dev 		= $device;
		$phone 		= $this->InvertNumbers('86'.$to);
		$mess		= $this->hex2str($txt);
		$message 	= "11000D91".$phone."000800".sprintf("%02X",strlen($mess)/2).$mess;
		
		$result = FALSE;
		
		$this->msg = "</br>Start Sending to ".$to."</br>";
		
		for($i = 0; $i < 8; $i++){
			$result = $this->SendAsPDU($phone, $message, $dev);
			
			if($result === TRUE){
				$this->used_device = $dev;
				break;
			}
			else{
				$dev = $this->NextDeviceNumber($dev);
			}
			//usleep(1000000);
		}
		return $result;
	}
	/**
	* Send in PDU Mode
	* @param undefined $to
	* @param undefined $txt
	* 
	* @return
	*/
	private function SendAsPDU($phone, $message, $device){
		$serial = new Serial;
		
		if($serial->deviceSet('/dev/ttyACM'.$device)){	// Setting Device
			$this->msg .= "</br>Set $device</br>";
			
			if($serial->deviceOpen()){	// Opening Device
				$this->msg .= "</br>Opened $device</br>";
				$serial->sendMessage("at+cmgf=0".chr(13));	// Set as PDU Mode
				$s = $serial->readPort();
				$this->msg .= "</br>1 command</br>".$s;
				
				if(strstr($s, "OK") !== FALSE){
					$this->msg .= "</br>TRUE</br>";
					$serial->sendMessage("at+cmgs=".sprintf("%d",strlen($message)/2).chr(13));	// Set Length
					$s = $serial->readPort();
					$this->msg .= "</br>2 command</br>".$s;
					
					if(strstr($s, ">") !== FALSE){
						$this->msg .= "</br>TRUE</br>";
						$serial->sendMessage('00'.$message.chr(26));	// Write Phone Number and Message
						$s = $serial->readPort();
						$this->msg .= "</br>3 command</br>".$s;
						
						if(strstr($s, "ERROR") === FALSE){
							$this->msg .= "</br>TRUE</br>";
							$this->msg .= "</br>Complete Sending From - $device</br>";
							$serial->deviceClose();
							return TRUE;
						}
						else{
							$this->msg .= "</br>Fail At Device - $device - on Command 3</br>";
							$serial->deviceClose();
							return FALSE;
						}
					}
					else{
						$this->msg .= "</br>Fail At Device - $device - on Command 2</br>";
						$serial->deviceClose();
						return FALSE;
					}
				}
				else{
					$this->msg .= "</br>Fail At Device - $device - on Command 1</br>";
					$serial->deviceClose();
					return FALSE;
				}
			}
			else{
				$this->msg .= "</br>Fail At Device - $device - on Openning</br>";
				return FALSE;
			}
		}
		else{
			$this->msg .= "</br>Fail At Device - $device - on Setting</br>";
			return FALSE;
		}
		
		$this->msg .= "</br>Unusually Fail At Device - $device - on Setting</br>";
		return FALSE;
	}
	/**
	* Send in Text Mode
	* @param undefined $to
	* @param undefined $txt
	* 
	* @return
	*/
	public function SendAsText($to, $txt){
		$this->serial->sendMessage("AT\n");
		$this->msg .= $this->serial->readPort();
		$this->serial->sendMessage("AT+CMGF=?\n");
		$this->msg .= $this->serial->readPort();
		$this->serial->sendMessage("AT+CMGF=1\n");
		$this->msg .= $this->serial->readPort();
		$this->serial->sendMessage("AT+CMGS=\"{$to}\"\n{$txt}".chr(26)."\n");
		$this->msg .= $this->serial->readPort();
		$this->serial->sendMessage("AT+CMSS=1\n");
		$this->msg .= $this->serial->readPort();
		
		$this->serial->deviceClose();
	}
	/**
	* Convert to UCS-2 - HEX
	* @param undefined $str
	* 
	* @return
	*/
	private function hex2str($str) {
	    $hexstring=iconv("UTF-8", "UCS-2", $str);
	    $str = '';
	    for($i=0; $i<strlen($hexstring)/2; $i++){
            $str .= sprintf("%02X",ord(substr($hexstring,$i*2+1,1)));
            $str .= sprintf("%02X",ord(substr($hexstring,$i*2,1)));
	    }
	    return $str;
	}
	/**
	* Revert to PDU phone number format
	* @param undefined $msisdn
	* 
	* @return
	*/
	private function InvertNumbers($msisdn) {
	    $len = strlen($msisdn);
	    if ( 0 != fmod($len, 2) ) {
            $msisdn .= "F";
            $len = $len + 1;
	    }

	    for ($i = 0; $i < $len; $i += 2) {
            $t = $msisdn[$i];
            $msisdn[$i] = $msisdn[$i+1];
            $msisdn[$i+1] = $t;
	    }
	    return $msisdn;
	}
	/**
	* Return next device number in 0 - 7
	* @param $num
	* 
	* @return int
	*/
	public function NextDeviceNumber($num){
		if($num == 7) $num = 0;
		else $num++;
		return $num;
	}
}