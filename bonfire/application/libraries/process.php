<?php
	/**
	* Executing a php script in background via shell_exec
	* 2014-01-14 23:40:
	*/
	class Process {
		private $_pid;
		
		function __construct() {
			$_pid = "";
		}
		
		public function runInBackground($command, $priority=0, $log=""){
			if($priority)
			   //$this->_pid = shell_exec("nohup nice -n $priority $command > $log 2>&1 & echo $!");
			   $this->_pid = shell_exec("nohup nice -n $priority $command 2> /dev/null & echo $!");
			else
			   //$this->_pid = shell_exec("nohup $command > $log 2>&1 & echo $!");
			   $this->_pid = shell_exec("nohup $command > /dev/null 2> /dev/null & echo $!");
			return $this->_pid;
		}
		
		public function isProcessRunning($PID){
			if($PID == 0)	return false;
			if($PID == "")	return false;

			exec("ps -p $PID 2>&1",$state);
			return(count($state) >= 2);
		}
		
		public function displayProcessLog($logfile){
			exec('cat $logfile 2>&1',$log);
			return implode("\r\n",$log);
		}
		
		public function killProcess($PID){
			exec('kill '.$PID.' 2>&1',$status);
			return implode("\r\n",$status);
		}
	}