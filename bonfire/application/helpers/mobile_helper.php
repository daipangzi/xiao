<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

///////////////////////////////////////////////////////////
// mobile functions
/////////////////////////////////////////////////////////// 

if ( ! function_exists('get_clean_fields_in'))
{
    /**
    * clean up fields in data from mobile
    * 
    * @param array $data
    */
    function get_clean_fields_in($data) 
    {
        if(empty($data)) return array();
        
        foreach($data as $code => $item)
        {
            if(!is_string($item)) continue;
            
            switch($code)
            {
                case 'charge_class':
                case 'subject_class':
                    $data[$code] = valid_array_string($item);
                    break;
                case 'user_id';
                    $data[$code] = strtolower(trim($item));
                    break;
                default:
                    $data[$code] = trim($item);
                    break;        
            }
        }   
        
        return $data; 
    }
}
   
if ( ! function_exists('get_connection_key'))
{
    /**
    * get connection ket string for mobile
    * 
    * @param array $data
    */
    function get_connection_key($data)
    {
        $fields = array('uid', 'user_id', 'user_type', 'device_type', 'device_info', 'login_date');
        if(!array_keys_exists($fields, $data))
        {
            return md5('unknown' . time());   
        }
        
        $key = md5($data['uid'] . $data['user_id'] . $data['user_type'] . $data['device_type'] . $data['device_info'] . $data['login_date'] . 's'); 
        return $key;
    } 
}

if( !function_exists('get_user_photo_url') )
{
    /**
    * get user photo url
    * 
    * @param string $file_name
    * @param int    $user_type
    */
    function get_user_photo_url($file_name, $user_type)
    {
        if($file_name == '') return '';
        
        $sub_path = '';
        switch($user_type)
        {
            case USER_TYPE_TEACHER:
            case USER_TYPE_MASTER:
            default:
                $sub_path = 'teacher/';
                break;
            case USER_TYPE_CHILDREN:
                $sub_path = 'children/';
                break;
            case USER_TYPE_PARENT:
                $sub_path = 'parent/';
                break;
        }
        
        $fp = substr($file_name, 0, 1);
        $sp = substr($file_name, 1, 1);
        $suffix = $fp . '/' . $sp;
        
        $file_path = MEDIA_PATH . "photo/{$sub_path}{$suffix}/" . $file_name;
        $file_url  = MEDIA_PATH_URL . "photo/{$sub_path}{$suffix}/" . $file_name;
        if(!file_exists($file_path))
        {
            return '';
        }
        
        return site_url($file_url);
    }
}

if ( ! function_exists('get_test_duration'))
{
    /**
    * get test duration when children is registered
    * 
    */
    function get_test_duration()
    {
        $test_days = CHILDREN_TEST_EXPIRE_DAYS;
        $day_num   = date('N');
        $suffix    = 0;

        if($day_num == 6)
        {
            $suffix = 1;
        }
        else if($day_num == 7)
        {
            $suffix = 2;
        }

        $last_day_num = $day_num + $test_days;
        if($last_day_num > 6 )
        {
            $last_day_num += 2;
        }
        
        $last_day_num += 100; //additionally
        return $last_day_num - $day_num - $suffix;
    }
}

if ( ! function_exists('log_error'))
{
    /**
    * register error to system
    * 
    * @param int    $error_no
    * @param string $error_msg
    * @param int    $user_type
    * @param int    $device_type
    * @param array  $params
    */
    function log_error($error_no, $user_type, $function, $device_type, $params)
    {
        $ci =& get_instance();
        $ci->load->model('log_error_model');
        
        $ci->config->load('error_msg');
        $error_messges = config_item('error_msg');
        
        $data = array();
        $data['error_no']   = $error_no;
        $data['error_msg']  = isset($error_messges[$error_no]) ? $error_messges[$error_no] : 'Unknown Error';
        $data['user_type']  = $user_type;
        $data['function']   = $function;
        $data['device_type']= $device_type;
        $data['ip_address'] = $ci->input->ip_address();
        $data['request_params']= ct_json_encode($params);
        return $ci->log_error_model->insert($data);
    }
}