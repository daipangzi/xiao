<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

///////////////////////////////////////////////////////////
// custom json functions
/////////////////////////////////////////////////////////// 

if ( ! function_exists('ct_json_encode'))
{
    /**\
    * encode array to json with utf8
    * 
    * @param mixed $data
    */
    function ct_json_encode($data)
    {
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }
} 

if ( ! function_exists('ct_json_decode'))
{
    /**\
    * decode json to array with utf8
    * 
    * @param string $string
    */
    function ct_json_decode($string)
    {
        return json_decode($string, JSON_UNESCAPED_UNICODE);
    }
}

if ( ! function_exists('ct_json_decode_param'))
{
    /**
    * decode json_data
    * 
    * @param mixed $data
    */
    function ct_json_decode_param($data)
    {
        if(!is_json($data))
        {
            if(TEST_MODE === TRUE)
                return $data;
            else
                return FALSE;
        }
        
        $data = (array)json_decode($data, JSON_UNESCAPED_UNICODE);

        $param = array();
        if(isset($data['client']))
        {
            $param['client'] = (array)$data['client'];
        }
        
        if(isset($data['data']))
        {
            $param['data'] = (array)$data['data'];
        }
                
        return $param;
    }
} 

if( !function_exists('pretty_json') )
{
    /**
     * JSON beautifier
     * 
     * @param string    The original JSON string
     * @param   string  Return string
     * @param string    Tab string
     * @return string
     */
    function pretty_json($json, $ret= "\n", $ind="\t") 
    {

        $beauty_json = '';
        $quote_state = FALSE;
        $level = 0; 

        $json_length = strlen($json);

        for ($i = 0; $i < $json_length; $i++)
        {                               

            $pre = '';
            $suf = '';

            switch ($json[$i])
            {
                case '"':                               
                    $quote_state = !$quote_state;                                                           
                    break;

                case '[':                                                           
                    $level++;               
                    break;

                case ']':
                    $level--;                   
                    $pre = $ret;
                    $pre .= str_repeat($ind, $level);       
                    break;

                case '{':

                    if ($i - 1 >= 0 && $json[$i - 1] != ',')
                    {
                        $pre = $ret;
                        $pre .= str_repeat($ind, $level);                       
                    }   

                    $level++;   
                    $suf = $ret;                                                                                                                        
                    $suf .= str_repeat($ind, $level);                                                                                                   
                    break;

                case ':':
                    $suf = ' ';
                    break;

                case ',':

                    if (!$quote_state)
                    {  
                        $suf = $ret;                                                                                                
                        $suf .= str_repeat($ind, $level);
                    }
                    break;

                case '}':
                    $level--;   

                case ']':
                    $pre = $ret;
                    $pre .= str_repeat($ind, $level);
                    break;

            }

            $beauty_json .= $pre.$json[$i].$suf;

        }

        return $beauty_json;

    }   
}

///////////////////////////////////////////////////////////
// custom array functions
/////////////////////////////////////////////////////////// 
if( !function_exists('array_keys_exists') )
{
    /**
    * check if all keys are in array
    * 
    * @param array $keys
    * @param array $search
    */
    function array_keys_exists($keys, $search)
    { 
        if(!is_array($search))
        {
            return FALSE;
        }
        
        if(!is_array($keys))
        {
            return array_key_exists($keys, $search);
        }
        
        foreach($keys as $key)
        {             
            if(!array_key_exists($key, $search))
            {
                return FALSE;
            }
        }
        
        return TRUE;
    }
}

if( !function_exists('array_one_key_exists') )
{
    /**
    * check if any key is in array
    * 
    * @param array $keys
    * @param array $search
    */
    function array_one_key_exists($keys, $search)
    { 
        if(!is_array($search))
        {
            return FALSE;
        }
        
        if(!is_array($keys))
        {
            return array_key_exists($keys, $search);
        }
        
        foreach($keys as $key)
        {             
            if(array_key_exists($key, $search))
            {
                return TRUE;
            }
        }
        
        return FALSE;
    }
}

if( !function_exists('array_column_ct') ) 
{
    function array_column_ct($array , $column_key, $index_key=FALSE) 
    {      
        if(!is_array($array) || empty($array) || empty($column_key)) 
        {
            return FALSE;
        }
        
        $result = array();
        foreach($array as $a) 
        {
            //convert to general array
            $temp = (array)$a; 
            
            $new_item = null;
            if(is_array($column_key)) 
            {
                //copy items to new array item
                $new_item = null;
                if(is_object($a))
                {
                    $new_item = new stdClass();
                }
                else
                {
                    $new_item = array();
                }
                
                foreach($column_key as $key) 
                {
                    if(is_object($a))
                    {
                        $new_item->{$key} = $temp[$key];
                    }
                    else
                    {
                        $new_item[$key] = $temp[$key];
                    }
                }
            }
            else
            {
                if(isset($temp[$column_key]))
                {
                    $new_item = $temp[$column_key];
                }
            }
            
            if($index_key != FALSE && isset($temp[$index_key]))
            {
                $result[$temp[$index_key]] = $new_item;
            }
            else
            {
                $result[] = $new_item;
            }
        }
        return $result;
    }
}

if( !function_exists('array_unique_ct') ) 
{
    /**
    * remove duplicate values from array
    * 
    * @param mixed $array
    */
    function array_unique_ct($array) 
    {  
        if(empty($array) || !is_array($array))
        {
            return array();
        }
          
        $temp = array();
        foreach($array as $index => $item)
        {
            $key = md5(ct_json_encode($item));
            if(!in_array($key, $temp))
            {
                $temp[] = $key;
                continue;
            }
            
            unset($array[$index]);
        }
        
        return $array;        
    }
}

if( !function_exists('array_equal') ) 
{
    /**
    * check two array is equal
    * 
    * @param array $array1
    * @param array $array2
    */
    function array_equal($array1, $array2)
    {
       $diff1 = array_diff($array1, $array2);
       $diff2 = array_diff($array2, $array1);

       return
       (
          (count($diff1) === 0) &&
          (count($diff2) === 0)
       );
    }
}

///////////////////////////////////////////////////////////
// custom string functions
/////////////////////////////////////////////////////////// 
if( !function_exists('ct_strlen') )
{
    /**
    * override strlen function
    * 
    * @param string $str
    */
    function ct_strlen($str)
    { 
        return mb_strlen($str, 'utf-8');
    }
}

if( !function_exists('ct_substr') )
{
    /**
    * override substr
    * 
    * @param string $str
    * @param int    $start
    * @param int    $length
    */
    function ct_substr($str, $start, $length=PHP_INT_MAX)
    { 
        return mb_substr($str, $start, $length, 'utf-8');
    }
}

if( !function_exists('ct_str_limiter') )
{
    /**
    * override strlen function
    * 
    * @param string $str
    */
    function ct_str_limiter($str, $len = 500, $end_char = '&#8230;')
    { 
        if(ct_strlen($str) < $len)
        {
            return $str;
        }
        
        return ct_substr($str, 0, $len) . $end_char;
    }
}

if( !function_exists('escape_squote') )
{
    function escape_squote($str)
    {
        $search = array("'", '"');
        $str = str_replace($search, '', $str);    
        return $str;
    }
}