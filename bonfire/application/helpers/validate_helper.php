<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//--------------------------------------------------------------------
if ( ! function_exists('valid_id_number'))
{
    /**
    * check that the entered string is right chinese id card number
    * 
    * @param string $str
    * 
    * @return bool
    */
    function valid_id_number($str, $birthday='')
    {
        if(strlen($str) != 18) return FALSE;
    
        $weight = Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
        $valcode = Array(1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2);
        
        $sum = 0;
        for ($i = 0; $i < 17; $i++ ) 
        {
            $sum += $str[$i] * $weight[$i];
        }
        $checksum = $valcode[$sum % 11];

        if(strtolower(substr($str, -1, 1)) != strtolower($checksum)) 
        {
            return FALSE;
        }
        
        if($birthday != '')
        {
            $birthday_str = str_replace('-', '', $birthday);
            if(strpos($str, $birthday_str) != 6)
            {         
                return FALSE;
            }
        }
        
        return TRUE;
    }
}

if ( ! function_exists('valid_passport_number'))
{
    /**
    * check that the entered string is right passport number
    * 
    * @param string $str
    * 
    * @return bool
    */
    function valid_passport_number($str)
    {
        if(strlen($str) < 8 || strlen($str) > 12) return FALSE;
    
        return ( ! preg_match("/^([a-z0-9])+$/i", $str)) ? FALSE : TRUE;
    }
}

if ( ! function_exists('valid_password'))
{
    /**
    * check that the length of password
    * 
    * @param string $str
    * 
    * @return bool
    */
    function valid_password($str)
    {
        if(ct_strlen($str) > PASSWORD_MAX_LEN || ct_strlen($str) < PASSWORD_MIN_LEN)
        {      
            return FALSE;
        }
        
        return TRUE;
    }
}

if ( ! function_exists('include_special_chars'))
{
    /**
    * check that the entered string include special characters
    * 
    * @param string $str
    * 
    * @return bool
    */
    function include_special_chars($str)
    {
        if (preg_match('/[\'^£$%&*()}{@#~?><>,;:`|=_+¬-]"/', $str))
        {
            return TRUE;
        }
        
        return FALSE;
    }
}

if ( ! function_exists('valid_date'))
{
    /**
    * check that the entered string is valid date format
    * 
    * @param string $str    Date String
    * 
    * @return bool
    */
    function valid_date($str)
    {
        if ( preg_match("/([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})/", $str) ) 
        {
            $arr = explode("-", $str);    // splitting the array
            $yyyy = $arr[0];            // first element of the array is year
            $mm = $arr[1];              // second element is month
            $dd = $arr[2];              // third element is days
            if(checkdate($mm, $dd, $yyyy))
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        } 
        else 
        {
            return FALSE;
        }
    }
}

if ( ! function_exists('valid_datetime'))
{
    /**
    * check that the entered string is valid datetime format
    * 
    * @param string $str    Datetime String
    * 
    * @return bool
    */
    function valid_datetime($str)
    {
        $time_str = date(FORMAT_DATETIME, strtotime($str));
        if($time_str === '1970-01-01 01:00:00')
        {
            return FALSE;
        }
        
        return TRUE;
    }
}

if ( ! function_exists('valid_email'))
{
    function valid_email($str)
    {
        return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
    }
}

if ( ! function_exists('valid_emails'))
{
    function valid_emails($str)
    {
        if (strpos($str, ',') === FALSE)
        {
            return valid_email(trim($str));
        }
          
        foreach (explode(',', $str) as $email)
        {
            if (trim($email) != '' && valid_email(trim($email)) === FALSE)
            {
                return FALSE;
            }
        }

        return TRUE;
    }
}

if ( ! function_exists('valid_phone'))
{
    /**
    * check that the entered string is right chinese phone number
    * 
    * @param string $str
    * 
    * @return bool
    */
    function valid_phone($str)
    {
        if(!is_numeric($str))
        {
            return FALSE;
        }
        
        if(strlen($str) != 11)
        {
            return FALSE;
        }

        return TRUE;
    }
}
    
if ( ! function_exists('valid_phones'))
{
    /**
    * check that the entered string is right chinese phone numbers
    * 
    * @param string $str
    * 
    * @return bool
    */
    function valid_phones($str)
    {
        if (strpos($str, ',') === FALSE)
        {
            return valid_phone(trim($str));
        }

        foreach (explode(',', $str) as $phone)
        {
            if (trim($phone) != '' && valid_phone(trim($phone)) === FALSE)
            {
                return FALSE;
            }
        }

        return TRUE;
    }
}

if ( ! function_exists('valid_iphone_uuid'))
{
    function valid_iphone_uuid($uuid)
    {
        return (boolean) preg_match('/^\{?[0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12}\}?$/i', trim($uuid));
    }
}

if ( ! function_exists('valid_user_type'))
{
    /**
    * check type is valid user type for our system
    * 
    * @param int $type
    */
    function valid_user_type($type)
    {
        $user_types = array(USER_TYPE_TEACHER, USER_TYPE_MASTER, USER_TYPE_PARENT, USER_TYPE_CHILDREN);
        if(!in_array($type, $user_types))
        {
            return FALSE;
        }
        
        return TRUE;
    }
}

if ( ! function_exists('valid_array_string'))
{
    /**
    * get valid array string: ex: admin@gmail.com, gom@gmail.com,, => admin@gmail.com,gom@gmail.com
    * 
    * @param string $string
    */
    function valid_array_string($string)
    {
        if($string == '') return '';
        
        $result = array();
        $values = explode(',', $string);
        foreach($values as $value)
        {
            if(strlen(trim($value)) == 0) continue;
            
            $result[] = trim($value);
        }
        
        return implode(',', array_unique($result));
    }
}

if ( ! function_exists('is_ascii'))
{
    function is_ascii($str) 
    {
        return preg_match('/^([\x00-\x7F])*$/', $str);
    }
}

if ( ! function_exists('is_json'))
{
    /**
    * check if string is valid json data
    * 
    * @param string $string
    */
    function is_json($string) 
    {
        if(!is_string($string)) return FALSE;
        
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}

if( !function_exists('is_image') )
{
    /**
    * check if file is image
    * 
    * @param string $file_type
    */
    function is_image($file_type)
    {
        $png_mimes  = array('image/x-png');
        $jpeg_mimes = array('image/jpg', 'image/jpe', 'image/jpeg', 'image/pjpeg', 'application/octet-stream');

        if (in_array($file_type, $png_mimes))
        {
            $file_type = 'image/png';
        }

        if (in_array($file_type, $jpeg_mimes))
        {
            $file_type = 'image/jpeg';
        }

        $img_mimes = array(
            'image/gif',
            'image/jpeg',
            'image/png',
        );

        return (in_array($file_type, $img_mimes, TRUE)) ? TRUE : FALSE;
    }
}