<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if( !function_exists('get_year_level') )
{
    /**
    * calculate year level of grade based on grade and current year
    * 
    * @param int $grade
    */
    function get_year_level($grade)
    {
        $date1 = new DateTime("now");
        $date2 = new DateTime(date('Y').'-09-01');
        $base_level = ($date1 >= $date2)? intval(date('Y')) : intval(date('Y')) - 1;
        return ($base_level - $grade + 1);
    }
}

if (!function_exists('render_filter_first_letter'))
{
    /**
     * Displays an alpha list used to filter a list by first letter.
     *
     * @access public
     *
     * @param string $caption A string with the text to display before the list.
     *
     * @return void
     */
    function render_filter_first_letter($caption=null)
    {
        $ci =& get_instance();

        $out = '<span class="filter-link-list">';

        // All get params
        $params = $ci->input->get();

        // Current Filter
        if (isset($params['firstletter']))
        {
            $current = strtolower($params['firstletter']);
            unset($params['firstletter']);
        }
        else
        {
            $current = '';
        }

        // Build our url
        if (is_array($params))
        {
            $url_params = array();

            foreach ($params as $key => $value)
            {
                $url_params[urlencode($key)] = urlencode($value);
            }
            $url = current_url() .'?'. array_implode('=', '&', $url_params);
        }
        else
        {
            $url = current_url() .'?';
        }

        // If there's a current filter, we need to
        // replace the caption with a clear button.
        if (!empty($current))
        {
            $href = htmlentities($url, ENT_QUOTES, 'UTF-8');

            $out .= '<a href="'. $href .'" class="btn btn-small btn-primary">'. lang('bf_clear') .'</a>';
        }
        else
        {
            $out .= $caption;
        }

        // Source
        $letters = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

        // Create our list.
        foreach ($letters as $letter)
        {
            $href_url = $url . '&firstletter='. strtolower($letter);
            $href = htmlentities($href_url, ENT_QUOTES, 'UTF-8');

            $out .= '<a href="'. $href .'">';
            $out .= $letter;
            $out .= '</a>';
        }

        $out .= '</span>';

        echo $out;

    }//end render_filter_first_letter()

}

///////////////////////////////////////////////////////////
// get defince arrays
/////////////////////////////////////////////////////////// 
if( !function_exists('get_school_types') )
{
    /**
    * get array of defined school types
    * 
    * @param bool $with_empty
    */
    function get_school_types($with_empty=TRUE)
    {
        $result = array();
        if($with_empty) $result[''] = lang('msg_select');
        $result[SCHOOL_PRIMARY] = lang('ct_school_primary');
        $result[SCHOOL_JUNIOR]  = lang('ct_school_junior');
        $result[SCHOOL_SENIOR]  = lang('ct_school_senior');
        return $result;
    }
}

///////////////////////////////////////////////////////////
// sort functions
/////////////////////////////////////////////////////////// 
if( !function_exists('sort_classes') )
{
    function sort_classes($ordered_field, $orderby, $order)
    {
        if($order != "" && $ordered_field == $orderby)
            return "sorted {$order}";
        else
            return "sortable desc";
    }
}

if( !function_exists('sort_direction') )
{
    function sort_direction($ordered_field, $orderby, $order)
    {
        if($order == "" || $order == "desc" || $ordered_field != $orderby)
            return "asc";
        else
            return "desc";
    }
}