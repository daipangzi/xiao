<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

///////////////////////////////////////////////////////////
// common functions
/////////////////////////////////////////////////////////// 
if ( ! function_exists('generate_sms_code'))
{
    /**
    * generate phone verify code
    * 
    * @param mixed $len
    */
    function generate_sms_code($len=2)
    {
        if($len <= 1) $len = 2;
        
        $alpha_len = rand(1, $len-1);
        $num_len = $len - $alpha_len;
         
        $string1 = strtolower(random_string('alpha', $alpha_len));
        $string2 = random_string('numeric', $num_len);
        
        return str_shuffle($string1.$string2);
    }
} 

if( !function_exists('user_photo') )
{
    function user_photo($file_name, $user_type, $size=AVATAR_SIZE_SMALL)
    {
        if($file_name == '') return site_url(MEDIA_PATH . 'placeholder/100x100.gif');
        
        $sub_path = '';
        switch($user_type)
        {
            case USER_TYPE_TEACHER:
            case USER_TYPE_MASTER:
            default:
                $sub_path = 'teacher/';
                break;
            case USER_TYPE_CHILDREN:
                $sub_path = 'children/';
                break;
            case USER_TYPE_PARENT:
                $sub_path = 'parent/';
                break;
        }
        
        $fp = substr($file_name, 0, 1);
        $sp = substr($file_name, 1, 1);
        $suffix = $fp . '/' . $sp;
        
        $file_path = MEDIA_PATH . "photo/{$sub_path}{$suffix}/" . $file_name;
        if(!file_exists($file_path))
        {
            return site_url(MEDIA_PATH . 'placeholder/100x100.gif');
        }
        
        $url = "photos/{$file_name}?size={$size}&type={$user_type}";
        return site_url($url);
    }
}

if( !function_exists('delete_user_photo') )
{
    function delete_user_photo($file_name, $user_type)
    {
        if($file_name == '') return;
        
        $sub_path = '';
        switch($user_type)
        {
            case USER_TYPE_TEACHER:
            case USER_TYPE_MASTER:
            default:
                $sub_path = 'teacher/';
                break;
            case USER_TYPE_CHILDREN:
                $sub_path = 'children/';
                break;
            case USER_TYPE_PARENT:
                $sub_path = 'parent/';
                break;
        }
        
        $fp = substr($file_name, 0, 1);
        $sp = substr($file_name, 1, 1);
        $suffix = $fp . '/' . $sp;
        
        $file_path = MEDIA_PATH . "photo/{$sub_path}{$suffix}/" . $file_name;
        if(file_exists($file_path))
        {
            unlink($file_path);
        }
        
        return;
    }
}

if( !function_exists('valid_path') )
{
    /**
    * recursively create a long directory path
    * 
    * @param string $path
    */
    function valid_path($path) 
    {
        if (is_dir($path)) return true;
        
        $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
        $return = valid_path($prev_path);
        return ($return && is_writable($prev_path)) ? mkdir($path) : false;
    }
}

if ( ! function_exists('get_user_model'))
{
    function get_user_model($user_type)
    {
        $user_model = '';
        switch($user_type)
        {
            case USER_TYPE_TEACHER:
            case USER_TYPE_MASTER:
            default:
                $user_model = 'teacher_model';
                break;
            case USER_TYPE_CHILDREN:
                $user_model = 'children_model';
                break;
            case USER_TYPE_PARENT:
                $user_model = 'parent_model';
                break;
        }
        
        return $user_model; 
    }    
}

if ( ! function_exists('get_long_language_name'))
{
    /**
    * get long language name
    *     
    * @param string $short_name
    */
    function get_long_language_name($short_name)
    {
        $result = '';
        switch(strtoupper($short_name))
        {
            case LANG_CN:
                $result = 'chinese';
                break;
            case LANG_JP:
                $result = 'japanese';
                break;
            case LANG_KO:
                $result = 'korean';
                break;
            case LANG_EN:
            default:
                $result = 'english';
                break;
        }
        $result = 'english';
        return $result;
    }    
}

if ( ! function_exists('get_short_language_name'))
{
    /**
    * gert long language name
    *     
    * @param string $short_name
    */
    function get_short_language_name($long_name)
    {
        $result = '';
        switch(strtolower($long_name))
        {
            case 'chinese':
                $result = LANG_CN;
                break;
            case 'japanese':
                $result = LANG_JP;
                break;
            case 'korean':
                $result = LANG_KO;
                break;
            case 'english':
            default:
                $result = LANG_EN;
                break;
        }
        return $result;
    }    
}

if ( ! function_exists('generate_personal_identity'))
{
    function generate_personal_identity($user_type, $user_id)
    {
        $channel_type = '';
        switch($user_type)
        {
            case USER_TYPE_TEACHER:
            case USER_TYPE_MASTER:
            default:
                $channel_type = MSG_CHL_TEACHER;
                break;
            case USER_TYPE_CHILDREN:
                $channel_type = MSG_CHL_CHILDREN;
                break;
            case USER_TYPE_PARENT:
                $channel_type = MSG_CHL_PARENT;
                break;
        }
        
        return sprintf($channel_type, $user_id); 
    }
}

if ( ! function_exists('generate_parse_channel'))
{
    /**
    * generate channel for push notification
    * give parameters by senquence
    * 
    */
    function generate_parse_channel()
    {
        $args = func_get_args();
        if(count($args) == 0) return '';
        
        $channel = "a{$args[0]}";       
        if(isset($args[1]))
        {
            $channel .= "b{$args[1]}";
        }
        if(isset($args[2]))
        {
            $channel .= "c{$args[2]}";
        }
        
        if(isset($args[3]))
        {
            $channel .= "d{$args[3]}";
        }
        
        return $channel;
    } 
}

if ( ! function_exists('parse_personal_identity'))
{
    function parse_personal_identity($personal_identity)
    {
        $prefix  = substr($personal_identity, 0, 1);
        $user_id = str_replace(array($prefix, 'X'), '', $personal_identity);
        
        $user_type = 0;
        switch($prefix)
        {
            case 'M':
                $user_type = USER_TYPE_MASTER;
                break;
            case 'T':
                $user_type = USER_TYPE_TEACHER;
                break;
            case 'C':
                $user_type = USER_TYPE_CHILDREN;
                break;
            case 'P':
                $user_type = USER_TYPE_PARENT;
                break;
        }
        
        $result = array();
        $result['user_type'] = $user_type;
        $result['user_id'] = $user_id;
        
        return $result;
    } 
}

if ( ! function_exists('send_push_notification'))
{
    /**
    * send push notification 
    * 
    * @param string $channel
    * @param int    $type
    * @param string $text
    */                           
    function send_push_notification($channels, $text='')
    {
        $APPLICATION_ID = settings_item('parse.application_id');
        $REST_API_KEY   = settings_item('parse.api_key');
        $url    = settings_item('parse.url');
         
        if(!is_array($channels))
        {
            $channels = array($channels);
        }
         
        $data = array(
            'where' => array(
                'channels'  => array('$in' => $channels),
                'deviceType'=> array('$in' => array('android', 'ios')),
            ),
            'data'   => array(
                'alert' => $text,   
            ),
        );
        $_data = json_encode($data);
        
        $headers = array(
            'X-Parse-Application-Id: ' . $APPLICATION_ID,
            'X-Parse-REST-API-Key: ' . $REST_API_KEY,
            'Content-Type: application/json',
        );

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($curl);
        curl_close($curl);
        
        return $result;
    }
}

if ( ! function_exists('get_application_name'))
{
    function get_application_name($app_type)
    {
        $app_name = '';
        switch($app_type)
        {
            case USER_TYPE_TEACHER:
                $app_name = 'ct_teacher';
                break;
            case USER_TYPE_CHILDREN:
                $app_name = 'ct_children';
                break;
            case USER_TYPE_PARENT:
                $app_name = 'ct_parent';
                break;
        }
        return lang($app_name);
    }
}

///////////////////////////////////////////////////////////
// format functions
/////////////////////////////////////////////////////////// 
if ( ! function_exists('format_date'))
{
    /**
    * format date
    * 
    * @param mixed $time
    */
    function format_date($time)
    {
        if(!is_int($time))
        {
            $time = strtotime($time);
        }
        $str = date(FORMAT_DATE, $time);
        return $str;
    }
}

if ( ! function_exists('format_time'))
{
    /**
    * format time
    * 
    * @param mixed $time
    */
    function format_time($time)
    {
        if(!is_int($time))
        {
            $time = strtotime($time);
        }
        $str = date(FORMAT_DATETIME, $time);
        return $str;
    }
}

if ( ! function_exists('format_dropdown'))
{
    function format_dropdown($records, $empty_row=TRUE, $empty_text='')
    {
        $result = array();
        if($empty_row)
        {
            $result[''] = $empty_text;
        }
        
        if(!empty($records))
        {
            foreach($records as $r)
            {
                $result[$r->uid] = $r->name;
            }
        }
        
        return $result;
    }
}