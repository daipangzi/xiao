<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Bonfire
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   Bonfire
 * @author    Bonfire Dev Team
 * @copyright Copyright (c) 2011 - 2012, Bonfire Dev Team
 * @license   http://guides.cibonfire.com/license.html
 * @link      http://cibonfire.com
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------
class Base_Controller extends MX_Controller
{
    protected $controller = '';
    protected $short_language = 'en';    
    
    public function __construct()
    {
        parent::__construct();
               
        //load library
        $this->load->library('upload');
        $this->load->library('process');
    }
    
    /**
    * upload files
    * 
    * @param string $file
    * @param array  $config
    * @return bool
    */
    protected function upload_file(&$file, $config=array())
    {
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($file)) 
        {  
            $file = '';
            return FALSE;
        }
        
        $upload_data = $this->upload->data();
        $file = $upload_data['file_name'];
        return TRUE;
    }
    
    /**
    * upload user's photo
    * 
    * @param string $file
    * @param int    $user_type
    * @return bool
    */
    protected function upload_photo(&$file, $user_type)
    {   
        if(!valid_user_type($user_type))
        {
            $file = '';
            return FALSE;
        } 
        
        //get sub folder        
        $subfoler = '';
        switch($user_type)
        {
            case USER_TYPE_PARENT:
                $subfoler = 'parent/';
                break;
            case USER_TYPE_CHILDREN:
                $subfoler = 'children/';
                break;
            case USER_TYPE_TEACHER:
            case USER_TYPE_MASTER:
            default:
                $subfoler = 'teacher/';
                break;
        }
                     
        $path = MEDIA_TMP_PATH . "photo/{$subfoler}";
        if(!valid_path($path))
        {
            $file = '';
            return FALSE;
        }
            
        $config = array();
        $config['upload_path']  = $path;
        $config['max_size']     = AVATAR_MAX_SIZE;
        $config['max_width']    = AVATAR_MAX_WIDTH;
        $config['max_height']   = AVATAR_MAX_HEIGHT;
        $config['overwrite']    = TRUE;
        $config['file_name']    = md5($file . $user_type . $_FILES[$file]['name'] . time());
        $config['allowed_types']= AVATAR_ALLOWED_TYPES;
        
        return $this->upload_file($file, $config);
    }
    
    /**
    * move user's photo file from tmp folder to proper folder
    * 
    * @param string $file_name
    * @param int    $user_type
    */
    protected function move_photo($file_name, $user_type)
    {        
        if($file_name === '') return FALSE;
        
        //get sub folder path        
        $subfoler = '';
        switch($user_type)
        {
            case USER_TYPE_PARENT:
                $subfoler = 'parent/';
                break;
            case USER_TYPE_CHILDREN:
                $subfoler = 'children/';
                break;
            case USER_TYPE_TEACHER:
            case USER_TYPE_MASTER:
            default:
                $subfoler = 'teacher/';
                break;
        }
             
        $fp = substr($file_name, 0, 1);
        $sp = substr($file_name, 1, 1);
        $suffix = $fp . '/' . $sp;
                    
        $file = MEDIA_TMP_PATH . "photo/{$subfoler}" . $file_name;
        $target_path = MEDIA_PATH . "photo/{$subfoler}{$suffix}/"; 
        if(!file_exists($file) || !valid_path($target_path))
        {
            return FALSE;
        }
            
        $target_file = $target_path . $file_name;
        if(@rename($file, $target_file))                 
        {    
            //moved successfully
            $img_info = getimagesize($target_file);
            $width  = $img_info[0];
            $height = $img_info[1];
            if($width > AVATAR_MAX_WIDTH || $height > AVATAR_MAX_HEIGHT)
            {
                //if size is greater than defined size, then resize it
                $sizes = config_item('avatar_sizes');
                $config['image_library']  = 'gd2';
                $config['source_image']   = $target_file;
                $config['create_thumb']   = TRUE;
                $config['maintain_ratio'] = FALSE;
                $config['width']  = AVATAR_MAX_WIDTH;
                $config['height'] = AVATAR_MAX_HEIGHT;
                
                $this->load->library('image_lib');
                $this->image_lib->initialize($config);
                $this->image_lib->resize();  
            }  
        }
        else
        {
            return FALSE;
        }            
        
        return TRUE;
    }
    
    /**
    * send verify code
    * 
    * @param string $phones :seperated by comma
    * @param string $emails :seperated by comma
    */
    protected function send_verify_code($phones='', $emails='')
    {
        if($phones == '' && $emails == '')    
        {
            $this->return_error(ERR_NO_CONTACT_PATH);
        }
                        
        //load sms model
        $this->load->model('sms_model');
        
        //add new sms code to gbs_sms table
        $data = array(
            'verify_code' => generate_sms_code(VERIFY_CODE_LEN)
        );
        $sms_uid = $this->sms_model->insert($data);
        if($sms_uid === FALSE)
        {
            $this->return_error(ERR_INSERT_TO_DATABASE);
        }
        
        if(SEND_SMS === TRUE)
        {
            //send code in background mode
            $language_name = get_long_language_name($this->short_language);
            $command = sprintf(COMMAND_SEND_VERIFY, $language_name, $sms_uid, $phones, $emails);
            $process = new Process;
            $pcs_id  = $process->runInBackground($command);
        }
        
        return $sms_uid;
    }
}

/**
 * Base Controller
 *
 * This controller provides a controller that your controllers can extend
 * from. This allows any tasks that need to be performed sitewide to be
 * done in one place.
 *
 * Since it extends from MX_Controller, any controller in the system
 * can be used in the HMVC style, using modules::run(). See the docs
 * at: https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc/wiki/Home
 * for more detail on the HMVC code used in Bonfire.
 *
 * @package    Bonfire\Core\Controllers
 * @category   Controllers
 * @author     Bonfire Dev Team
 * @link       http://guides.cibonfire.com/helpers/file_helpers.html
 *
 */
class Web_base_Controller extends Base_Controller
{
    /**
     * Stores the previously viewed page's complete URL.
     *
     * @var string
     */
    protected $previous_page;

    /**
     * Stores the page requested. This will sometimes be
     * different than the previous page if a redirect happened
     * in the controller.
     *
     * @var string
     */
    protected $requested_page;

    /**
     * Stores the current user's details, if they've logged in.
     *
     * @var object
     */
    protected $current_user = NULL;

    //--------------------------------------------------------------------

    /**
     * Class constructor
     *
     */
    public function __construct()
    {
        Events::trigger('before_controller', get_class($this));

        parent::__construct();

        // Auth setup
        $this->load->model('users/User_model', 'user_model');
        $this->load->library('users/auth');

        // Load our current logged in user so we can access it anywhere.
        if ($this->auth->is_logged_in())
        {
            $this->current_user = $this->user_model->find($this->auth->user_id());
            $this->current_user->id = (int)$this->current_user->id;
            $this->current_user->user_img = gravatar_link($this->current_user->email, 22, $this->current_user->email, "{$this->current_user->email} Profile");

            // if the user has a language setting then use it
            if (isset($this->current_user->language))
            {
                $this->short_language = get_short_language_name($this->current_user->language);
                $this->config->set_item('language', $this->current_user->language);
            }

        }

        // Make the current user available in the views
        $this->load->vars( array('current_user' => $this->current_user) );

        // load the application lang file here so that the users language is known
        $this->lang->load('application');
        $this->lang->load('terms');
        $this->lang->load('message');

        /*
            Performance optimizations for production environments.
        */
        if (ENVIRONMENT == 'production')
        {
            $this->db->save_queries = FALSE;

            $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        }

        // Testing niceties...
        else if (ENVIRONMENT == 'testing')
        {
            $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        }

        // Development niceties...
        else if (ENVIRONMENT == 'development')
        {
            $this->load->driver('cache', array('adapter' => 'dummy'));
        }

        // Make sure no assets in up as a requested page or a 404 page.
        if ( ! preg_match('/\.(gif|jpg|jpeg|png|css|js|ico|shtml)$/i', $this->uri->uri_string()))
        {
            $this->previous_page = $this->session->userdata('previous_page');
            $this->requested_page = $this->session->userdata('requested_page');
        }

        // Pre-Controller Event
        Events::trigger('after_controller_constructor', get_class($this));
    }//end __construct()

    //--------------------------------------------------------------------
}//end Base_Controller
//--------------------------------------------------------------------

/**
 * Front Controller
 *
 * This class provides a common place to handle any tasks that need to
 * be done for all public-facing controllers.
 *
 * @package    Bonfire\Core\Controllers
 * @category   Controllers
 * @author     Bonfire Dev Team
 * @link       http://guides.cibonfire.com/helpers/file_helpers.html
 *
 */
class Front_Controller extends Web_base_Controller
{

    //--------------------------------------------------------------------

    /**
     * Class constructor
     *
     */
    public function __construct()
    {
        parent::__construct();

        Events::trigger('before_front_controller');
        
        $this->load->helper('form');
        $this->load->library('template');
        $this->load->library('assets');

        Template::set_theme($this->config->item('default_theme'));

        Events::trigger('after_front_controller');
    }//end __construct()
    //--------------------------------------------------------------------

}//end Front_Controller
//--------------------------------------------------------------------

/**
 * Authenticated Controller
 *
 * Provides a base class for all controllers that must check user login
 * status.
 *
 * @package    Bonfire\Core\Controllers
 * @category   Controllers
 * @author     Bonfire Dev Team
 * @link       http://guides.cibonfire.com/helpers/file_helpers.html
 *
 */
class Authenticated_Controller extends Web_base_Controller
{

    //--------------------------------------------------------------------

    /**
     * Class constructor setup login restriction and load various libraries
     *
     */
    public function __construct()
    {
        parent::__construct();

        // Make sure we're logged in.
        $this->auth->restrict();

        // Load additional libraries
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->CI =& $this;    // Hack to make it work properly with HMVC

        Template::set_theme($this->config->item('default_theme'));
    }//end construct()
    //--------------------------------------------------------------------


}//end Authenticated_Controller
//--------------------------------------------------------------------

/**
 * Admin Controller
 *
 * This class provides a base class for all admin-facing controllers.
 * It automatically loads the form, form_validation and pagination
 * helpers/libraries, sets defaults for pagination and sets our
 * Admin Theme.
 *
 * @package    Bonfire
 * @subpackage MY_Controller
 * @category   Controllers
 * @author     Bonfire Dev Team
 * @link       http://guides.cibonfire.com/helpers/file_helpers.html
 *
 */
class Admin_Controller extends Authenticated_Controller
{
    //--------------------------------------------------------------------

    /**
     * Class constructor - setup paging and keyboard shortcuts as well as
     * load various libraries
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('application');
        $this->load->helper('text');
        $this->load->helper('admin'); 

        $this->load->library('template');
        $this->load->library('assets');
        $this->load->library('contexts');
        $this->load->library('pagination');
        
        $this->lang->load('admin_message');
        $this->lang->load('admin_terms');
        
        // Pagination config
        $this->pager = array();
        $this->pager['full_tag_open']    = '<div class="pagination pagination-right"><ul>';
        $this->pager['full_tag_close']    = '</ul></div>';
        $this->pager['next_link']         = '&rarr;';
        $this->pager['prev_link']         = '&larr;';
        $this->pager['next_tag_open']    = '<li>';
        $this->pager['next_tag_close']    = '</li>';
        $this->pager['prev_tag_open']    = '<li>';
        $this->pager['prev_tag_close']    = '</li>';
        $this->pager['first_tag_open']    = '<li>';
        $this->pager['first_tag_close']    = '</li>';
        $this->pager['last_tag_open']    = '<li>';
        $this->pager['last_tag_close']    = '</li>';
        $this->pager['cur_tag_open']    = '<li class="active"><a href="#">';
        $this->pager['cur_tag_close']    = '</a></li>';
        $this->pager['num_tag_open']    = '<li>';
        $this->pager['num_tag_close']    = '</li>';

        $this->limit = $this->settings_lib->item('site.list_limit');

        // Profiler Bar?
        if (ENVIRONMENT == 'development')
        {
            if ($this->settings_lib->item('site.show_profiler') AND has_permission('Bonfire.Profiler.View'))
            {
                // Profiler bar?
                if ( ! $this->input->is_cli_request() AND ! $this->input->is_ajax_request())
                {
                    $this->load->library('Console');
                    $this->output->enable_profiler(TRUE);
                }
            }
        }

        // Basic setup
        Template::set_theme($this->config->item('template.admin_theme'), 'junk');
        
        //added custom parts
        $this->save_index_url();
    }//end construct()

    //--------------------------------------------------------------------
    
    /**
    * save current index url to session
    * 
    * @param string $url
    */
    protected function save_index_url($url='')
    {
        if($this->controller == '')
        {
            return;
        }
        
        if($url != '')
        {
            $this->session->set_userdata("{$this->controller}_index", $url);
            return;
        }
        
        if(!$this->session->userdata("{$this->controller}_index"))
        {
            if($this->controller_index != '')
            {
                $index_url = site_url(SITE_AREA.'/'.$this->controller_index);
                $this->session->set_userdata("{$this->controller}_index", $index_url);
            }
            else
            {
                $index_url = site_url(SITE_AREA);
                $this->session->set_userdata("{$this->controller}_index", $index_url);
            }
        }
    }
    
    /**
    * get saved index url from session
    * 
    */
    protected function get_index_url()
    {
        $url = $this->session->userdata("{$this->controller}_index");
        if(!$url)
        {
            site_url(SITE_AREA);
        }
        
        return $url;
    }
    
    /**
    * redirect to index page of each controller
    * 
    */
    protected function redirect_to_index()
    {
        $redirect = $this->session->userdata("{$this->controller}_index");
        if(!$redirect)
        {
            $redirect = SITE_AREA;
        }
        
        Template::redirect($redirect);
    }
    
    protected function setup_pagination($total_records, $current_rows, $url_suffix='')
    {
        $this->pager['base_url']    = site_url(SITE_AREA . "/{$this->controller_index}/index");
        $this->pager['total_rows']  = $total_records;
        $this->pager['per_page']    = $this->limit;
        $this->pager['uri_segment'] = 5;
        $this->pager['suffix']      = $url_suffix;
        $this->pager['first_url']   = site_url(SITE_AREA . "/{$this->controller_index}") . $url_suffix;
        $this->pager['current_rows'] = $current_rows;
        $this->pagination->initialize($this->pager);
    }
}//end Admin_Controller

class Admin_Clients_Controller extends Admin_Controller
{
    /**
    * index actions
    * 
    */
    protected function _index_action_process()
    {
        $permission = ucfirst($this->controller) . '.System.Edit';
        
        $fields = array('delete', 'activate', 'deactivate');
        if(!array_one_key_exists($fields, $_POST)) return;
        
        $checked = $this->input->post('checked');
        if (!is_array($checked) || count($checked) == 0) return;
                
        $action_method = '';
        $message_success = '';
        $message_fail = '';
        $user_model = $this->controller . '_model';
        $msg_prefix = 'msg_teacher_';
        if(isset($_POST['delete'])) 
        {
            $this->auth->restrict($permission);
        
            $action_method = 'delete';
            $message_success = $msg_prefix . 'delete_success';
            $message_fail = 'msg_teacher_delete_fail';
        }
        else if(isset($_POST['activate']))
        {
            $this->auth->restrict($permission);
        
            $action_method = 'activate';
            $message_success = $msg_prefix . 'activate_success';
            $message_fail = $msg_prefix . 'activate_fail';
        }
        else if(isset($_POST['deactivate'])) 
        {
            $this->auth->restrict($permission);
        
            $action_method = 'deactivate';
            $message_success = $msg_prefix . 'deactivate_success';
            $message_fail = $msg_prefix . 'deactivate_fail';
        }
        
        $messages = '';
        foreach($checked as $pid)
        {
            $user = $this->$user_model->find($pid);
            if(empty($user))
            {
                continue;
            }
            
            if($action_method === 'activate' && (int)$user->user_status === STATUS_ACTIVE)
            {
                $messages .= sprintf(lang($msg_prefix . 'activate_already'), $user->user_name) . "<br>";
                continue;
            }
            else if($action_method === 'deactivate' && (int)$user->user_status === STATUS_INACTIVE)
            {
                $messages .= sprintf(lang($msg_prefix . 'deactivate_already'), $user->user_name) . "<br>";
                continue;
            }
            
            $status = $this->$user_model->$action_method($pid);
            if($status)
            {                
                $messages .= sprintf(lang($message_success), $user->user_name) . "<br>";
            }
            else
            {
                $messages .= sprintf(lang($message_fail), $user->user_name) . "<br>";
            }
        }
        
        if($messages !== '')
        {
            Template::set_message($messages, 'success');
        }
    }
            
    /**
    * save user photo
    * 
    * @param int $uid
    */
    protected function _save_user_photo($uid, $user_type)
    {   
        // make sure we only pass in the fields we want
        $data = array();
        
        //upload images  
        $action = $this->input->post('image_action');
        if($action == 'changed') 
        {  
            $image_name = $this->input->post('image_name');
            if($this->move_photo($image_name, $user_type))
            {
                $data['user_photo'] = $image_name;
            }
            else
            {
                return FALSE;
            }
        } 
        else if($action == 'deleted')
        {
            $data['user_photo'] = '';
        }
        
        $user_model = $this->controller . '_model'; 
        return $this->$user_model->update($uid, $data);
    }
    
    /**
    * update user password
    * 
    * @param int $uid
    */
    protected function _save_user_password($uid)
    {   
        $pwd_extra = '';
        if(ct_strlen($this->input->post('user_passwd')) > 0)
        {
            $pwd_extra .= "|required|min_length[5]";
            $this->form_validation->set_rules('user_passwd_confirm', 'lang:ct_password_confirm', 'required|trim|strip_tags|matches[user_passwd]');
        }
        $this->form_validation->set_rules('user_passwd','lang:ct_user_password',"trim|max_length[40]|strip_tags{$pwd_extra}");
        
        if ($this->form_validation->run() === FALSE)
        {
            return FALSE;
        }
                
        // make sure we only pass in the fields we want
        $data = array();
        $data['user_passwd'] = $this->input->post('user_passwd');
        
        $user_model = $this->controller . '_model'; 
        return $this->$user_model->update($uid, $data);
    }
}
//--------------------------------------------------------------------

/**
 * Mobile Controller
 *
 */
class Mobile_Controller extends Base_Controller
{
    //--------------------------------------------------------------------
    protected $params = FALSE;
    protected $user   = FALSE; 
    
    /**
     * Class constructor
     *
     */
    public function __construct($check_version=TRUE)
    {
        parent::__construct();
        
        //load helper
        $this->load->helper('security'); 
        $this->load->helper('string'); 
        $this->load->helper('mobile'); 
        
        //load model
        $this->load->model('log_login_model');
        $this->load->model('log_action_model');
        
        $this->load->model('teacher_model');
        $this->load->model('children_model');
        $this->load->model('parent_model');

        //check the request from mobile 
        $this->_check_request_header($check_version);
    }//end __construct()
    
    /**
    * return result
    * 
    * @param array $result
    */
    protected function return_result($result)
    {
        $return = array();
        $return['result'] = $result;
        $return['errors'] = array('error_type'=>0, 'error_details'=>'');
        
        echo ct_json_encode($return);
        exit;
    }
    
    /**
    * return result with error
    * 
    * @param int $err_no
    */
    protected function return_error($error_no)
    {
        $errors = array();
        $errors['error_type']    = $error_no;
        $errors['error_details'] = $this->get_error_message($error_no); 
        
        $return = array();
        $return['result'] = array();
        $return['errors'] = $errors;
        
        echo ct_json_encode($return);
        exit;
    }
    
    /////////////////////////////////////////////////////////////////////
    //ACCESSARRIES
    /////////////////////////////////////////////////////////////////////
    /**
    * check all user fields in data array
    *  
    * @param  array $data
    * @return bool
    */
    protected function check_user_fields_in($data, $type)
    {
        //check user types if it is valid
        if(!valid_user_type($type))
        {
            return FALSE;
        }

        $fields = array('user_id', 'user_name', 'user_sex', 'user_birthday', 'user_email', 'user_phone', 'user_passwd', 'user_photo');            
        if($type == USER_TYPE_CHILDREN)
        {
            $new = array('school_uid', 'children_class');
            $fields = array_merge($fields, $new);
        }
        else if($type == USER_TYPE_TEACHER)
        {
            $new = array('school_uid', 'is_charge', 'charge_class', 'is_subject', 'subject_uid', 'subject_class');
            $fields = array_merge($fields, $new);
        }
        
        if(!array_keys_exists($fields, $data)) 
        {
            return FALSE;
        }
        
        return TRUE;
    }
    
    /**
    * get number of users by type
    * 
    * @param int $school_uid
    */
    protected function get_number_of_users($school_uid)
    {
        $result = array();
        $teachers = $this->teacher_model->filter_school($school_uid)->count_all();
        $children_uids = $this->children_model
            ->select('children_uuid')
            ->filter_school($school_uid)
            ->with_class()
            ->find_all(RETURN_TYPE_ARRAY);
        $children_uids = array_column_ct($children_uids, 'children_uuid');
        $parent_uids = $this->children_model->get_children_parent_uids($children_uids);
        $parents = $this->parent_model->filter_user_uids($parent_uids)->count_all();

        $result['teachers'] = $teachers;
        $result['children'] = count($children_uids);
        $result['parents']  = $parents;
        
        return $result;
    }
    
    /**
    * get error message by error_no
    * 
    * @param int $err_no
    */
    protected function get_error_message($error_no)
    {
        if(TEST_MODE === FALSE)
        {
            return '';
        }
        
        //get matched error message
        $this->config->load('error_msg');
        $error_messges = config_item('error_msg');
        if(isset($error_messges[$error_no]))
        {
            return $error_messges[$error_no];
        }
        
        return $error_messges[ERR_UNKNOWN]; //Unknown error
    }
    
    /**
    * get user record from teacher, parent, children table
    * 
    * @param string $user_id
    * @param int    $user_type
    */
    protected function get_user_record($user_id, $user_type)
    {
        $user_model = get_user_model($user_type);
        
        $select = "*, IFNULL(user_birthday, '') user_birthday, {$this->user_photo_field($user_type)}";
        $query  = $this->{$user_model}->select($select, FALSE);
        if($user_type === USER_TYPE_CHILDREN)
        {
            $query->with_class();
        }
        return $query->find_by_userid($user_id);
    } 
    
    /**
    * get user_photo sql string
    * 
    * @param int $user_type
    * @param string $alias
    */
    protected function user_photo_field($user_type, $alias='user_photo_url')
    {
        $sub_path = 'photo/';
        switch($user_type)
        {
            case USER_TYPE_TEACHER:
            case USER_TYPE_MASTER:
            default:
                $sub_path .= 'teacher/';
                break;
            case USER_TYPE_CHILDREN:
                $sub_path .= 'children/';
                break;
            case USER_TYPE_PARENT:
                $sub_path .= 'parent/';
                break;
        }
        $base_url = site_url(MEDIA_PATH_URL . $sub_path) . '/';
        
        $sql = "IF(user_photo='', '', CONCAT('{$base_url}', LEFT(user_photo,1), '/', MID(user_photo,2,1), '/', user_photo)) {$alias}";
        return $sql;
    }
    
    /**
    * get last payment info for children
    * 
    * @param string $children_id
    */
    protected function last_payment_of_children($children_id)
    {
        //TODO
        $result = array();
        return $result;
    }
    
    /////////////////////////////////////////////////////////////////////
    //VALIDATE PART
    /////////////////////////////////////////////////////////////////////
    /**
    * validate user parameters
    * 
    * @param array $data
    * @return int
    */
    protected function validate_user_fields_in($data)
    {
        //user_id
        if(!valid_id_number($data['user_id']) && !valid_passport_number($data['user_id']))
        {
            return INVALID_USER_ID;
        }
         
        //general fields      
        if(($return = $this->validate_profile_general($data)) !== NO_ERROR)
        {
            return $return;
        }
        
        //user_password
        if(!valid_password($data['user_passwd']))
        {      
            return INVALID_USER_PASSWORD;
        }
          
        //user_photo
        $file = $data['user_photo'];
        if(($return = $this->validate_profile_photo($file)) !== NO_ERROR)
        {
            return $return;
        }
        
        return NO_ERROR;
    }
    
    /**
    * validate general fields of profile
    * 
    * @param mixed $data
    * @return mixed
    */
    protected function validate_profile_general($data)
    {
        //user_name
        if(include_special_chars($data['user_name']) 
            || ct_strlen($data['user_name']) > USERNAME_MAX_LEN 
            || ct_strlen($data['user_name']) < USERNAME_MIN_LEN)
        {      
            return INVALID_USER_NAME;
        }
        
        //user_sex
        if(!in_array($data['user_sex'], array(MAN, WOMAN)))
        {      
            return INVALID_USER_SEX;
        }
        
        //user_birthday
        if($data['user_birthday'] != '' && !valid_date($data['user_birthday']))
        {
            return INVALID_USER_BIRTHDAY;
        }
          
        //user_address
        if(isset($data['user_address'])) 
        {
            if(include_special_chars($data['user_address']) 
                || ct_strlen($data['user_address']) > ADDR_MAX_LEN)
            {      
                return INVALID_USER_ADDRESS;
            }
        }
                     
        //user_email
        if($data['user_email'] != '' && !valid_email($data['user_email']))
        {
            return INVALID_USER_EMAIL;
        }
        
        //user_phone
        if(!valid_phone($data['user_phone']))
        {
            return INVALID_USER_PHONE;
        }
        
        return NO_ERROR;
    }
    
    /**
    * user's photo validation
    * 
    * @param file $file
    * @return error_no or TRUE
    */
    protected function validate_profile_photo($file)
    {
        if(isset($_FILES[$file]) && $_FILES[$file]['size'] > 0 && $_FILES[$file]['name'] != '')
        {
            $photo = $_FILES[$file];
            $name  = $photo['name'];
            $size  = $photo['size']/1024; //KB
            $type  = $photo['type'];
            
            if(!is_image($type))
            {
                return INVALID_USER_PHOTO_TYPE;   
            }
            
            $size_info = getimagesize($photo['tmp_name']);
            if($size > AVATAR_MAX_SIZE)
            {
                return INVALID_USER_PHOTO_SIZE;
            }
              
            if(($size_info[0] > AVATAR_MAX_WIDTH) || ($size_info[1] > AVATAR_MAX_HEIGHT))
            {
                return INVALID_USER_PHOTO_DIMENSION;   
            }
            
            if(ct_strlen($name) > AVATAR_MAX_NAME_LENGTH)
            {
                return INVALID_USER_PHOTO_NAME;
            }
        }
        else
        {
            if($file !== '')
            {
                return INVALID_USER_PHOTO_NO_EXIST;
            }
        }
        
        return NO_ERROR;    
    }
    
    /////////////////////////////////////////////////////////////////////
    //AUTH PART
    /////////////////////////////////////////////////////////////////////    
    /**
    * restrict for logged users
    * 
    * @param bool $check_login_status
    */
    protected function restrict($check_login_status=TRUE)
    {
        $client    = $this->params['client'];
        $user_id   = $client['user_id'];
        $user_type = (int)$client['user_type'];
        $login_uid = $client['connection_uid'];
        
        $device_type = $client['device_type'];
        $device_info = $client['device_info'];
        
        //check valid id number
        if(!valid_id_number($user_id) && !valid_passport_number($user_id))
        {
            $this->return_error(INVALID_USER_ID);
        }
        
        //check if user is in proper school
        $this->user = $this->get_user_record($user_id, $user_type);
        if(empty($this->user))
        {
            $this->return_error(FORCE_INVALID_USER);
        }
        
        if($check_login_status === TRUE)
        {
            //check if logged in user
            $login = $this->log_login_model
                ->filter_uid($login_uid)
                ->filter_userid($user_id)
                ->filter_user_type($user_type)
                ->filter_device_type($device_type)
                ->filter_device_info($device_info)
                ->find_row();
                
            if(empty($login))
            {
                $this->return_error(FORCE_NO_LOGIN);
            }
            
            $login_status = (int)$login->active_status;
            if($login_status === LOGIN_STATUS_OUT)
            {
                $this->return_error(FORCE_NO_LOGIN);
            }
            
            //duplicate login
            if($login_status === LOGIN_STATUS_DUPLICATE)
            {
                $this->return_error(FORCE_DUPLICATE);
            }
            
            //logged out by admin
            if($login_status === LOGIN_STATUS_OUT_BY_ADMIN)
            {
                $this->return_error(FORCE_BY_ADMIN);
            }
            
            //when children's school/class is updated by one of parent
            if($user_type === USER_TYPE_CHILDREN && $login_status === LOGIN_STATUS_YOU_UPDATED)
            {
                $this->return_error(FORCE_CHILDREN_CLASS_UPDATED);
            }
            
            //when children's school/class is updated by one of parent, then his all parent have to relogin to system
            if($user_type === USER_TYPE_PARENT && $login_status === LOGIN_STATUS_YOUR_CHILDREN_UPDATED)
            {
                $this->return_error(FORCE_PARENT_CHILDREN_UPDATED);
            }
        }
        
        if($user_type === USER_TYPE_PARENT)
        {
            $this->user->school_uid = $client['school_uid'];        
        }
        
        //check valid school
        $school = $this->school_model->find($this->user->school_uid);
        if(empty($school))
        {
            $this->return_error(FORCE_INVALID_SCHOOL);            
        }           
        
        if((int)$school->active_status === STATUS_INACTIVE)
        {
            $this->return_error(FORCE_SCHOOL_INACTIVE);
        }
    
        //check if user status is inactived
        if((int)$this->user->user_status === STATUS_INACTIVE)
        {
            $this->return_error(FORCE_INACTIVE);    
        }
    }
    
    /////////////////////////////////////////////////////////////////////
    //PRIVATE PART
    /////////////////////////////////////////////////////////////////////
    /**
    * check params if all valid
    * 
    */
    private function _check_request_header($check_version=TRUE)
    {
        if((int)$this->settings_lib->item('site.status') === STATUS_NO)
        {
            $this->return_error(SYS_MAINTAINENCE); //maintenance mode
        }
        
        if(!$this->input->post('param'))
        {
            $this->return_error(ERR_ILLEGAL_REQUEST);
        }

        $this->params = ct_json_decode_param($this->input->post('param'));
        $params       = $this->params;
        if(!isset($params['data']) || !isset($params['client']))
        {
            $this->return_error(ERR_ILLEGAL_REQUEST);
        }
            
        //check all fields exists in request   
        $device_fields = array('connection_uid', 'device_type', 'device_info', 'user_id', 'user_type', 'lang', 'version');
        if(!array_keys_exists($device_fields, $params['client']))
        {
            $this->return_error(ERR_MISSING_CLIENT_PARAMETERS);
        }
        
        //check device type
        $valid_devices = array(DEVICE_ANDROID, DEVICE_IPHONE);
        if(!in_array($params['client']['device_type'], $valid_devices))
        {
            $this->return_error(INVALID_DEVICE);
        }
        
        //check user type
        $valid_user_types = array(USER_TYPE_TEACHER, USER_TYPE_MASTER, USER_TYPE_PARENT, USER_TYPE_CHILDREN);
        if(!in_array($params['client']['user_type'], $valid_user_types))
        {
            $this->return_error(INVALID_USER_TYPE);    
        }
        
        //check language
        $valid_languages = array(LANG_CN, LANG_KO, LANG_EN, LANG_JP);
        if(!in_array(strtoupper($params['client']['lang']), $valid_languages))
        {
            $this->return_error(INVALID_LANGUAGE);
        }
        
        $this->short_language = strtolower($params['client']['lang']);
        $language_name = get_long_language_name($this->short_language);
        $this->config->set_item('language', $language_name);
        
        if($check_version === TRUE)
        {
            //check minimal apk version that can response exactly from request
            $version = (double)$params['client']['version'];
            if($version < $this->_get_min_apk_version($params['client']['user_type']))
            {
                $this->return_error(APK_REQUIRE_NEW_VERSION);
            }
        }
        
        //check ip address
        if($this->input->ip_address() == '0.0.0.0')
        {
            $this->return_error(ERR_INVALID_IPADDR);
        }
        
        //make userid to lower
        $params['client']['user_id'] = strtolower($params['client']['user_id']);
        if(isset($params['data']['user_id']))
        {
            $params['data']['user_id'] = strtolower($params['data']['user_id']);
        }
    }
    
    /**
    * get minimal apk version for good result
    * 
    * @param int $user_type
    */
    private function _get_min_apk_version($user_type)
    {
        $version = 0;
        switch($user_type)
        {
            case USER_TYPE_CHILDREN:
                $version = APK_CHILDREN_VERSION;
                break;
            case USER_TYPE_PARENT:
                $version = APK_PARENT_VERSION;
                break;
            case USER_TYPE_MASTER:
            case USER_TYPE_TEACHER:
            default:
                $version = APK_TEACHER_VERSION;
                break;
        }
        return $version;
    }
 }//end Mobile_Controller
/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */