<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['error_msg'] = array(
    //APK
    '10001' => 'There is no apk file.',
    '10002' => 'Current version is too low to work, please update it.',
    
    //System
    '10101' => 'System is under maintainence.',
    
    //Init
    '12001' => 'Unknown error.',
    '12002' => 'Illegal Request',
    '12003' => 'Missing parameters to work.',
    '12004' => 'Neigther email nor phone.',
    '12005' => 'User type doesn not match.',
    '12006' => 'File upload fail.',
    '12007' => 'Missing parameters of client packet.',
    '12091' => 'There is no grade and class for school.',
    '12092' => 'There is no subjects for school.',
    
    '12101' => 'Invalid device type',
    '12102' => 'Invalid ip address',
    '12103' => 'Invalid user',
    '12104' => 'Invalid user type',
    '12106' => 'Invalid school is selected.',
    '12111' => 'Selected subject is invalid.',
    '12112' => 'There is invalid class in selected.',
    
    //Force Logout
    '12201' => 'There is new login on other device.',
    '12202' => 'Your role is changed. Relogin is required.',
    '12203' => 'You are invalid user. Relogin is required.',
    '12204' => 'You are not logged in user. Relogin is required.',
    '12205' => 'Your account is inactived. Relogin is required.',
    '12206' => 'Your school is not valid. Try to relogin.',
    '12207' => 'Your school is inactived. Try to relogin.',
    '12208' => 'You are now logged out by system. Try again.',
    '12221' => 'You have not children.',
    '12222' => 'Your one of children\'s class is updated. Relogin is required.',
    '12231' => 'Your account is expired.',
    '12232' => 'Your class is inactived.',
    '12233' => 'Your class is updated by one of your parent.',
    
    //Message
    '12250' => 'There is no permission to send message.',
    '12251' => 'Bad message sending request',
    '12252' => 'There is no selected participants.',
    '12253' => 'System can not create or get id of chat.',
    '12254' => 'Invalid chat session.',
    
    //Login
    '13101' => 'Your school is invalid currently. Please contact to support team.',
    '13102' => 'System can not get login uid.',
    
    //Forgot
    '13111' => 'Concat path is not selected.',
    '13112' => 'Adding forgot data error.',
    
    //Profile
    '13141' => 'Teacher role is invalid.',
    '13181' => 'Requested children is not yours.',
    '13182' => 'Requested children is not in selected school.',
    '13183' => 'There is only one children. So can\'t remove him.',
    
    //Register
    '13201' => 'Buffer is expired.',
    '13202' => 'Buffer is no logner exists.',
    '13203' => 'Buffer is not for you.',
    '13211' => 'No charge class is selected.',
    '13212' => 'There is invalid charge class.',
    '13213' => 'No charge subject is selected.',
    '13214' => 'Invalid charge subject.',
    '13215' => 'Charge subject is invalid.',
    ////parent
    '13221' => 'Missing parent info.',
    '13222' => 'Missing children info.',
    '13223' => 'Missing father parameters.',
    '13224' => 'Missing mother parameters.',
    '13225' => 'Missing children parameters.',
    '13226' => 'Invald children\'s class.',
    
    //User Fields
    '19105' => 'Invalid user id',
    '19106' => 'Invalid user name',
    '19107' => 'Invalid user sex',
    '19108' => 'Invalid user birthday',
    '19109' => 'Invalid user address',
    '19110' => 'Invalid user email',
    '19111' => 'Invalid user phone',
    '19112' => 'Invalid user password',
    '19113' => 'Invalid user photo type',
    '19114' => 'Invalid user photo size',
    '19115' => 'Invalid user photo dimession',
    '19116' => 'Invalid user photo name',
    '19117' => 'User photo doesn\'s exist',
    
    '19205' => 'Invalid father id',
    '19206' => 'Invalid father name',
    '19207' => 'Invalid father sex',
    '19208' => 'Invalid father birthday',
    '19209' => 'Invalid father address',
    '19210' => 'Invalid father email',
    '19211' => 'Invalid father phone',
    '19212' => 'Invalid father password',
    '19213' => 'Invalid father photo type',
    '19214' => 'Invalid father photo size',
    '19215' => 'Invalid father photo dimession',
    '19216' => 'Invalid father photo name',
    '19217' => 'User photo doesn\'s exist',
    
    '19305' => 'Invalid mother id',
    '19306' => 'Invalid mother name',
    '19307' => 'Invalid mother sex',
    '19308' => 'Invalid mother birthday',
    '19309' => 'Invalid mother address',
    '19310' => 'Invalid mother email',
    '19311' => 'Invalid mother phone',
    '19312' => 'Invalid mother password',
    '19313' => 'Invalid mother photo type',
    '19314' => 'Invalid mother photo size',
    '19315' => 'Invalid mother photo dimession',
    '19316' => 'Invalid mother photo name',
    '19317' => 'User photo doesn\'s exist',
    
    '19405' => 'Invalid children id',
    '19406' => 'Invalid children name',
    '19407' => 'Invalid children sex',
    '19408' => 'Invalid children birthday',
    '19409' => 'Invalid children address',
    '19410' => 'Invalid children email',
    '19411' => 'Invalid children phone',
    '19412' => 'Invalid children password',
    '19413' => 'Invalid children photo type',
    '19414' => 'Invalid children photo size',
    '19415' => 'Invalid children photo dimession',
    '19416' => 'Invalid children photo name',
    '19417' => 'User photo doesn\'s exist',
);