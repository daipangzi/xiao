<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


define('BONFIRE_VERSION', 'v0.6.1');

/*
	The 'App Area' allows you to specify the base folder used for all of
	the contexts in the app. By default, this is set to '/admin', but this
	does not make sense for all applications.
*/
define('SITE_AREA', 'admin');

/*
	The 'IS_AJAX' constant allows for a quick simple check to verify that the
	request is infact a XHR Request, it can be used to help secure your AJAX
	methods by just verifying that 'IS_AJAX' == TRUE.
*/

$ajax_request = ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ? TRUE : FALSE;
define('IS_AJAX' , $ajax_request );
unset ( $ajax_request );

/**
* system constants
*/

//////////////////////////////////////////////////////////
//SYSTEM CONSTANTS
//////////////////////////////////////////////////////////

//min apk versions
define ('APK_TEACHER_VERSION', 1.35);
define ('APK_CHILDREN_VERSION', 1.33);
define ('APK_PARENT_VERSION', 1.35);

//mode
define ('TEST_MODE', TRUE);
define ('SEND_SMS',  TRUE);
define ('MAINTENANCE', FALSE);

//path
define ('MEDIA_PATH', './media/');
define ('MEDIA_PATH_URL', '/media/');
define ('MEDIA_TMP_PATH', './media/tmp/');
define ('MEDIA_CACHE_PATH', './media/cache/');

$src_path = "/worldwide/source";
define ('COMMAND_SEND_VERIFY', "php5 {$src_path}/direct/cmd_send_verify_code.php %s %s %s %s");
define ('COMMAND_SEND_SECURITY', "php5 {$src_path}/direct/cmd_send_security.php %s %s %s %s %s");
define ('COMMAND_PUSH_NOTIFICATION', "php5 {$src_path}/direct/cmd_send_notification.php %s %s %s %s");

define ('FORMAT_DATETIME', 'Y-m-d H:i:s');
define ('FORMAT_DATE', 'Y-m-d');

define ('LANG_EN', 'EN');
define ('LANG_CN', 'CN');
define ('LANG_KO', 'KO');
define ('LANG_JP', 'JP');

//consts
define ('MAN', 1);
define ('WOMAN', 2);
define ('PARENT_FATHER', 1);
define ('PARENT_MOTHER', 2);

define ('DEVICE_ANDROID', 1);
define ('DEVICE_IPHONE', 2);
define ('DEVICE_PC', 3);

define ('STATUS_SUCCESS', 1);
define ('STATUS_FAIL', 0);
define ('STATUS_ERROR', -1);
//////////////////////////////
define ('STATUS_FORCED', 3); 
define ('STATUS_ACTIVE', 1);  
define ('STATUS_INACTIVE', 2);
//////////////////////////////
define ('STATUS_YES', 1);
define ('STATUS_NO', 0);
define ('NO_CHANGE', -1);

define ('LOGIN_STATUS_ONLINE', 1);
define ('LOGIN_STATUS_OUT', 2);
define ('LOGIN_STATUS_DUPLICATE', 3);
define ('LOGIN_STATUS_YOU_UPDATED', 4);
define ('LOGIN_STATUS_YOUR_CHILDREN_UPDATED', 5);
define ('LOGIN_STATUS_OUT_BY_ADMIN', 6);

define ('SECURITY_PATH_BOTH', 3);
define ('SECURITY_PATH_PHONE', 1);
define ('SECURITY_PATH_EMAIL', 2);

define ('VERIFY_SUCCESS', 1);
define ('VERIFY_FAIL', 0);
define ('VERIFY_EXPIRED', -1);

define ('USER_TYPE_TEACHER', 1);
define ('USER_TYPE_MASTER', 9);
define ('USER_TYPE_CHILDREN', 10);
define ('USER_TYPE_PARENT', 100);

define ('MSG_INIT_DAYS', 7);
define ('MSG_CHL_ALL', 'AL');
define ('MSG_CHL_ALL_TEACHER', 'TA');
define ('MSG_CHL_ALL_CHILDREN', 'CA');
define ('MSG_CHL_ALL_PARENT', 'PA');
define ('MSG_CHL_TEACHER', 'T%sX');
define ('MSG_CHL_CHILDREN', 'C%sX');
define ('MSG_CHL_PARENT', 'P%sX');

define ('MSG_NORMAL', 0);
define ('MSG_NOTIFICATION', 1);
define ('MSG_HOMEWORK', 2);

define ('ROLE_CHARGE', 1);
define ('ROLE_SUBJECT', 2);
define ('TEACHER_SUBJECT_REQUEST', 1);
define ('TEACHER_CHARGE_CLASS_REQUEST', 2);
define ('TEACHER_SUBJECT_CLASS_REQUEST', 3);
define ('TEACHER_SCHOOL_REQUEST', 4);

define ('CHILDREN_SERVICE_EXPIRED', 1);
define ('CHILDREN_SERVICE_AVAILABLE', 0);
define ('CHILDREN_PARENTS_LIMIT', 4); //peoples
define ('CHILDREN_TEST_EXPIRE_DAYS', 4); //days

define ('NO_REGISTERED', 0);
define ('NO_EXIST', 0);
define ('NO_ERROR', 12000);
define ('NONE', 0);
define ('SELECTED', 1);

define ('RETURN_TYPE_OBJECT', 0);
define ('RETURN_TYPE_ARRAY', 1);

define ('CHANNEL_TYPE_LOGOUT', 0);
define ('CHANNEL_TYPE_MESSAGE', 1);

define ('BUFFER_EXPIRE_TIME', 30);          //mins
define ('BUFFER_LIMIT_REQUEST_NUMS', 5);    //times
define ('LOGIN_OUT_EXPIRE_TIME', 15);       //hrs
define ('LOGIN_ATTEMPT_LIMIT_NUMS', 10);    //times
define ('LOGIN_ATTEMPT_CYCLE', 10);         //mins

//validations
define ('VERIFY_CODE_LEN', 6);  //letters
define ('USERNAME_MIN_LEN', 1); //letters
define ('USERNAME_MAX_LEN', 50);//letters
define ('PASSWORD_MIN_LEN', 5); //letters
define ('PASSWORD_MAX_LEN', 50);//letters
define ('ADDR_MAX_LEN', 100);   //letters
define ('MSG_ATTACH_MAX_SIZE', 20*1024);   //20MB

define ('SMS_DEVICE_DELAY_TIME', 5*1000*1000);  //5sec

//school types
define ('SCHOOL_PRIMARY', 1);
define ('SCHOOL_JUNIOR', 2);
define ('SCHOOL_SENIOR', 3);

define ('AVATAR_SIZE_LARGE', 500);
define ('AVATAR_SIZE_SMALL', 100);
define ('AVATAR_MAX_SIZE', 1000);
define ('AVATAR_MAX_WIDTH', 500);
define ('AVATAR_MAX_HEIGHT', 500);
define ('AVATAR_MAX_NAME_LENGTH', 255);
define ('AVATAR_ALLOWED_TYPES', 'jpg,jpeg,png,gif,bmp');



//////////////////////////////////////////////////////////
//ERROR CODES
//////////////////////////////////////////////////////////

//apk
define('APK_NO_FILE', 10001);
define('APK_REQUIRE_NEW_VERSION', 10002);
//system
define('SYS_MAINTAINENCE', 10101);
//common
define('ERR_UNKNOWN', 12001);
define('ERR_ILLEGAL_REQUEST', 12002);
define('ERR_MISSING_PARAMETERS', 12003);
define('ERR_NO_CONTACT_PATH', 12004);
define('ERR_USER_TYPE_NOT_MATCH', 12005);
define('ERR_FILE_UPLOAD_FAIL', 12006);
define('ERR_MISSING_CLIENT_PARAMETERS', 12007);
define('ERR_INSERT_TO_DATABASE', 12051);
define('ERR_NO_GRADE_CLASS', 12091); //critical
define('ERR_NO_SUBJECTS', 12092); //critical
                        
define('INVALID_DEVICE', 12101);
define('INVALID_IPADDR', 12102);
define('INVALID_USER', 12103);
define('INVALID_USER_TYPE', 12104);
define('INVALID_LANGUAGE', 12105);
define('INVALID_SCHOOL', 12106);
define('INVALID_SCHOOL_SUBJECT', 12111);
define('INVALID_SCHOOL_CLASS', 12112);

define('FORCE_DUPLICATE', 12201); //notice
define('FORCE_CHANGED_TYPE', 12202); //notice
define('FORCE_INVALID_USER', 12203); //notice
define('FORCE_NO_LOGIN', 12204); //notice
define('FORCE_INACTIVE', 12205); //notice
define('FORCE_INVALID_SCHOOL', 12206); //notice
define('FORCE_SCHOOL_INACTIVE', 12207); //notice
define('FORCE_BY_ADMIN', 12208); //notice
define('FORCE_PARENT_NO_CHILDREN', 12221); //notice
define('FORCE_PARENT_CHILDREN_UPDATED', 12222); //notice
define('FORCE_CHILDREN_EXPIRED', 12231); //notice
define('FORCE_CHILDREN_CLASS_INACTIVE', 12232); //notice
define('FORCE_CHILDREN_CLASS_UPDATED', 12233); //notice

define('MESSAGE_NO_PERMISSION', 12250); 
define('MESSAGE_INVALID_SEND_REQUEST', 12251);
define('MESSAGE_NO_PARTICIPANTS', 12252); 
define('MESSAGE_NO_CHAT_ID', 12253); 
define('MESSAGE_INVALID_CHAT_SESSION', 12254); 
define('MESSAGE_INVALID_SEARCH_REQUEST', 12255); 

//error by pages
//login
define('LOGIN_INVALID_SCHOOL', 13101);  //critical
//forgot
define('FORGOT_NO_CONTACT_PATH', 13111);
define('FORGOT_INSERT_FAIL', 13112);
//profile
//--teacher
define('TEACHER_INVALID_ROLE', 13141);
//--parent
define('PARENT_INVALID_CHILDREN', 13181);
define('PARENT_CHILDREN_NOT_IN_SCHOOL', 13182);
define('PARENT_ONE_CHILDREN', 13183);

//register
define('BUFFER_EXPIRED', 13201);
define('BUFFER_NO_EXIST', 13202);
define('BUFFER_NOT_MATCH', 13203);
//--teacher                                    
define('REGISTER_MISSING_CHARGE_CLASS', 13211);
define('REGISTER_INVALID_CHARGE_CLASS', 13212);
define('REGISTER_MISSING_SUBJECT', 13213);
define('REGISTER_INVALID_SUBJECT', 13214);
define('REGISTER_INVALID_SUBJECT_CLASS', 13215);
//--parent
define('REGISTER_MISSING_PARENT_INFO', 13221);
define('REGISTER_MISSING_CHILDREN_INFO', 13222);
define('REGISTER_MISSING_FATHER_PARAMETERS', 13223);
define('REGISTER_MISSING_MOTHER_PARAMETERS', 13224);
define('REGISTER_MISSING_CHILDREN_PARAMETERS', 13225);
define('REGISTER_INVALID_CHILDREN_CLASS', 13226);

//user fields
define('INVALID_USER_ID', 19105);
define('INVALID_USER_NAME', 19106);
define('INVALID_USER_SEX', 19107);
define('INVALID_USER_BIRTHDAY', 19108);
define('INVALID_USER_ADDRESS', 19109);
define('INVALID_USER_EMAIL', 19110);
define('INVALID_USER_PHONE', 19111);
define('INVALID_USER_PASSWORD', 19112);
define('INVALID_USER_PHOTO_TYPE', 19113);
define('INVALID_USER_PHOTO_SIZE', 19114);
define('INVALID_USER_PHOTO_DIMENSION', 19115);
define('INVALID_USER_PHOTO_NAME', 19116);
define('INVALID_USER_PHOTO_NO_EXIST', 19117);

define('INVALID_FATHER_ID', 19205);
define('INVALID_FATHER_NAME', 19206);
define('INVALID_FATHER_SEX', 19207);
define('INVALID_FATHER_BIRTHDAY', 19208);
define('INVALID_FATHER_ADDRESS', 19209);
define('INVALID_FATHER_EMAIL', 19210);
define('INVALID_FATHER_PHONE', 19211);
define('INVALID_FATHER_PASSWORD', 19212);
define('INVALID_FATHER_PHOTO_TYPE', 19213);
define('INVALID_FATHER_PHOTO_SIZE', 19214);
define('INVALID_FATHER_PHOTO_DIMENSION', 19215);
define('INVALID_FATHER_PHOTO_NAME', 19216);
define('INVALID_FATHER_PHOTO_NO_EXIST', 19217);

define('INVALID_MOTHER_ID', 19305);
define('INVALID_MOTHER_NAME', 19306);
define('INVALID_MOTHER_SEX', 19307);
define('INVALID_MOTHER_BIRTHDAY', 19308);
define('INVALID_MOTHER_ADDRESS', 19309);
define('INVALID_MOTHER_EMAIL', 19310);
define('INVALID_MOTHER_PHONE', 19311);
define('INVALID_MOTHER_PASSWORD', 19312);
define('INVALID_MOTHER_PHOTO_TYPE', 19313);
define('INVALID_MOTHER_PHOTO_SIZE', 19314);
define('INVALID_MOTHER_PHOTO_DIMENSION', 19315);
define('INVALID_MOTHER_PHOTO_NAME', 19316);
define('INVALID_MOTHER_PHOTO_NO_EXIST', 19317);

define('INVALID_CHILD_ID', 19405);
define('INVALID_CHILD_NAME', 19406);
define('INVALID_CHILD_SEX', 19407);
define('INVALID_CHILD_BIRTHDAY', 19408);
define('INVALID_CHILD_ADDRESS', 19409);
define('INVALID_CHILD_EMAIL', 19410);
define('INVALID_CHILD_PHONE', 19411);
define('INVALID_CHILD_PASSWORD', 19412);
define('INVALID_CHILD_PHOTO_TYPE', 19413);
define('INVALID_CHILD_PHOTO_SIZE', 19414);
define('INVALID_CHILD_PHOTO_DIMENSION', 19415);
define('INVALID_CHILD_PHOTO_NAME', 19416);
define('INVALID_CHILD_PHOTO_NO_EXIST', 19417);

/* End of file constants.php */
/* Location: ./application/config/constants.php */