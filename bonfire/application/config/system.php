<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* define the module locations and offset */
$config['school_grades'] = array(
    SCHOOL_PRIMARY => 6,
    SCHOOL_JUNIOR  => 3,
    SCHOOL_SENIOR  => 3,
);

/* End of file config.php */
/* Location: ./application/config/system.php */
