<h2><?php echo lang('ct_applications'); ?></h2>
<hr class="bs-docs-separator"/>

<div class="table-responsive">
    <table class="table table-bordered">
    <colgroup>
        <col width="200"/>
        <col width="100"/>
        <col width="200" class="hide_cell2"/>
        <col width="" class="hide_cell"/>
        <col width="150"/>
    </colgroup>
    <thead>
        <tr>
            <th><?php echo lang('ct_application'); ?></th>
            <th><?php echo lang('ct_version'); ?></th>
            <th class="hide_cell2"><?php echo lang('ct_release_date'); ?></th>
            <th class="hide_cell"><?php echo lang('ct_description'); ?></th>
            <th><?php echo lang('ct_btn_download'); ?></th>
        </tr>
    </thead>
    <tbody>
    <?php if (isset($records) && is_array($records) && count($records)) : ?>
    <?php foreach ($records as $record) : ?>
        <tr>
            <td><?php echo get_application_name($record->app_type); ?></td>
            <td><?php echo $record->version?></td>
            <td class="hide_cell2"><?php echo $record->release_date?></td>
            <td class="hide_cell"><?php echo $record->description?></td>
            <td>
                <?php 
                $path = MEDIA_PATH . 'download/';
                if(file_exists($path . $record->file_name))
                {
                    echo anchor(site_url($path.$record->file_name), lang('ct_btn_download'), 'class="btn"');
                }
                else
                {
                    echo 'No File Exist';
                }
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <td colspan="4"><?php echo lang('msg_no_records'); ?></td>
        </tr>
    <?php endif; ?>
    </tbody>
    </table>
</div>