<?php 

$ftime = 'f' . time();
$mtime = 'm' . time();
$ctime1 = 'c1' . time();
$ctime2 = 'c2' . time();
$ctime3 = 'c3' . time();
?>

<input type="hidden" id="controller" value="parents"/>
<input type="hidden" id="method" value="register"/>

<label><input type="checkbox" id="father"/>Father</label>
<label><input type="checkbox" id="mother"/>Mother</label>
<label><input type="checkbox" id="children1"/>Child1</label>
<label><input type="checkbox" id="children2"/>Child2</label>
<label><input type="checkbox" id="children3"/>Child3</label>

<div id="father_box">
    <h4>Father</h4>
    
    <div class="hidden">
        <input type="hidden" name="param[data][mother]" value="">
        <input type="text" name="param[data][father][user_sex]" value="1">
        <input type="text" name="param[data][father][user_passwd]" value="qqqqq">
        <input type="text" name="param[data][father][user_birthday]" value="">
    </div>
    <div class="control-group">
        <label class="control-label">User ID</label>
        <div class="controls">
            <input type="text" name="param[data][father][user_id]" value="">
        </div>
    </div> 

    <div class="control-group">
        <label class="control-label">Name</label>
        <div class="controls">
            <input type="text" name="param[data][father][user_name]" value="Xiao Father">
        </div>
    </div> 

    <div class="control-group">
        <label class="control-label">Phone</label>
        <div class="controls">
            <input type="text" name="param[data][father][user_phone]" value="12626789282">
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label">Email</label>
        <div class="controls">
            <input type="text" name="param[data][father][user_email]" value="daipangzi@gmail.com">
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label">Photo Name</label>
        <div class="controls">
            <input type="text" name="param[data][father][user_photo]" value="<?php echo $ftime; ?>">
        </div>
    </div>     
    
    <div class="control-group">
        <label class="control-label">Photo</label>
        <div class="controls">
            <input type="file" name="<?php echo $ftime; ?>" value="">
        </div>
    </div>      
</div>
<!--end father-->

<div id="mother_box">
    <h4>Mother</h4>
    
    <div class="hidden">
        <input type="hidden" name="param[data][father]" value="">
        <input type="text" name="param[data][mother][user_sex]" value="2">
        <input type="text" name="param[data][mother][user_passwd]" value="qqqqq">
        <input type="text" name="param[data][mother][user_birthday]" value="">
    </div>
    <div class="control-group">
        <label class="control-label">User ID</label>
        <div class="controls">
            <input type="text" name="param[data][mother][user_id]" value="">
        </div>
    </div> 

    <div class="control-group">
        <label class="control-label">Name</label>
        <div class="controls">
            <input type="text" name="param[data][mother][user_name]" value="Xiao Mother">
        </div>
    </div> 

    <div class="control-group">
        <label class="control-label">Phone</label>
        <div class="controls">
            <input type="text" name="param[data][mother][user_phone]" value="12626789282">
        </div>
    </div> 
    
    <div class="control-group">
        <label class="control-label">Email</label>
        <div class="controls">
            <input type="text" name="param[data][mother][user_email]" value="idaipangzi@gmail.com">
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label">Photo Name</label>
        <div class="controls">
            <input type="text" name="param[data][mother][user_photo]" value="<?php echo $mtime; ?>">
        </div>
    </div>   
    
    <div class="control-group">
        <label class="control-label">Photo</label>
        <div class="controls">
            <input type="file" name="<?php echo $mtime; ?>" value="">
        </div>
    </div>           
</div>
<!--end mother-->

<div id="children1_box">
    <h4>Child1</h4>
    
    <div class="hidden">
        <input type="hidden" name="param[data][children][0][user_sex]" value="1">
        <input type="hidden" name="param[data][children][0][user_passwd]" value="qqqqq">
        <input type="hidden" name="param[data][children][0][user_birthday]" value="">
    </div>
    <div class="control-group">
        <label class="control-label">User ID</label>
        <div class="controls">
            <input type="text" name="param[data][children][0][user_id]" value="">
        </div>
    </div> 

    <div class="control-group">
        <label class="control-label">Name</label>
        <div class="controls">
            <input type="text" name="param[data][children][0][user_name]" value="Xiao Jin">
        </div>
    </div> 
    
    <div class="control-group">
        <label class="control-label">Phone</label>
        <div class="controls">
            <input type="text" name="param[data][children][0][user_phone]" value="12626789282">
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label">Email</label>
        <div class="controls">
            <input type="text" name="param[data][children][0][user_email]" value="child1@gmail.com">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label">School UID</label>
        <div class="controls">
            <input type="text" name="param[data][children][0][school_uid]" value="1">
        </div>
    </div>
     
    <div class="control-group">
        <label class="control-label">Class UID</label>
        <div class="controls">
            <input type="text" name="param[data][children][0][children_class]" value="">
        </div>
    </div>  
    
    <div class="control-group">
        <label class="control-label">Photo Name</label>
        <div class="controls">
            <input type="text" name="param[data][children][0][user_photo]" value="<?php echo $ctime1; ?>">
        </div>
    </div> 
    
    <div class="control-group">
        <label class="control-label">Photo</label>
        <div class="controls">
            <input type="file" name="<?php echo $ctime1; ?>" value="">
        </div>
    </div>    
</div>

<div id="children2_box">
    <h4>Child2</h4>
    <div class="hidden">
        <input type="hidden" name="param[data][children][1][user_sex]" value="2">
        <input type="hidden" name="param[data][children][1][user_passwd]" value="qqqqq">
        <input type="hidden" name="param[data][children][1][user_birthday]" value="">
    </div>
    <div class="control-group">
        <label class="control-label">User ID</label>
        <div class="controls">
            <input type="text" name="param[data][children][1][user_id]" value="">
        </div>
    </div> 

    <div class="control-group">
        <label class="control-label">Name</label>
        <div class="controls">
            <input type="text" name="param[data][children][1][user_name]" value="Xiao Jin">
        </div>
    </div> 
    
    <div class="control-group">
        <label class="control-label">Phone</label>
        <div class="controls">
            <input type="text" name="param[data][children][1][user_phone]" value="12626789283">
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label">Email</label>
        <div class="controls">
            <input type="text" name="param[data][children][1][user_email]" value="child2@gmail.com">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label">School UID</label>
        <div class="controls">
            <input type="text" name="param[data][children][1][school_uid]" value="1">
        </div>
    </div>
     
    <div class="control-group">
        <label class="control-label">Class UID</label>
        <div class="controls">
            <input type="text" name="param[data][children][1][children_class]" value="">
        </div>
    </div>  
    
    <div class="control-group">
        <label class="control-label">Photo Name</label>
        <div class="controls">
            <input type="text" name="param[data][children][1][user_photo]" value="<?php echo $ctime2; ?>">
        </div>
    </div>  
    
    <div class="control-group">
        <label class="control-label">Photo</label>
        <div class="controls">
            <input type="file" name="<?php echo $ctime2; ?>" value="">
        </div>
    </div>   
</div>

<div id="children3_box">
    <h4>Child3</h4>
    <div class="hidden">
        <input type="hidden" name="param[data][children][2][user_sex]" value="1">
        <input type="hidden" name="param[data][children][2][user_passwd]" value="qqqqq">
        <input type="hidden" name="param[data][children][2][user_birthday]" value="">
    </div>
    <div class="control-group">
        <label class="control-label">User ID</label>
        <div class="controls">
            <input type="text" name="param[data][children][2][user_id]" value="">
        </div>
    </div> 

    <div class="control-group">
        <label class="control-label">Name</label>
        <div class="controls">
            <input type="text" name="param[data][children][2][user_name]" value="Xiao Jin">
        </div>
    </div> 
    
    <div class="control-group">
        <label class="control-label">Phone</label>
        <div class="controls">
            <input type="text" name="param[data][children][2][user_phone]" value="12626789284">
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label">Email</label>
        <div class="controls">
            <input type="text" name="param[data][children][2][user_email]" value="child3@gmail.com">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label">School UID</label>
        <div class="controls">
            <input type="text" name="param[data][children][2][school_uid]" value="1">
        </div>
    </div>
     
    <div class="control-group">
        <label class="control-label">Class UID</label>
        <div class="controls">
            <input type="text" name="param[data][children][2][children_class]" value="">
        </div>
    </div> 
    
    <div class="control-group">
        <label class="control-label">Photo Name</label>
        <div class="controls">
            <input type="text" name="param[data][children][2][user_photo]" value="<?php echo $ctime3; ?>">
        </div>
    </div> 
     
    <div class="control-group">
        <label class="control-label">Photo</label>
        <div class="controls">
            <input type="file" name="<?php echo $ctime3; ?>" value="">
        </div>
    </div>    
</div>

<div class="form-actions">
    <input type="button" id="btn_submit" class="btn" value="Register">    
</div>