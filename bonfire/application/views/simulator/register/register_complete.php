<input type="hidden" id="method" value="register_complete"/>

<div class="control-group">
    <label class="control-label">Buffer UID</label>
    <div class="controls">
        <input type="text" name="param[data][buffer_uid]" value="">
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Buffer Key</label>
    <div class="controls">
        <input type="text" name="param[data][buffer_key]" value="">
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Verify Code</label>
    <div class="controls">
        <input type="text" name="param[data][verify_code]" value="">
    </div>
</div>

<div class="form-actions">
    <input type="button" id="btn_submit" class="btn" value="Register Complete">    
</div>