<?php $time = time(); ?>
<input type="hidden" id="controller" value="teacher"/>
<input type="hidden" id="method" value="register"/>

<div class="control-group">
    <label class="control-label">User ID</label>
    <div class="controls">
        <input type="text" name="param[data][user_id]" value="">
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Name</label>
    <div class="controls">
        <input type="text" name="param[data][user_name]" value="Tester">
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Gender</label>
    <div class="controls">
        <select name="param[data][user_sex]">
            <option value="1">Man</option>
            <option value="2">Woman</option>
        </select>
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Email</label>
    <div class="controls">
        <input type="text" name="param[data][user_email]" value="daipangzi@gmail.com">
    </div>
</div>          

<div class="control-group">
    <label class="control-label">Phone</label>
    <div class="controls">
        <input type="text" name="param[data][user_phone]" value="12626789282">
    </div>
</div>   

<div class="control-group">
    <label class="control-label">Birthday</label>
    <div class="controls">
        <input type="text" name="param[data][user_birthday]" value="">
    </div>
</div>        

<div class="control-group">
    <label class="control-label">Password</label>
    <div class="controls">
        <input type="text" name="param[data][user_passwd]" value="qqqqq">
    </div>
</div> 

<div class="control-group">
    <label class="control-label">School UID</label>
    <div class="controls">
        <input type="text" name="param[data][school_uid]" value="1">
    </div>
</div>

<div class="control-group">
    <label class="control-label">Charge Class</label>
    <div class="controls">
        <input type="hidden" name="param[data][is_charge]" value="1">
        <input type="text" name="param[data][charge_class]" value="1">
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Charge Subject</label>
    <div class="controls">
        <input type="hidden" name="param[data][is_subject]" value="1">
        <input type="text" name="param[data][subject_uid]" value="1">
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Subject Class</label>
    <div class="controls">
        <input type="text" name="param[data][subject_class]" value="1">
    </div>
</div>

<div class="control-group">
    <label class="control-label">Phot Name</label>
    <div class="controls">
        <input type="text" name="param[data][user_photo]" value="<?php echo $time; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label">Photo</label>
    <div class="controls">
        <input type="file" name="<?php echo $time; ?>" value="">
    </div>
</div>

<div class="form-actions">
    <input type="button" id="btn_submit" class="btn" value="Register">    
</div>