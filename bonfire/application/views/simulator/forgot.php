<input type="hidden" id="method" value="forgot"/>

<div class="control-group">
    <label class="control-label">User ID</label>
    <div class="controls">
        <input type="text" name="param[data][user_id]" value="">
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Via Email</label>
    <div class="controls">
        <select name="param[data][via_email]">
            <option value="1">Yes</option>
            <option value="0">No</option>
        </select> 
    </div>
</div>           

<div class="control-group">
    <label class="control-label">Via Phone</label>
    <div class="controls">
        <select name="param[data][via_phone]">
            <option value="1">Yes</option>
            <option value="0">No</option>
        </select> 
    </div>
</div>           

<div class="form-actions">
    <input type="button" id="btn_submit" class="btn" value="Request">    
</div>