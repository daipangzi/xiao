<input type="hidden" id="controller" value="message"/>
<input type="hidden" id="method" value="search_by_date_type"/>

<div class="control-group">
    <label class="control-label">Chat Type</label>
    <div class="controls">
        <select name="param[data][chat_type]">
            <option value="0">Normal</option>
            <option value="1">Notification</option>
            <option value="2">Homework</option>
        </select>
    </div>
</div>  

<div class="control-group">
    <label class="control-label">Chat Date</label>
    <div class="controls">
        <input type="text" name="param[data][chat_date]" value="<?php echo date(FORMAT_DATETIME); ?>">
    </div>
</div>

<div class="form-actions">
    <input type="button" id="btn_submit" class="btn" value="Search">    
</div>