<input type="hidden" id="controller" value="message"/>
<input type="hidden" id="method" value="chat_conversations"/>

<div class="control-group">
    <label class="control-label">Session Key</label>
    <div class="controls">
        <input type="text" name="param[data][session_key]" value="">
    </div>
</div>

<div class="control-group">
    <label class="control-label">Show All</label>
    <div class="controls">
        <select name="param[data][start_time_option]">
            <option value="0" selected="selected">1 Week</option>
            <option value="1">2 Week</option>
            <option value="2">1 Month</option>
            <option value="3">3 Month</option>
            <option value="4">6 Month</option>
            <option value="5">1 Year</option>
            <option value="5">All</option>
        </select>
    </div>
</div>

<div class="control-group">
    <label class="control-label">End Time</label>
    <div class="controls">
        <input type="text" name="param[data][end_time]" value="<?php echo date(FORMAT_DATETIME); ?>">
    </div>
</div>

<div class="form-actions">
    <input type="button" id="btn_submit" class="btn" value="Get">    
</div>