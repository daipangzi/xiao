<input type="hidden" id="controller" value="message"/>
<input type="hidden" id="method" value="send"/>
<input type="hidden" name="param[data][receivers]" value=""/>

<?php
    $time1 = time();
?>

<div class="control-group">
    <label class="control-label">Temp ID</label>
    <div class="controls">
        <input type="text" name="param[data][temp_id]" value="<?php echo "{$time1}";?>">
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Chat Type</label>
    <div class="controls">
        <select name="param[data][message_type]">
            <option value="0">Normal</option>
            <option value="1">Notification</option>
            <option value="2">Homework</option>
        </select>
    </div>
</div>  

<div class="control-group">
    <label class="control-label">Attachments</label>
    <div class="controls">
        <input type="hidden" name="param[data][attachment]" value="<?php echo "{$time1}";?>">
        <input type="hidden" name="param[data][attachment_name]" value="test">
        <input type="file" name="<?php echo $time1; ?>"><br/>
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Content</label>
    <div class="controls">
        <textarea rows="" name="param[data][message_contents]">teset</textarea>
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Chat Session</label>
    <div class="controls">
        <input type="text" name="param[data][session_key]" value=""/>
    </div>
</div> 

<div class="form-actions">
    <input type="button" id="btn_submit" class="btn" value="Send">    
</div>