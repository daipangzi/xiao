<input type="hidden" id="controller" value="message"/>
<input type="hidden" id="method" value="send"/>

<?php
    $time1 = time();
?>

<div class="control-group">
    <label class="control-label">Temp ID</label>
    <div class="controls">
        <input type="text" name="param[data][temp_id]" value="<?php echo "{$time1}";?>">
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Chat Type</label>
    <div class="controls">
        <select name="param[data][message_type]">
            <option value="0">Normal</option>
            <option value="1">Notification</option>
            <option value="2">Homework</option>
        </select>
    </div>
</div>  

<div class="control-group">
    <label class="control-label">Attachments</label>
    <div class="controls">
        <input type="hidden" name="param[data][attachment]" value="<?php echo "{$time1}";?>">
        <input type="hidden" name="param[data][attachment_name]" value="test">
        <input type="file" name="<?php echo $time1; ?>"><br/>
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Content</label>
    <div class="controls">
        <textarea rows="" name="param[data][message_contents]">teset</textarea>
    </div>
</div> 

<h4>Receivers</h4>
<div id="master_box" style="display:none;">
    <div class="control-group">
        <label class="control-label">To All</label>
        <div class="controls">
            <label><input type="radio" name="param[data][receivers][to_all]" value="0" disabled="disabled">None</label>
            <label><input type="radio" name="param[data][receivers][to_all]" value="1" disabled="disabled">Selected</label>
        </div>
    </div> 

    <div class="control-group">
        <label class="control-label">To All Teachers</label>
        <div class="controls">
            <input type="checkbox" name="param[data][receivers][to_all_teachers]" value="1" disabled="disabled">
        </div>
    </div> 

    <div class="control-group">
        <label class="control-label">To All Children</label>
        <div class="controls">
            <input type="checkbox" name="param[data][receivers][to_all_children]" value="1" disabled="disabled">
        </div>
    </div> 

    <div class="control-group">
        <label class="control-label">To All Parents</label>
        <div class="controls">
            <input type="checkbox" name="param[data][receivers][to_all_parents]" value="1" disabled="disabled">
        </div>
    </div> 
</div>

<div id="teacher_box">
    <div class="control-group">
        <label class="control-label">To All Children of these classes</label>
        <div class="controls">
            <input type="text" name="param[data][receivers][to_all_children_group][]" value=""><br/>
            <input type="text" name="param[data][receivers][to_all_children_group][]" value=""><br/>
            <input type="text" name="param[data][receivers][to_all_children_group][]" value="">
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label">To All parents of these classes</label>
        <div class="controls">
            <input type="text" name="param[data][receivers][to_all_parents_group][]" value=""><br/>
            <input type="text" name="param[data][receivers][to_all_parents_group][]" value=""><br/>
            <input type="text" name="param[data][receivers][to_all_parents_group][]" value=""><br/>
        </div>
    </div>
</div>

<div id="other_box">
    <div class="control-group">
        <label class="control-label">Custom1</label>
        <div class="controls">
            <input type="text" name="param[data][receivers][to_custom_group][0][user_id]" value="">
            <select name="param[data][receivers][to_custom_group][0][user_type]">
                <option value="1">Teacher</option>
                <option value="9">Master</option>
                <option value="10">Children</option>
                <option value="100">Parent</option>
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Custom2</label>
        <div class="controls">
            <input type="text" name="param[data][receivers][to_custom_group][1][user_id]" value="">
            <select name="param[data][receivers][to_custom_group][1][user_type]">
                <option value="1">Teacher</option>
                <option value="9">Master</option>
                <option value="10">Children</option>
                <option value="100">Parent</option>
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Custom3</label>
        <div class="controls">
            <input type="text" name="param[data][receivers][to_custom_group][2][user_id]" value="">
            <select name="param[data][receivers][to_custom_group][2][user_type]">
                <option value="1">Teacher</option>
                <option value="9">Master</option>
                <option value="10">Children</option>
                <option value="100">Parent</option>
            </select>
        </div>
    </div>    
</div>

<div class="form-actions">
    <input type="button" id="btn_submit" class="btn" value="Send">    
</div>