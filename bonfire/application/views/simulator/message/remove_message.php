<input type="hidden" id="controller" value="message"/>
<input type="hidden" id="method" value="remove_message"/>

<div class="control-group">
    <label class="control-label">Session Key</label>
    <div class="controls">
        <input type="text" name="param[data][session_key]" value="">
    </div>
</div>

<div class="control-group">
    <label class="control-label">Message Key</label>
    <div class="controls">
        <input type="text" name="param[data][message_key]" value="">
    </div>
</div>

<div class="form-actions">
    <input type="button" id="btn_submit" class="btn" value="Get">    
</div>