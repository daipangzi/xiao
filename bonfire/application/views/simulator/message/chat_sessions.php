<input type="hidden" id="controller" value="message"/>
<input type="hidden" id="method" value="chat_sessions"/>

<div class="control-group">
    <label class="control-label">Show All</label>
    <div class="controls">
        <input type="text" name="param[data][show_all]" value="0">
    </div>
</div>

<div class="control-group">
    <label class="control-label">Last Date</label>
    <div class="controls">
        <input type="text" name="param[data][last_time]" value="<?php echo date(FORMAT_DATETIME); ?>">
    </div>
</div>

<div class="form-actions">
    <input type="button" id="btn_submit" class="btn" value="Get">    
</div>