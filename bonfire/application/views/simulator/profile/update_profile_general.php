<input type="hidden" id="method" value="update_profile_general"/>

<div class="control-group">
    <label class="control-label">User Name</label>
    <div class="controls">
        <input type="text" name="param[data][user_name]" value="">
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Gender</label>
    <div class="controls">
        <select name="param[data][user_sex]">
            <option value="1">Man</option>
            <option value="2">Woman</option>
        </select>
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Birthday</label>
    <div class="controls">
        <input type="text" name="param[data][user_birthday]" value="">
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Email</label>
    <div class="controls">
        <input type="text" name="param[data][user_email]" value="">
    </div>
</div>          

<div class="control-group">
    <label class="control-label">Phone</label>
    <div class="controls">
        <input type="text" name="param[data][user_phone]" value="">
    </div>
</div>          

<div class="control-group">
    <label class="control-label">Address</label>
    <div class="controls">
        <input type="text" name="param[data][user_address]" value="">
    </div>
</div>           

<div class="form-actions">
    <input type="button" id="btn_submit" class="btn" value="Update">    
</div>