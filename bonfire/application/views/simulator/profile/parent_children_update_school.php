<input type="hidden" id="controller" value="parents"/>
<input type="hidden" id="method" value="update_children_school_class"/>

<div class="control-group">
    <label class="control-label">Children ID</label>
    <div class="controls">
        <input type="text" name="param[data][children_id]" value="">
    </div>
</div> 

<div class="control-group">
    <label class="control-label">School UID</label>
    <div class="controls">
        <input type="text" name="param[data][school_uid]" value="1">
    </div>
</div> 

<div class="control-group">
    <label class="control-label">Class UID</label>
    <div class="controls">
        <input type="text" name="param[data][class_uid]" value="1">
    </div>
</div> 

<div class="form-actions">
    <input type="button" id="btn_submit" class="btn" value="Change">    
</div>