<input type="hidden" id="method" value="update_profile_password"/>

<div class="control-group">
    <label class="control-label">Old Password</label>
    <div class="controls">
        <input type="text" name="param[data][old_password]" value="">
    </div>
</div> 

<div class="control-group">
    <label class="control-label">New Password</label>
    <div class="controls">
        <input type="text" name="param[data][new_password]" value="">
    </div>
</div> 

<div class="form-actions">
    <input type="button" id="btn_submit" class="btn" value="Update">    
</div>