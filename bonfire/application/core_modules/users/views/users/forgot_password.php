 <?php echo form_open('admin/forgot_password', array('autocomplete' => 'off', 'id' => "pass_form")); ?>        
    <div class="top_b"><?php echo lang('us_reset_password'); ?></div>    
    
    <div class="alert-login">
        <?php if(Template::message()) { ?>
            <?php echo Template::message(); ?>
        <?php } else { ?>
            <div class="alert alert-info"><?php echo lang('us_reset_note'); ?></div>
        <?php } ?>
    </div>
    
    <div class="cnt_b">
        <div class="formRow clearfix">
            <div class="input-prepend">
                <span class="add-on">@</span><input type="text" name="email" id="email" placeholder="<?php echo lang('bf_email'); ?>" value="<?php echo set_value('email') ?>"/>
            </div>
        </div>
    </div>
    <div class="btm_b tac">
        <button class="btn btn-inverse" type="submit" name="submit" value="1"><?php echo lang('us_request_new_password'); ?></button>
    </div>  
<?php echo form_close(); ?>

<div class="links_b links_btm clearfix">
    <span class="linkform"><?php echo lang('us_never_mind'); ?><a href="<?php echo site_url('admin/login'); ?>"><?php echo lang('us_back_to_login_page'); ?></a></span>
</div>