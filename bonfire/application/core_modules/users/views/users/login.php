<?php echo form_open('admin/login', array('autocomplete' => 'off', 'id' => "login_form")); ?>        
    <div class="top_b">Xiaoxin Adminitrator</div>    
    
    <div class="alert-login">
        <?php echo Template::message(); ?>
    </div>
    
    <div class="cnt_b">
        <div class="formRow">
            <div class="input-prepend">
                <span class="add-on"><i class="icon-user"></i></span><input type="text" id="username" name="username" placeholder="<?php echo $this->settings_lib->item('auth.login_type') == 'both' ? lang('bf_username') .'/'. lang('bf_email') : ucwords($this->settings_lib->item('auth.login_type')) ?>" value="<?php echo set_value('username'); ?>" />
            </div>
        </div>
        <div class="formRow">
            <div class="input-prepend">
                <span class="add-on"><i class="icon-lock"></i></span><input type="password" id="password" name="password" placeholder="<?php echo lang('bf_password'); ?>" value="" />
            </div>
        </div>
        <div class="formRow clearfix">
            <label class="checkbox"><input type="checkbox" name="remember_me" id="remember_me" value="1"/> <?php echo lang('us_remember_note'); ?></label>
        </div>
    </div>
    <div class="btm_b clearfix">
        <button class="btn btn-inverse pull-right" type="submit" name="submit" id="submit" value="1"><?php echo lang('us_sign_in'); ?></button>
        <span class="link_pass"><a href="<?php echo site_url('admin/forgot_password'); ?>"><?php echo lang('us_forgot_your_password'); ?></a></span>
    </div>  
<?php echo form_close(); ?>