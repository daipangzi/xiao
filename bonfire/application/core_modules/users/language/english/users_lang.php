<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
	Copyright (c) 2011 Lonnie Ezell

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/

$lang['us_account_deleted']			= 'Unfortunately your account has been deleted. It has not yet been purged and <strong>may still</strong> be restored. Contact the administrator at %s.';

$lang['us_bad_email_pass']			= 'Incorrect email or password.';
$lang['us_no_permission']			= 'You do not have permission to access that page.';
$lang['us_fields_required']         = '%s and Password fields must be filled out.';

$lang['us_purge_del_confirm']		= 'Are you sure you want to completely remove the user account(s) - there is no going back?';
$lang['us_action_purged']			= 'Users purged.';
$lang['us_action_deleted']			= 'The User was successfully deleted.';
$lang['us_action_not_deleted']		= 'We could not delete the user: ';
$lang['us_delete_account_confirm']	= 'Are you sure you want to delete the user account(s)?';

$lang['us_role']					= 'Role';
$lang['us_role_lower']				= 'role';
$lang['us_no_users']				= 'No users found that match your selection.';
$lang['us_create_user']				= 'Create New User';
$lang['us_edit_user']				= 'Edit User';
$lang['us_restore_note']			= 'Restore the user and allow them access to the site again.';
$lang['us_unban_note']				= 'Un-Ban the user and all them access to the site.';
$lang['us_account_status']			= 'Account Status';

$lang['us_banned_admin_note']		= 'This user has been banned from the site.';
$lang['us_banned_msg']				= 'This account does not have permission to enter the site.';

$lang['us_user_management']			= 'User Management';
$lang['us_edit_profile']			= 'Edit Profile';

$lang['us_reset_password']			= 'Reset Password';
$lang['us_reset_note']				= 'We will send a temporary password to your email.';

$lang['us_remember_note']			= 'Remember me';
$lang['us_forgot_your_password']	= 'Forgot Your Password?';

$lang['us_password_mins']			= 'Minimum 8 characters.';

$lang['us_unauthorized']			= 'Unauthorized. Sorry you do not have the appropriate permission to manage the "%s" role.';
$lang['us_empty_id']				= 'No userid provided. You must provide a userid to perform this action.';
$lang['us_self_delete']				= 'Unauthorized. Sorry, you can not delete yourself.';

$lang['us_filter_first_letter']		= 'Username starts with: ';
$lang['us_last_login']				= 'Last Login';

$lang['us_no_password']             = 'No Password present.';
$lang['us_no_email']                = 'No Email given.';
$lang['us_email_taken']             = 'Email already exists.';
$lang['us_invalid_email']           = 'Cannot find that email in our records.';

$lang['us_reset_invalid_email']     = 'That did not appear to be a valid password reset request.';
$lang['us_reset_pass_subject']      = 'Your Temporary Password';
$lang['us_reset_pass_message']      = 'Please check your email for instructions to reset your password.';
$lang['us_reset_pass_error']        = 'Unable to send an email: ';
$lang['us_reset_password_success']  = 'Please login using your new password.';
$lang['us_reset_password_error']    = 'There was an error resetting your password: ';

$lang['us_profile_updated_success'] = 'Profile successfully updated.';
$lang['us_profile_updated_error']   = 'There was a problem updating your profile ';

$lang['us_user_created_success']    = 'User successfully created.';
$lang['us_user_update_success']     = 'User successfully updated.';

$lang['us_user_restored_success']   = 'User successfully restored.';
$lang['us_user_restored_error']     = 'Unable to restore user: ';


/* Activations */
$lang['us_status']					= 'Status';
$lang['us_no_inactive']				= 'There are not any users requiring activation in the database.';
$lang['us_active']					= 'Active.';
$lang['us_inactive']				= 'Inactive.';
$lang['us_active_status_changed'] 	= 'The user status was successfully changed.';
// Activation Errors
$lang['us_err_no_id'] 				= 'No User ID was received.';
$lang['us_err_status_error'] 		= 'The users status was not changed: ';
$lang['us_err_no_email'] 			= 'Unable to send an email: ';
$lang['us_err_no_matching_id'] 		= 'No matching user id was found in the system.';
$lang['us_err_user_is_active'] 		= 'The user is already active.';
$lang['us_err_user_is_inactive'] 	= 'The user is already inactive.';

/* Password strength/match */
$lang['us_pass_strength']			= 'Strength';
$lang['us_pass_match']				= 'Comparison';
$lang['us_passwords_no_match']		= 'No match!';
$lang['us_passwords_match']			= 'Match!';
$lang['us_pass_weak']				= 'Weak';
$lang['us_pass_good']				= 'Good';
$lang['us_pass_strong']				= 'Strong';

$lang['us_sign_in']                 = 'Sign In';
$lang['us_request_new_password']    = 'Request New Password';
$lang['us_back_to_login_page']      = 'send me back to the sign-in screen';
$lang['us_never_mind']              = 'Never mind, ';
