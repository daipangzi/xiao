<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['set_allow_name_change_note']	= 'Allow users to change their display name after registering?';
$lang['set_name_change_frequency']	= ' Changes every';
$lang['set_days']					= ' days';
$lang['settings_saved_success']     = 'Your settings were successfully saved.';
$lang['settings_error_success']     = 'There was an error saving your settings.';

$lang['ct_application_id']  = 'Application ID';
$lang['ct_api_key']         = 'API Key';
$lang['ct_parse_url']       = 'Parse Url';
$lang['ct_log_error']       = 'Log Errors';
$lang['ct_log_action']      = 'Log Actions';