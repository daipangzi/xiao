<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Parent_model extends User_list_Model {

    protected $table        = "gus_parents";
    protected $key          = "parent_uuid";
    protected $soft_deletes = true;
    protected $date_format  = "datetime";
    protected $set_created  = true;
    protected $set_modified = true;
    protected $created_field  = "reg_date";
    protected $modified_field = "mod_date";
    
    ///////////////////////////////////////////////////
    //OVERRIDES
    ///////////////////////////////////////////////////
    
    /**
    * delete parent info 
    * 
    * @param int $parent_uid
    * @return bool
    */
    public function delete($parent_uid)
    {     
        //$this->delete_parent_children_router($parent_uid);        
        return parent::delete($parent_uid);
    }

    /**
    * insert paretn-children relation
    * 
    * @param int $parent_uid
    * @param int $children_uid
    */
    public function insert_parent_children_router($parent_uid, $children_uid)
    {
        $data = array();
        $data['parent_uid']   = $parent_uid;    
        $data['children_uid'] = $children_uid;
        $data['active_date']  = date(FORMAT_DATETIME);
        
        return $this->db->insert($this->parent_children_table, $data);
    }
    
    /**
    * delete parent-children relation
    *  
    * @param int $parent_uid
    * @param int $children_uid
    */
    public function delete_parent_children_router($parent_uid, $children_uid=FALSE)
    {
        if($children_uid !== FALSE)
        {
            $this->where('children_uid', $children_uid);
        }
        
        return $this->db
            ->where('parent_uid', $parent_uid)
            ->delete($this->parent_children_table);    
    }
    
    ///////////////////////////////////////////////////////////////////
    //!COMMON METHODS
    ///////////////////////////////////////////////////////////////////
    
    /**
    * count children of registered parent
    * 
    * @param int $parent_uid
    */
    public function count_parent_children($parent_uid)
    {
        return $this->db->where('parent_uid', $uid)->count_all_results($this->parent_children_table);
    }
    
    /**
    * get parent's children uids
    * 
    * @param array $parent_uids
    */
    public function get_parent_children_uids($parent_uids=array())
    {
        if(!is_array($parent_uids))
        {
            $parent_uids = (array)$parent_uids;
        }
        
        $records = $this->db
            ->select('children_uid')
            ->where_in('parent_uid', $parent_uids)
            ->group_by('children_uid')
            ->get($this->parent_children_table)
            ->result_array();
        
        if(empty($records))
        {
            return FALSE;
        }
        
        return array_column_ct($records, 'children_uid');
    }   
    
    ///////////////////////////////////////////////////////////////////
    //!PRIVATE METHODS
    ///////////////////////////////////////////////////////////////////
    
    
}