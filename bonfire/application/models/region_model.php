<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Region_model extends BF_Model {

	protected $table		= "bss_region";
	protected $key			= "region_uuid";
	protected $soft_deletes	= true;
	protected $date_format	= "datetime";
	protected $set_created	= false;
	protected $set_modified = false;
	protected $created_field = "created_on";
	protected $modified_field = "modified_on";
    
    /**
    * get provinces by dropdown format
    * 
    * @param bool   $empty_row
    * @param string $empty_text
    */
    function province_dropdown_list($empty_row=TRUE, $empty_text='')
    {
        $records = $this
            ->where(array('level' => 0, 'LENGTH(region_uuid) =' => '2'))
            ->order_by('region_uuid', 'asc')
            ->find_all();
        
        $result = array();
        if($empty_row)
        {
            $result[''] = $empty_text;
        }
        
        foreach($records as $r)
        {
            $result[$r->region_uuid] = $r->region_name;
        }
            
        return $result;
    }
    
    /**
    * get cities by dropdown format
    * 
    * @param string $province
    * @param bool   $empty_row
    * @param string $empty_text
    */
    function city_dropdown_list($province='00', $empty_row=TRUE, $empty_text='')
    {
        $records = $this
            ->where(array('level' => 1, 'LENGTH(region_uuid) =' => '4', 'region_uuid LIKE' => $province.'%'))
            ->order_by('region_uuid', 'asc')
            ->find_all();
                 
        $result = array();
        if($empty_row)
        {
            $result[''] = $empty_text;
        }
        
        if(!empty($records)) 
        {
            foreach($records as $r)
            {
                $result[$r->region_uuid] = $r->region_name;
            }
        }
            
        return $result;
    }
}
