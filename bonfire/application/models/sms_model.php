<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sms_model extends BF_Model {

    protected $table        = "gbl_sms";
    protected $key          = "sms_uuid";
    protected $soft_deletes = false;
    protected $date_format  = "datetime";
    protected $set_created  = true;
    protected $set_modified = false;
    protected $created_field  = "sent_time";
    protected $modified_field = "modified_on";
    
    /**
    * get latest device_num registered on g_sms table
    * 
    */
    public function get_latest_device()
    {
        $record = $this
            ->where('device_num IS NOT NULL', NULL, FALSE)
            ->limit(1)
            ->order_by()
            ->find_all();
        if(empty($record))
        {
            return 0;
        }
        
        return $record[0]->device_num;
    } 
    
    public function get_code($uid)
    {
        $record = $this->find($uid);
        if(empty($record))
        {
            return FALSE;
        }
        
        return $record->verify_code;
    }
}
