<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apps_model extends BF_Model {

	protected $table		= "gbs_apps";
	protected $key			= "uid";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";
	protected $set_created	= false;
	protected $set_modified = false;
}
