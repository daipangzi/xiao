<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Amnesiac_model extends Info_base_model {

    protected $table        = "gbl_amnesiacs";
    protected $key          = "uid";
    protected $soft_deletes = false;
    protected $date_format  = "datetime";
    protected $set_created  = true;
    protected $set_modified = false;
    protected $created_field  = "reg_date";
    protected $modified_field = "modified_on";
    
    /**
    * find history if user retrieved his password in a day
    * 
    * @param string $user_id
    * @param int    $user_type
    * @param int    $path
    */
    public function find_row($user_id, $user_type, $path)
    {
        //remove expires
        $this->remove_expires();
        
        $records = $this
            ->where('LOWER(user_id)', $user_id)
            ->where_in('user_type', $user_type)
            ->find_all();   
         
        if(empty($records))
        {
            return FALSE;
        }
        
        //check valid 
        $check_sum = 0;
        foreach($records as $record)
        {
            if($record->path == SECURITY_PATH_BOTH)
            {
                return $record;
            } 
            else if($record->path == $path)
            {
                return $record;
            } 
            
            $check_sum += (int)$record->path;    
        }
        
        if($check_sum + $path >= SECURITY_PATH_BOTH || $check_sum === $path)
        {
            return $records[0];
        }
        
        return FALSE;
    }
    
    /**
    * remove expires
    * 
    */
    private function remove_expires()
    {
        $expire = date('Y-m-d H:i:s', strtotime('-1 day'));
        $where = array('reg_date <=' => $expire);
        return $this->delete_where($where);
    }
}
