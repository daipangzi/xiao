<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Buffer_model extends Info_base_model {

    protected $table        = "gbl_buffer";
    protected $key          = "buffer_uuid";
    protected $soft_deletes = false;
    protected $date_format  = "datetime";
    protected $set_created  = true;
    protected $set_modified = true;
    protected $created_field  = "reg_date";
    protected $modified_field = "reg_date";
    
    protected $sms_table = 'gbl_sms';
    
    /**
    * delete buffer row with sms row
    * 
    * @param mixed $uid
    * @return bool
    */
    public function delete($uid)
    {
        $record = $this->find($uid);
        if(empty($record)) return FALSE;
        
        $sms_uid = $record->sms_uid;
        $this->db->where('sms_uuid', $sms_uid)->delete($this->sms_table);
        
        return parent::delete($uid);
    }
    
    /**
    * find buffer row with uid and key
    */
    public function find_buffer($uid, $key)
    {
        //remove old rows
        $this->remove_expires();
        
        $record = $this->find($uid);
        if(empty($record))
        {
            return FALSE;
        }
        
        if($record->key != $key)
        {
            return FALSE;
        }
        
        return $record;
    }
    
    //////////////////////////////////////////////////
    //FILTER METHODS
    //////////////////////////////////////////////////
    public function filter_key($key)
    {
        return $this->where('key', $key);
    }
    
    public function with_sms()
    {
        return $this->join($this->sms_table, 'sms_uuid=sms_uid', 'left');
    }
    
    //////////////////////////////////////////////////
    //PRIVATE METHODS
    //////////////////////////////////////////////////
    /**
    * remove older than a day
    * 
    */
    public function remove_expires() 
    {                                 
        $time = date(FORMAT_DATETIME, strtotime('-1 day'));
        $where1 = array('reg_date  <=' => $time);
        $where2 = array('sent_time <=' => $time);
        
        $this->db->where($where2)->delete($this->sms_table);
        return $this->db->where($where1)->delete($this->table);
    }
}
