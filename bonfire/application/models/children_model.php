<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Children_model extends User_list_Model {

    protected $table        = "gus_children";
    protected $key          = "children_uuid";
    protected $soft_deletes = true;
    protected $date_format  = "datetime";
    protected $set_created  = true;
    protected $set_modified = true;
    protected $created_field  = "reg_date";
    protected $modified_field = "mod_date";
    
    protected $set_school_date = TRUE;
    
    ///////////////////////////////////////////////////
    //FILTERS
    ///////////////////////////////////////////////////
    
    /**
    * select children in class
    * 
    * @param int $class_uid
    */
    public function filter_class($class_uid)
    {
        if(is_array($class_uid))
        {
            $this->where_in('class_uid', $class_uid);
        }
        else
        {
            $this->where('class_uid', $class_uid);
        }
        
        return $this;
    }
    
    /**
    * select children in grade 
    *     
    * @param int $grade_uid
    */
    public function filter_grade($grade_uid)
    {
        return $this->where('grade_uid', $grade_uid);
    }
    
    /**
    * select expired children
    * 
    */
    public function filter_expires()
    {
        $time = date(FORMAT_DATETIME);
        $this->where('expire_date >=', $time);
        
        return $this;
    }
    
    /**
    * select unexpired children
    * 
    */
    public function filter_out_expires()
    {
        $time = date(FORMAT_DATETIME);
        $this->where('expire_date <=', $time);
        
        return $this;
    }
    
    /**
    * join grade clas class table
    *  
    */
    public function with_class()
    {
        $this->join($this->grade_class_table, "class_uuid=class_uid", 'left')->join("{$this->grade_table} gct", "gct.grade_uuid=grade_uid", 'left');
        return $this;
    }
    
    ///////////////////////////////////////////////////
    //OVERRIDES
    ///////////////////////////////////////////////////
    /**
    * delete children info from table
    * 
    * @param int $children_uid
    * @return bool
    */
    public function delete($children_uid)
    {     
        //$this->_delete_children_parent_router($children_uid);
        return parent::delete($children_uid);
    }
    
    /**
    * update class
    * 
    * @param int $children_uid
    * @param int $class_uid
    */
    public function update_class($children_uid, $class_uid)
    {
        $update = array('class_uid' => $class_uid);
        return $this->where('children_uuid', $children_uid)->update($children_uid, $update);
    }
    
    ///////////////////////////////////////////////////////////////////
    //!COMMON METHODS
    ///////////////////////////////////////////////////////////////////
    
    /**
    * count related parents with children
    * 
    * @param int $children_uid
    */
    public function count_children_parents($children_uid)
    {
        return $this->db->where('children_uid', $children_uid)->count_all_results($this->parent_children_table);
    }
    
    /**
    * get parent uids of children
    * 
    * @param array $children_uids
    */
    public function get_children_parent_uids($children_uids=array())
    {
        if(!is_array($children_uids))
        {
            $children_uids = (array)$children_uids;
        }
        
        $records = $this->db
            ->select('parent_uid')
            ->where_in('children_uid', $children_uids)
            ->group_by('parent_uid')
            ->get($this->parent_children_table)
            ->result_array();
        
        if(empty($records))
        {
            return FALSE;
        }
        
        return array_column_ct($records, 'parent_uid');
    }    
    
    ///////////////////////////////////////////////////////////////////
    //!PRIVATE METHODS
    ///////////////////////////////////////////////////////////////////
        
    /**
    * delete parent-children router
    *  
    * @param int $children_uid
    */
    private function _delete_children_parent_router($children_uid)
    {
        return $this->db->where('children_uid', $children_uid)->delete($this->parent_children_table);    
    }
}