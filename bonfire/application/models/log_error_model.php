<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class log_error_model extends Info_base_model {

    protected $table        = "gbl_errors";
    protected $key          = "uid";
    protected $soft_deletes = false;
    protected $date_format  = "datetime";
    protected $set_created  = true;
    protected $set_modified = false;
    protected $created_field = "reg_date";
    protected $modified_field= "modified_on";
    
}