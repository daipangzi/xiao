<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class log_action_model extends Info_base_model {

    protected $table        = "gbl_actions";
    protected $key          = "uid";
    protected $soft_deletes = false;
    protected $date_format  = "datetime";
    protected $set_created  = true;
    protected $set_modified = false;
    protected $created_field  = "action_date";
    protected $modified_field = "modified_on";
    
}
