<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class log_login_model extends Info_base_model {

    protected $table        = "gul_logins";
    protected $key          = "uid";
    protected $soft_deletes = false;
    protected $date_format  = "datetime";
    protected $set_created  = true;
    protected $set_modified = true;
    protected $created_field  = "login_date";
    protected $modified_field = "login_date";
    
    private $login_attempt_table = "gax_login_attempts";
    
    /**
    * log out by user_id
    * 
    * @param string $user_id
    * @param int    $user_type
    */
    public function log_out_by_userid($user_id, $user_type, $current_connection_uid=FALSE, $login_status=LOGIN_STATUS_DUPLICATE)
    {
        if($current_connection_uid !== FALSE)
        {
            $this->db->where('uid !=', $current_connection_uid);
        }
        
        $update = array('active_status' => $login_status);                                      
        return $this->db
            ->where('user_id', $user_id)
            ->where_in('user_type', $user_type)
            ->where('active_status', LOGIN_STATUS_ONLINE)
            ->update($this->table, $update);
    }
    
    /**
    * log out: normal mode
    * 
    * @param int $uid
    * @return bool
    */
    public function log_out($uid, $login_status=LOGIN_STATUS_OUT)
    {
        $update = array('active_status' => $login_status);
        return $this->update($uid, $update);
    }
    
    /**
    * get connection key for mobile
    * 
    * @param int $uid
    */
    public function get_connection_key($uid)
    {
        $record = $this->find($uid);
        if(empty($record))
        {
            return md5('unknown');
        }
        
        return get_connection_key((array)$record);
    }
    
    /////////////////////////////////////////////////////////////////////
    //login_attempts
    /////////////////////////////////////////////////////////////////////
    
    /**
    * Records a login attempt into the database.
    * 
    * @param string $user_id
    * @param int    $user_type
    * @param string $ip_address
    */
    public function increase_login_attempts($user_id=NULL, $user_type=NULL, $ip_address=NULL)
    {
        if(empty($ip_address) || empty($user_id) || empty($user_type))
        {
            return FALSE;
        }
        
        $data = array();
        $data['user_id']    = $user_id;
        $data['user_type']  = $user_type;
        $data['ip_address'] = $ip_address;
        $data['reg_date']   = date(FORMAT_DATETIME);
        return $this->db->insert($this->login_attempt_table, $data);
    }//end increase_login_attempts()
    //--------------------------------------------------------------------

    /**
    * Clears all login attempts for this user, as well as cleans out old logins.
    * 
    * @param string $login
    * @param int    $expires
    */
    public function clear_login_attempts($user_id=NULL, $user_type=NULL, $ip_address=NULL)
    {
        if (empty($ip_address) || empty($user_id) || empty($user_type))
        {
            return FALSE;
        }
        
        $expires = LOGIN_ATTEMPT_CYCLE * 60;
        $where_str = "(ip_address = '{$ip_address}' AND user_id = '{$user_id}' AND user_type = '{$user_type}')";
        $this->db
            ->where($where_str, NULL)
            ->or_where('reg_date <', date(FORMAT_DATETIME, time() - $expires))
            ->delete($this->login_attempt_table);
        return;
    }//end clear_login_attempts()
    //--------------------------------------------------------------------
    
    /**
    * count login attempts
    * 
    * @param mixed $user_id
    * @param mixed $user_type
    * @param mixed $ip_address
    */
    public function count_login_attempts($user_id=NULL, $user_type=NULL, $ip_address=NULL)
    {
        //remove expires
        $expires = LOGIN_ATTEMPT_CYCLE * 60;
        $this->db
            ->where('reg_date <', date(FORMAT_DATETIME, time() - $expires))
            ->delete($this->login_attempt_table);
        
        return $this->db
            ->where(array('ip_address' => $ip_address, 'user_id' => $user_id, 'user_type' => $user_type))
            ->count_all_results($this->login_attempt_table);
    }
    
    ////////////////////////////////////////////
    //crontab function
    ////////////////////////////////////////////
    /**
    * remove expired rows
    * 
    */
    public function remove_expires() 
    {
        //remove inactive status rows
        $time = time() - 60 * 60 * LOGIN_OUT_EXPIRE_TIME;
        return $this->db
            ->where('login_date <=', date(FORMAT_DATETIME, $time))
            ->where('active_status !=', LOGIN_STATUS_ONLINE)
            ->delete($this->table);       
    }   
}
