<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Clients_model extends Global_List_Model {

	protected $table		= "gus_clients";
	protected $key			= "uuid";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";
	protected $set_created	= false;
	protected $set_modified = false;
}
