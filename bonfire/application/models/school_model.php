<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class School_model extends System_base_model {

	protected $table		= "gbs_school";
	protected $key			= "school_uuid";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";
	protected $set_created	= true;
	protected $set_modified = true;
	protected $created_field = "reg_date";
    protected $modified_field= "mod_date";
    
    ///////////////////////////////////////////////////////////////////
    //!GET METHODS
    ///////////////////////////////////////////////////////////////////
    /**
    * get all classes of school
    * 
    * @param int $school_uid
    */
    public function get_classes($school_uid)
    {
        if($this->selects)
        {
            $this->db->select($this->selects);
        }
        
        return $this->db
            ->join($this->grade_table, "grade_uuid=grade_uid", 'inner')
            ->where('school_uid', $school_uid)
            ->order_by('grade_num')
            ->order_by('class_num')
            ->get($this->grade_class_table)
            ->result(); 
    }
    
    /**
    * count classes
    * 
    * @param int $school_uid
    */
    public function count_classes($school_uid)
    {
        return $this->db
            ->join($this->grade_table, "grade_uuid=grade_uid", 'inner')
            ->where('school_uid', $school_uid)
            ->count_all_results($this->grade_class_table);
    }
    
    /**
    * get classes from uid array
    * 
    * @param array $class_uids
    */
    public function get_some_classes($class_uids)
    {
        if(empty($class_uids))
        {
            return FALSE;
        }
        
        if($this->selects)
        {
            $this->db->select($this->selects);
        }
        
        return $this->db
            ->join($this->grade_table, "grade_uuid=grade_uid", 'inner')
            ->where_in('class_uuid', $class_uids)
            ->order_by('grade_num')
            ->order_by('class_num')
            ->get($this->grade_class_table)
            ->result();     
    }
    
    /**
    * get all subjects by school type
    * 
    * @param int $school_uid
    */
    public function get_subjects($school_uid)
    {
        $selects = $this->selects;
        $school_type = $this->get_school_type($school_uid);
        
        return $this->db
            ->select($selects)
            ->where('subject_scope', $school_type)
            ->order_by('subject_name')
            ->get($this->subject_table)
            ->result();
    }
    
    /**
    * get subject name by subject_uid
    * 
    * @param int $subject_uid
    */
    public function get_subject_name($subject_uid, $school_uid)
    {
        $school_type = $this->get_school_type($school_uid);
        
        $result = $this->db
            ->where('subject_uuid', $subject_uid)
            ->where('subject_scope', $school_type)
            ->get($this->subject_table)
            ->row();        
        
        if(empty($result))
        {
            return FALSE;
        }
        
        return $result->subject_name;
    }
    
    /**
    * get school type of school by uid
    * 
    * @param int $school_uid
    */
    public function get_school_type($school_uid)
    {
        $record = $this->select('school_type')->find($school_uid);
        
        if(empty($record))
        {
            return 1;
        }
                        
        return $record->school_type;
    }
    
    /**
    * get all grades
    * 
    * @param int $school_uid
    */
    public function get_grades($school_uid)
    {      
        return $this->db
            ->where('school_uid', $school_uid)
            ->order_by('grade_num')
            ->get($this->grade_table)
            ->result();    
    }
    
    /**
    * get highest grade of school
    * 
    * @param int $school_uid
    */
    public function get_highest_grade($school_uid)
    {
        $return = $this->db
            ->select_max('grade_num')
            ->where('school_uid', $school_uid)
            ->where('graduate_date IS NULL', NULL)
            ->get($this->grade_table)
            ->row();
        
        if(empty($return))
        {
            return 1;
        }
        
        return $return->grade_num;
    }
    
    /////////////////////////////////////////////////////////
    /**
    * get school list by dropdown format
    * 
    * @param bool   $empty_row
    * @param string $empty_text
    * @return array
    */
    public function format_dropdown($empty_row=TRUE, $empty_text='')
    {
        $records = $this->find_all();
        
        $result = array();
        if($empty_row)
        {
            $result[''] = $empty_text;
        }
        
        if(!empty($records))
        {
            foreach($records as $r)
            {
                $result[$r->school_uuid] = $r->school_name;
            }
        }
        
        return $result;
    }
    
    ///////////////////////////////////////////////////////////////////
    //!ACTION METHODS
    ///////////////////////////////////////////////////////////////////
    
    /**
    * activate school by id
    * 
    * @param int $uid
    */
    public function activate($uid)
    {
        return parent::update($uid, array('active_status'=>STATUS_ACTIVE));
    }
    
    /**
    * deactivate school by id
    * 
    * @param int $uid
    */
    public function deactivate($uid)
    {
        return parent::update($uid, array('active_status'=>STATUS_INACTIVE));
    }
    
    /**
    * delete school completely
    * 
    * @param int $uid
    * @return bool
    */
    public function delete($uid)
    {
        //start trasaction
        $this->db->trans_start();
        $this->_dropSchoolTables($uid);
        $this->_createViews();
        $this->_delete_grade_classes($uid);
        $this->db->trans_complete(); 
        
        //if transaction failed
        $return = TRUE;
        if ($this->db->trans_status() === FALSE)
        {      
            $return = FALSE;
        }
        
        return $return;   
    }
    
    /**
    * insert new school
    * 
    * @param array $data
    * @param array $grade_class_nums
    */
    public function insert_with($data, $grade_class_nums)
    {
        $school_id = parent::insert($data);
        
        //create school tables
        if(is_numeric($school_id))
        {
            //start trasaction
            $this->db->trans_start();
            $this->_dropSchoolTables($school_id);
            $this->_createSchoolTables($school_id);
            $this->_createViews();
            $this->db->trans_complete(); 
            
            //if transaction failed
            if ($this->db->trans_status() === FALSE)
            {      
                $this->delete($school_id);                
                return FALSE;
            }
            //if transaction is complete
            else 
            {
                //insert grade-classes
                krsort($grade_class_nums);
                foreach($grade_class_nums as $grade_no => $class_nums)
                {
                    $class_nums = $class_nums>0?$class_nums:1;
                    $year = get_year_level($grade_no); 
                                          
                    $grade = array();
                    $grade['school_uid'] = $school_id;
                    $grade['grade_num']  = $grade_no; 
                    $grade['year_level'] = $year;
                    
                    $status = $this->insert_grade($grade, $class_nums);
                    if($status === FALSE)
                    {
                        $this->delete($school_id);
                    }
                }
            }
        }
        
        return $school_id;
    }
    
    ///////////////////////////////////////////////////////////////////
    //!GRADE METHODS
    ///////////////////////////////////////////////////////////////////
    
    /**
    * insert new grade for school
    * 
    * @param int    $school_uid
    * @param array  $grade_info
    * @param int    $class_nums
    * 
    * @return bool
    */
    public function insert_grade($grade_info, $class_nums=1)
    {
        $status = $this->db->insert($this->grade_table, $grade_info);            
        if($status === TRUE)
        {
            $grade_uid = $this->db->insert_id();
            for($class_no=1; $class_no<=$class_nums; $class_no++)
            {
                $class_info = array();
                $class_info['grade_uid'] = $grade_uid;
                $class_info['class_num'] = $class_no;
                
                $_status = $this->db->insert($this->grade_class_table, $class_info);            
                if($_status !== TRUE)
                {
                    return FALSE;
                }
            }
        }
        else
        {
            return FALSE;
        }
        
        return TRUE;
    }
    
    /**
    * upgrade all grades by one if new grade exist
    * 
    * @param int $school_uid
    */
    public function upgrade_grades($school_uid)
    {
        $table  = $this->db->dbprefix . $this->grade_table;
        $return = $this->db->query("UPDATE {$table} SET grade_num = grade_num+1 WHERE school_uid = '{$school_uid}'");        
        return $return;
    }
    
    /**
    * set gradeuate date
    * 
    * @param int    $school_uid
    * @param int    $grade_uid
    * @param date   $graduate_date
    */
    public function graduate_grade($school_uid, $grade_uid, $graduate_date=FALSE)
    {
        if($graduate_date === FALSE)
        {
            $graduate_date = date(FORMAT_DATE);
        }
        
        $update = array('graduate_date' => $graduate_date);
        return $this->db
            ->where('grade_uuid', $grade_uid)
            ->where('school_uid', $school_uid)
            ->update($this->grade_table, $update);   
    }
    
    /**
    * change all grade classes to inactive or active
    * 
    * @param int $school_uid
    * @param int $grade_uid
    */
    public function change_grade_class_status($school_uid, $grade_uid, $status=STATUS_ACTIVE)
    {
        $update = array('active_status' => $status);
        return $this->db
            ->where('grade_uid', $grade_uid)
            ->update("sch{$school_uid}_grade_class", $update);   
    }
    
    /**
    * change all grade classes to inactive or active
    * 
    * @param int $school_uid
    * @param int $grade_uid
    */
    public function change_class_status($school_uid, $class_uids, $status=STATUS_ACTIVE)
    {
        $update = array('active_status' => $status);
        
        if(is_array($class_uids))
        {
            $this->db->where_in('uid', $class_uids);
        }
        else
        {
            $this->db->where('uid', $class_uids);
        }
        
        return $this->db->update("sch{$school_uid}_grade_class", $update);   
    }   
    
    /**
    * check if new grade is exist
    * 
    * @param int $school_uid
    */
    public function exist_new_grade($school_uid)
    {
        $count = $this->db
            ->where('school_uid', $school_uid)
            ->where('grade_num', 0)    
            ->count_all_results($this->grade_table);
        
        return $count>0?TRUE:FALSE;
    }
    
    /**
    * get newest year level of school grades
    * 
    * @param int $school_uid
    */
    public function newest_year_level($school_uid)
    {
        $return = $this->db
            ->select_max('year_level')
            ->where('school_uid', $school_uid)
            ->get($this->grade_table)
            ->row();
        
        if(empty($return))
        {
            return 0;
        }
        
        return $return->year_level;
    }
    
    /**
    * get class row by uid
    * 
    * @param int $school_uid
    * @param int $class_uid
    * @param int $return_type
    */
    public function get_class_by_uid($school_uid, $class_uid, $return_type=RETURN_TYPE_OBJECT)
    {
        $select = '*';
        if($this->selects != '')
        {
            $select = $this->selects;
        }
        
        $query = $this->db
            ->select($select)
            ->join("sch{$school_uid}_grade", "grade_uuid=grade_uid", 'left')
            ->where('school_uid', $school_uid)
            ->where('class_uuid', $class_uid)
            ->get($this->grade_class_table);
        
        $record = FALSE;
        if($return_type === RETURN_TYPE_OBJECT)
        {
            $record = $query->row($class_uid);
        }
        else
        {
            $record = $query->row_array($class_uid);
        }
        
        return $record;
    }
    
    ///////////////////////////////////////////////////////////////////
    //!VALID METHODS
    ///////////////////////////////////////////////////////////////////
    
    /**
    * check subject is valid
    * 
    * @param int $school_uid
    * @param int $subject_id
    */
    public function valid_subject($school_uid, $subject_id)
    {
        $school_type = $this->get_school_type($school_uid);
        $count = $this->db
            ->where('subject_scope', $school_type)
            ->where('subject_uuid', $subject_id)
            ->count_all_results($this->subject_table);
            
        return $count=1?TRUE:FALSE;
    }
    
    /**
    * check all subjects are valid
    * 
    * @param int    $school_uid
    * @param array  $subject_ids
    */
    public function valid_subjects($school_uid, $subject_ids=array())
    {
        $school_type = $this->get_school_type($school_uid);
        $count = $this->db
            ->where('subject_scope', $school_type)
            ->where_in('subject_uuid', $subject_ids)
            ->count_all_results($this->subject_table);
            
        return $count==count($subject_ids)?TRUE:FALSE;
    }
    
    /**
    * check class is valid
    * 
    * @param int $school_uid
    * @param int $class_id
    */
    public function valid_class($school_uid, $class_id)
    {
        $count = $this->db
            ->join($this->grade_table, 'grade_uuid=grade_uid', 'left')
            ->where_in('class_uuid', $class_id)
            ->where('school_uid', $school_uid)
            ->count_all_results($this->grade_class_table);
            
        return $count==1?TRUE:FALSE;
    }
    
    /**
    * check all classes are valid
    * 
    * @param int $school_uid
    * @param int $class_ids
    */
    public function valid_classes($school_uid, $class_ids=array())
    {
        $count = $this->db
            ->join($this->grade_table, 'grade_uuid=grade_uid', 'left')
            ->where_in('class_uuid', $class_ids)
            ->where('school_uid', $school_uid)
            ->count_all_results($this->grade_class_table);
            
        return $count==count($class_ids)?TRUE:FALSE;
    }
    
    /**
    * check school is valid
    * 
    * @param int $school_uid
    */
    public function valid_school($school_uid)
    {
        $record = $this->find($school_uid);
        
        if(empty($record))
        {
            return FALSE;
        }
        
        if((int)$record->active_status === STATUS_INACTIVE)
        {
            return FALSE;
        }
        
        return TRUE;
    }
    
    ///////////////////////////////////////////////////////////////////
    //!ACCESSORY METHODS
    /////////////////////////////////////////////////////////////////// 
    
    /**
    * select school_name by given language
    * 
    * @param string $short_lang : ko, cn, en, jp
    */
    public function select_name($short_lang)
    {
        $field = 'school_name_' . $short_lang;
        if($this->selects)
        {
            $this->selects .= ", school_name_{$short_lang} school_name";
        }
        else
        {
            $this->selects .= "*, school_name_{$short_lang} school_name";
        }
        
        return $this;
    }
    
    //left join region table
    public function with_region()
    {
        return $this->join($this->region_table, 'region_uuid=region_uid', 'left');        
    }
    
    ///////////////////////////////////////////////////////////////////
    //!FILTER METHODS
    /////////////////////////////////////////////////////////////////// 
    /**
    * select rows where active_status = 1
    * 
    * @return School_model
    */
    public function filter_lives()
    {
        return $this->where('active_status', STATUS_ACTIVE);
    }
    
    /**
    * select only live grades
    * 
    * @return School_model
    */
    public function filter_live_grades()
    {
        $this->db->where('graduate_date IS NULL', NULL);
        return $this;
    }
    
    /**
    * select only graduated grades
    * 
    * @return School_model 
    */
    public function filter_old_grades()
    {
        $this->db->where('graduate_date IS NOT NULL', NULL);
        return $this;
    }    
    
    /**
    * select only one grade
    *  
    * @param int $grade_uid
    * @return School_model
    */
    public function filter_grade($grade_uid)
    {
        $this->db->where('grade_uid', $grade_uid);
        return $this;
    }   
    
    /**
    * select only live classes
    * 
    * @return School_model
    */
    public function filter_live_classes()
    {
        $this->db->where('active_status', STATUS_ACTIVE);
        return $this;
    }    
    
    ///////////////////////////////////////////////////////////////////
    //!ORDER METHODS
    ///////////////////////////////////////////////////////////////////
    public function order_by_name($order='asc')
    {
        $this->order_by('school_name', $order);
        return $this;
    }
    
    public function order_by_type($order='asc')
    {
        $this->order_by('school_type', $order);
        return $this;
    }
    
    public function order_by_date($order='asc')
    {
        $this->order_by('reg_date', $order);
        return $this;
    }
    
    public function order_by_state($order='asc')
    {
        $this->order_by('status', $order);
        return $this;
    }
    
    public function order_by_region($order='asc')
    {
        $this->order_by('region_uid', $order);
        return $this;
    }
    
    ///////////////////////////////////////////////////////////////////
    //!PRIVATE METHODS
    ///////////////////////////////////////////////////////////////////
    
    /**
    * create tables for new school
    * 
    * @param int $school_id
    */
    private function _createSchoolTables($school_id)
    {
        $prefix = $this->db->dbprefix . sprintf($this->school_table_prifix_rule, $school_id);
               
        //create bf_sch_attendance
        /*$this->db->query("CREATE TABLE {$prefix}attendance (
          `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `class_uid` smallint(5) unsigned NOT NULL,
          `standard_date` datetime DEFAULT NULL,
          `attendance_title` varchar(255) NOT NULL,
          `attendance_contents` text NOT NULL,
          `reg_date` datetime DEFAULT NULL,
          `register_name` varchar(10) NOT NULL,
          PRIMARY KEY (`uid`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
        
        //create bf_sch_attendance_details
        $this->db->query("CREATE TABLE {$prefix}attendance_details (
          `children_uid` mediumint(8) unsigned NOT NULL,
          `attendance_uid` int(10) unsigned NOT NULL,
          `attendance_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
          `attendance_date` datetime DEFAULT NULL,
          `attendance_details` text NOT NULL,
          `sending_date` datetime DEFAULT NULL,
          PRIMARY KEY (`children_uid`,`attendance_uid`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
        
        //create bf_sch_score
        $this->db->query("CREATE TABLE {$prefix}score (
          `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `class_uid` smallint(5) unsigned NOT NULL,
          `subject_uid` tinyint(3) unsigned NOT NULL,
          `standard_score` varchar(10) NOT NULL,
          `test_title` varchar(255) NOT NULL,
          `test_contents` text NOT NULL,
          `test_date` date DEFAULT NULL,
          `reg_date` datetime DEFAULT NULL,
          `register_name` varchar(10) NOT NULL,
          PRIMARY KEY (`uid`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
        
        //create bf_sch_score_details
        $this->db->query("CREATE TABLE {$prefix}score_details (
          `children_uid` mediumint(8) unsigned NOT NULL,
          `score_uid` int(10) unsigned NOT NULL,
          `score_value` varchar(10) NOT NULL,
          `score_details` text NOT NULL,
          `sending_date` datetime DEFAULT NULL,
          PRIMARY KEY (`children_uid`,`score_uid`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8"); */
        
        //create bf_sch_message
        $this->db->query("CREATE TABLE {$prefix}chat (
          `chat_uuid` bigint(30) unsigned NOT NULL AUTO_INCREMENT,
          `participants` text NOT NULL,
          `creator` varchar(20) NOT NULL,
          `unsubscriber` mediumtext,
          `created` datetime NOT NULL,
          `last_change` datetime NOT NULL,
          PRIMARY KEY (`chat_uuid`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
        
        //created bf_sch_message_attachements
        $this->db->query("CREATE TABLE {$prefix}chat_messages (
          `message_uuid` bigint(30) unsigned NOT NULL AUTO_INCREMENT,
          `chat_uid` bigint(30) unsigned NOT NULL,
          `message_contents` text,
          `file_uid` bigint(20) DEFAULT NULL,
          `sent_time` datetime NOT NULL,
          `sender_id` varchar(20) NOT NULL,
          `sender_name` varchar(50) NOT NULL,
          `sender_type` tinyint(3) NOT NULL,
          `sender_additional` text,
          `subscriber` mediumtext,
          PRIMARY KEY (`message_uuid`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
        
        //create bf_sch_message_sender
        $this->db->query("CREATE TABLE {$prefix}chat_files (  
          `file_uuid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
          `file_name` varchar(512) NOT NULL,
          `file_size` decimal(15,1) NOT NULL DEFAULT '0.0' COMMENT 'kb',
          `file_path` varchar(100) NOT NULL,
          PRIMARY KEY (`file_uuid`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
    }
    
    /**
    * drop tables for school if deleted
    * 
    * @param int $school_id
    */
    private function _dropSchoolTables($school_id) 
    {
        $prefix = $this->db->dbprefix . sprintf($this->school_table_prifix_rule, $school_id);
        
        /*$this->db->query("DROP TABLE IF EXISTS {$prefix}attendance");
        $this->db->query("DROP TABLE IF EXISTS {$prefix}attendance_details");
        $this->db->query("DROP TABLE IF EXISTS {$prefix}score");
        $this->db->query("DROP TABLE IF EXISTS {$prefix}score_details");*/
        $this->db->query("DROP TABLE IF EXISTS {$prefix}chat");
        $this->db->query("DROP TABLE IF EXISTS {$prefix}chat_messages");
        $this->db->query("DROP TABLE IF EXISTS {$prefix}chat_files");
    }
    
    /**
    * create additional views for convenience when create or delete
    * 
    */
    private function _createViews()
    {
        
    }
    
    /**
    * create messages view
    * 
    */
    private function _createMessageView()
    {
        $pf = $this->db->dbprefix;
        
        $this->db->query("DROP VIEW IF EXISTS {$pf}g_messages");
        
        $records = $this->find_all();
        if(!empty($records))
        {
            $i  = 0;
            $sql = "CREATE VIEW `{$pf}g_messages` AS";
            
            $keys = 'mg.*, mgs.sender_id, mgs.sender_name, mgs.sender_type';
            foreach($records as $r)
            {
                if($i == 0)
                {
                    $sql .= " SELECT {$r->uid} school_uid, {$keys}  FROM {$pf}sch{$r->uid}_message mg LEFT JOIN {$pf}sch{$r->uid}_message_sender mgs ON mgs.message_uid=mg.uid ";
                }
                else
                {
                    $sql .= " UNION SELECT {$r->uid} school_uid, {$keys} FROM {$pf}sch{$r->uid}_message mg LEFT JOIN {$pf}sch{$r->uid}_message_sender mgs ON mgs.message_uid=mg.uid ";
                }
                $i++;
            }
                    
            $this->db->query($sql);
        }
    }
    
    /**
    * delete school with grade-class
    * 
    * @param int $school_uid
    */
    private function _delete_grade_classes($school_uid)
    {
        $prefix = $this->db->dbprefix;
        
        $grades = $this->get_grades($school_uid);
        foreach($grades as $grade)
        {
            $this->db->query("DELETE FROM {$prefix}{$this->grade_class_table} WHERE grade_uid={$grade->grade_uuid}");
        }
        
        $this->db->query("DELETE FROM {$prefix}{$this->grade_table} WHERE school_uid={$school_uid}");
        $this->db->query("DELETE FROM {$prefix}{$this->table} WHERE school_uuid={$school_uid}");
    }
}
