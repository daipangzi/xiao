<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Messages_model extends Global_List_Model {

    protected $table        = "g_messages";
    protected $key          = "uid";
    protected $soft_deletes = false;
    protected $date_format  = "datetime";
    protected $set_created  = false;
    protected $set_modified = false;
    protected $created_field  = "reg_date";
    protected $modified_field = "modified_on";
    
}
