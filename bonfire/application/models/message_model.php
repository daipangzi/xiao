<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Message_model extends System_base_model {

    protected $table		= 'sch%1$05d_chat_messages';
    protected $key			= "message_uuid";
    protected $soft_deletes	= false;
    protected $date_format	= "datetime";
    protected $set_created	= true;
    protected $set_modified = false;
    protected $created_field  = "sent_time";
    protected $modified_field = "modified_on";

    private $_attach_base_url   = '';
    private $_file_key_rule     = '';
    private $_session_key_rule  = '';
    private $_session_key_rule2 = '';


    public function __construct()
    {
        parent::__construct();
        
        $this->_attach_base_url = '';    
        $this->_file_key_rule   = "MD5(CONCAT(file_uuid, file_name, file_path))";
        $this->_session_key_rule = "MD5(CONCAT(chat_uid, creator, chat_type, 'c'))";
        $this->_session_key_rule2= "MD5(CONCAT(chat_uuid, creator, chat_type, 'c'))";
    }

    /**
    * create new chat session
    * 
    * @param array  $participants
    * @param string $personal_identity
    */
    public function create_chat_session($chat_type, $participants, $personal_identity)
    {
        $data = array();
        $data['participants']= implode(':', $participants);    
        $data['creator']     = $personal_identity;
        $data['chat_type']   = $chat_type;
        $data['created']     = date(FORMAT_DATETIME);
        $data['last_change'] = $data['created'];
                  
        $find_key = md5($data['participants'] . $data['creator'] . $chat_type);
        $exist_session_uid = $this->_get_chat_uid_by_creator_participant($find_key);
        $new_session_uid = 0;
        if($exist_session_uid === 0)
        {
            $status = $this->db->insert($this->chat_table, $data);
            if($status === FALSE)
            {
                return 0;
            }
              
            $new_session_uid = $this->db->insert_id(); 
        }
        else
        {
            $new_session_uid = $exist_session_uid; 
        }
        
        return $new_session_uid;
    }
    
    /**
    * update last_changet field of chat table
    * 
    * @param int $chat_uid
    */
    public function update_chat_session_change_time($chat_uid)
    {
        $update = array();
        $update['last_change'] = date(FORMAT_DATETIME);
        $update['chat_unsubscriber'] = NULL;
        
        return $this->db
            ->where('chat_uuid', $chat_uid)
            ->update($this->chat_table, $update);
    }
    
    /**
    * get chat session info by session_key
    * 
    * @param string $chat_session_key
    * @param string $chat_conditions
    */
    public function get_chat_session($chat_session_key, $chat_conditions)
    {
        $result = $this->db
            ->where("{$this->_session_key_rule2} = '{$chat_session_key}'", NULL)
            ->where($chat_conditions, NULL)
            ->get($this->chat_table)
            ->row();
            
        if(empty($result))
        {
            return NULL;
        }
        
        return $result;
    }
    
    /**
    * insert attachment files of message
    * 
    * @param array $data
    */
    public function insert_attachment($data)
    {
        $status = $this->db->insert($this->chat_file_table, $data);
        if($status === TRUE)
        {
            return $this->db->insert_id();
        }
        
        return FALSE;
    }
    
    public function get_exist_attachment($file_key)
    {
        if($file_key == '') return FALSE;
        
        $result = $this->db
            ->select()
            ->where("{$this->_file_key_rule}=", $file_key)
            ->get($this->chat_file_table)
            ->row();
        
        if(empty($result))
        {
            return FALSE;
        }
        
        return $result;
    }
    
    /**
    * get just sent message
    * 
    * @param int $message_uid
    */
    public function get_sent_message($message_uid)
    {
        $result = $this
            ->select("message_uuid message_key, sent_time, message_type, message_contents, 
                IFNULL({$this->_file_key_rule}, '') file_key,
                IFNULL(file_name, '') file_name, 
                IFNULL(file_size, '') file_size, 
                IF(file_path IS NULL, '', CONCAT('{$this->_attach_base_url()}', file_path)) file_path", FALSE)
            ->join("{$this->chat_file_table}", 'file_uid=file_uuid', 'left')
            ->find($message_uid);
            
        return $result;
    }
    
    /**
    * get chat session array
    * 
    * @param string $chat_conditions
    */
    public function get_chat_sessions($chat_conditions, $personal_identity)
    {
        $result = $this
            ->select("{$this->_session_key_rule} session_key, participants, created, last_change, chat_type,
                IF(LOCATE('{$personal_identity}', subscriber)=0, 1, 0) is_new", FALSE)
            ->join($this->chat_table, 'chat_uuid=chat_uid', 'left')
            ->where($chat_conditions, NULL)
            ->group_by('chat_uid')
            ->order_by('last_change', 'desc')
            ->find_all();
        
        return $result;
    }
    
    /**
    * get chat history
    * 
    * @param string $session_key
    * @param time   $start_time
    * @param time   $end_time
    */
    public function get_chat_conversations($session_key, $start_time, $end_time, $personal_identity)
    {
        $base_url = $this->_attach_base_url();
        $chat_uid = $this->_get_chat_uid_by_key($session_key);
        
        if($chat_uid === 0)
        {
            return array();
        }
        
        $conditions = "((sent_time >= '{$start_time}' AND sent_time < '{$end_time}') OR (LOCATE('{$personal_identity}', IFNULL(subscriber, '')) = 0))";
        $result = $this
            ->select("message_uuid  message_key, message_type, message_contents, sent_time, 
                sender_id, sender_name, sender_type, '' sender_photo, sender_additional,
                IFNULL({$this->_file_key_rule}, '') file_key,
                IFNULL(file_name, '') file_name, 
                IFNULL(file_size, '') file_size, 
                IF(file_path IS NULL, '', CONCAT('{$base_url}', file_path)) file_path", FALSE)
            ->join("{$this->chat_file_table}", 'file_uid=file_uuid', 'left')
            ->where('chat_uid', $chat_uid)
            ->where($conditions, NULL)
            ->where("LOCATE('{$personal_identity}', IFNULL(unsubscriber, '')) = 0", NULL)
            ->order_by('sent_time', 'desc')
            ->find_all();
            
        if(empty($result))
        {
            return array();
        }
        
        return $result;
    }
    
    /**
    * get new chat conversations
    * 
    * @param string $session_key
    * @param string $personal_identity
    */
    public function get_new_chat_conversations($session_key, $personal_identity)
    {
        $base_url = $this->_attach_base_url();
        $chat_uid = $this->_get_chat_uid_by_key($session_key);
              
        if($chat_uid === 0)
        {
            return array();
        }
        
        $result = $this
            ->select("message_uuid  message_key, message_type, message_contents, sent_time, 
                sender_id, sender_name, sender_type, '' sender_photo, sender_additional,
                IFNULL({$this->_file_key_rule}, '') file_key,
                IFNULL(file_name, '') file_name, 
                IFNULL(file_size, '') file_size, 
                IF(file_path IS NULL, '', CONCAT('{$base_url}', file_path)) file_path", FALSE)
            ->join("{$this->chat_file_table}", 'file_uid=file_uuid', 'left')
            ->where('chat_uid', $chat_uid)
            ->where("LOCATE('{$personal_identity}', subscriber) = 0", NULL)
            ->order_by('sent_time', 'desc')
            ->find_all();
            
        if(empty($result))
        {
            return array();
        }
        
        return $result;
    }
    
    /**
    * get message numbers per chat sessions
    * 
    * @param string $chat_conditions
    */
    public function get_message_nums_by_chat_session($chat_conditions, $personal_identity)
    {
        $result = $this
            ->select("{$this->_session_key_rule} chat_session, participants, 
                created, chat_type, last_change, count(*) new_message_nums", FALSE)
            ->join($this->chat_table, 'chat_uuid=chat_uid', 'left')
            ->where($chat_conditions, NULL)
            ->where("LOCATE('{$personal_identity}', subscriber)=0", NULL)
            ->group_by('chat_uid')
            ->order_by('last_change', 'desc')
            ->find_all();
            
        return $result;    
    }
    
    /**
    * count new meessage nums for chat_session
    * 
    * @param string $session_key
    */
    public function get_message_nums($session_key, $personal_identity)
    {
        return $this
            ->join($this->chat_table, 'chat_uuid=chat_uid', 'left')
            ->where("{$this->_session_key_rule} =", $session_key)
            ->where("LOCATE('{$personal_identity}', subscriber)=0", NULL)
            ->group_by('chat_uid')
            ->count_all();
    }
    
    /**
    * add my personal_identity to subscribers field
    * 
    * @param string     $session_key
    * @param string     $personal_identity
    * @param timestamp  $search_time
    */
    public function subscribe_me_to_chat_session($session_key, $personal_identity, $search_time)
    {
        $chat_uid = $this->_get_chat_uid_by_key($session_key);
        $tbl_name = $this->db->dbprefix . $this->table;
        $sql = "UPDATE {$tbl_name} 
                SET subscriber = CONCAT(subscriber, ':', '{$personal_identity}')
                WHERE chat_uid = {$chat_uid} AND
                    sent_time <= '{$search_time}' AND
                    LOCATE('{$personal_identity}', subscriber) = 0";
        $this->db->query($sql);    
    }
    
    /**
    * add personal_identity to chat_unsubscriber
    * 
    * @param int    $chat_uid
    * @param string $personal_identity
    */
    public function unsubscribe_chat_session($chat_uid, $personal_identity)
    {
        $tbl_name = $this->db->dbprefix . $this->chat_table;
        $sql = "UPDATE {$tbl_name} 
                SET chat_unsubscriber = CONCAT(IFNULL(chat_unsubscriber, ''), '{$personal_identity}', ':')
                WHERE chat_uuid = {$chat_uid} 
                    AND LOCATE('{$personal_identity}', IFNULL(chat_unsubscriber, '')) = 0";
        $this->db->query($sql);        
         
        return TRUE;
    }
    
    /**
    * add personal_identity to message unsubscriber
    * 
    * @param int    $chat_uuid
    * @param int    $message_key
    * @param string $personal_identity
    */
    public function unsubscribe_message($chat_uid, $message_key, $personal_identity)
    {
        $tbl_name = $this->db->dbprefix . $this->table;
        $sql = "UPDATE {$tbl_name} 
                SET unsubscriber = CONCAT(IFNULL(unsubscriber, ''), '{$personal_identity}', ':')
                WHERE chat_uid = {$chat_uid} 
                    AND message_uuid = {$message_key} 
                    AND LOCATE('{$personal_identity}', IFNULL(unsubscriber, '')) = 0";
        $this->db->query($sql);        
         
        return TRUE;
    }
    
    /**
    * get chat contents by date and type(no session)
    * 
    * @param int    $chat_type
    * @param string $chat_date
    * @param string $chat_conditions
    */
    public function get_by_date_and_type($chat_type, $chat_date, $chat_conditions)
    {
        $base_url = $this->_attach_base_url();
        $result = $this
            ->select("{$this->_session_key_rule} session_key, message_uuid  message_key, 
                message_type, message_contents, sent_time, 
                sender_id, sender_name, sender_type, '' sender_photo,
                IFNULL({$this->_file_key_rule}, '') file_key,
                IFNULL(file_name, '') file_name, 
                IFNULL(file_size, '') file_size, 
                IF(file_path IS NULL, '', CONCAT('{$base_url}', file_path)) file_path", FALSE)
            ->join("{$this->chat_file_table}", 'file_uid=file_uuid', 'left')
            ->join($this->chat_table, 'chat_uuid=chat_uid', 'left')         
            ->where("message_type", $chat_type)
            ->where("chat_type", $chat_type)
            ->where("DATE(sent_time)=DATE('{$chat_date}')", NULL)
            ->where_in("sender_type", array(USER_TYPE_MASTER, USER_TYPE_TEACHER))
            ->where($chat_conditions, NULL)
            ->order_by('sent_time', 'desc')
            ->find_all();
        
        if(empty($result))
        {
            $result = array();
        }
            
        return $result;     
    }
    /////////////////////////////////////////////////////////////////////
    //PRIVATE METHODS
    /////////////////////////////////////////////////////////////////////
    
    /**
    * get exist chat_session_uid by participanrt and creator if exist
    * 
    * @param string $find_key
    */
    private function _get_chat_uid_by_creator_participant($find_key)
    {
        $result = $this->db
            ->select('chat_uuid')
            ->where('MD5(CONCAT(participants, creator, chat_type)) = ', $find_key)
            ->get($this->chat_table)
            ->row();
                     
        if(empty($result))
        {
            return 0;
        }
        
        return $result->chat_uuid;
    }
    
    /**
    * get chat_uid by session_key
    * 
    * @param string $session_key
    */
    private function _get_chat_uid_by_key($session_key)
    {
        $result = $this->db
            ->where("{$this->_session_key_rule2} = '{$session_key}'", NULL)
            ->get($this->chat_table)
            ->row();
          
        if(empty($result))
        {
            return 0;
        }
        
        return $result->chat_uuid;
    }
    
    private function _attach_base_url()
    {
        return site_url('media/attachments/' . $this->school_uid) . '/';
    }
}
