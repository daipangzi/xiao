<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Teacher_model extends User_list_Model {

	protected $table		= "gus_teachers";
	protected $key			= "teacher_uuid";
	protected $soft_deletes	= true;
	protected $date_format	= "datetime";
	protected $set_created	= true;
	protected $set_modified = true;
    protected $created_field  = "reg_date";
    protected $modified_field = "mod_date";
    
    protected $set_school_date = TRUE;
    
    /**
    * insert teacher datas
    * 
    * @param array  $data
    * @param int    $status
    */
    public function insert_with($data, $status=STATUS_INACTIVE)
    {
        //insert teacher info        
        $teacher = array();
        $teacher['school_uid']  = $data['school_uid'];
        $teacher['user_id']     = $data['user_id'];
        $teacher['user_name']   = $data['user_name'];
        $teacher['user_sex']    = $data['user_sex'];
        $teacher['user_phone']  = $data['user_phone'];
        $teacher['user_email']  = $data['user_email'];
        $teacher['user_passwd'] = $data['user_passwd'];
        $teacher['user_status'] = $status; 
        $teacher['user_birthday']= $data['user_birthday'];
        //$teacher['user_address']= $data['user_address'];
        $teacher['user_photo'] = $data['user_photo'];
        
        $teacher_uid = $this->insert($teacher);
        if($teacher_uid === FALSE)
        {
            return FALSE;
        }
                
        if(!$this->add_links($teacher_uid, $data))
        {
            $this->delete($teacher_uid);
            return FALSE;
        }
        
        return $teacher_uid;
    }
    
    /**
    * delete teacher info 
    * 
    * @param int $teacher_uid
    * @return bool
    */
    public function delete($teacher_uid)
    {     
        $this->delete_class_links($teacher_uid);
        $this->delete_subject_link($teacher_uid);
        
        return parent::delete($teacher_uid);
    }
    
    /**
    * delete subject and class links
    * 
    * @param int $teacher_uid
    */
    public function delete_links($teacher_uid)
    {
        $this->delete_class_links($teacher_uid);
        $this->delete_subject_link($teacher_uid);
    }
    
    /**
    * add subject als class links
    * 
    * @param int    $teacher_uid
    * @param array  $data
    * 
    * @return bool
    */
    public function add_links($teacher_uid, $data)
    {
        //insert charge classes
        if($data['is_charge'] == 1 && $data['charge_class'] != '')
        {
            $ins_rows = $this->add_class_link($teacher_uid, ROLE_CHARGE, explode(',', $data['charge_class']));
            if($ins_rows == 0)
            {
                //rollback
                $this->delete($teacher_uid);
                return FALSE;
            }
        }
        
        //insert subject
        if(isset($data['subject_uid']) && $data['subject_uid'] != '')
        { 
            $status = $this->add_subject_link($teacher_uid, $data['subject_uid']);
            if($status === FALSE)
            {
                //rollback
                $this->delete($teacher_uid);
                return FALSE;
            }
        }   
           
        //insert subject classes
        if($data['is_subject'] == 1 && $data['subject_class'] != '')
        {
            //insert charge subject classes 
            $ins_rows = $this->add_class_link($teacher_uid, ROLE_SUBJECT, explode(',', $data['subject_class']));
            if($ins_rows == 0)
            {
                //rollback
                $this->delete($teacher_uid);
                return FALSE;
            }
        }
        
        return TRUE;    
    }
    
    /**
    * make as master
    * 
    * @param string $teacher_uid
    * 
    * @return bool
    */
    public function update_as_master($teacher_uid)
    {
        //remove if master exist
        $update = array('teacher_type' => USER_TYPE_TEACHER);
        $this->db->update($this->table, $update);
        
        //make this as master
        $update = array('teacher_type' => USER_TYPE_MASTER);
        return parent::update($teacher_uid, $update);
    }
    
    /**
    * make as teacher
    * 
    * @param int $teacher_uid
    * 
    * @return bool
    */
    public function update_as_teacher($teacher_uid)
    {
        $update = array('teacher_type' => USER_TYPE_TEACHER);
        return parent::update($teacher_uid, $update);
    }
    
    /**
    * delete old subject and add new subject for teacher
    * 
    * @param int $teacher_uid
    * @param int $subject_uid
    * 
    * @return bool
    */
    public function change_subject($teacher_uid, $subject_uid)
    {
        $this->delete_subject_link($teacher_uid);
        return $this->add_subject_link($teacher_uid, $subject_uid);
    }
    
    /**
    * insert class links of teacher
    * 
    * @param int    $teacher_uid
    * @param int    $teacher_role
    * @param array  $class_uids
    * @return int
    */
    public function add_class_link($teacher_uid, $teacher_role, $class_uids)
    {
        if(count($class_uids) == 0) 
        {
            return 0;
        }
        
        $rows = 0;
        foreach($class_uids as $class_uid)
        {
            $data = array();
            $data['teacher_uid'] = $teacher_uid;
            $data['teacher_role']= $teacher_role;
            $data['class_uid']   = $class_uid;
            $data['active_date'] = date(FORMAT_DATETIME);
            if($this->db->insert($this->teacher_class_table, $data))
            {
                $rows++;
            }
        }
        
        return $rows;
    } 
    
    /**
    * make it as a inactive status
    * 
    * @param int $teacher_uid
    * @param int $teacher_role
    * @param array $class_uids
    */
    public function delete_class_links($teacher_uid, $teacher_role=FALSE, $class_uids=array())
    {       
        if(!empty($class_uids))
        {
            $this->db->where_in('class_uid', $class_uids);
        }
        
        if($teacher_role !== FALSE)
        {
            $this->db->where('teacher_role', $teacher_role);
        }
        
        return $this->db
            ->where('teacher_uid', $teacher_uid)
            ->delete($this->teacher_class_table);
    }
    
    /**
    * insert charge subject for teacher
    * 
    * @param int $teacher_uid
    * @param int $subject_uid
    * @return bool
    */
    public function add_subject_link($teacher_uid, $subject_uid)
    {
        $data = array();
        $data['teacher_uid'] = $teacher_uid;
        $data['subject_uid'] = $subject_uid;
        $data['active_date'] = date(FORMAT_DATETIME);
        
        return $this->db->insert($this->teacher_subject_table, $data);
    }
    
    /**
    * delete charge subjects for teacher
    * 
    * @param int $teacher_uid
    */
    public function delete_subject_link($teacher_uid)
    {
        return $this->db
            ->where('teacher_uid', $teacher_uid)
            ->delete($this->teacher_subject_table);
    }
    ///////////////////////////////////////////////////////////////////
    //!COMMON METHODS
    ///////////////////////////////////////////////////////////////////
    /**
    * find master row
    * 
    * @param int $school_uid
    */
    public function find_master($school_uid)
    {
        $records = $this
            ->where('teacher_type', USER_TYPE_MASTER)
            ->where('school_uid', $school_uid)
            ->find_all();
        
        if(empty($records))
        {
            return new stdClass();
        }
        
        return $records[0];
    }
    
    /**
    * find teachers by class
    * 
    * @param int $class_uid
    * @param itn $teacher_role
    * @param int $return_type
    */
    public function find_all_by_class($class_uid, $teacher_role=FALSE, $return_type=RETURN_TYPE_OBJECT)
    {
        if(is_array($class_uid))
        {
            $this->db->where_in('class_uid', array_unique($class_uid));    
        }
        else
        {
            $this->db->where('class_uid', $class_uid);    
        }
        
        if($teacher_role !== FALSE)
        {
            $this->where('teacher_role', $teacher_role);
        }
        
        $records = $this
            ->join($this->teacher_class_table, "teacher_uid={$this->key}", 'left')
            ->where('user_status', STATUS_ACTIVE)
            ->order_by('teacher_role')
            ->order_by('class_uid')
            ->group_by('teacher_uid')
            ->find_all($return_type);
                  
        return $records;
    }
        
    /**
    * get teacher's class uids
    * 
    * @param int $teacher_uid
    * @param int $teacher_role
    */
    public function get_class_uids($teacher_uid, $teacher_role=FALSE)
    {
        if($teacher_role !== FALSE)
        {
            $this->db->where('teacher_role', $teacher_role);
        }
        
        $records = $this->db
            ->select("class_uid")
            ->where('teacher_uid', $teacher_uid)
            ->get($this->teacher_class_table)
            ->result_array();
            
        if(empty($records))
        {
            return array();
        }
        
        return array_column_ct($records, 'class_uid');                    
    }
    
    /**
    * get teacher's charge subject
    * 
    * @param int $teacher_uid
    */
    public function get_charge_subject($teacher_uid)
    {
        $records = $this->db
            ->select('subject_uid')
            ->where('teacher_uid', $teacher_uid)
            ->get($this->teacher_subject_table)
            ->result();
            
        if(empty($records))
        {
            return FALSE;
        }        
        
        return $records[0]->subject_uid;
    }
    
    /**
    * get teacher's charge subject
    * 
    * @param int $teacher_uid
    */
    public function get_charge_subject_name($teacher_uid)
    {
        $records = $this->db
            ->select('subject_name')
            ->join($this->subject_table, 'subject_uid=subject_uuid', 'left')
            ->where('teacher_uid', $teacher_uid)
            ->get($this->teacher_subject_table)
            ->result();
            
        if(empty($records))
        {
            return '';
        }        
        
        return $records[0]->subject_name;
    }
    
    /**
    * get charge teacher uids of class
    * 
    * @param int $class_uid
    */
    public function get_class_teacher_uids($class_uid)
    {
        $records = $this->db
            ->where('class_uid', $class_uid)
            ->where("teacher_role", ROLE_CHARGE)
            ->order_by('active_date', 'desc')
            ->get($this->teacher_class_table)
            ->result();    
        
        if(empty($records))
        {
            return FALSE;
        }
        
        return array_column_ct($records, 'teacher_uid');
    }
    
    ///////////////////////////////////////////////////////////////////
    //!PRIVATE METHODS
    ///////////////////////////////////////////////////////////////////
    
    /**
    * check if charge subject is changed
    * 
    * @param int $teacher_uid
    * @param int $subject_uid
    */
    private function _is_subject_changed($teacher_uid, $subject_uid)
    {
        $count = $this->db
            ->where('teacher_uid', $teacher_uid)
            ->where('subject_uid', $subject_uid)
            ->count_all_results($this->teacher_subject_table);
        
        return $count>0?FALSE:TRUE;
    }
    
    
    ///////////////////////////////////////////////////////////////////
    //!For only admin
    ///////////////////////////////////////////////////////////////////
    /**
    * update with all related info
    * 
    * @param int    $teacher_uid
    * @param array  $data
    * @return bool
    */
    public function update_school_info($teacher_uid, $data=array())
    {
        if($data['is_charge'] == '1')
        {
            $current_classes = $this->get_class_uids($teacher_uid, ROLE_CHARGE);
            $new_classes = explode(',', $data['charge_class']);
            
            $must_delete_links = array_diff($current_classes, $new_classes);
            $must_add_links    = array_diff($new_classes, $current_classes);
            
            if(!empty($must_delete_links))
            {
                $this->delete_class_links($teacher_uid, ROLE_CHARGE, $must_delete_links);
            }
            
            if(!empty($must_add_links))
            {
                $this->add_class_link($teacher_uid, ROLE_CHARGE, $must_add_links);
            }
        }   
        else        
        {
            $this->delete_class_links($teacher_uid, ROLE_CHARGE);
        } 
        
        if(isset($data['subject_uid']) && $data['subject_uid'] != '')
        { 
            if($this->_is_subject_changed($teacher_uid, $data['subject_uid']))
            {
                //delete old subject
                $this->delete_subject_link($teacher_uid);
                
                //add new subject
                $status = $this->add_subject_link($teacher_uid, $data['subject_uid']);
                if($status === FALSE)
                {
                    return FALSE;
                }
            }
        }   
        
        if($data['is_subject'] == '1')
        {
            $current_classes = $this->get_class_uids($teacher_uid, ROLE_SUBJECT);
            $new_classes = explode(',', $data['subject_class']);
            
            $must_delete_links = array_diff($current_classes, $new_classes);
            $must_add_links    = array_diff($new_classes, $current_classes);
            
            if(!empty($must_delete_links))
            {
                $this->delete_class_links($teacher_uid, ROLE_SUBJECT, $must_delete_links);
            }
            
            if(!empty($must_add_links))
            {
                $this->add_class_link($teacher_uid, ROLE_SUBJECT, $must_add_links);
            }
        }   
        else
        {
            $this->delete_class_links($teacher_uid, ROLE_SUBJECT);
        } 
        
        return TRUE;
    }
}
