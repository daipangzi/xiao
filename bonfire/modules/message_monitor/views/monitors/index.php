<?php 
$user_types = array(''=>'&nbsp;', USER_TYPE_MASTER=>lang('ct_master'), 
    USER_TYPE_TEACHER=>lang('ct_teacher'), USER_TYPE_CHILDREN=>lang('ct_children'), USER_TYPE_PARENT=>lang('ct_parent')); 
$options = array(''=>'&nbsp;', STATUS_YES=>lang('bf_yes'), STATUS_NO=>lang('bf_no'));
?>
                    
<div class="admin-box">
	<h3><?php echo lang('ct_message_list'); ?><small><?php echo $this->pagination->get_page_status();?></small></h3>
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped">
        <colgroup>   
            <?php if ($this->auth->has_permission('Message_monitor.Monitors.Delete') && isset($records) && is_array($records) && count($records)) : ?>
            <col width="30"/>                                                                                                                
            <?php endif;?>                   
            
            <col width=""/>
            <col width="200"/>
            <col width="120"/>
            <col width="150"/>
            <col width="120"/>
            <col width="150"/>
            <col width="100"/>
        </colgroup>
		<thead>
			<tr>
				<?php if ($this->auth->has_permission('Message_monitor.Monitors.Delete') && isset($records) && is_array($records) && count($records)) : ?>
				<th class="column-check"><input class="check-all" type="checkbox" /></th>
				<?php endif;?>
                
                <th class="<?php echo sort_classes($params['orderby'], "message_contents", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/monitors/message_monitor?orderby=message_contents&amp;order='.sort_direction($params['orderby'], "message_contents", $params['order'])); ?>"><span><?php echo lang('ct_message'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "school", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/monitors/message_monitor?orderby=school&amp;order='.sort_direction($params['orderby'], "school", $params['order'])); ?>"><span><?php echo lang('ct_school'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "attachment_nums", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/monitors/message_monitor?orderby=attachment_nums&amp;order='.sort_direction($params['orderby'], "attachment_nums", $params['order'])); ?>"><span><?php echo lang('ct_attachments'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "sender_name", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/monitors/message_monitor?orderby=sender_name&amp;order='.sort_direction($params['orderby'], "sender_name", $params['order'])); ?>"><span><?php echo lang('ct_sender_name'); ?></span><span class="sorting-indicator"</span></a>
                </th>				
                <th class="<?php echo sort_classes($params['orderby'], "sender_type", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/monitors/message_monitor?orderby=sender_type&amp;order='.sort_direction($params['orderby'], "sender_type", $params['order'])); ?>"><span><?php echo lang('ct_sender_type'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "reg_date", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/monitors/message_monitor?orderby=reg_date&amp;order='.sort_direction($params['orderby'], "reg_date", $params['order'])); ?>"><span><?php echo lang('ct_registered'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th></th>
			</tr>
            <tr class="search_row">
                <?php if ($this->auth->has_permission('Message_monitor.Monitors.Delete') && isset($records) && is_array($records) && count($records)) : ?>
                <td></td>
                <?php endif;?>
                
                <td><input type="text" name="search[message_title]" value="<?php echo isset($search['message_contents']) ? $search['message_contents'] : ''; ?>"/></td>
                <td><?php echo form_dropdown2('search[school]', $schools, set_value('school', isset($search['school']) ? $search['school'] : ''), 'class="auto-width"')?></td>
                <td><?php echo form_dropdown2('search[has_attachments]', $options, set_value('has_attachments', isset($search['has_attachments']) ? $search['has_attachments'] : ''), 'class="auto-width" style="min-width:80px;"')?></td>
                <td><input type="text" name="search[sender_name]" value="<?php echo isset($search['sender_name']) ? $search['sender_name'] : ''; ?>"/></td>
                <td><?php echo form_dropdown2('search[sender_type]', $user_types, set_value('sender_type', isset($search['sender_type']) ? $search['sender_type'] : ''), 'class="auto-width" style="min-width:80px;"')?></td>
                <td>
                    <input type="text" name="search[reg_date_from]" class="date" value="<?php echo isset($search['reg_date_from']) ? $search['reg_date_from'] : ''; ?>"/><br/>
                    <input type="text" name="search[reg_date_to]" class="date" value="<?php echo isset($search['reg_date_to']) ? $search['reg_date_to'] : ''; ?>"/>    
                </td>
                <td>
                    <button type="submit" name="search_entries" class="btn" value="1" title="<?php echo lang('ct_btn_search'); ?>"><i class="icon-filter"></i></button>
                    <button type="submit" name="reset_search" class="btn" value="1" title="<?php echo lang('ct_btn_reset'); ?>"><i class="icon-refresh"></i></button>
                </td>
            </tr>
		</thead>
		<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<tfoot>
			<?php if ($this->auth->has_permission('Message_monitor.Monitors.Delete')) : ?>
			<tr>
				<td colspan="20">
					<input type="submit" name="delete" class="btn btn-danger" value="<?php echo lang('ct_btn_delete') ?>" onclick="return confirm('<?php echo lang('msg_delete_list_confirm'); ?>')">
				</td>
			</tr>
			<?php endif;?>
		</tfoot>
		<?php endif; ?>
		<tbody>
		<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<?php foreach ($records as $record) : ?>
			<tr>
				<?php if ($this->auth->has_permission('Message_monitor.Monitors.Delete')) : ?>
				<td><input type="checkbox" name="checked[]" value="<?php echo $record->school_uid.":".$record->uid ?>" /></td>
				<?php endif;?>
				
                <td>
                    <?php 
                        $text = $record->message_contents;
                        if($text == '')
                        {
                            $text = lang('ct_attachments') . "[{$record->attachment_nums}]";
                        }
                        echo anchor(SITE_AREA .'/monitors/message_monitor/detail/'.$record->school_uid.'/'.$record->uid, ct_str_limiter($text, 60)); 
                    ?>
                </td>
                <td><?php echo $record->school_name; ?></td>
                <td class="text-center"><?php echo $record->attachment_nums; ?></td>
                <td><?php echo $record->sender_name; ?></td>
                <td><?php echo $user_types[$record->sender_type]; ?></td>
                <td class="text-center"><?php echo format_time($record->reg_date); ?></td>
                <td></td>
			</tr>
		<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="20"><?php echo lang('msg_no_records'); ?></td>
			</tr>
		<?php endif; ?>
		</tbody>
		</table>
        <?php echo $this->pagination->create_links(); ?>
	<?php echo form_close(); ?>
</div>