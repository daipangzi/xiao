<?php 
$user_types = array(''=>'&nbsp;', USER_TYPE_MASTER=>lang('ct_master'), 
    USER_TYPE_TEACHER=>lang('ct_teacher'), USER_TYPE_CHILDREN=>lang('ct_children'), USER_TYPE_PARENT=>lang('ct_parent')); 
$options = array(''=>'&nbsp;', 1=>lang('bf_yes'), 0=>lang('bf_no'));
$parent_types = array(PARENT_FATHER=>lang('ct_father'), PARENT_MOTHER=>lang('ct_mother'));
?>

<div class="admin-box">
    <h3><?php echo $toolbar_title; ?></h3>
    
    <div class="form-horizontal">
        <div class="control-group">
            <?php echo form_label(lang('ct_sender_name'), '', array('class' => "control-label") ); ?>
            <div class='controls'>
                <?php 
                $text = '';
                if($record->sender_type == USER_TYPE_MASTER)
                {
                    $text = sprintf(lang('ct_from_teacher'), $record->sender_name, lang('ct_master'));
                }
                else if($record->sender_type == USER_TYPE_TEACHER)
                {
                    $text = sprintf(lang('ct_from_teacher'), $record->sender_name, lang('ct_teacher'));
                }
                else if($record->sender_type == USER_TYPE_CHILDREN)
                {
                    $info = ct_json_encode($record->additional);
                    $text = sprintf(lang('ct_from_children_of_class'), $record->sender_name, $info['grade_num'], $info['class_num']);
                }
                else if($record->sender_type == USER_TYPE_PARENT)
                {
                    $info = ct_json_encode($record->additional);
                    $text = sprintf(lang('ct_from_parent_of_children_class'), $record->sender_name, $parent_types[$info['parent_type']], $info['grade_num'], $info['class_num']);
                }
                ?>
                <input type="text" class="span4" value="<?php echo $text; ?>" readonly="readonly"/>
            </div>
        </div>
        
        <div class="control-group">
            <?php echo form_label(lang('ct_attachments'), '', array('class' => "control-label") ); ?>
            <div class='controls'>
            <?php 
            if(isset($record->attachment_files)) {
                echo "<ul>";
                foreach($record->attachment_files as $att) 
                {
                    echo "<li>";
                    if($att->file != '')
                    {
                        $size = $att->file_size;
                        if($size > 1000) //greater than 1000kb
                        {
                            $size = number_format($att->file_size/1024, 2) . 'M';
                        }
                        else
                        {
                            $size = $size. 'KB';
                        }
                        
                        echo anchor($att->file, "{$att->file_name}({$size})", 'target="_blank"');
                    }
                    else
                    {
                        echo $att->file_name;
                    }
                    echo "</li>";
                }
                echo "</ul>";
            }
            ?>
            </div>
        </div>
        
        <div class="control-group">
            <?php echo form_label(lang('ct_receiver_groups'), '', array('class' => "control-label") ); ?>
            <div class='controls'>
            <?php
            if(isset($record->receiver_groups) && is_array($record->receiver_groups)) {
                $groups = $record->receiver_groups;
                
                echo "<ul>";     
                
                //all           
                if(isset($groups['to_all']))
                {
                    echo "<li>" . lang('ct_to_all') . "</li>";
                }
                
                //all teachers
                if(isset($groups['to_all_teachers']))
                {
                    echo "<li>" . lang('ct_to_all_teachers') . "</li>";
                }
                
                //all children
                if(isset($groups['to_all_children']))
                {
                    echo "<li>" . lang('ct_to_all_children') . "</li>";
                }
                
                //all parents
                if(isset($groups['to_all_parents']))
                {
                    echo "<li>" . lang('ct_to_all_parents') . "</li>";
                }
                
                //all children group
                if(isset($groups['to_all_children_group']))
                {
                    foreach($groups['to_all_children_group'] as $group)
                    {
                        echo "<li>" . sprintf(lang('ct_to_all_children_group'), $group['grade_num'], $group['class_num']) . "</li>";    
                    }
                }
                
                //all parents group
                if(isset($groups['to_all_parents_group']))
                {
                    foreach($groups['to_all_parents_group'] as $group)
                    {
                        echo "<li>" . sprintf(lang('ct_to_all_parents_group'), $group['grade_num'], $group['class_num']) . "</li>";    
                    }
                }
                
                //all parents group
                if(isset($groups['to_custom_group']))
                {
                    foreach($groups['to_custom_group'] as $rc)
                    {
                        $user_type = (int)$rc['receiver_type'];
                        if($user_type === USER_TYPE_MASTER)
                        {
                            echo "<li>" . sprintf(lang('ct_to_teacher'), $rc['receiver_name'], lang('ct_master')) . "</li>";    
                        }
                        else if($user_type === USER_TYPE_TEACHER)
                        {
                            echo "<li>" . sprintf(lang('ct_to_teacher'), $rc['receiver_name'], lang('ct_teacher')) . "</li>";    
                        }
                        else if($user_type === USER_TYPE_CHILDREN)                                 
                        {
                            echo "<li>" . sprintf(lang('ct_to_children_of_class'), $rc['receiver_name'], $rc['receiver_additional']['grade_num'], $rc['receiver_additional']['class_num']) . "</li>";    
                        }
                        else if($user_type === USER_TYPE_PARENT)                                 
                        {
                            $text = sprintf(lang('ct_to_parent_of_children_class'), 
                                $rc['receiver_name'], $parent_types[$rc['receiver_additional']['parent_type']], $rc['receiver_additional']['children_name'],
                                $rc['receiver_additional']['grade_num'], $rc['receiver_additional']['class_num']); 
                            echo "<li>" . $text . "</li>";    
                        }
                    }
                }
                
                echo "</ul>";
            }
            ?>    
            </div>
        </div>
        
        <div class="control-group">
            <?php echo form_label(lang('ct_registered'), '', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input type="text" class="span2" value="<?php echo isset($record->reg_date) ? format_time($record->reg_date) : ''; ?>" readonly="readonly"/>
            </div>
        </div>
        
        <div class="control-group">
            <?php echo form_label(lang('ct_content'), 'content', array('class' => "control-label") ); ?>
            <div class='controls'>
                <textarea class="span6" rows="8" readonly="readonly"><?php echo isset($record->message_contents) ? $record->message_contents : ''; ?></textarea>
            </div>
        </div>
        
        <div class="form-actions">
            <?php echo anchor($prev_page, lang('ct_btn_back'), 'class="btn btn-inverse"'); ?>
        </div> 
    </div>   
    
</div>