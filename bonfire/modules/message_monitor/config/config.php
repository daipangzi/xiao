<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');$config['module_config'] = array(
	'description'	=> 'Monitoring all messages in the system',
	'name'		    => 'Message monitor',
	'version'		=> '0.0.1',
	'author'		=> 'admin',
    'weights'       => 190,
);
