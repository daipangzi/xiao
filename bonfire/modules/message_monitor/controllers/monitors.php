<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class monitors extends Admin_Controller {

	//--------------------------------------------------------------------

    public function __construct()
	{
        $this->controller       = 'message_monitor';
        $this->controller_index = 'monitors/message_monitor';
        
		parent::__construct();

		$this->auth->restrict('Message_monitor.Monitors.View');
		$this->load->model('message_model', null, true);
        $this->load->model('messages_model', null, true);
        
        $this->load->model('teacher_model');
        $this->load->model('children_model');
        $this->load->model('parent_model');
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
                    list($school_uid, $message_uid) = explode(':', $pid);
					$result = $this->message_model->select_school($school_uid)->delete($message_uid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('msg_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('msg_delete_failure'), 'error');
				}
			}
		}     
        
        //setup where conditions
        $where = array();
        $search = FALSE;
        if($this->input->post('search_entries')) {
            $search = $this->input->post('search');    
            $this->session->set_userdata('message_monitor_search', $search);
        } else if($this->input->post('reset_search')) {
            $this->session->unset_userdata('message_monitor_search'); 
        } else {
            $search = $this->session->userdata('message_monitor_search')?$this->session->userdata('message_monitor_search'):array();
        }
        
        if(!empty($search['message_contents']))  $where['message_contents LIKE '] = "%" . $search['message_contents'] . "%";
        if(!empty($search['reg_date_from']))  $where['g_messages.reg_date >= '] = $search['reg_date_from'];
        if(!empty($search['reg_date_to']))    $where['g_messages.reg_date <= '] = $search['reg_date_to'];
        if(!empty($search['sender_id']))      $where['sender_id LIKE '] = "%" . $search['sender_id'] . "%";
        if(!empty($search['sender_name']))    $where['sender_name LIKE '] = "%" . $search['sender_name'] . "%";
        if(!empty($search['sender_type']))    $where['sender_type'] = $search['sender_type'];
        if(!empty($search['school_uid']))     $where['school_uid'] = $search['school_uid'];
        if(isset($search['has_attachments']) && $search['has_attachments'] == STATUS_YES) $where['attachment_nums > '] = 0;
        if(isset($search['has_attachments']) && $search['has_attachments'] == STATUS_NO) $where['attachment_nums'] = 0;
        Template::set('search', $search);
        
        //setup ordder array
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);
        
        $orders  = array();
        switch($params['orderby']) {
            case "message_contents": 
            case "reg_date": 
            case "sender_id": 
            case "sender_name": 
            case "sender_type": 
            case "has_attachments": 
                $orders[$params['orderby']] = $params['order'];
                break;
            case "school": 
                $orders["name"] = $params['order'];
                break;
            case "reg_date": 
                $orders["g_messages.reg_date"] = $params['order'];
                break;
            default:
                $orders["g_messages.reg_date"] = 'desc';
                break;
        }
        
        //get school lists
        $schools = $this->school_model
            ->order_by_name()
            ->format_dropdown();            
        Template::set('schools', $schools);        
        
        //get total
        $total_records = $this->messages_model
            ->where($where)
            ->count_all();
                  
        $records = $this->messages_model
            ->select('g_messages.*, gs.name school_name')
            ->join('g_school gs', 'gs.uid=school_uid', 'left')
            ->where($where)
            ->order_by($orders)
            ->limit($this->limit, $offset)
            ->find_all(); 
        Template::set('records', $records);
          
        //setup pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->setup_pagination($total_records, count($records), $url_suffix);
        
        // set request page                                                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->save_index_url($url);
                
        Template::set('prev_page', $url);
		Template::set('toolbar_title', lang('ct_message_monitor'));
		Template::render();
	}
	//--------------------------------------------------------------------
    
    public function detail()
    {
        $school_uid  = $this->uri->segment(5);
        $message_uid = $this->uri->segment(6);
        
        if (empty($message_uid) || empty($school_uid))
        {
            Template::set_message(lang('msg_invalid_id'), 'error');
            redirect(SITE_AREA .'/monitors/error_monitor');
        }
        
        //get message
        $record = $this->message_model
            ->select_school($school_uid)
            ->join("sch{$school_uid}_message_sender", 'uid=message_uid', 'left')
            ->find($message_uid);
        
        //get attachments
        $record->attachment_files = $this->message_model
            ->select_school($school_uid)
            ->get_attachments($message_uid);
        
        //get receivers
        $record->receiver_groups = $this->_parse_message_channel($record->channels, $school_uid);
        
        Template::set('record', $record);
        Template::set('prev_page', $this->get_index_url());
        Template::set('toolbar_title', lang('ct_message_detail'));
        Template::render();
    }
    //--------------------------------------------------------------------
    
    /**
    * parse and return message channels for app
    * 
    * @param string $string
    */
    private function _parse_message_channel($string, $school_uid)
    {
        $result = array();
        $channels = explode(":", $string);
        
        $temp_list = array();
        if(!empty($channels)) 
        {
            foreach($channels as $ch)
            {
                if($ch === MSG_CHL_ALL)
                {
                    $result['to_all'] = STATUS_YES;    
                    break;
                }
                else if($ch === MSG_CHL_ALL_TEACHER)
                {
                    $result['to_all_teachers'] = STATUS_YES;        
                }
                else if($ch === MSG_CHL_ALL_CHILDREN)
                {
                    $result['to_all_children'] = STATUS_YES;        
                }
                else if($ch === MSG_CHL_ALL_PARENT)
                {
                    $result['to_all_parents'] = STATUS_YES;        
                }
                //all children of class   
                //ex: RCG10X                                                           
                else if(strpos($ch, MSG_CHL_GROUP_CHILDREN_SD_PREFIX) !== FALSE && strpos($ch, '-') !== FALSE)
                {
                    if(!isset($result['to_all_children_group']))
                    {
                        $result['to_all_children_group'] = array();
                    }
                    
                    //get class info
                    $class_uid = str_replace(array(MSG_CHL_GROUP_CHILDREN_SD_PREFIX, '[', ']', MSG_CHL_SUFIX), '', $ch);
                    list($grade_num, $class_num) = explode('-', $class_uid);
                     
                    $class = array();
                    $class['grade_num'] = $grade_num;
                    $class['class_num'] = $class_num;
                    
                    $result['to_all_children_group'][] = $class;
                }
                //all parents of class
                //ex: RPG10X
                else if(strpos($ch, MSG_CHL_GROUP_PARENTS_SD_PREFIX) !== FALSE && strpos($ch, '-') !== FALSE)
                {
                    if(!isset($result['to_all_parents_group']))
                    {
                        $result['to_all_parents_group'] = array();
                    }
                    
                    //get class info
                    $class_uid = str_replace(array(MSG_CHL_GROUP_PARENTS_SD_PREFIX, '[', ']', MSG_CHL_SUFIX), '', $ch);
                    list($grade_num, $class_num) = explode('-', $class_uid);
                    
                    $class = array();
                    $class['grade_num'] = $grade_num;
                    $class['class_num'] = $class_num;
                    
                    $result['to_all_parents_group'][] = $class;
                }
                // prefix + id number : ex: if children, C440382199406040824
                else if(strlen($ch) === 20) 
                {
                    if(!isset($result['to_custom_group']))
                    {
                        $result['to_custom_group'] = array();
                    }  
                    
                    $prefix = substr($ch, 0, 2);
                    $user_type = 0;
                    $user_id = str_replace($prefix, '', $ch);
                    $prefix  = str_replace(MSG_CHL_PREFIX, '', $prefix);
                    switch($prefix)
                    {   
                        case 'T':
                            $user_type = USER_TYPE_TEACHER;
                            break;
                        case 'P';
                            $user_type = USER_TYPE_PARENT;
                            break;
                        case 'C';
                            $user_type = USER_TYPE_CHILDREN;
                            break;
                    }
                    
                    $user_model = get_user_model($user_type);
                    $user = $this->$user_model->select_school($school_uid)->find_by_userid($user_id);
                    if(empty($user)) continue;
                    
                    //check duplicate users
                    //ex: if teacher, teacher can receive message as general teacher and charge class teacher or subject class teacher, but message is one
                    //ex: if parent, and he have 2 children in same class, then he will be duplicated
                    if(isset($temp_list[$ch])) continue;
                    $temp_list[$ch] = STATUS_YES;
                    
                    $item = array();
                    $item['receiver_id']   = $user_id;
                    $item['receiver_name'] = $user->user_name;
                    $item['receiver_type'] = $user_type;
                    $result['to_custom_group'][] = $item;  
                }
                //children with grade_num and class_num
                else if(strpos($ch, MSG_CHL_CLASS_CHILDREN_PREFIX) !== FALSE && strpos($ch, '-') !== FALSE) 
                {
                    if(!isset($result['to_custom_group']))
                    {
                        $result['to_custom_group'] = array();
                    }  
                    
                    list($class_text, $user_id) = explode(MSG_CHL_CLASS_CHILDREN_PREFIX, $ch);
                    list($grade_num, $class_num)= explode('-', str_replace(array('[', ']'), '', $class_text));
                    
                    $user = $this->children_model->select_school($school_uid)->find_by_userid($user_id);
                    if(empty($user)) continue;
                    
                    $item = array();
                    $item['receiver_id']   = $user_id;
                    $item['receiver_name'] = $user->user_name;
                    $item['receiver_type'] = USER_TYPE_CHILDREN;
                    $item['receiver_additional'] = array(
                        'grade_num' => $grade_num,
                        'class_num' => $class_num
                    );
                    
                    $result['to_custom_group'][] = $item;  
                }
                //parent with chilren_info -> grade_num ,class_num, children_name
                else if(strpos($ch, MSG_CHL_CLASS_PARENT_PREFIX) !== FALSE && strpos($ch, '-') !== FALSE && strpos($ch, '<>') !== FALSE) 
                {
                    if(!isset($result['to_custom_group']))
                    {
                        $result['to_custom_group'] = array();
                    }  
                                    
                    list($temp_text, $user_id) = explode(MSG_CHL_CLASS_PARENT_PREFIX, $ch);
                    list($class_text, $children_name)= explode('<>', str_replace(array('[', ']'), '', $temp_text));
                    list($grade_num, $class_num)= explode('-', $class_text);
                    
                    $user = $this->parent_model->select_school($school_uid)->find_by_userid($user_id);
                    if(empty($user)) continue;
                    
                    $item = array();
                    $item['receiver_id']   = $user_id;
                    $item['receiver_name'] = $user->user_name;
                    $item['receiver_type'] = USER_TYPE_PARENT;
                    $item['receiver_additional'] = array(
                        'grade_num' => $grade_num,
                        'class_num' => $class_num,
                        'parent_type'   => $user->parent_type,
                        'children_name' => $children_name
                    );
                    
                    $result['to_custom_group'][] = $item;  
                }
            }
        }
        
        return $result;    
    }
}