<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');$config['module_config'] = array(
	'description'	=> 'List all clients(teacher, children, parents) in our system',
	'name'		    => lang('ct_all_clients'),
	'version'		=> '0.0.1',
	'author'		=> 'admin',
    'weights'       => 40,
);
