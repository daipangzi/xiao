<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class system extends Admin_Controller {

	//--------------------------------------------------------------------

    public function __construct()
	{
        $this->controller       = 'client';
        $this->controller_index = 'system/client';
                                         
		parent::__construct();

		$this->auth->restrict('Client.System.View');
        $this->load->model('clients_model', null, true);
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
        //setup where conditions
        $where = array();
        $search = FALSE;
        if($this->input->post('search_entries')) {
            $search = $this->input->post('search');    
            $this->session->set_userdata('client_search', $search);
        } else if($this->input->post('reset_search')) {
            $this->session->unset_userdata('client_search'); 
        } else {
            $search = $this->session->userdata('client_search')?$this->session->userdata('client_search'):array();
        }
        
        if(!empty($search['idnum']))    $where['user_id LIKE '] = "%" . $search['idnum'] . "%";
        if(!empty($search['name']))     $where['user_name LIKE '] = "%" . $search['name'] . "%";
        if(!empty($search['email']))    $where['user_email LIKE '] = "%" . $search['email'] . "%";
        if(!empty($search['gender']))   $where['user_sex'] = $search['gender'];
        if(!empty($search['birthday_from'])) $where['user_birthday >= '] = $search['birthday_from'];
        if(!empty($search['birthday_to']))   $where['user_birthday <= '] = $search['birthday_to'];
        if(!empty($search['register_from'])) $where['reg_date >= '] = $search['register_from'];
        if(!empty($search['register_to']))   $where['reg_date <= '] = $search['register_to'];
        if(!empty($search['recent_from']))  $where['recent_visit_date >= '] = $search['recent_from'];
        if(!empty($search['recent_to']))    $where['recent_visit_date <= '] = $search['recent_to'];
        if(!empty($search['phone']))        $where['user_phone LIKE '] = "%" . $search['phone'] . "%";
        if(!empty($search['address']))      $where['user_address LIKE'] = "%" . $search['address'] . "%";
        if(!empty($search['status']))       $where['user_status'] = $search['status'];
        if(!empty($search['type']))         $where['user_type'] = $search['type'];
        Template::set('search', $search);
        
        //setup ordder array
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);
        
        $orders  = array();
        switch($params['orderby']) {
            case "userid":
                $orders['user_id']      = $params['order'];
                break;
            case "name":
            case "email":
            case "birthday": 
            case "address": 
            case "phone": 
            case "status": 
                $orders["user_{$params['orderby']}"] = $params['order'];
                break;
            case "gender": 
                $orders["user_sex"] = $params['order'];
                break;
            case "date": 
                $orders["reg_date"] = $params['order'];
                break;
            case "recent": 
                $orders["recent_visit_date"] = $params['order'];
                break;
            case "school": 
                $orders["school_uid"] = $params['order'];
                break;
            case "type": 
                $orders["user_type"] = $params['order'];
                break;
            default:
                $orders["reg_date"] = 'desc';
                break;
        }
        
        //get tottals
        $total_records = $this->clients_model
            ->where($where)
            ->count_all();
                  
        $records = $this->clients_model
            ->where($where)
            ->order_by($orders)
            ->limit($this->limit, $offset)
            ->find_all(); 
        Template::set('records', $records);
          
        //setup pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->setup_pagination($total_records, count($records), $url_suffix);
                
        // set request page                                                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->save_index_url($url);

        Template::set('prev_page', $url);
		Template::set('toolbar_title', '&nbsp;');
		Template::render();
	}

	//--------------------------------------------------------------------
}