<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class reports extends Admin_Controller {

	//--------------------------------------------------------------------

    public function __construct()
	{
        $this->controller       = 'reports';
        $this->controller_index = 'reports/actions';
        
		parent::__construct();

		$this->auth->restrict('Actions.Reports.View');
		$this->load->model('log_action_model', null, true);
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
		$this->_index_process_action();    
        
        //setup where conditions
        $where = array();
        $search = FALSE;
        if($this->input->post('search_entries')) {
            $search = $this->input->post('search');    
            $this->session->set_userdata('actions_search', $search);
        } else if($this->input->post('reset_search')) {
            $this->session->unset_userdata('actions_search'); 
        } else {
            $search = $this->session->userdata('actions_search')?$this->session->userdata('actions_search'):array();
        }
        
        if(!empty($search['idnum']))    $where['user_id LIKE '] = "%" . $search['idnum'] . "%";
        if(!empty($search['name']))     $where['user_name LIKE '] = "%" . $search['name'] . "%";
        if(!empty($search['type']))         $where['user_type'] = $search['type'];
        if(!empty($search['device_type']))   $where['device_type'] = $search['device_type'];
        if(!empty($search['register_from'])) $where['action_date >= '] = $search['register_from'];
        if(!empty($search['register_to']))   $where['action_date <= '] = $search['register_to'];
        if(!empty($search['device_info']))   $where['device_info LIKE'] = "%" . $search['device_info'] . "%";
        if(!empty($search['action_detail'])) $where['action_detail LIKE'] = "%" . $search['action_detail'] . "%";
        Template::set('search', $search);
        
        //setup ordder array
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);
        
        $orders  = array();
        switch($params['orderby']) {
            case "userid":
                $orders["user_id"] = $params['order'];
                break;
            case "name":
            case "type":
                $orders["user_{$params['orderby']}"] = $params['order'];
                break;
            case "device_type": 
            case "device_info": 
                $orders[$params['orderby']] = $params['order'];
                break;
            case "date": 
                $orders["action_date"] = $params['order'];
                break;
            default:
                $orders["action_date"] = 'desc';
                break;
        }
        
        //get total
        $total_records = $this->log_action_model
            ->where($where)
            ->count_all();
                  
        $records = $this->log_action_model
            ->where($where)
            ->order_by($orders)
            ->limit($this->limit, $offset)
            ->find_all(); 
        Template::set('records', $records);
          
        //setup pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->setup_pagination($total_records, count($records), $url_suffix);
        
        // set request page                                                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->save_index_url($url);
                
        Template::set('prev_page', $url);
		Template::render();
	}
	//--------------------------------------------------------------------
    
    private function _index_process_action()
    {
        if(isset($_POST['delete']))
        {
            $this->auth->restrict('Actions.Reports.Manage');
            
            $checked = $this->input->post('checked');
            if (is_array($checked) && count($checked))
            {
                $result = 0;
                foreach ($checked as $pid)
                {
                    if($this->log_action_model->delete($pid))
                    {
                        $result++;
                    }
                }

                if ($results > 0)
                {
                    Template::set_message(sprintf(lang('msg_action_delete_success'), $results), 'success');
                }
            }
        }
        //delte all records
        else if (isset($_POST['truncate']))
        {
            $this->auth->restrict('Actions.Reports.Manage');
            
            $result = $this->log_action_model->truncate();
            if ($result)
            {
                Template::set_message(lang('msg_action_truncate_success'), 'success');
            }
            else
            {
                Template::set_message(lang('msg_action_truncate_fail'), 'error');
            }
        }     
    }
}