<?php 
$user_types = array(''=>'&nbsp;', USER_TYPE_TEACHER=>lang('ct_teacher'), USER_TYPE_MASTER=>lang('ct_master'), USER_TYPE_CHILDREN=>lang('ct_children'), USER_TYPE_PARENT=>lang('ct_parent')); 
$devices = array('', DEVICE_ANDROID=>lang('ct_android'), DEVICE_IPHONE=>lang('ct_iphone'), DEVICE_PC=>lang('ct_pc'));
$url = SITE_AREA.'/reports/actions?orderby=' . $params['orderby'] . '&amp;order=' . $params['order'];
?>
                    
<div class="admin-box">
	<h3><?php echo lang('ct_actions'); ?><small><?php echo $this->pagination->get_page_status();?></small></h3>
	<?php echo form_open($url); ?>
		<table class="table table-striped">
        <colgroup>   
            <?php if ($this->auth->has_permission('Actions.Reports.Manage') && isset($records) && is_array($records) && count($records)) : ?>
            <col width="30"/>                                                                                                                
            <?php endif;?>                   
            
            <col width="160"/>
            <col width="120"/>
            <col width="100"/>
            <col width="110"/>
            <col width="260"/>
            <col width="150"/>
            <col width=""/>
            <col width="100"/>
        </colgroup>
		<thead>
			<tr>
				<?php if ($this->auth->has_permission('Actions.Reports.Manage') && isset($records) && is_array($records) && count($records)) : ?>
				<th class="column-check"><input class="check-all" type="checkbox" /></th>
				<?php endif;?>
				
                <th class="<?php echo sort_classes($params['orderby'], "userid", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/actions?orderby=userid&amp;order='.sort_direction($params['orderby'], "userid", $params['order'])); ?>"><span><?php echo lang('ct_user_id'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "name", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/actions?orderby=name&amp;order='.sort_direction($params['orderby'], "name", $params['order'])); ?>"><span><?php echo lang('ct_name'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "type", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/actions?orderby=type&amp;order='.sort_direction($params['orderby'], "type", $params['order'])); ?>"><span><?php echo lang('ct_type'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "device_type", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/actions?orderby=device_type&amp;order='.sort_direction($params['orderby'], "device_type", $params['order'])); ?>"><span><?php echo lang('ct_device_type'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "device_info", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/actions?orderby=device_info&amp;order='.sort_direction($params['orderby'], "device_info", $params['order'])); ?>"><span><?php echo lang('ct_device_info'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "date", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/actions?orderby=date&amp;order='.sort_direction($params['orderby'], "date", $params['order'])); ?>"><span><?php echo lang('ct_registered'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "action_detail", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/actions?orderby=action_detail&amp;order='.sort_direction($params['orderby'], "action_detail", $params['order'])); ?>"><span><?php echo lang('ct_action_detail'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th></th>
			</tr>
            <tr class="search_row">
                <?php if ($this->auth->has_permission('Action_monitor.Monitors.Delete') && isset($records) && is_array($records) && count($records)) : ?>
                <td></td>
                <?php endif;?>
                
                <td><input type="text" name="search[idnum]" value="<?php echo isset($search['idnum']) ? $search['idnum'] : ''; ?>" style=""/></td>
                <td><input type="text" name="search[name]" value="<?php echo isset($search['name']) ? $search['name'] : ''; ?>" style=""/></td>
                <td><?php echo form_dropdown2('search[type]', $user_types, set_value('type', isset($search['type']) ? $search['type'] : ''), 'class="auto-width" style="min-width:80px;"')?></td>
                <td><?php echo form_dropdown2('search[device_type]', $devices, set_value('device_type', isset($search['device_type']) ? $search['device_type'] : ''), 'class="auto-width" style="min-width:80px;"')?></td>
                <td><input type="text" name="search[device_info]" value="<?php echo isset($search['device_info']) ? $search['device_info'] : ''; ?>" style=""/></td>
                <td>
                    <input type="text" name="search[register_from]" class="date" value="<?php echo isset($search['register_from']) ? $search['register_from'] : ''; ?>" style=""/><br/>
                    <input type="text" name="search[register_to]" class="date" value="<?php echo isset($search['register_to']) ? $search['register_to'] : ''; ?>" style=""/>    
                </td>
                <td>&nbsp;</td>
                <td>
                    <button type="submit" name="search_entries" class="btn" value="1" title="<?php echo lang('ct_btn_search'); ?>"><i class="icon-filter"></i></button>
                    <button type="submit" name="reset_search" class="btn" value="1" title="<?php echo lang('ct_btn_reset'); ?>"><i class="icon-refresh"></i></button>
                </td>
            </tr>
		</thead>
		<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<tfoot>
			<?php if ($this->auth->has_permission('Actions.Reports.Manage')) : ?>
			<tr>
				<td colspan="20">
					<input type="submit" name="delete" class="btn btn-danger select_one" value="<?php echo lang('ct_btn_delete') ?>" onclick="return confirm('<?php echo lang('msg_action_delete_list_confirm'); ?>')" disabled="disabled">
                    <input type="submit" name="truncate" class="btn btn-warning" value="<?php echo lang('ct_btn_truncate') ?>" onclick="return confirm('<?php echo lang('msg_action_truncate_confirm'); ?>')">
				</td>
			</tr>
			<?php endif;?>
		</tfoot>
		<?php endif; ?>
		<tbody>
		<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<?php foreach ($records as $record) : ?>
			<tr>
				<?php if ($this->auth->has_permission('Actions.Reports.Manage')) : ?>
				<td><input type="checkbox" name="checked[]" value="<?php echo $record->uid ?>" /></td>
				<?php endif;?>
				
                <td><?php echo $record->user_id; ?></td>
                <td><?php echo $record->user_name; ?></td>
                <td><?php echo $user_types[$record->user_type]; ?></td>
                <td><?php echo isset($devices[$record->device_type])?$devices[$record->device_type]:lang('ct_unknown'); ?></td>
                <td><?php echo $record->device_info; ?></td>
                <td><?php echo $record->action_details; ?></td>
                <td><?php echo format_time($record->action_date); ?></td>
                <td></td>
			</tr>
		<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="20"><?php echo lang('msg_action_no_records'); ?></td>
			</tr>
		<?php endif; ?>
		</tbody>
		</table>
	<?php echo form_close(); ?>
</div>
<?php echo $this->pagination->create_links(); ?>