<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');$config['module_config'] = array(
	'description'	=> 'Monitoring all user actions for logged users',
	'name'		    => lang('ct_actions'),
	'version'		=> '0.0.1',
	'author'		=> 'admin',
    'weights'       => 180,
);
