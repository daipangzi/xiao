<?php 

$title = "{$record->user_name} ({$record->user_id})";
$sm = $this->model('school_model');

?>
<div class="admin-box">
    <h3><?php echo $title; ?></h3>
    
    <?php echo form_open($this->uri->uri_string(), "class='form-horizontal' id='user-form' autocomplete='off'"); ?>
    <input type="hidden" name="image_name" id="image_name" value="<?php echo $image_name; ?>"/>
    <input type="hidden" name="image_action" id="image_action" value="<?php echo $image_action; ?>"/>
    <input type="hidden" name="selected_item" id="selected_item" value=""/>
    <input type="hidden" name="selected_tab" id="selected_tab" value="<?php echo $selected_tab; ?>"/>

    <div class="form-buttons">
        <?php echo anchor($prev_page, lang('ct_btn_back'), 'class="btn btn-inverse"'); ?>
        
        <?php if ($this->auth->has_permission('Parent.System.Delete')) : ?>
            <?php if(isset($record->user_status) && $record->user_status == STATUS_ACTIVE) { ?>
            <button type="submit" name="deactivate" class="btn"><?php echo lang('ct_btn_deactivate'); ?></button>
            <?php } else if(isset($record->user_status) && $record->user_status == STATUS_INACTIVE) { ?>
            <button type="submit" name="activate" class="btn"><?php echo lang('ct_btn_activate') ?></button>
            <?php } ?>
        
            <button type="submit" name="delete" class="btn" onclick="return confirm('<?php echo lang('msg_delete_one_confirm'); ?>')"><?php echo lang('ct_btn_delete'); ?></button>
        <?php endif; ?>
    </div> 
    
    <ul class="nav nav-tabs" >
        <li class="active"><a href="#general-box" data-toggle="tab"><?php echo lang('ct_general'); ?></a></li>
        <li><a href="#photo-box" data-toggle="tab"><?php echo lang('ct_user_photo'); ?></a></li>
        <li><a href="#password-box" data-toggle="tab"><?php echo lang('ct_user_password'); ?></a></li>
        <li><a href="#children-box" data-toggle="tab"><?php echo lang('ct_children'); ?></a></li>
    </ul>
    
    <div class="tab-content">
        <div class="tab-pane fade in active" id="general-box">
            <div class="control-group">
                <?php echo form_label(lang('ct_user_photo'), 'user_photo', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <?php
                        $user_photo = isset($record->user_photo)?$record->user_photo:''; 
                    ?>
                    <span class="photo_img_box">
                        <img class="img_user_photo" src="<?php echo user_photo($user_photo, USER_TYPE_PARENT); ?>" width="100" height="100"/>
                    </span>
                </div>
            </div>
            
            <div class="control-group <?php echo form_error('user_name') ? 'error' : ''; ?>">
                <?php echo form_label(lang('ct_user_name').lang('bf_form_label_required'), 'user_name', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="user_name" type="text" name="user_name" maxlength="50" class="span3" value="<?php echo set_value('user_name', isset($record->user_name) ? $record->user_name : ''); ?>"  />
                    <span class="help-inline"><?php echo form_error('user_name'); ?></span>
                </div>
            </div>
            
            <?php $options = array(''=>lang('msg_select'), 1=>lang('ct_father'), 2=>lang('ct_mother')); ?>
            <?php echo form_dropdown('parent_type', $options, set_value('parent_type', isset($record->parent_type) ? $record->parent_type : ''), lang('ct_parent').lang('bf_form_label_required'), 'class="auto-width"')?>
            
            <div class="control-group <?php echo form_error('user_email') ? 'error' : ''; ?>">
                <?php echo form_label(lang('ct_user_email').lang('bf_form_label_required'), 'user_email', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="user_email" type="text" name="user_email" maxlength="255" class="span3" value="<?php echo set_value('user_email', isset($record->user_email) ? $record->user_email : ''); ?>"  />
                    <span class="help-inline"><?php echo form_error('user_email'); ?></span>
                </div>
            </div>
            
            <div class="control-group <?php echo form_error('user_phone') ? 'error' : ''; ?>">
                <?php echo form_label(lang('ct_user_phone').lang('bf_form_label_required'), 'user_phone', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="user_phone" type="text" name="user_phone" maxlength="255" class="span3" value="<?php echo set_value('user_phone', isset($record->user_phone) ? $record->user_phone : ''); ?>"  />
                    <span class="help-inline"><?php echo form_error('user_phone'); ?></span>
                </div>
            </div>
            
            <div class="control-group <?php echo form_error('user_birthday') ? 'error' : ''; ?>">
                <?php echo form_label(lang('ct_user_birthday'), 'user_birthday', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="user_birthday" type="text" name="user_birthday" maxlength="10" class="span3 date" value="<?php echo set_value('user_birthday', isset($record->user_birthday) ? $record->user_birthday : ''); ?>"  />
                    <span class="help-inline"><?php echo form_error('user_birthday'); ?></span>
                </div>
            </div>
            
            <div class="control-group <?php echo form_error('user_address') ? 'error' : ''; ?>">
                <?php echo form_label(lang('ct_user_address'), 'user_address', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="user_address" type="text" name="user_address" maxlength="255" class="span3" value="<?php echo set_value('user_address', isset($record->user_address) ? $record->user_address : ''); ?>"  />
                    <span class="help-inline"><?php echo form_error('user_address'); ?></span>
                </div>
            </div>
            
            <div class="form-actions">
                <?php echo anchor($prev_page, lang('ct_btn_back'), 'class="btn btn-inverse"'); ?>
                <button type="submit" name="save_general" class="btn btn-info" id="save_general"><?php echo  lang('ct_btn_save'); ?></button>
                <button type="submit" name="apply_general" class="btn" id="apply_general"><?php echo  lang('ct_btn_apply'); ?></button>
            </div> 
        </div>
        
        <div class="tab-pane fade in" id="photo-box">  
            <div class="control-group">
                <?php echo form_label(lang('ct_user_photo'), 'user_photo', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <?php $user_photo = isset($record->user_photo)?$record->user_photo:''; ?>
                    <span id="photo_img_box">
                        <?php if($image_path == 'tmp'): ?>
                        <img id="img_user_photo" src="<?php echo site_url('media/tmp/photo/parent/'.$image_name); ?>" width="100" height="100"/>
                        <?php else: ?>
                        <img id="img_user_photo" src="<?php echo user_photo($user_photo, USER_TYPE_PARENT); ?>" width="100" height="100"/>
                        <?php endif; ?>
                        <span class="remove" id="remover" <?php echo ($user_photo!='')?'"':'style="display:none;"'; ?>><i class="icon-remove">&nbsp;</i></span>
                    </span><br/>
                    <input id="user_photo" type="file" name="user_photo"/>
                    <span id="msg_user_photo" class="help-inline" style="color:red; display:block;"></span>
                </div>
            </div>
            
            <div class="form-actions">
                <?php echo anchor($prev_page, lang('ct_btn_back'), 'class="btn btn-inverse"'); ?>
                <button type="submit" name="save_photo" class="btn btn-info" id="save_photo" disabled="disabled"><?php echo  lang('ct_btn_save'); ?></button>
                <button type="submit" name="apply_photo" class="btn" id="apply_photo" disabled="disabled"><?php echo  lang('ct_btn_apply'); ?></button>            
            </div>
        </div>
        
        <div class="tab-pane fade in" id="password-box">
            <div id="passrow" class="control-group <?php echo form_error('user_passwd') ? 'error' : ''; ?>">
                <?php echo form_label(lang('ct_user_password'), 'user_passwd', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="user_passwd" type="password" name="user_passwd" maxlength="40" min-length="5" class="span3" value=""/>     
                    <span class="help-inline pass-error"><?php echo form_error('user_passwd'); ?></span>
                    <span class="help-inline"><?php echo isset($record->user_passwd) ? $record->user_passwd : ''; ?></span>
                </div>
            </div>
            
            <div id="confirmrow" class="control-group <?php echo form_error('user_passwd_confirm') ? 'error' : ''; ?>">
                <?php echo form_label(lang('ct_user_password_confirm'), 'user_passwd_confirm', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="user_passwd_confirm" type="password" name="user_passwd_confirm" maxlength="40" min-length="5" class="span3" value=""/>
                    <span class="help-inline confirm-error"><?php echo form_error('user_passwd_confirm'); ?></span>
                </div>
            </div>
            
            <div class="form-actions">
                <?php echo anchor($prev_page, lang('ct_btn_back'), 'class="btn btn-inverse"'); ?>
                <button type="submit" name="save_password" class="btn btn-info" id="save_password" disabled="disabled"><?php echo  lang('ct_btn_save'); ?></button>
                <button type="submit" name="apply_password" class="btn" id="apply_password" disabled="disabled"><?php echo  lang('ct_btn_apply'); ?></button>            
            </div>
        </div>
        <!--end password-box-->
        
        <div class="tab-pane fade in" id="children-box">  
            <div class="control-group <?php echo form_error('new_children') ? 'error' : ''; ?>">
                <?php echo form_label(lang('ct_children'), '', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <?php if(!empty($parent_children)) : ?>    
                    <ul class="menu horizontal children_parents">
                        <?php foreach($parent_children as $children) : ?>
                        <li><a href="<?php echo site_url(SITE_AREA .'/system/children/edit/'.$children->children_uuid.'/'.md5($children->children_uuid)); ?>"><img src="<?php echo user_photo($children->user_photo, USER_TYPE_CHILDREN); ?>" width="100" height="100"/><br/>
                            <span class="name"><?php echo "{$children->user_name}({$children->user_id})";?></span>
                        </a></li>
                        <?php endforeach; ?>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
            
            <div class="form-actions">
                <?php echo anchor($prev_page, lang('ct_btn_back'), 'class="btn btn-inverse"'); ?>
            </div>
        </div>
        <!--end children-box-->
    </div>
    
    <?php echo form_close(); ?>

</div>