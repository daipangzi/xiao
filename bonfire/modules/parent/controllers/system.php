<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class system extends Admin_Clients_Controller {

	//--------------------------------------------------------------------

    public function __construct()
	{
        $this->controller       = 'parent';
        $this->controller_index = 'system/parent';
        
		parent::__construct();

		$this->auth->restrict('Parent.System.View');
        
        $this->load->model('children_model', null, true);
		$this->load->model('parent_model', null, true);
        
        Assets::add_js('jquery.form.js');
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
		$this->_index_action_process();
        
        //setup where conditions
        $where = array();
        $search = FALSE;
        if($this->input->post('search_entries')) {
            $search = $this->input->post('search');    
            $this->session->set_userdata('parent_search', $search);
        } else if($this->input->post('reset_search')) {
            $this->session->unset_userdata('parent_search'); 
        } else {
            $search = $this->session->userdata('parent_search')?$this->session->userdata('parent_search'):array();
        }
        
        if(!empty($search['idnum']))    $where['user_id LIKE '] = "%" . $search['idnum'] . "%";
        if(!empty($search['name']))     $where['user_name LIKE '] = "%" . $search['name'] . "%";
        if(!empty($search['email']))    $where['user_email LIKE '] = "%" . $search['email'] . "%";
        if(!empty($search['parent_type']))   $where['parent_type'] = $search['parent_type'];
        if(!empty($search['birthday_from'])) $where['user_birthday >= '] = $search['birthday_from'];
        if(!empty($search['birthday_to']))   $where['user_birthday <= '] = $search['birthday_to'];
        if(!empty($search['register_from'])) $where['reg_date >= '] = $search['register_from'];
        if(!empty($search['register_to']))   $where['reg_date <= '] = $search['register_to'];
        if(!empty($search['recent_from']))  $where['recent_visit_date >= '] = $search['recent_from'];
        if(!empty($search['recent_to']))    $where['recent_visit_date <= '] = $search['recent_to'];
        if(!empty($search['phone']))        $where['user_phone LIKE '] = "%" . $search['phone'] . "%";
        if(!empty($search['address']))      $where['user_address LIKE'] = "%" . $search['address'] . "%";
        if(!empty($search['status']))       $where['user_status'] = $search['status'];
        
        Template::set('search', $search);
        
        //setup ordder array
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);
        
        $orders  = array();
        switch($params['orderby']) {
            case "userid":
                $orders['user_id']      = $params['order'];
                break;
            case "name":
            case "email":
            case "birthday": 
            case "address": 
            case "phone": 
            case "status": 
                $orders["user_{$params['orderby']}"] = $params['order'];
                break;
            case "parent_type": 
                $orders["parent_type"] = $params['order'];
                break;
            case "date": 
                $orders["reg_date"] = $params['order'];
                break;
            case "recent": 
                $orders["recent_visit_date"] = $params['order'];
                break;
            case "children": 
                $orders["childrens"] = $params['order'];
                break;
            case "school": 
                $orders["school_uid"] = $params['order'];
                break;
            case "type": 
                $orders["user_type"] = $params['order'];
                break;
            default:
                $orders["reg_date"] = 'desc';
                break;
        }
        
        //get school lists
        $schools = $this->school_model
            ->select_name($this->short_language)
            ->order_by_name()
            ->format_dropdown();            
        Template::set('schools', $schools);        
        
        //get total counts
        $total_records = $this->parent_model
            //->join('gur_parent_children', 'parent_uid=parent_uuid', 'left')
            ->where($where)
            ->count_all();
       
        $records = $this->parent_model
            ->select('*, count(children_uid) childrens')
            ->join('gur_parent_children', 'parent_uid=parent_uuid', 'left')
            ->where($where)
            ->group_by('parent_uuid')
            ->order_by($orders)
            ->limit($this->limit, $offset)
            ->find_all(); 
            
        Template::set('records', $records);
        
        //setup pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->setup_pagination($total_records, count($records), $url_suffix);
                
        // set request page                                                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->save_index_url($url);

        Template::set('prev_page', $url);
		Template::set('toolbar_title', '&nbsp;');
		Template::render();
	}
	//--------------------------------------------------------------------

	/*
		Method: edit()

		Allows editing of Parent data.
	*/
	public function edit()
	{
		$uid = $this->uri->segment(5);
        $key = $this->uri->segment(6);
        
		if (empty($uid) || $key != md5($uid))
		{
			Template::set_message(lang('msg_parent_invalid_id'), 'error');
			$this->redirect_to_index();
		}
        
        $this->_edit_action_process($uid);
        Assets::add_module_js('parent', 'parent.js');
        
        //save selected tab
        $selected_tab = $this->input->post('selected_tab');
        Template::set('selected_tab', $selected_tab);
        
        //get school lists
        $schools = $this->school_model
            ->select_name($this->short_language)
            ->order_by_name()
            ->format_dropdown(FALSE);            
        Template::set('schools', $schools); 
        
        //get record
        $record = $this->parent_model->find($uid);
		Template::set('record', $record); 
        
        //get childrens
        $parent_children_uids = $this->parent_model->get_parent_children_uids($uid);
        $parent_children = $this->children_model->filter_user_uids($parent_children_uids)->find_all();
        Template::set('parent_children', $parent_children); 
        
        //if save failed, them save avatar image
        if($this->input->post('image_name') && $this->input->post('image_action') == "changed")
        {
            Template::set("image_name", $this->input->post('image_name'));    
            Template::set("image_path", 'tmp');    
        }
        else
        {
            Template::set("image_name", trim($record->user_photo));    
            Template::set("image_path", '');    
        }   
        Template::set("image_action", $this->input->post('image_action'));          
		
        Template::set('prev_page', $this->get_index_url());
		Template::set('toolbar_title', '&nbsp;');
		Template::render();
	}

	//--------------------------------------------------------------------

	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------  
    /**
    * update general profile
    *  
    * @param int $uid
    */
    private function _save_user_general($uid)
    {   
        $_POST['parent_uuid'] = $uid;
        
        $email_extra = $birthday_extra = '';
        if($this->input->post('user_email')) $email_extra = '|valid_email';
        if($this->input->post('user_birthday')) $birthday_extra = '|valid_date';
        
        //validate fields
        $this->form_validation->set_rules('user_name','lang:ct_name','required|trim|max_length[50]|strip_tags|xss_clean|special_chars');
        $this->form_validation->set_rules('parent_type','lang:ct_parent','required|trim|max_length[1]|strip_tags|xss_clean');
        $this->form_validation->set_rules('user_birthday','lang:ct_birthday', "'trim|strip_tags|xss_clean{$birthday_extra}");
        $this->form_validation->set_rules('user_address','lang:ct_address','trim|max_length[255]|strip_tags|xss_clean|special_chars');
        $this->form_validation->set_rules('user_phone','lang:ct_phone','required|trim|valid_phone|unique[gus_parents.user_phone,gus_parents.parent_uuid]|strip_tags|xss_clean');
        $this->form_validation->set_rules('user_email','lang:ct_email',"trim|unique[gus_parents.user_email,gus_parents.parent_uuid]|strip_tags|xss_clean{$email_extra}");

        if ($this->form_validation->run() === FALSE)
        {
            return FALSE;
        }
        
        // make sure we only pass in the fields we want
        $pt = $this->input->post('parent_type');
        
        $data = array();
        $data['user_name']      = $this->input->post('user_name');
        $data['user_sex']       = $pt==PARENT_FATHER?MAN:WOMAN;
        $data['parent_type']    = $pt==PARENT_FATHER?PARENT_FATHER:PARENT_MOTHER;
        $data['user_phone']     = $this->input->post('user_phone');
        $data['user_email']     = $this->input->post('user_email');
        $data['user_address']   = $this->input->post('user_address');
        if($this->input->post('user_birthday'))
        {
            $data['user_birthday'] = $this->input->post('user_birthday');
        }
        
        return $this->parent_model->update($uid, $data);
    }
    
    /**
    * edit actions to update parent
    * 
    * @param int $parent_uid
    */
    private function _edit_action_process($parent_uid) 
    {
        $fields = array('save_general', 'apply_general', 
            'save_photo', 'apply_photo', 
            'save_password', 'apply_password',
            'delete', 'activate', 'deactivate');
        if(!array_one_key_exists($fields, $_POST)) return;
        
        $user = $this->parent_model->find($parent_uid);
        if(empty($user)) return;
        
        //update general profile
        if (isset($_POST['save_general']) || isset($_POST['apply_general']))
        {
            $this->auth->restrict('Parent.System.Edit');
                               
            if ($this->_save_user_general($parent_uid))
            {
                Template::set_message(sprintf(lang('msg_parent_update_success'), $user->user_name), 'success');
                
                if (isset($_POST['save_general']))
                {
                    $this->redirect_to_index();
                }
            }
            else
            {
                Template::set_message(sprintf(lang('msg_parent_update_fail'), $user->user_name), 'error');
            }
        } 
        else if ($this->input->post('image_action') && (isset($_POST['save_photo']) || isset($_POST['apply_photo'])))
        {
            $this->auth->restrict('Parent.System.Edit');
                               
            if ($this->_save_user_photo($parent_uid, USER_TYPE_PARENT))
            {
                Template::set_message(sprintf(lang('msg_parent_photo_update_success'), $user->user_name), 'success');
                if (isset($_POST['save_photo']))
                {
                    $this->redirect_to_index();
                }
            }
            else
            {
                Template::set_message(sprintf(lang('msg_parent_photo_update_fail'), $user->user_name), 'success');
            }
            
            unset($_POST['image_name']);
            unset($_POST['image_action']);
        } 
        else if (isset($_POST['save_password']) || isset($_POST['apply_password']))
        {
            $this->auth->restrict('Parent.System.Edit');
                               
            if ($this->_save_user_password($parent_uid))
            {
                Template::set_message(sprintf(lang('msg_parent_pass_update_success'), $user->user_name), 'success');
                if (isset($_POST['save_password']))
                {
                    $this->redirect_to_index();
                }
            }
            else
            {
                Template::set_message(sprintf(lang('msg_parent_pass_update_fail'), $user->user_name), 'error');
            }
        }      
        else if (isset($_POST['delete']))
        {
            $this->auth->restrict('Parent.System.Delete');

            if ($this->parent_model->delete($parent_uid))
            {
                Template::set_message(sprintf(lang('msg_parent_delete_success'), $user->user_name), 'success');
                $this->redirect_to_index();
            } else
            {
                Template::set_message(sprintf(lang('msg_parent_delete_fail'), $user->user_name), 'error');
            }
        }
        else if (isset($_POST['activate']))
        {
            $this->auth->restrict('Parent.System.Edit');

            if ($this->parent_model->activate($parent_uid))
            {
                Template::set_message(sprintf(lang('msg_parent_activate_success'), $user->user_name), 'success');
            } 
            else
            {
                Template::set_message(sprintf(lang('msg_parent_activate_fail'), $user->user_name), 'error');
            }
        }
        else if (isset($_POST['deactivate']))
        {
            $this->auth->restrict('Parent.System.Edit');

            if ($this->parent_model->deactivate($parent_uid))
            {
                Template::set_message(sprintf(lang('msg_parent_deactivate_success'), $user->user_name), 'success');
            } 
            else
            {
                Template::set_message(sprintf(lang('msg_parent_deactivate_fail'), $user->user_name), 'error');
            }
        }
    }
    
    ///////////////////////////////////////////////////////////////////////
    //!AJAX METHODS
    ///////////////////////////////////////////////////////////////////////
    public function upload_photo() {
        $file_name = "user_photo";
        $status = parent::upload_photo($file_name, USER_TYPE_PARENT);
        if($status)
        {
            echo $file_name;
        }
        else
        {
            echo '';
        }
        exit;          
    }
}