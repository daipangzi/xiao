//province change
$('#school').change(function() {
    $.ajax({
      url: base_url+"admin/system/children/school_class_info",
      type: "post",
      data: {'school_uid': $(this).val(), "ci_csrf_token": ci_csrf_token()},
      success: function(res){
          $('#school_class_info').html(res);
      }   
    });    
});

$("#change_class_relation").click(function(){
    if($(this).is(':checked'))
    {
        $("#school, #children_class, #save_school, #apply_school").removeAttr("disabled");
    }
    else
    {
        $("#school, #children_class, #save_school, #apply_school").attr("disabled", "disabled");
    }
});

select_saved_tab();

//for user photo
set_user_photo('children');
user_tab_changes();