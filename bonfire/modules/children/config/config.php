<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');$config['module_config'] = array(
	'description'	=> 'Manage all children in our system',
	'name'		    => lang('ct_children'),
	'version'		=> '0.0.1',
	'author'		=> 'admin',
    'weights'       => 20,
);
