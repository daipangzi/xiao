<?php 
$extra = isset($disable_flag)?"disabled='disabled'":'';
$lang_entrance = lang('ct_entrance');
?>

<select id="children_class" name="children_class" class="auto-width" <?php echo $extra; ?>>
<?php foreach($all_classes as $class) { ?>
    <option value="<?php echo $class->class_uuid; ?>" <?php echo ($children_class==$class->class_uuid)?'selected="selected"':''; ?>><?php echo sprintf(lang('ct_grade_class_with'), $class->grade_num, $class->class_num); ?></option>
<?php } ?>
</select>