<?php
$children_model = $this->model('children_model');
$url = SITE_AREA.'/system/children?orderby=' . $params['orderby'] . '&amp;order=' . $params['order'];
?>

<div class="admin-box">
	<h3><?php echo lang('ct_children'); ?><small><?php echo $this->pagination->get_page_status();?></small></h3>
	<?php echo form_open($url); ?>
		<table class="table table-striped">
        <colgroup>   
            <?php if ($this->auth->has_permission('Children.System.Delete') && isset($records) && is_array($records) && count($records)) : ?>
            <col width="30"/>                                                                                                                
            <?php endif;?>                   
            
            <col width="30"/>
            <col width="145"/>
            <col width="145"/>
            <col width="90"/>
            <col width="230"/>
            <col width="120"/>
            <col width="150"/>
            <col width="90"/>
            <col width=""/>
            <col width="100"/>
            <col width="100"/>
            <col width="100"/>
            <col width="90"/>
            <col width="90"/>
            <col width="70"/>
            <col width="90"/>
        </colgroup>
		<thead>
			<tr>
				<?php if ($this->auth->has_permission('Children.System.Delete') && isset($records) && is_array($records) && count($records)) : ?>
				<th class="column-check"><input class="check-all" type="checkbox" /></th>
				<?php endif;?>
				
                <th></th>
                <th class="<?php echo sort_classes($params['orderby'], "userid", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/children?orderby=userid&amp;order='.sort_direction($params['orderby'], "userid", $params['order'])); ?>"><span><?php echo lang('ct_user_id'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "name", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/children?orderby=name&amp;order='.sort_direction($params['orderby'], "name", $params['order'])); ?>"><span><?php echo lang('ct_user_name'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "gender", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/children?orderby=gender&amp;order='.sort_direction($params['orderby'], "gender", $params['order'])); ?>"><span><?php echo lang('ct_user_sex'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "school", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/children?orderby=school&amp;order='.sort_direction($params['orderby'], "school", $params['order'])); ?>"><span><?php echo lang('ct_school'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "phone", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/children?orderby=phone&amp;order='.sort_direction($params['orderby'], "phone", $params['order'])); ?>"><span><?php echo lang('ct_user_phone'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "email", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/children?orderby=email&amp;order='.sort_direction($params['orderby'], "email", $params['order'])); ?>"><span><?php echo lang('ct_user_email'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "birthday", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/children?orderby=birthday&amp;order='.sort_direction($params['orderby'], "birthday", $params['order'])); ?>"><span><?php echo lang('ct_user_birthday'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "address", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/children?orderby=address&amp;order='.sort_direction($params['orderby'], "address", $params['order'])); ?>"><span><?php echo lang('ct_user_address'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "date", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/children?orderby=date&amp;order='.sort_direction($params['orderby'], "date", $params['order'])); ?>"><span><?php echo lang('ct_registered'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "recent", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/children?orderby=recent&amp;order='.sort_direction($params['orderby'], "recent", $params['order'])); ?>"><span><?php echo lang('ct_user_recent'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "expire", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/children?orderby=expire&amp;order='.sort_direction($params['orderby'], "expire", $params['order'])); ?>"><span><?php echo lang('ct_user_expire_date'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "status", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/children?orderby=status&amp;order='.sort_direction($params['orderby'], "status", $params['order'])); ?>"><span><?php echo lang('ct_status'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <!--<th class="<?php echo sort_classes($params['orderby'], "class", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/children?orderby=class&amp;order='.sort_direction($params['orderby'], "class", $params['order'])); ?>"><span><?php echo lang('ct_class'); ?></span><span class="sorting-indicator"</span></a>
                </th>-->
                <th><?php echo lang('ct_class'); ?></th>
                <th><span><?php echo lang('ct_parent'); ?></span></th>
                <th><span><?php //echo lang('ct_actions'); ?></span></th>
			</tr>
            <tr class="search_row">
                <?php if ($this->auth->has_permission('Children.System.Delete') && isset($records) && is_array($records) && count($records)) : ?>
                <td></td>
                <?php endif;?>
                
                <td></td>
                <td><input type="text" name="search[idnum]" value="<?php echo isset($search['idnum']) ? $search['idnum'] : ''; ?>" style=""/></td>
                <td><input type="text" name="search[name]" value="<?php echo isset($search['name']) ? $search['name'] : ''; ?>" style=""/></td>
                <td>
                    <?php $options = array(''=>'&nbsp;', 1=>lang('ct_man'), 2=>lang('ct_woman')); ?>
                    <?php echo form_dropdown2('search[gender]', $options, set_value('gender', isset($search['gender']) ? $search['gender'] : ''), 'class="auto-width" style="min-width:80px;"')?>
                </td>
                <td><?php echo form_dropdown2('search[school]', $schools, set_value('school', isset($search['school']) ? $search['school'] : ''), 'class="auto-width" style="width:220px !important;"')?></td>
                <td><input type="text" name="search[phone]" value="<?php echo isset($search['phone']) ? $search['phone'] : ''; ?>" style=""/></td>
                <td><input type="text" name="search[email]" value="<?php echo isset($search['email']) ? $search['email'] : ''; ?>" style=""/></td>
                <td>
                    <input type="text" name="search[birthday_from]" class="date" value="<?php echo isset($search['birthday_from']) ? $search['birthday_from'] : ''; ?>" style=""/><br/>
                    <input type="text" name="search[birthday_to]" class="date" value="<?php echo isset($search['birthday_to']) ? $search['birthday_to'] : ''; ?>" style=""/>    
                </td>
                <td><input type="text" name="search[address]" value="<?php echo isset($search['address']) ? $search['address'] : ''; ?>" style=""/></td>
                <td>
                    <input type="text" name="search[register_from]" class="date" value="<?php echo isset($search['register_from']) ? $search['register_from'] : ''; ?>" style=""/><br/>
                    <input type="text" name="search[register_to]" class="date" value="<?php echo isset($search['register_to']) ? $search['register_to'] : ''; ?>" style=""/>    
                </td>
                <td>
                    <input type="text" name="search[recent_from]" class="date" value="<?php echo isset($search['recent_from']) ? $search['recent_from'] : ''; ?>" style=""/><br/>
                    <input type="text" name="search[recent_to]" class="date" value="<?php echo isset($search['recent_to']) ? $search['recent_to'] : ''; ?>" style=""/>    
                </td>
                <td>
                    <input type="text" name="search[expire_from]" class="date" value="<?php echo isset($search['expire_from']) ? $search['expire_from'] : ''; ?>" style=""/><br/>
                    <input type="text" name="search[expire_to]" class="date" value="<?php echo isset($search['expire_to']) ? $search['expire_to'] : ''; ?>" style=""/>    
                </td>
                <td>
                    <?php $options = array(''=>'&nbsp;', 1=>lang('bf_yes'), 2=>lang('bf_no')); ?>
                    <?php echo form_dropdown2('search[status]', $options, set_value('status', isset($search['status']) ? $search['status'] : ''), 'class="auto-width" style="min-width:80px;"')?>
                </td>
                <td></td>
                <td></td>
                <td>
                    <button type="submit" name="search_entries" class="btn" value="1" title="<?php echo lang('ct_btn_search'); ?>"><i class="icon-filter"></i></button>
                    <button type="submit" name="reset_search" class="btn" value="1" title="<?php echo lang('ct_btn_reset'); ?>"><i class="icon-refresh"></i></button>
                </td>
            </tr>
		</thead>
		<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<tfoot>
			<?php if ($this->auth->has_permission('Children.System.Delete')) : ?>
			<tr>
				<td colspan="20">
					<input type="submit" name="delete" class="btn btn-danger select_one" value="<?php echo lang('ct_btn_delete') ?>" onclick="return confirm('<?php echo lang('msg_children_delete_list_confirm'); ?>')"  disabled="disabled">
                    <input type="submit" name="activate" class="btn select_one" value="<?php echo lang('ct_btn_activate') ?>" disabled="disabled"/>
                    <input type="submit" name="deactivate" class="btn select_one" value="<?php echo lang('ct_btn_deactivate') ?>" disabled="disabled"/>
				</td>
			</tr>
			<?php endif;?>
		</tfoot>
		<?php endif; ?>
		<tbody>
		<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<?php foreach ($records as $record) : ?>
			<tr>
				<?php if ($this->auth->has_permission('Children.System.Delete')) : ?>
				<td><input type="checkbox" name="checked[]" value="<?php echo $record->children_uuid ?>" /></td>
				<?php endif;?>
				
                <td><img src="<?php echo user_photo($record->user_photo, USER_TYPE_CHILDREN, 30); ?>" width="30" height="30"/></td>
                <td><?php echo $record->user_id; ?></td>
                <td><?php 
                        if($this->auth->has_permission('Children.System.Edit'))
                        {
                            echo anchor(SITE_AREA .'/system/children/edit/'.$record->children_uuid.'/'.md5($record->children_uuid), $record->user_name);
                        }
                        else
                        {
                            echo $record->user_name;
                        }
                    ?>
                </td>
                <td><?php echo $record->user_sex==1?lang('ct_man'):lang('ct_woman'); ?></td>
                <td>
                    <?php 
                        if(isset($schools[$record->school_uid]))
                        {
                            if($this->auth->has_permission('School.System.Edit'))
                            { 
                                echo anchor(SITE_AREA .'/system/school/edit/'.$record->school_uid.'/'.md5($record->school_uid).'?return_url='.rawurlencode($prev_page), $schools[$record->school_uid]);
                            }
                            else
                            {
                                echo $schools[$record->school_uid];
                            }
                        }
                    ?>
                </td>
                <td><?php echo $record->user_phone; ?></td>
                <td><?php echo mailto($record->user_email); ?></td>
                <td><?php echo $record->user_birthday==''?'':format_date($record->user_birthday); ?></td>
                <td><?php echo $record->user_address; ?></td>
                <td><?php echo $record->reg_date==''?'':format_date($record->reg_date); ?></td>
                <td><?php echo $record->recent_visit_date==''?'':format_date($record->recent_visit_date); ?></td>
                <td><?php echo $record->expire_date==''?'':format_date($record->expire_date); ?></td>
                <td>
                    <span class="label <?php echo $record->user_status==1?'label-success':'label-warning'; ?>">
                    <?php echo $record->user_status==1?lang('ct_active'):lang('ct_inactive'); ?>
                    </span>
                </td>
                <td><?php echo sprintf(lang('ct_grade_class_with'), $record->grade_num, $record->class_num); ?></td>
                <td><?php echo $children_model->count_children_parents($record->children_uuid); ?></td>
                <td></td>
			</tr>
		<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="20"><?php echo lang('msg_children_no_records'); ?></td>
			</tr>
		<?php endif; ?>
		</tbody>
		</table>
	<?php echo form_close(); ?>
</div>
<?php echo $this->pagination->create_links(); ?>