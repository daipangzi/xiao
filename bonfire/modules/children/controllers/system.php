<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class system extends Admin_Clients_Controller {

	//--------------------------------------------------------------------

    public function __construct()
	{
        $this->controller       = 'children';
        $this->controller_index = 'system/children';
        
		parent::__construct();

		$this->auth->restrict('Children.System.View');
        $this->load->model('parent_model', null, true);
		$this->load->model('children_model', null, true);

        Assets::add_js('jquery.form.js');
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
		$this->_index_action_process();
        
        //setup where conditions
        $where = array();
        $search = FALSE;
        if($this->input->post('search_entries')) {
            $search = $this->input->post('search');    
            $this->session->set_userdata('children_search', $search);
        } else if($this->input->post('reset_search')) {
            $this->session->unset_userdata('children_search'); 
        } else {
            $search = $this->session->userdata('children_search')?$this->session->userdata('children_search'):array();
        }
        
        if(!empty($search['idnum']))    $where['user_id LIKE '] = "%" . $search['idnum'] . "%";
        if(!empty($search['name']))     $where['user_name LIKE '] = "%" . $search['name'] . "%";
        if(!empty($search['email']))    $where['user_email LIKE '] = "%" . $search['email'] . "%";
        if(!empty($search['gender']))   $where['user_sex'] = $search['gender'];
        if(!empty($search['birthday_from'])) $where['user_birthday >= '] = $search['birthday_from'];
        if(!empty($search['birthday_to']))   $where['user_birthday <= '] = $search['birthday_to'];
        if(!empty($search['register_from'])) $where['reg_date >= '] = $search['register_from'];
        if(!empty($search['register_to']))   $where['reg_date <= '] = $search['register_to'];
        if(!empty($search['recent_from']))  $where['recent_visit_date >= '] = $search['recent_from'];
        if(!empty($search['recent_to']))    $where['recent_visit_date <= '] = $search['recent_to'];
        if(!empty($search['expire_from']))  $where['expire_date >= '] = $search['expire_from'];
        if(!empty($search['expire_to']))    $where['expire_date <= '] = $search['expire_to'];
        if(!empty($search['phone']))        $where['user_phone LIKE '] = "%" . $search['phone'] . "%";
        if(!empty($search['address']))      $where['user_address LIKE'] = "%" . $search['address'] . "%";
        if(!empty($search['status']))       $where['user_status'] = $search['status'];
        if(isset($search['school']) && is_numeric($search['school'])) $where['school_uid'] = $search['school'];
        
        Template::set('search', $search);
        
        //setup ordder array
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);
        
        $orders  = array();
        switch($params['orderby']) {
            case "userid":
                $orders['user_id']      = $params['order'];
                break;
            case "name":
            case "email":
            case "birthday": 
            case "address": 
            case "phone": 
            case "status": 
                $orders["user_{$params['orderby']}"] = $params['order'];
                break;
            case "gender": 
                $orders["user_sex"] = $params['order'];
                break;
            case "date": 
                $orders["reg_date"] = $params['order'];
                break;
            case "recent": 
                $orders["recent_visit_date"] = $params['order'];
                break;
            case "expire": 
                $orders["expire_date"]  = $params['order'];
                break;
            case "school": 
                $orders["school_uid"]   = $params['order'];
                break;
            default:
                $orders["reg_date"]     = 'desc';
                break;
        }
        
        //get school lists
        $schools = $this->school_model
            ->select_name($this->short_language)
            ->order_by_name()
            ->format_dropdown();            
        Template::set('schools', $schools);        
        
        //get total records        
        $total_records = $this->children_model
            ->with_class()
            ->where($where)
            ->count_all();
                  
        $records = $this->children_model
            ->with_class()
            ->where($where)
            ->order_by($orders)
            ->limit($this->limit, $offset)
            ->find_all(); 
        Template::set('records', $records);
          
        //setup pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->setup_pagination($total_records, count($records), $url_suffix);
                
        // set request page                                                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->save_index_url($url);

        Template::set('prev_page', $url);
		Template::render();
	}

	//--------------------------------------------------------------------

	/*
		Method: edit()

		Allows editing of Children data.
	*/
	public function edit()
	{
		$uid = $this->uri->segment(5);
        $key = $this->uri->segment(6);

		if (empty($uid) || $key != md5($uid))
		{
			Template::set_message(lang('msg_children_invalid_id'), 'error');
			$this->redirect_to_index();
		}
        
        //update general profile
        $this->_edit_action_process($uid);
        Assets::add_module_js('children', 'children.js');
        
        //save selected tab
        $selected_tab = $this->input->post('selected_tab');
        Template::set('selected_tab', $selected_tab);
         
        //get school lists
        $schools = $this->school_model
            ->select_name($this->short_language)
            ->order_by_name()
            ->format_dropdown(FALSE);            
        Template::set('schools', $schools); 
        
        //get record
        $record = $this->children_model->with_class()->find($uid);
		Template::set('record', $record); 
        Template::set('selected_school', $record->school_uid);  
        
        //parents 
        $children_parent_uids = $this->children_model->get_children_parent_uids($uid);
        $children_parents = $this->parent_model->filter_user_uids($children_parent_uids)->find_all();
        Template::set('children_parents', $children_parents);  
        
        //get grade-classes
        $all_classes = $this->school_model
            ->filter_live_grades()
            ->filter_live_classes()
            ->order_by('grade_num')
            ->order_by('class_num')
            ->get_classes($record->school_uid);
            
        $class_info = array();
        $class_info['all_classes']  = $all_classes;
        $class_info['children_class'] = $record->class_uid;
        $class_info['disable_flag'] = TRUE;
        Template::set('class_info', $class_info); 
        
        //if save failed, them save avatar image
        if($this->input->post('image_name') && $this->input->post('image_action') == "changed")
        {
            Template::set("image_name", $this->input->post('image_name'));    
            Template::set("image_path", 'tmp');    
        }
        else
        {
            Template::set("image_name", trim($record->user_photo));    
            Template::set("image_path", '');    
        }   
        Template::set("image_action", $this->input->post('image_action'));          
		
        Template::set('prev_page', $this->get_index_url());
		Template::render();
	}

	//--------------------------------------------------------------------

	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------
   
   /**
   * save changes of children class
   * 
   * @param int $uid
   */
    private function _save_children_school($uid)
    {
        $this->form_validation->set_rules('school','lang:ct_school',"required|trim|max_length[40]|strip_tags|valid_school");
        $this->form_validation->set_rules('children_class','lang:ct_class','required|trim|strip_tags|xss_clean|valid_class[school]'); 
           
        if ($this->form_validation->run() === FALSE)
        {     
            return STATUS_NO;
        }
        
        //collect data  
        $school_uid = $this->input->post('school');
        $class_uid  = $this->input->post('children_class');
        $children   = $this->children_model->with_class()->find($uid);
        if($class_uid == $children->class_uid)
        {
            return NO_CHANGE;
        }
                
        $data = array();
        $data['class_uid'] = $class_uid;
        if($school_uid != $children->school_uid)
        {
            $data['school_date'] = date(FORMAT_DATETIME);
        }
        $status = $this->children_model->update($uid, $data);
        if($status === FALSE)
        {
            return STATUS_NO;
        }
             
        return STATUS_YES;
    }
    
    /**
    * edit actions to update parent
    * 
    * @param int $children_uid
    */
    private function _edit_action_process($children_uid) 
    {
        $fields = array('save_general', 'apply_general', 
            'save_photo', 'apply_photo', 
            'save_password', 'apply_password',
            'save_school', 'apply_school',
            'delete', 'activate', 'deactivate');
        if(!array_one_key_exists($fields, $_POST)) return;
        
        $user = $this->children_model->find($children_uid);
        if(empty($user)) return;
        
        //update general profile
        if (isset($_POST['save_general']) || isset($_POST['apply_general']))
        {
            $this->auth->restrict('Children.System.Edit');
                               
            if ($this->_save_user_general($children_uid))
            {
                Template::set_message(sprintf(lang('msg_children_update_success'), $user->user_name), 'success');
                
                if (isset($_POST['save_general']))
                {
                    $this->redirect_to_index();
                }
            }
            else
            {
                Template::set_message(sprintf(lang('msg_children_update_fail'), $user->user_name), 'error');
            }
        } 
        else if ($this->input->post('image_action') && (isset($_POST['save_photo']) || isset($_POST['apply_photo'])))
        {
            $this->auth->restrict('Children.System.Edit');
                               
            if ($this->_save_user_photo($children_uid, USER_TYPE_CHILDREN))
            {
                Template::set_message(sprintf(lang('msg_children_photo_update_success'), $user->user_name), 'success');
                if (isset($_POST['save_photo']))
                {
                    $this->redirect_to_index();
                }
            }
            else
            {
                Template::set_message(sprintf(lang('msg_children_photo_update_fail'), $user->user_name), 'success');
            }
            
            unset($_POST['image_name']);
            unset($_POST['image_action']);
        } 
        else if (isset($_POST['save_password']) || isset($_POST['apply_password']))
        {
            $this->auth->restrict('Children.System.Edit');
                               
            if ($this->_save_user_password($children_uid))
            {
                Template::set_message(sprintf(lang('msg_children_pass_update_success'), $user->user_name), 'success');
                if (isset($_POST['save_password']))
                {
                    $this->redirect_to_index();
                }
            }
            else
            {
                Template::set_message(sprintf(lang('msg_children_pass_update_fail'), $user->user_name), 'error');
            }
        }      
        else if (isset($_POST['delete']))
        {
            $this->auth->restrict('Children.System.Delete');

            if ($this->parent_model->delete($children_uid))
            {
                Template::set_message(sprintf(lang('msg_children_delete_success'), $user->user_name), 'success');
                $this->redirect_to_index();
            } else
            {
                Template::set_message(sprintf(lang('msg_children_delete_fail'), $user->user_name), 'error');
            }
        }
        else if (isset($_POST['activate']))
        {
            $this->auth->restrict('Children.System.Edit');

            if ($this->parent_model->activate($children_uid))
            {
                Template::set_message(sprintf(lang('msg_children_activate_success'), $user->user_name), 'success');
            } 
            else
            {
                Template::set_message(sprintf(lang('msg_children_activate_fail'), $user->user_name), 'error');
            }
        }
        else if (isset($_POST['deactivate']))
        {
            $this->auth->restrict('Children.System.Edit');

            if ($this->parent_model->deactivate($children_uid))
            {
                Template::set_message(sprintf(lang('msg_children_deactivate_success'), $user->user_name), 'success');
            } 
            else
            {
                Template::set_message(sprintf(lang('msg_children_deactivate_fail'), $user->user_name), 'error');
            }
        }
        else if ($this->input->post('change_class_relation') && (isset($_POST['save_school']) || isset($_POST['apply_school'])))
        {
            $this->auth->restrict('Children.System.Edit');
                
            $status = $this->_save_children_school($children_uid);                      
            if ($status === STATUS_YES)
            {
                Template::set_message(sprintf(lang('msg_children_school_update_success'), $user->user_name), 'success');
                
                if (isset($_POST['save_school']))
                {
                    $this->redirect_to_index();
                }
            }
            else if ($status === STATUS_NO)
            {
                Template::set_message(sprintf(lang('msg_children_school_update_fail'), $user->user_name), 'error');
            }
            else if ($status === NO_CHANGE)
            {
                Template::set_message(sprintf(lang('msg_children_school_no_changes'), $user->user_name), 'warning');
            }
        }
    }
    ///////////////////////////////////////////////////////////////////////
    //!AJAX METHODS
    ///////////////////////////////////////////////////////////////////////
    /**
    * save general profile info
    * 
    * @param int $uid
    */
    private function _save_user_general($uid)
    {   
        $birthday_extra = '';
        if($this->input->post('user_birthday'))
        {
            $birthday_extra = '|valid_date';
        }
        
        //validate fields
        $this->form_validation->set_rules('user_name','lang:ct_user_name','required|trim|max_length[50]|strip_tags|xss_clean|special_chars');
        $this->form_validation->set_rules('user_sex','lang:ct_user_sex','required|trim|max_length[1]|strip_tags|xss_clean');
        $this->form_validation->set_rules('user_birthday','lang:ct_user_birthday', "trim|strip_tags|xss_clean{$birthday_extra}");
        $this->form_validation->set_rules('user_address','lang:ct_user_address','trim|max_length[255]|strip_tags|xss_clean|special_chars');
        $this->form_validation->set_rules('user_phone','lang:ct_user_phone','required|trim|valid_phone|strip_tags|xss_clean');
        $this->form_validation->set_rules('user_email','lang:ct_user_email','trim|valid_email|strip_tags|xss_clean');
        $this->form_validation->set_rules('expire_date','lang:ct_user_exire_date','required|trim|valid_date|strip_tags|xss_clean');

        if ($this->form_validation->run() === FALSE)
        {
            return FALSE;
        }
        
        // make sure we only pass in the fields we want
        $data = array();
        $data['user_name']    = $this->input->post('user_name');
        $data['user_sex']     = $this->input->post('user_sex')==MAN?MAN:WOMAN;
        $data['user_phone']   = $this->input->post('user_phone');
        $data['user_email']   = $this->input->post('user_email');
        $data['user_address'] = $this->input->post('user_address');
        $data['expire_date']  = $this->input->post('expire_date');
        if($this->input->post('user_birthday'))
        {
            $data['user_birthday'] = $this->input->post('user_birthday');
        }
        
        return $this->children_model->update($uid, $data);
    }
    
    public function school_class_info() {
        $school_uid = $this->input->post('school_uid');
        
        if(!$this->school_model->valid_school($school_uid) || !is_numeric($school_uid))
        {
            echo '';
            exit;
        }
        
        //get grade-classes
        $all_classes = $this->school_model
            ->filter_live_grades()
            ->filter_live_classes()
            ->order_by('grade_num')
            ->order_by('class_num')
            ->get_classes($school_uid);
        
        $data = array();
        $data['all_classes'] = $all_classes;
        $data['children_class'] = '';
        Template::block('school_class_info', 'system/_school_info', $data);
        
        exit;
    }//end school_class_info()
    
    public function upload_photo() {
        $file_name = "user_photo";
        $status = parent::upload_photo($file_name, USER_TYPE_CHILDREN);
        if($status)
        {
            echo $file_name;
        }
        else
        {
            echo '';
        }
        exit;          
    }
}