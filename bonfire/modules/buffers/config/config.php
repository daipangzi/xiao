<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');$config['module_config'] = array(
	'description'	=> 'Monitoring all data in buffer table',
	'name'		    => lang('ct_buffers'),
	'version'		=> '0.0.1',
	'author'		=> 'admin',
    'weights'       => 160,
);
