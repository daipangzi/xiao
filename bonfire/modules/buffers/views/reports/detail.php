<div class="admin-box">

    <div class="form-horizontal">
        <div class="control-group">
            <?php echo form_label(lang('ct_buffer_content'), 'content', array('class' => "control-label") ); ?>
            <div class='controls'>
                <textarea class="span12" rows="15"><?php echo pretty_json($record->content); ?></textarea>
            </div>
        </div>
        
        <div class="form-actions">
            <?php echo anchor($prev_page, lang('ct_btn_back'), 'class="btn btn-inverse"'); ?>
        </div> 
    </div>   
    
</div>