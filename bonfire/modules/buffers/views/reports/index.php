<?php 
$user_types = array(''=>'&nbsp;', USER_TYPE_TEACHER=>lang('ct_teacher'), USER_TYPE_PARENT=>lang('ct_parent')); 
$devices = array('', DEVICE_ANDROID=>lang('ct_android'), DEVICE_IPHONE=>lang('ct_iphone'));
$url = SITE_AREA.'/reports/buffers?orderby=' . $params['orderby'] . '&amp;order=' . $params['order'];
?>
                    
<div class="admin-box">
	<h3><?php echo lang('ct_buffers'); ?><small><?php echo $this->pagination->get_page_status();?></small></h3>
	<?php echo form_open($url); ?>
		<table class="table table-striped">
        <colgroup>   
            <?php if ($this->auth->has_permission('Buffers.Reports.Manage') && isset($records) && is_array($records) && count($records)) : ?>
            <col width="30"/>                                                                                                                
            <?php endif;?>                   
            
            <col width="100"/>
            <col width="120"/>
            <col width="260"/>
            <col width="150"/>
            <col width="100"/>
            <col width=""/>
            <col width="150"/>
            <col width="100"/>
        </colgroup>
		<thead>
			<tr>
				<?php if ($this->auth->has_permission('Buffers.Reports.Manage') && isset($records) && is_array($records) && count($records)) : ?>
				<th class="column-check"><input class="check-all" type="checkbox" /></th>
				<?php endif;?>
				
                <th class="<?php echo sort_classes($params['orderby'], "user_type", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/buffers?orderby=user_type&amp;order='.sort_direction($params['orderby'], "user_type", $params['order'])); ?>"><span><?php echo lang('ct_user_type'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "device_type", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/buffers?orderby=device_type&amp;order='.sort_direction($params['orderby'], "device_type", $params['order'])); ?>"><span><?php echo lang('ct_device_type'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "device_info", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/buffers?orderby=device_info&amp;order='.sort_direction($params['orderby'], "device_info", $params['order'])); ?>"><span><?php echo lang('ct_device_info'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "ip_address", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/buffers?orderby=ip_address&amp;order='.sort_direction($params['orderby'], "ip_address", $params['order'])); ?>"><span><?php echo lang('ct_ip_address'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "verify_code", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/buffers?orderby=verify_code&amp;order='.sort_direction($params['orderby'], "verify_code", $params['order'])); ?>"><span><?php echo lang('ct_buffer_code'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "key", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/buffers?orderby=key&amp;order='.sort_direction($params['orderby'], "key", $params['order'])); ?>"><span><?php echo lang('ct_buffer_content'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "reg_date", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/buffers?orderby=date&amp;order='.sort_direction($params['orderby'], "reg_date", $params['order'])); ?>"><span><?php echo lang('ct_registered'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th></th>
			</tr>
            <tr class="search_row">
                <?php if ($this->auth->has_permission('Buffers.Reports.Manage') && isset($records) && is_array($records) && count($records)) : ?>
                <td></td>
                <?php endif;?>
                
                <td><?php echo form_dropdown2('search[user_type]', $user_types, set_value('user_type', isset($search['user_type']) ? $search['user_type'] : ''), 'class="auto-width" style="min-width:80px;"')?></td>
                <td><?php echo form_dropdown2('search[device_type]', $devices, set_value('device_type', isset($search['device_type']) ? $search['device_type'] : ''), 'class="auto-width" style="min-width:80px;"')?></td>
                <td><input type="text" name="search[device_info]" value="<?php echo isset($search['device_info']) ? $search['device_info'] : ''; ?>" style=""/></td>
                <td><input type="text" name="search[ip_address]" value="<?php echo isset($search['ip_address']) ? $search['ip_address'] : ''; ?>" style=""/></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <button type="submit" name="search_entries" class="btn" value="1" title="<?php echo lang('ct_btn_search'); ?>"><i class="icon-filter"></i></button>
                    <button type="submit" name="reset_search" class="btn" value="1" title="<?php echo lang('ct_btn_reset'); ?>"><i class="icon-refresh"></i></button>
                </td>
            </tr>
		</thead>
		<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<tfoot>
			<?php if ($this->auth->has_permission('Buffers.Reports.Manage')) : ?>
			<tr>
				<td colspan="20">
					<input type="submit" name="delete" class="btn btn-danger select_one" value="<?php echo lang('ct_btn_delete') ?>" onclick="return confirm('<?php echo lang('msg_delete_list_confirm'); ?>')" disabled="disabled">
                    <input type="submit" name="truncate" class="btn btn-warning" value="<?php echo lang('ct_btn_truncate') ?>" onclick="return confirm('<?php echo lang('msg_truncate_confirm'); ?>')">
				</td>
			</tr>
			<?php endif;?>
		</tfoot>
		<?php endif; ?>
		<tbody>
		<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<?php foreach ($records as $record) : ?>
			<tr>
				<?php if ($this->auth->has_permission('Buffers.Reports.Manage')) : ?>
				<td><input type="checkbox" name="checked[]" value="<?php echo $record->buffer_uuid ?>" /></td>
				<?php endif;?>
				
                <td><?php echo $user_types[$record->user_type]; ?></td>
                <td><?php echo isset($devices[$record->device_type])?$devices[$record->device_type]:lang('ct_unknown'); ?></td>
                <td><?php echo $record->device_info; ?></td>
                <td><?php echo $record->ip_address; ?></td>
                <td><?php echo $record->verify_code; ?></td>
                <td>
                    <?php echo anchor(SITE_AREA .'/reports/buffers/detail/'.$record->buffer_uuid, ct_str_limiter($record->content, 150)); ?>
                    <div class="hidden"><?php echo $record->content; ?></div>
                </td>
                <td><?php echo format_time($record->reg_date); ?></td>
                <td></td>
			</tr>
		<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="20"><?php echo lang('msg_buffer_no_records'); ?></td>
			</tr>
		<?php endif; ?>
		</tbody>
		</table>
	<?php echo form_close(); ?>
</div>
<?php echo $this->pagination->create_links(); ?>