<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class reports extends Admin_Controller {

	//--------------------------------------------------------------------

    public function __construct()
	{
        $this->controller       = 'buffers';
        $this->controller_index = 'reports/buffers';
        
		parent::__construct();

		$this->auth->restrict('Buffers.Reports.View');
		$this->load->model('buffer_model', null, true);
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
		$this->_index_process_action();  
        
        //setup where conditions
        $where = array();
        $search = FALSE;
        if($this->input->post('search_entries')) {
            $search = $this->input->post('search');    
            $this->session->set_userdata('buffers_search', $search);
        } else if($this->input->post('reset_search')) {
            $this->session->unset_userdata('buffers_search'); 
        } else {
            $search = $this->session->userdata('buffers_search')?$this->session->userdata('buffers_search'):array();
        }
        
        if(!empty($search['ip_address']))  $where['ip_address LIKE '] = "%" . $search['ip_address'] . "%";
        if(!empty($search['device_info'])) $where['device_info LIKE '] = "%" . $search['device_info'] . "%";
        if(!empty($search['user_type']))   $where['user_type'] = $search['user_type'];
        if(!empty($search['device_type'])) $where['device_type'] = $search['device_type'];
        Template::set('search', $search);
        
        //setup ordder array
        $params['order']   = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby'] = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);
        
        $orders  = array();
        switch($params['orderby']) {
            case "verify_code":
                $orders["verify_code"] = $params['order'];
                break;
            case "user_type": 
            case "device_type": 
            case "device_info": 
            case "ip_address": 
            case "key": 
                $orders[$params['orderby']] = $params['order'];
                break;
            case "date": 
                $orders["reg_date"] = $params['order'];
                break;
            default:
                $orders["reg_date"] = 'desc';
                break;
        }
        
        //get total
        $total_records = $this->buffer_model
            ->where($where)
            ->count_all();
                  
        $records = $this->buffer_model
            ->join('gbl_sms', 'sms_uuid=sms_uid', 'left')
            ->where($where)
            ->order_by($orders)
            ->limit($this->limit, $offset)
            ->find_all(); 
        Template::set('records', $records);
          
        //setup pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->setup_pagination($total_records, count($records), $url_suffix);
        
        // set request page                                                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->save_index_url($url);
                
        Template::set('prev_page', $url);
		Template::render();
	}
	//--------------------------------------------------------------------
    
    public function detail($id)
    {
        if (empty($id))
        {
            Template::set_message(lang('msg_buffer_invalid_id'), 'error');
            redirect(SITE_AREA .'/reports/buffers');
        }
        
        $record = $this->buffer_model->find($id);
        Template::set('record', $record);
        
        Template::set('prev_page', $this->get_index_url());
        Template::render();
    }
    //--------------------------------------------------------------------
    
    private function _index_process_action()
    {
        if(isset($_POST['delete']))
        {
            $this->auth->restrict('Buffers.Reports.Manage');
            
            $checked = $this->input->post('checked');
            if (is_array($checked) && count($checked))
            {
                $result = 0;
                foreach ($checked as $pid)
                {
                    if($this->log_error_model->delete($pid))
                    {
                        $result++;
                    }
                }

                if ($results > 0)
                {
                    Template::set_message(sprintf(lang('msg_buffer_delete_success'), $results), 'success');
                }
            }
        }
        //delte all records
        else if (isset($_POST['truncate']))
        {
            $this->auth->restrict('Buffers.Reports.Manage');
            
            $result = $this->log_error_model->truncate();
            if ($result)
            {
                Template::set_message(lang('msg_buffer_truncate_success'), 'success');
            }
            else
            {
                Template::set_message(lang('msg_buffer_truncate_fail'), 'error');
            }
        }     
    }
}