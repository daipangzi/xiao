<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class developer extends Admin_Controller {

	//--------------------------------------------------------------------


	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Toolkit.Developer.View');
		$this->load->model('apps_model', null, true);
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index()
	{
        $records = $this->apps_model->find_all();

		Template::set('records', $records);
		Template::render();
	}
  
	//--------------------------------------------------------------------

    /*
		Method: edit()

		Allows editing of Apks data.
	*/
	public function edit()
	{
		$uid = $this->uri->segment(5);
        $key = $this->uri->segment(6);
		if (empty($uid) || $key != md5($uid))
		{
			Template::set_message(lang('msg_app_invalid_id'), 'error');
			redirect(SITE_AREA .'/developer/apps');
		}

		if (isset($_POST['save']) || isset($_POST['apply']))
		{
			$this->auth->restrict('Toolkit.Developer.Edit');

			if ($this->_save_app($uid))
			{
				Template::set_message(lang('msg_app_edit_success'), 'success');
                
                if(isset($_POST['save']))
                {
                    redirect(SITE_AREA .'/developer/toolkit');   
                }
			}
			else
			{
				Template::set_message(lang('msg_app_edit_fail'), 'error');
			}
		}
        
		Template::set('record', $this->apps_model->find($uid));
		Template::render();
	}

	//--------------------------------------------------------------------

    /**
    * uploader for developers
    * 
    */
    public function uploader()
    {
        if(isset($_POST['upload']))
        {
            $path = MEDIA_PATH . $this->input->post('upload_path');
            if(!valid_path($path))
            {
                Template::set_message(lang('msg_toolkit_invalid_path'), 'error');
            }
            else
            {
                $this->settings_lib->set('upload.path', $this->input->post('upload_path'));
                
                $config = array();
                $config['upload_path']  = $path;
                $config['overwrite']    = TRUE;
                $config['allowed_types']= '*';
                
                $file = 'files';                
                if($this->upload_file($file, $config))
                {
                    Template::set_message(lang('msg_toolkit_upload_success'), 'success');
                }
                else
                {
                    Template::set_message(lang('msg_toolkit_upload_fail'), 'error');
                }
            }
        }
        
        $path = $this->input->post('upload_path')?$this->input->post('upload_path'):$this->settings_lib->item('upload.path');
        Template::set('path', $path);
        Template::render();  
    }
    
    //--------------------------------------------------------------------
    
    /**
    * test sms and push notification for developers
    * 
    */
    public function tester()
    {
        $this->load->helper('validate');
        
        $channel = $this->input->post('channel')?$this->input->post('channel'):'global_channel';
        $message = $this->input->post('message')?$this->input->post('message'):'';
        $sms_phones  = $this->input->post('sms_phones')?$this->input->post('sms_phones'):'';
        $sms_message = $this->input->post('sms_message')?$this->input->post('sms_message'):'';
        
        if(isset($_POST['send_notification']) && $message != '' && $channel != '')
        {
            $result = send_push_notification($channel, $message);
            $result = json_decode($result);
            
            if(isset($result->result) && $result->result === TRUE)
            {
                Template::set_message(lang('msg_toolkit_push_success'), 'success');
            }
            else
            {
                Template::set_message(lang('msg_toolkit_push_fail'), 'error');
            }
        }
        else if(isset($_POST['send_sms']) && valid_phone($sms_phones) && $sms_message != '')
        {
            $this->load->library('smessage');
            $status = $this->smessage->StartSend($sms_phones, $sms_message, $this->smessage->NextDeviceNumber(0));
            
            if( $status === TRUE )
            {
                Template::set_message(lang('msg_toolkit_sms_success'), 'success');    
            }           
            else
            {
                Template::set_message(lang('msg_toolkit_sms_fail'), 'error');
            }
        }
        
        Template::set('push_channel', $channel);
        Template::set('push_message', $message);
        Template::set('sms_phones', $sms_phones);
        Template::set('sms_message', $sms_message);
        Template::render();  
    }

	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
    * save app
    * 
    * @param int $uid
    */
	private function _save_app($uid)
	{
        $this->form_validation->set_rules('file_name', lang('ct_file_name'), 'required|trim|max_length[20]');
		$this->form_validation->set_rules('version', lang('ct_version'), 'required|trim|max_length[15]|is_numeric');
		$this->form_validation->set_rules('release_date', lang('ct_release_date'), 'required|trim|valid_date');
        $this->form_validation->set_rules('description', lang('ct_description'), 'trim');

		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
        $data['file_name']    = $this->input->post('file_name');
		$data['version']      = $this->input->post('version');
		$data['release_date'] = $this->input->post('release_date') ? $this->input->post('release_date') : '0000-00-00';
        $data['description']  = $this->input->post('description');

		return $this->apps_model->update($uid, $data);
	}

	//--------------------------------------------------------------------



}