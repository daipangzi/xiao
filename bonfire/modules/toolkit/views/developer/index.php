<div class="admin-box">
	<h3><?php echo lang('ct_applications'); ?></h3>
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped">
		<thead>
			<tr>
				<th><?php echo lang('ct_application'); ?></th>
				<th><?php echo lang('ct_version'); ?></th>
				<th><?php echo lang('ct_release_date'); ?></th>
                <th><?php echo lang('ct_description'); ?></th>
                <th><?php echo lang('ct_file'); ?></th>
			</tr>
		</thead>
		<tbody>
		<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<?php foreach ($records as $record) : ?>
			<tr>
				<td><?php echo anchor(SITE_AREA .'/developer/toolkit/edit/' . $record->uid . '/' . md5($record->uid), '&nbsp;' .  get_application_name($record->app_type)); ?></td>
				<td><?php echo $record->version?></td>
				<td><?php echo $record->release_date?></td>
                <td><?php echo $record->description?></td>
                <td>
                    <?php 
                    $path = MEDIA_PATH . 'download/';
                    if(file_exists($path . $record->file_name))
                    {
                        echo lang('bf_yes');
                    }
                    else
                    {
                        echo lang('bf_no');
                    }
                    ?>
                </td>
			</tr>
		<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="10"><?php echo lang('msg_app_no_records'); ?></td>
			</tr>
		<?php endif; ?>
		</tbody>
		</table>
	<?php echo form_close(); ?>
</div>