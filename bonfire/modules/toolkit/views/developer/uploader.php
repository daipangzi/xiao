<div class="admin-box">
    <h3><?php echo lang('ct_upload_files'); ?></h3>
    
    <?php echo form_open($this->uri->uri_string(), "class='form-horizontal' enctype='multipart/form-data'" ); ?>
    <div class="control-group">
        <?php echo form_label(lang('ct_file'), 'files', array('class' => "control-label") ); ?>
        <div class='controls'>
            <input type="file" name="files" id="files" multiple/>
        </div>
    </div>
    
    <div class="control-group">
        <?php echo form_label(lang('ct_path'), 'upload_path', array('class' => "control-label") ); ?>
        <div class='controls'>
            <small>media/</small><input type="text" name="upload_path" id="upload_path" value="<?php echo $path; ?>"/>
        </div>
    </div>
    
    <div class="form-actions">
        <button type="submit" name="upload" class="btn"><?php echo lang('ct_btn_upload'); ?></button>
    </div> 
    <?php echo form_close(); ?>
</div>