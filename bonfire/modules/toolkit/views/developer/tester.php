<div class="admin-box">
    <h3>Tester</h3>
    
    <?php echo form_open($this->uri->uri_string(), "class='form-horizontal'" ); ?>
    <fieldset>
        <legend>Notification</legend>
        
        <div class="control-group">
            <?php echo form_label('Channel', 'channel', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input type="text" id="channel" name="channel" maxlength="50" class="span4" value="<?php echo $push_channel; ?>"  />
            </div>
        </div>
        
        <div class="control-group">
            <?php echo form_label('Message', 'message', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input type="text" id="message" name="message" maxlength="50" class="span4" value="<?php echo $push_message; ?>"  />
            </div>
        </div>
        
        <div class="form-actions">
            <button type="submit" name="send_notification" class="btn">Send</button>
        </div> 
    </fieldset>
    
    <fieldset>
        <legend>SMS</legend>
        
        <div class="control-group">
            <?php echo form_label('Phones', 'sms_phones', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input type="text" id="sms_phones" name="sms_phones" maxlength="11" class="span4" value="<?php echo $sms_phones; ?>"  />
            </div>
        </div>
        
        <div class="control-group">
            <?php echo form_label('Message', 'sms_message', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input type="text" id="sms_message" name="sms_message" maxlength="50" class="span4" value="<?php echo $sms_message; ?>"  />
            </div>
        </div>
        
        <div class="form-actions">
            <button type="submit" name="send_sms" class="btn">Send</button>
        </div> 
    </fieldset>
    <?php echo form_close(); ?>
</div>