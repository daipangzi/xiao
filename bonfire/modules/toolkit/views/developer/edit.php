<div class="admin-box">
    <h3>
        <?php 
            $app_name = '';
            if($record->app_type == USER_TYPE_TEACHER) $app_name = 'ct_teacher';
            else if($record->app_type == USER_TYPE_CHILDREN) $app_name = 'ct_children';
            else if($record->app_type == USER_TYPE_PARENT) $app_name = 'ct_parent';
            
            echo lang($app_name);
        ?>
    </h3>
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
        <div class="control-group <?php echo form_error('file_name') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ct_file_name'). lang('bf_form_label_required'), 'file_name', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="file_name" type="text" name="file_name" maxlength="20" value="<?php echo set_value('file_name', isset($record->file_name) ? $record->file_name : ''); ?>"  />
                <span class="help-inline"><?php echo form_error('file_name'); ?></span>
            </div>
        </div>
        
        <div class="control-group <?php echo form_error('version') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ct_version'). lang('bf_form_label_required'), 'apks_version', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="version" type="text" name="version" maxlength="15" value="<?php echo set_value('version', isset($record->version) ? $record->version : ''); ?>"  />
                <span class="help-inline"><?php echo form_error('version'); ?></span>
            </div>
        </div>
        
        <div class="control-group <?php echo form_error('release_date') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ct_release_date'). lang('bf_form_label_required'), 'release_date', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="release_date" type="text" name="release_date" class="date" value="<?php echo set_value('release_date', isset($record->release_date) ? $record->release_date : ''); ?>"  />
                <span class="help-inline"><?php echo form_error('release_date'); ?></span>
            </div>
        </div>
        
        <div class="control-group <?php echo form_error('description') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ct_description'), 'description', array('class' => "control-label") ); ?>
            <div class='controls'>
                <textarea id="description" name="description" cols="" rows="5"><?php echo set_value('description', isset($record->description) ? $record->description : ''); ?></textarea>
                <span class="help-inline"><?php echo form_error('description'); ?></span>
            </div>
        </div>
        
        <div class="control-group">
            <?php echo form_label(lang('ct_file'), '', array('class' => "control-label") ); ?>
            <div class='controls'>
                <p class="help-inline">
                <?php 
                    $path = MEDIA_PATH . 'download/';
                    if(file_exists($path . $record->file_name))
                    {
                        echo lang('bf_yes');
                    }
                    else
                    {
                        echo lang('bf_no');
                    }
                ?>
                </p>
            </div>
        </div>

        <div class="form-actions">
            <?php echo anchor(SITE_AREA.'/developer/apps', lang('ct_btn_back'), 'class="btn btn-inverse"'); ?>
            <button type="submit" name="save" class="btn btn-info"><?php echo  lang('ct_btn_save'); ?></button>
            <button type="submit" name="apply" class="btn"><?php echo  lang('ct_btn_apply'); ?></button>
        </div> 
    <?php echo form_close(); ?>

</div>
