<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['module_config'] = array(
    'menus'    => array(
        'developer'    => 'toolkit/developer/_menu',
    ),
	'description'	=> 'Manage APK Files',
	'name'		    => lang('ct_applications'),
	'version'		=> '0.0.1',
	'author'		=> 'admin'
);
