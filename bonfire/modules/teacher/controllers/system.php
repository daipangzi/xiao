<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class system extends Admin_Clients_Controller {

	//--------------------------------------------------------------------

    public function __construct()
	{
        $this->controller       = 'teacher';
        $this->controller_index = 'system/teacher';
        
		parent::__construct();

		$this->auth->restrict('Teacher.System.View');
        
        $this->load->model('teacher_model', null, true);        

        Assets::add_js('jquery.form.js');
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
		$this->_index_action_process();       
        
        //setup where conditions
        $where = array();
        $search = FALSE;
        if($this->input->post('search_entries')) {
            $search = $this->input->post('search');    
            $this->session->set_userdata('teacher_search', $search);
        } else if($this->input->post('reset_search')) {
            $this->session->unset_userdata('teacher_search'); 
        } else {
            $search = $this->session->userdata('teacher_search')?$this->session->userdata('teacher_search'):array();
        }
        
        if(!empty($search['idnum']))    $where['user_id LIKE '] = "%" . $search['idnum'] . "%";
        if(!empty($search['name']))     $where['user_name LIKE '] = "%" . $search['name'] . "%";
        if(!empty($search['email']))    $where['user_email LIKE '] = "%" . $search['email'] . "%";
        if(!empty($search['gender']))   $where['user_sex'] = $search['gender'];
        if(!empty($search['birthday_from'])) $where['user_birthday >= '] = $search['birthday_from'];
        if(!empty($search['birthday_to']))   $where['user_birthday <= '] = $search['birthday_to'];
        if(!empty($search['register_from'])) $where['reg_date >= '] = $search['register_from'];
        if(!empty($search['register_to']))   $where['reg_date <= '] = $search['register_to'];
        if(!empty($search['recent_from']))  $where['recent_visit_date >= '] = $search['recent_from'];
        if(!empty($search['recent_to']))    $where['recent_visit_date <= '] = $search['recent_to'];
        if(!empty($search['phone']))        $where['user_phone LIKE '] = "%" . $search['phone'] . "%";
        if(!empty($search['address']))      $where['user_address LIKE'] = "%" . $search['address'] . "%";
        if(!empty($search['status']))       $where['user_status'] = $search['status'];
        if(!empty($search['type']))         $where['teacher_type'] = $search['type'];
        if(isset($search['school']) && is_numeric($search['school'])) $where['school_uid'] = $search['school'];
        Template::set('search', $search);
        
        //setup ordder array
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);
        
        $orders  = array();
        switch($params['orderby']) {
            case "userid":
                $orders['user_id'] = $params['order'];
                break;
            case "name":
            case "email":
            case "birthday": 
            case "address": 
            case "phone": 
            case "status": 
                $orders["user_{$params['orderby']}"] = $params['order'];
                break;
            case "gender": 
                $orders["user_sex"] = $params['order'];
                break;
            case "date": 
                $orders["reg_date"] = $params['order'];
                break;
            case "recent": 
                $orders["recent_visit_date"] = $params['order'];
                break;
            case "school": 
                $orders["school_uid"] = $params['order'];
                break;
            case "type": 
                $orders["teacher_type"] = $params['order'];
                break;
            default:
                $orders["reg_date"] = 'desc';
                break;
        }
        
        //get school lists
        $schools = $this->school_model
            ->select_name($this->short_language)
            ->order_by_name()
            ->format_dropdown();            
        Template::set('schools', $schools);        
        
        //get total records        
        $total_records = $this->teacher_model
            ->where($where)
            ->count_all();
                  
        $records = $this->teacher_model
            ->where($where)
            ->order_by($orders)
            ->limit($this->limit, $offset)
            ->find_all(); 
        Template::set('records', $records);
          
        //setup pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->setup_pagination($total_records, count($records), $url_suffix);
                
        // set request page                                                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->save_index_url($url);

        Template::set('prev_page', $url);
		Template::render();
	}
	//--------------------------------------------------------------------

	/*
		Method: edit()

		Allows editing of Teacher data.
	*/
	public function edit()
	{
		$uid = $this->uri->segment(5);
        $key = $this->uri->segment(6);

		if (empty($uid) || $key != md5($uid))
		{
			Template::set_message(lang('msg_teacher_invalid_id'), 'error');
			$this->redirect_to_index();
		}
        
        $this->_edit_action_process($uid);
        Assets::add_css('multi-select.css');
        Assets::add_js('jquery.multi-select.js');
        Assets::add_module_js('teacher', 'teacher.js');
        
        //save selected tab
        $selected_tab = $this->input->post('selected_tab');
        Template::set('selected_tab', $selected_tab);
        
        //get school lists
        $schools = $this->school_model
            ->select_name($this->short_language)
            ->order_by_name()
            ->format_dropdown(FALSE);            
        Template::set('schools', $schools); 
        
        //get record
        $record = $this->teacher_model->find($uid);
        Template::set('record', $record); 
        Template::set('selected_school', $record->school_uid);  
        
        //get subjects
        $subjects = $this->school_model
            ->select('subject_uuid uid, subject_name name')
            ->get_subjects($record->school_uid);
        $subjects = format_dropdown($subjects, TRUE, lang('msg_select'));
        Template::set('subjects', $subjects); 
        
        //get master id
        $master = $this->teacher_model->find_master($record->school_uid);
        if(isset($master->teacher_uuid))
        {
            $record->can_master = 0;
        }
        else
        {
            $record->can_master = 1;
        }
        
        //get grade-classes
        $all_classes = $this->school_model
            ->filter_live_grades()
            ->filter_live_classes()
            ->order_by('grade_num')
            ->order_by('class_num')
            ->get_classes($record->school_uid);
        $charge_classes = $this->teacher_model->get_class_uids($uid, ROLE_CHARGE);
        $subject_classes= $this->teacher_model->get_class_uids($uid, ROLE_SUBJECT);
        $charge_subject = $this->teacher_model->get_charge_subject($uid);
            
        $class_info = array();
        $class_info['all_classes']  = $all_classes;
        $class_info['subjects']     = $subjects;
        $class_info['charge_classes']  = $charge_classes;
        $class_info['subject_classes'] = $subject_classes;
        $class_info['charge_subject']  = $charge_subject;
        $class_info['disable_flag']    = TRUE;
        Template::set('class_info', $class_info); 
        
        //if save failed, them save avatar image
        if($this->input->post('image_name') && $this->input->post('image_action') == "changed")
        {
            Template::set("image_name", $this->input->post('image_name'));    
            Template::set("image_path", 'tmp');    
        }
        else
        {
            Template::set("image_name", trim($record->user_photo));    
            Template::set("image_path", '');    
        }   
        Template::set("image_action", $this->input->post('image_action'));          
		  
        Template::set('prev_page', $this->get_index_url());
		Template::render();
	}

	//--------------------------------------------------------------------

	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------    
    
    /**
    * save general profile info
    * 
    * @param int $uid
    */
    private function _save_user_general($uid)
    {   
        $_POST['teacher_uuid'] = $uid;
        
        $birthday_extra = '';
        if($this->input->post('user_birthday'))
        {
            $birthday_extra = '|valid_date';
        }
        
        //validate fields
        $this->form_validation->set_rules('user_name','lang:ct_user_name','required|trim|max_length[50]|strip_tags|xss_clean|special_chars');
        $this->form_validation->set_rules('user_sex','lang:ct_user_sex','required|trim|max_length[1]|strip_tags|xss_clean');
        $this->form_validation->set_rules('user_birthday','lang:ct_user_birthday', "trim|strip_tags|xss_clean{$birthday_extra}");
        $this->form_validation->set_rules('user_address','lang:ct_user_address','trim|max_length[255]|strip_tags|xss_clean|special_chars');
        $this->form_validation->set_rules('user_phone','lang:ct_user_phone','required|unique[gus_teachers.user_phone,gus_teachers.teacher_uuid]|trim|valid_phone|strip_tags|xss_clean');
        $this->form_validation->set_rules('user_email','lang:ct_user_email','required|unique[gus_teachers.user_email,gus_teachers.teacher_uuid]|trim|valid_email|strip_tags|xss_clean');

        if ($this->form_validation->run() === FALSE)
        {
            return FALSE;
        }
        
        // make sure we only pass in the fields we want
        $data = array();
        $data['user_name']    = $this->input->post('user_name');
        $data['user_sex']     = $this->input->post('user_sex')==MAN?MAN:WOMAN;
        $data['user_phone']   = $this->input->post('user_phone');
        $data['user_email']   = $this->input->post('user_email');
        $data['user_address'] = $this->input->post('user_address');
        if($this->input->post('user_birthday'))
        {
            $data['user_birthday'] = $this->input->post('user_birthday');
        }
        
        return $this->teacher_model->update($uid, $data);
    }
       
    /**
    * update school and class relations
    * 
    * @param int $uid
    * @param int $old_school_uid
    */
    private function _save_teacher_school($uid, $old_school_uid)
    {   
        $this->form_validation->set_rules('school','lang:ct_school',"required|trim|max_length[40]|strip_tags|valid_school");
        
        if ($this->form_validation->run() === FALSE)
        {
            return FALSE;
        }
        
        //collect data  
        $school_uid = $this->input->post('school');
        $data   = array();
        $return = FALSE;
        
        //check if school-class is changed
        $school_changed = FALSE;
        if($school_uid != $old_school_uid)
        {
            $school_changed = TRUE;    
        }
        
        $charge_class  = $this->input->post('charge_class');
        $subject_class = $this->input->post('subject_class');
        if(!empty($charge_class) && is_array($charge_class))
        {
            $data['is_charge']    = 1;
            $data['charge_class'] = implode(',', $charge_class); 
        }
        else
        {
            $data['is_charge']    = 0;
            $data['charge_class'] = ''; 
        }
          
        $subject = $this->input->post('subject');
        if($this->school_model->valid_subject($school_uid, $subject))
        {
            $data['subject_uid']  = $subject;
        }
        
        if($subject && !empty($subject_class) && is_array($subject_class))
        {        
            $data['is_subject']   = 1;
            $data['subject_class']= implode(',', $subject_class); 
        }
        else
        {
            $data['is_subject']   = 0;
            $data['subject_class']= ''; 
        }
        
        if($school_changed === TRUE)
        {
            //delete subject and charge/subject classes
            $this->teacher_model->delete_links($uid);
            
            //update school_uid in user table
            $update_patch = array(
                'school_uid' => $school_uid,
                'school_date'=> date(FORMAT_DATETIME)
            );
            $this->teacher_model->update($uid, $update_patch);
            
            //add subject and charge/subject classes of new school
            $return = $this->teacher_model->add_links($uid, $data);
        }
        else
        {
            $return = $this->teacher_model->update_school_info($uid, $data);    
        }
        
        return $return;
    }
    
    /**
    * edit actions to update teacher
    * 
    * @param int $teacher_uid
    */
    private function _edit_action_process($teacher_uid) 
    {
        $fields = array('save_general', 'apply_general', 
            'save_photo', 'apply_photo', 
            'save_password', 'apply_password',
            'save_school', 'apply_school',
            'delete', 'activate', 'deactivate',
            'to_master', 'to_teacher');
        if(!array_one_key_exists($fields, $_POST)) return;
        
        $user = $this->teacher_model->find($teacher_uid);
        $school_uid = $user->school_uid;
        if(empty($user)) return;
        
        //update general profile
        if (isset($_POST['save_general']) || isset($_POST['apply_general']))
        {
            $this->auth->restrict('Teacher.System.Edit');
                               
            if ($this->_save_user_general($teacher_uid))
            {
                Template::set_message(sprintf(lang('msg_teacher_update_success'), $user->user_name), 'success');
                
                if (isset($_POST['save_general']))
                {
                    $this->redirect_to_index();
                }
            }
            else
            {
                Template::set_message(sprintf(lang('msg_teacher_update_fail'), $user->user_name), 'error');
            }
        } 
        else if ($this->input->post('image_action') && (isset($_POST['save_photo']) || isset($_POST['apply_photo'])))
        {
            $this->auth->restrict('Teacher.System.Edit');
                               
            if ($this->_save_user_photo($teacher_uid, USER_TYPE_TEACHER))
            {
                Template::set_message(sprintf(lang('msg_teacher_photo_update_success'), $user->user_name), 'success');
                if (isset($_POST['save_photo']))
                {
                    $this->redirect_to_index();
                }
            }
            else
            {
                Template::set_message(sprintf(lang('msg_teacher_photo_update_fail'), $user->user_name), 'success');
            }
            
            unset($_POST['image_name']);
            unset($_POST['image_action']);
        } 
        else if (isset($_POST['save_password']) || isset($_POST['apply_password']))
        {
            $this->auth->restrict('Teacher.System.Edit');
                               
            if ($this->_save_user_password($teacher_uid))
            {
                Template::set_message(sprintf(lang('msg_teacher_pass_update_success'), $user->user_name), 'success');
                if (isset($_POST['save_password']))
                {
                    $this->redirect_to_index();
                }
            }
            else
            {
                Template::set_message(sprintf(lang('msg_teacher_pass_update_fail'), $user->user_name), 'error');
            }
        }      
        else if ($this->input->post('change_class_relation') && (isset($_POST['save_school']) || isset($_POST['apply_school'])))
        {
            $this->auth->restrict('Teacher.System.Edit');
                               
            if ($this->_save_teacher_school($teacher_uid, $school_uid))
            {
                Template::set_message(sprintf(lang('msg_teacher_school_update_success'), $user->user_name), 'success');
                if (isset($_POST['save_school']))
                {
                    $this->redirect_to_index();
                }
            }
            else
            {
                Template::set_message(sprintf(lang('msg_teacher_school_update_fail'), $user->user_name), 'error');
            }
        }
        else if (isset($_POST['delete']))
        {
            $this->auth->restrict('Teacher.System.Delete');

            if ($this->teacher_model->delete($teacher_uid))
            {
                Template::set_message(sprintf(lang('msg_teacher_delete_success'), $user->user_name), 'success');
                $this->redirect_to_index();
            } else
            {
                Template::set_message(sprintf(lang('msg_teacher_delete_fail'), $user->user_name), 'error');
            }
        }
        else if (isset($_POST['activate']))
        {
            $this->auth->restrict('Teacher.System.Edit');

            if ($this->teacher_model->activate($teacher_uid))
            {
                Template::set_message(sprintf(lang('msg_teacher_activate_success'), $user->user_name), 'success');
            } 
            else
            {
                Template::set_message(sprintf(lang('msg_teacher_activate_fail'), $user->user_name), 'error');
            }
        }
        else if (isset($_POST['deactivate']))
        {
            $this->auth->restrict('Teacher.System.Edit');

            if ($this->teacher_model->deactivate($teacher_uid))
            {
                Template::set_message(sprintf(lang('msg_teacher_deactivate_success'), $user->user_name), 'success');
            } 
            else
            {
                Template::set_message(sprintf(lang('msg_teacher_deactivate_fail'), $user->user_name), 'error');
            }
        }
        else if (isset($_POST['to_master']))
        {
            $this->auth->restrict('Teacher.System.Edit');
            
            //check if master already exist in this school
            $master = $this->teacher_model->find_master($user->school_uid);
            if(!isset($master->teacher_uuid))
            {
                if ($this->teacher_model->update_as_master($teacher_uid))
                {
                    Template::set_message(sprintf(lang('msg_teacher_to_master_success'), $user->user_name), 'success');
                } 
                else
                {
                    Template::set_message(sprintf(lang('msg_teacher_to_master_fail'), $user->user_name), 'error');
                }
            }
            else
            {
                $school = $this->school_model->select_name($this->short_language)->find($user->school_uid);
                Template::set_message(sprintf(lang('msg_teacher_master_exist'), $school->school_name), 'error');    
            }
        }
        else if (isset($_POST['to_teacher']))
        {
            $this->auth->restrict('Teacher.System.Edit');

            if ($this->teacher_model->update_as_teacher($teacher_uid))
            {
                Template::set_message(sprintf(lang('msg_teacher_to_teacher_success'), $user->user_name), 'success');
            } 
            else
            {
                Template::set_message(sprintf(lang('msg_teacher_to_teacher_fail'), $user->user_name), 'error');
            }
        }
    }
    
    ///////////////////////////////////////////////////////////////////////
    //!AJAX METHODS
    ///////////////////////////////////////////////////////////////////////
    public function school_class_info() 
    {
        $school_uid = $this->input->post('school_uid');
        
        if(!$this->school_model->valid_school($school_uid) || !is_numeric($school_uid))
        {
            echo '';
            exit;
        }
        
        //get grade-classes
        $all_classes = $this->school_model
            ->filter_live_grades()
            ->filter_live_classes()
            ->order_by('grade_num')
            ->order_by('class_num')
            ->get_classes($school_uid);
        
        //get subjects
        $subjects = $this->school_model->select('subject_uuid uid, subject_name name')->get_subjects($school_uid);
        $subjects = format_dropdown($subjects, TRUE, lang('msg_select'));
        
        $data = array();
        $data['subjects']       = $subjects;
        $data['all_classes']    = $all_classes;
        $data['charge_classes'] = array();
        $data['subject_classes']= array();
        Template::block('school_class_info', 'system/_school_info', $data);
        
        exit;
    }//end school_class_info()
    
    public function upload_photo() 
    {
        $file_name = "user_photo";
        $status = parent::upload_photo($file_name, USER_TYPE_TEACHER);
        if($status)
        {
            echo $file_name;
        }
        else
        {
            echo '';
        }
        exit;     
    }
}