<?php 
    $extra = isset($disable_flag)?"disabled='disabled'":'';
    $lang_entrance = lang('ct_entrance');
?>

<?php echo form_dropdown('subject', $subjects, set_value('subject', isset($charge_subject)?$charge_subject:''), lang('ct_subject'), "class='auto-width' id='subject' {$extra}")?>

<div class="control-group">
    <?php echo form_label(lang('ct_charge_class'), 'charge_class', array('class' => "control-label") ); ?>
    <div class='controls'>
        <select id="charge_class" name="charge_class[]" class="multiple-selectbox" multiple="multiple" <?php echo $extra; ?> size="20">
        <?php foreach($all_classes as $class) { ?>
                <option value="<?php echo $class->class_uuid; ?>" <?php echo in_array($class->class_uuid, $charge_classes)?'selected="selected"':''; ?>><?php echo sprintf(lang('ct_grade_class_with'), $class->grade_num, $class->class_num); ?></option>
        <?php } ?>
        </select>
    </div>
</div>

<div class="control-group">
    <?php echo form_label(lang('ct_subject_class'), 'subject_class', array('class' => "control-label") ); ?>
    <div class='controls'>
        <select id="subject_class" name="subject_class[]" class="multiple-selectbox" multiple="multiple" <?php echo $extra; ?> size="20">
        <?php foreach($all_classes as $class) { ?>
                <option value="<?php echo $class->class_uuid; ?>" <?php echo in_array($class->class_uuid, $subject_classes)?'selected="selected"':''; ?>><?php echo sprintf(lang('ct_grade_class_with'), $class->grade_num, $class->class_num); ?></option>
        <?php } ?>
        </select>
    </div>
</div>