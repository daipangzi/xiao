$('.multiple-selectbox').multiSelect();

//province change
$('#school').change(function() {
    $.ajax({
      url: base_url+"admin/system/teacher/school_class_info",
      type: "post",
      data: {'school_uid': $(this).val(), "ci_csrf_token": ci_csrf_token()},
      success: function(res){
          $('#school_class_info').html(res);
          
          $('.multiple-selectbox').multiSelect();
      }   
    });    
});

$("#change_class_relation").click(function(){
    if($(this).is(':checked'))
    {
        $("#school_class_info input[type=checkbox], #school_class_info select, #school, #apply_school, #save_school").removeAttr("disabled");
    }
    else
    {
        $("#school_class_info input[type=checkbox], #school_class_info select, #school, #apply_school, #save_school").attr("disabled", "disabled");
    }
    $('.multiple-selectbox').multiSelect('refresh');
});

select_saved_tab();

//for user photo
set_user_photo('teacher');
user_tab_changes();