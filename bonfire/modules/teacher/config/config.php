<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');$config['module_config'] = array(
	'description'	=> 'Manage all teachers in our system',
	'name'		    => lang('ct_teachers'),
	'version'		=> '0.0.1',
	'author'		=> 'admin',
    'weights'       => 30,
);
