<?php 
$user_types = array(''=>'&nbsp;', USER_TYPE_TEACHER=>lang('ct_teacher'), USER_TYPE_MASTER=>lang('ct_master'), USER_TYPE_CHILDREN=>lang('ct_children'), USER_TYPE_PARENT=>lang('ct_parent')); 
$devices = array('', DEVICE_ANDROID=>lang('ct_android'), DEVICE_IPHONE=>lang('ct_iphone'));
$url = SITE_AREA.'/reports/errors?orderby=' . $params['orderby'] . '&amp;order=' . $params['order'];
?>
                    
<div class="admin-box">
	<h3><?php echo lang('ct_errors'); ?><small><?php echo $this->pagination->get_page_status();?></small></h3>
	<?php echo form_open($url); ?>
		<table class="table table-striped">
        <colgroup>   
            <?php if (isset($records) && is_array($records) && count($records)) : ?>
            <col width="30"/>                                                                                                                
            <?php endif;?>                   
            
            <col width="100"/>
            <col width="100"/>
            <col width="120"/>
            <col width="100"/>
            <col width="150"/>
            <col width=""/>
        </colgroup>
		<thead>
			<tr>
				<?php if (isset($records) && is_array($records) && count($records)) : ?>
				<th class="column-check"><input class="check-all" type="checkbox" /></th>
				<?php endif;?>
				
                <th class="<?php echo sort_classes($params['orderby'], "error", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/errors?orderby=error&amp;order='.sort_direction($params['orderby'], "error", $params['order'])); ?>"><span><?php echo lang('ct_error_no'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "user_type", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/errors?orderby=user_type&amp;order='.sort_direction($params['orderby'], "user_type", $params['order'])); ?>"><span><?php echo lang('ct_user_type'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "device_type", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/errors?orderby=device_type&amp;order='.sort_direction($params['orderby'], "device_type", $params['order'])); ?>"><span><?php echo lang('ct_device_type'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "ip_address", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/errors?orderby=ip_address&amp;order='.sort_direction($params['orderby'], "ip_address", $params['order'])); ?>"><span><?php echo lang('ct_ip_address'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "date", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/errors?orderby=date&amp;order='.sort_direction($params['orderby'], "date", $params['order'])); ?>"><span><?php echo lang('ct_registered_time'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th><span><?php echo lang('ct_error_message'); ?></span></th>
			</tr>
		</thead>
		<?php if (isset($records) && is_array($records) && count($records) && $this->auth->has_permission('Errors.Reports.Manage')) : ?>
		<tfoot>
			<tr>
				<td colspan="20">
					<input type="submit" name="delete" class="btn btn-danger select_one" value="<?php echo lang('ct_btn_delete') ?>" onclick="return confirm('<?php echo lang('msg_error_delete_list_confirm'); ?>')" disabled="disabled"/>
                    <input type="submit" name="truncate" class="btn btn-warning" value="<?php echo lang('ct_btn_truncate') ?>" onclick="return confirm('<?php echo lang('msg_error_truncate_confirm'); ?>')">
				</td>
			</tr>
		</tfoot>
		<?php endif; ?>
		<tbody>
		<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<?php foreach ($records as $record) : ?>
			<tr>
				<td><input type="checkbox" name="checked[]" value="<?php echo $record->uid ?>" /></td>
                <td><?php echo anchor(SITE_AREA .'/reports/errors/detail/'.$record->uid, $record->error_no); ?></td>
                <td><?php echo $user_types[$record->user_type]; ?></td>
                <td><?php echo isset($devices[$record->device_type])?$devices[$record->device_type]:lang('ct_unknown'); ?></td>
                <td><?php echo $record->ip_address; ?></td>
                <td><?php echo format_time($record->reg_date); ?></td>
                <td>
                    <?php echo ct_str_limiter($record->error_msg, 150); ?>
                </td>
			</tr>
		<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="20"><?php echo lang('msg_error_no_records'); ?></td>
			</tr>
		<?php endif; ?>
		</tbody>
		</table>
	<?php echo form_close(); ?>
</div>
<?php echo $this->pagination->create_links(); ?>