<div class="admin-box">
    <div class="form-horizontal">
        <div class="control-group">
            <?php echo form_label(lang('ct_error_no'), 'error_no', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input class="span6" value="<?php echo $record->error_no; ?>" readonly="readonly"/>
            </div>
        </div>
        
        <div class="control-group">
            <?php echo form_label(lang('ct_error_message'), 'error_message', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input class="span6" value="<?php echo $record->error_msg; ?>" readonly="readonly"/>
            </div>
        </div>
        
        <div class="control-group">
            <?php echo form_label(lang('ct_request_params'), 'json_content', array('class' => "control-label") ); ?>
            <div class='controls'>
                <textarea class="span6" rows="15" readonly="readonly"><?php echo pretty_json($record->request_params); ?></textarea>
            </div>
        </div>
        
        <div class="form-actions">
            <?php echo anchor($prev_page, lang('ct_btn_back'), 'class="btn btn-inverse"'); ?>
        </div> 
    </div>   
    
</div>