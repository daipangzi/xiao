<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class reports extends Admin_Controller {

	//--------------------------------------------------------------------

    public function __construct()
	{
        $this->controller       = 'errors';
        $this->controller_index = 'reports/errors';
        
		parent::__construct();

		$this->auth->restrict('Errors.Reports.View');
		$this->load->model('log_error_model', null, true);
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
		$this->_index_process_action();       
        
        //setup ordder array
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);
        
        $orders  = array();
        switch($params['orderby']) {
            case "error":
                $orders["error_no"] = $params['order'];
                break;
            case "user_type": 
            case "device_type": 
            case "ip_address": 
                $orders[$params['orderby']] = $params['order'];
                break;
            case "date": 
                $orders["reg_date"] = $params['order'];
                break;
            default:
                $orders["reg_date"] = 'desc';
                break;
        }
        
        //get total
        $total_records = $this->log_error_model
            ->count_all();
                  
        $records = $this->log_error_model
            ->order_by($orders)
            ->limit($this->limit, $offset)
            ->find_all(); 
        Template::set('records', $records);
          
        //setup pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->setup_pagination($total_records, count($records), $url_suffix);
        
        // set request page                                                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->save_index_url($url);
                
        Template::set('prev_page', $url);
		Template::render();
	}
	//--------------------------------------------------------------------
    
    public function detail($id)
    {
        if (empty($id))
        {
            Template::set_message(lang('msg_error_invalid_id'), 'error');
            redirect(SITE_AREA .'/reports/errors');
        }
        
        $record = $this->log_error_model->find($id);
        Template::set('record', $record);
        
        Template::set('prev_page', $this->get_index_url());
        Template::render();
    }
    //--------------------------------------------------------------------
    
    private function _index_process_action()
    {
        if(isset($_POST['delete']))
        {
            $this->auth->restrict('Errors.Reports.Manage');
            
            $checked = $this->input->post('checked');
            if (is_array($checked) && count($checked))
            {
                $result = 0;
                foreach ($checked as $pid)
                {
                    if($this->log_error_model->delete($pid))
                    {
                        $result++;
                    }
                }

                if ($results > 0)
                {
                    Template::set_message(sprintf(lang('msg_error_delete_success'), $results), 'success');
                }
            }
        }
        //delte all records
        else if (isset($_POST['truncate']))
        {
            $this->auth->restrict('Errors.Reports.Manage');
            
            $result = $this->log_error_model->truncate();
            if ($result)
            {
                Template::set_message(lang('msg_error_truncate_success'), 'success');
            }
            else
            {
                Template::set_message(lang('msg_error_truncate_fail'), 'error');
            }
        }     
    }
}