<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class system extends Admin_Controller {

	//--------------------------------------------------------------------

	public function __construct()
	{
        $this->controller       = 'school';
        $this->controller_index = 'system/school';
        
		parent::__construct();

		$this->auth->restrict('School.System.View');
        
        $this->load->model('region_model', null, true);
		
        $provinces = $this->region_model->province_dropdown_list(TRUE, lang('msg_select'));
        Template::set('provinces', $provinces);    
        
		Template::set_block('sub_nav', 'system/_sub_nav');
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
	    $this->_index_action_process();

        //setup where conditions
        $where = array();
        $search = FALSE;
        
        if($this->input->post('search_entries')) {
            $search = $this->input->post('search');    
            $this->session->set_userdata('school_search', $search);
        } else if($this->input->post('reset_search')) {
            $this->session->unset_userdata('school_search'); 
        } else {
            $search = $this->session->userdata('school_search')?$this->session->userdata('school_search'):array();
        }

        if(!empty($search['name']))     $where["school_name_".$this->short_language.' LIKE '] = "%" . $search['name'] . "%";
        if(!empty($search['province'])) $where['region_uid LIKE '] = $search['province'] . "%";
        if(!empty($search['district'])) $where['region_uid']   = $search['district'];
        if(!empty($search['from_date']))$where['reg_date >= '] = $search['from_date'];
        if(!empty($search['to_date']))  $where['reg_date <= '] = $search['to_date'];
        if(!empty($search['school_type']))     $where['school_type'] = $search['school_type'];
        if(!empty($search['active_status']))   $where['active_status'] = $search['active_status'];
        
        $s_province = '00';
        if(!empty($search['province'])) $s_province = $search['province'];
        $cities = $this->region_model->city_dropdown_list($s_province, TRUE, lang('msg_select'));
        Template::set('cities', $cities);
        Template::set('search', $search);

        //setup ordder array
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);

        $orders  = array();
        switch($params['orderby']) {
            case "name":
                $orders[$name_field] = $params['order'];
                break;
            case "status":
                $orders["active_status"]= $params['order'];
                break;
            case "type":
                $orders["school_type"] = $params['order'];
                break;
            case "date":
                $orders["reg_date"]    = $params['order'];
                break;
            case "region": 
                $orders["region_uid"]  = $params['order'];
                break;
            default:
                $orders["school_uuid"] = "desc";
                break;
        }
        
        //get total counts
        $total_records = $this->school_model
                ->with_region()
                ->where($where)
                ->count_all(TRUE);
        
        //get all products
        $records = $this->school_model
                ->select_name($this->short_language)
                ->with_region()
                ->where($where)
                ->limit($this->limit, $offset)
                ->order_by($orders)
                ->find_all(0, TRUE);          
        Template::set('records', $records);
       
        //setup pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->setup_pagination($total_records, count($records), $url_suffix);
                
        // set request page                                                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->save_index_url($url);
         
		//Template::set('toolbar_title', lang('school_manage'));
		Template::render();
	}

	//--------------------------------------------------------------------

	/*
		Method: create()

		Creates a School object.
	*/
	public function create()
	{
		$this->auth->restrict('School.System.Create');

		if (isset($_POST['save']) || isset($_POST['apply']))
		{
			if ($insert_id = $this->_save_school())
			{
				Template::set_message(lang('msg_school_create_success'), 'success');
                
                if(isset($_POST['save'])) 
                {
                    $this->redirect_to_index();
                }
                else if(isset($_POST['apply']))
                {
                    Template::redirect(SITE_AREA .'/system/school/edit/'.$insert_id.'/'.md5($insert_id));
                }
			}
			else
			{
				Template::set_message(lang('msg_school_create_fail'), 'error');
			}
		}
		Assets::add_module_js('school', 'school.js');
        
        $region = array();
        $province = '00';
        if($this->input->post('province'))
        {
            $region['province'] = $this->input->post('province');
            $province = $this->input->post('province');
        }
        
        if($this->input->post('school_type'))
        {
            $region['school_type'] = $this->input->post('school_type');
        }
        Template::set('region', $region);
                
        $cities = $this->region_model->city_dropdown_list($province, TRUE, lang('msg_select'));
        Template::set('cities', $cities);

        Template::set('prev_page', $this->session->userdata('school_index'));
		Template::render();
	}

	//--------------------------------------------------------------------


	/*
		Method: edit()

		Allows editing of School data.
	*/
	public function edit()
	{
        $this->auth->restrict('School.System.Edit');
        
		$uid = $this->uri->segment(5);
        $key = $this->uri->segment(6);

		if (empty($uid) || $key != md5($uid))
		{
			Template::set_message(lang('msg_school_invalid_id'), 'error');
			$this->redirect_to_index();
		}
        
        //process actions
        $this->_edit_action_process($uid);
        Assets::add_module_js('school', 'school.js');
        
        //save selected tab
        $selected_tab = $this->input->post('selected_tab');
        Template::set('selected_tab', $selected_tab);
        
        $record = $this->school_model->find($uid);
        Template::set('record', $record);
        
        //save province when failure
        $region = array();
        if(isset($_POST['general']['province']))
        {
            $region['province'] = $_POST['general']['province'];
        }
        else
        {
            $region['province'] = substr($record->region_uid, 0, 2);
        }
        if($this->input->post('school_type'))
        {
            $region['school_type'] = $this->input->post('school_type');
        }
        Template::set('region', $region);
       
        //get region data 
        $province = substr($record->region_uid, 0 , 2);
        if($this->input->post('province'))
        {
            $province = $this->input->post('province');
        }
        
        $cities = $this->region_model->city_dropdown_list($province, TRUE, lang('msg_select'));
        Template::set('cities', $cities);
        
        //get grade-classes
        //live grades only
        $grades = $this->school_model->filter_live_grades()->get_grades($record->school_uuid);
        Template::set('grades', $grades);
                    
        $exist_new_grade = $this->school_model->exist_new_grade($record->school_uuid);
        Template::set('exist_new_grade', $exist_new_grade);
              
        $newest_year_level = intval($this->school_model->newest_year_level($record->school_uuid));
        $current_year   = intval(date('Y', time()));
        $able_new_grade = $current_year <= $newest_year_level?FALSE:TRUE;
        Template::set('able_new_grade', $able_new_grade);
        
        $highes_grade = $this->school_model->get_highest_grade($record->school_uuid);
        Template::set('highes_grade', $highes_grade);
              
        Template::set('prev_page', $this->get_index_url());
		//Template::set('toolbar_title', lang('school_edit'));
		Template::render();
	}
	//--------------------------------------------------------------------

	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	private function _save_school()
	{
		$this->form_validation->set_rules('school_name','lang:ct_school_name','required|trim|max_length[128]|strip_tags|xss_clean');
		$this->form_validation->set_rules('school_region','lang:ct_city','required|trim|max_length[8]');
        $this->form_validation->set_rules('school_address','lang:ct_school_address','trim|max_length[255]|strip_tags|xss_clean');
		$this->form_validation->set_rules('school_latitude','lang:ct_latitude','required|trim|is_numeric|max_length[255]');
        $this->form_validation->set_rules('school_longitude','lang:ct_longitude','required|trim|is_numeric|max_length[255]');
        $this->form_validation->set_rules('school_type','lang:ct_school_type','required|trim|integer|max_length[1]');

		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}

		// make sure we only pass in the fields we want
		$valid_school_types = array(SCHOOL_PRIMARY, SCHOOL_JUNIOR, SCHOOL_SENIOR);
        $school_type        = $this->input->post('school_type');
        $grade_class_nums   = $this->input->post('grade_class')[$school_type];
        
		$data = array();
		$data['school_name_cn'] = $this->input->post('school_name');
        $data['school_name_ko'] = $this->input->post('school_name');
        $data['school_name_en'] = $this->input->post('school_name');
		$data['school_address'] = $this->input->post('school_address');
        $data['school_type'] = array_key_exists($school_type, $valid_school_types)?$school_type:SCHOOL_PRIMARY;        
        $data['region_uid']  = $this->input->post('school_region');
		$data['latitude']    = $this->input->post('school_latitude');
		$data['longitude']   = $this->input->post('school_longitude');

        return $this->school_model->insert_with($data, $grade_class_nums);
	}
    
    /**
    * save general school info
    * 
    * @param int $school_uid
    */
    private function _save_school_general($school_uid)
    {    
        $this->form_validation->set_rules('general[school_name_cn]',lang('ct_name').'('.lang('ct_chinese').')','required|trim|max_length[128]|strip_tags|xss_clean');
        $this->form_validation->set_rules('general[school_name_ko]',lang('ct_name').'('.lang('ct_korean').')','required|trim|max_length[128]|strip_tags|xss_clean');
        $this->form_validation->set_rules('general[school_name_en]',lang('ct_name').'('.lang('ct_english').')','required|trim|max_length[128]|strip_tags|xss_clean');
        $this->form_validation->set_rules('general[school_region]','lang:ct_city','required|trim|max_length[8]');
        $this->form_validation->set_rules('general[school_name]','lang:ct_address','trim|max_length[255]|strip_tags|xss_clean');
        $this->form_validation->set_rules('general[school_latitude]','lang:ct_latitude','required|trim|is_numeric|max_length[255]');
        $this->form_validation->set_rules('general[school_longitude]','lang:ct_longitude','required|trim|is_numeric|max_length[255]');
        $this->form_validation->set_rules('general[school_type]','lang:ct_type','required|trim|integer|max_length[1]');
        
        if ($this->form_validation->run() === FALSE)
        {
            return FALSE;
        }

        // make sure we only pass in the fields we want
        $valid_school_types = get_school_types(FALSE);
        $general = $this->input->post('general');
        $school_type = $general['school_type'];
               
        $data = array();
        $data['school_name_cn'] = $general['school_name_cn'];
        $data['school_name_ko'] = $general['school_name_ko'];
        $data['school_name_en'] = $general['school_name_en'];
        $data['school_address'] = $general['school_address'];
        $data['school_type']    = array_key_exists($school_type, $valid_school_types)?$school_type:SCHOOL_PRIMARY;
        $data['region_uid']     = $general['school_region'];
        $data['latitude']    = $this->input->post('school_latitude');
        $data['longitude']   = $this->input->post('school_longitude');
           
        return $this->school_model->update($school_uid, $data);
    }
    
    /**
    * save classes of school
    * 
    * @param int $school_uid
    */
    private function _save_new_class($school_uid)
    {
        if($this->input->post('new_grade_added') == '1')
        {
            $new_grade = $this->input->post('new_grade');
            $year_level = $new_grade['year_level'];
            $class_nums = $new_grade['class_nums'];
            
            $data = array();
            $data['school_uid'] = $school_uid;
            $data['grade_num']  = 0;
            $data['year_level'] = intval($year_level)>2000?$year_level:date('Y', time());
            
            return $this->school_model->insert_grade($data, $class_nums);
        }
        
        return FALSE;        
    }
    
    /**
    * make grade to graduate status
    * 
    * @param int $school_uid
    */
    private function _save_grade_graduate($school_uid)
    {
        $grade_uid     = $this->input->post('apply_graduate');
        $graduate_date = $this->input->post('graduate_date');
        
        return $this->school_model->graduate_grade($school_uid, $grade_uid, $graduate_date);
    }
    
    /**
    * edit actions to update school
    * 
    * @param int $school_uid
    */
    private function _edit_action_process($school_uid) 
    {
        $fields = array('save_general', 'apply_general', 
            'save_new_grade', 'apply_new_grade', 
            'apply_graduate', 'delete', 'upgrade', 
            'activate', 'deactivate');
        if(!array_one_key_exists($fields, $_POST)) return;
                  
        $school = $this->school_model->select_name($this->short_language)->find($school_uid);
        if(empty($school)) return;
        
        if (isset($_POST['save_general']) || isset($_POST['apply_general']))
        {
            $this->auth->restrict('School.System.Edit');
            
            if ($this->_save_school_general($school_uid))
            {
                Template::set_message(sprintf(lang('msg_school_edit_success'), $school->school_name), 'success');
                if(isset($_POST['save_general'])) $this->redirect_to_index();
            }
            else
            {
                Template::set_message(sprintf(lang('msg_school_edit_fail'), $school->school_name), 'error');
            }
        }
        else if (isset($_POST['save_new_grade']) || isset($_POST['apply_new_grade']))
        {
            $this->auth->restrict('School.System.Edit');
                                   
            if ($this->_save_new_class($school_uid))
            {
                Template::set_message(sprintf(lang('msg_school_add_grade_success'), $school->school_name), 'success');
                
                if(isset($_POST['save_new_class'])) 
                {
                    $this->redirect_to_index();
                }
                else
                {
                    $url = SITE_AREA . '/system/school/edit/' . $school_uid . '/' . md5($school_uid);
                    Template::redirect($url);
                }
            }
            else
            {
                Template::set_message(lang('msg_school_add_grade_fail'), 'error');
            }
        }
        else if (isset($_POST['apply_graduate']))
        {
            $this->auth->restrict('School.System.Edit');

            if ($this->_save_grade_graduate($school_uid))
            {
                Template::set_message(lang('msg_school_graduate_grade_success'), 'success');
            }
            else
            {
                Template::set_message(lang('msg_school_graduate_grade_fail'), 'error');
            }
        }
        else if (isset($_POST['delete']))
        {
            $this->auth->restrict('School.System.Delete');

            if ($this->school_model->delete($school_uid))
            {
                Template::set_message(sprintf(lang('msg_school_delete_success'), $school->school_name), 'success');

                $this->redirect_to_index();
            } 
            else
            {
                Template::set_message(sprintf(lang('msg_school_delete_fail'), $school->school_name), 'error');
            }
        }
        else if (isset($_POST['activate']))
        {
            $this->auth->restrict('School.System.Edit');

            if ($this->school_model->activate($school_uid))
            {
                Template::set_message(sprintf(lang('msg_school_activate_success'), $school->school_name), 'success');
            } 
            else
            {
                Template::set_message(sprintf(lang('msg_school_activate_fail'), $school->school_name), 'error');
            }
        }
        else if (isset($_POST['deactivate']))
        {
            $this->auth->restrict('School.System.Edit');

            if ($this->school_model->deactivate($school_uid))
            {
                Template::set_message(sprintf(lang('msg_school_deactivate_success'), $school->school_name), 'success');
            } 
            else
            {
                Template::set_message(sprintf(lang('msg_school_deactivate_fail'), $school->school_name), 'error');
            }
        }
        else if (isset($_POST['upgrade']))
        {
            if($this->school_model->exist_new_grade($school_uid))
            {
                if ($this->school_model->upgrade_grades($school_uid))
                {    
                    Template::set_message(lang('msg_school_grade_upgrade_success'), 'success');
                }
                else
                {
                    Template::set_message(lang('msg_school_grade_upgrade_fail'), 'error');
                }
            }
            else
            {
                Template::set_message(lang('msg_school_no_new_grade'), 'warning');
            }
        }
    }

    /**
    * index actions
    * 
    */
    private function _index_action_process()
    {
        $fields = array('delete', 'activate', 'deactivate');
        if(!array_one_key_exists($fields, $_POST)) return;
        
        $checked = $this->input->post('checked');
        if (!is_array($checked) || count($checked) == 0) return;
                
        $action_method = '';
        $message_success = '';
        $message_fail = '';
        if(isset($_POST['delete'])) 
        {
            $action_method = 'delete';
            $message_success = 'msg_school_delete_success';
            $message_fail = 'msg_school_delete_fail';
        }
        else if(isset($_POST['activate']))
        {
            $action_method = 'activate';
            $message_success = 'msg_school_activate_success';
            $message_fail = 'msg_school_activate_fail';
        }
        else if(isset($_POST['deactivate'])) 
        {
            $action_method = 'deactivate';
            $message_success = 'msg_school_deactivate_success';
            $message_fail = 'msg_school_deactivate_fail';
        }
        
        foreach($checked as $pid)
        {
            $school = $this->school_model->select_name($this->short_language)->find($pid);
            if(empty($school))
            {
                continue;
            }
            
            $messages = '';
            if($action_method === 'activate' && (int)$school->active_status === STATUS_ACTIVE)
            {
                $messages .= sprintf(lang('msg_teacher_activate_already'), $school->school_name) . "<br>";
                continue;
            }
            else if($action_method === 'deactivate' && (int)$school->active_status === STATUS_INACTIVE)
            {
                $messages .= sprintf(lang('msg_teacher_deactivate_already'), $school->school_name) . "<br>";
                continue;
            }
            
            $status = $this->school_model->$action_method($pid);
            if($status)
            {                
                $messages .= sprintf(lang($message_success), $school->school_name) . "<br>";
            }
            else
            {
                $messages .= sprintf(lang($message_fail), $school->school_name) . "<br>";
            }
        }
        
        if($messages !== '')
        {
            Template::set_message($messages, 'success');
        }
    }
    //--------------------------------------------------------------------
}