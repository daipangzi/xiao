$('#school_type').change(function() {
    type = $(this).val();
    
    $('.admin-box .grade_class:visible').slideUp('fast', 'linear', function(){
        $('#grade_class'+type).slideDown();
    });
});

$('#add_new_grade').click(function() {
    box = $('#new_grade_box');
    if(box.is(":visible"))
    {
        $('#new_grade_added').val('0');
        $('#save_new_grade').prop('disabled', true);
        $('#apply_new_grade').prop('disabled', true);
        box.slideUp();
    }
    else
    {
        $('#new_grade_added').val('1');
        $('#save_new_grade').prop('disabled', false);
        $('#apply_new_grade').prop('disabled', false);
        box.slideDown();
    }
});

///////////////////////////////////////////////
function get_max_class_number(clicked)
{
    row = clicked.parent();
    max = 0;
    $('input[type=checkbox]', row).each(function() {
        if(parseInt($(this).val()) > max)
        {
            max = parseInt($(this).val());
        }
    });
    
    return max;
}