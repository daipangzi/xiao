<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');$config['module_config'] = array(
	'description'	=> 'Manage schools in user system',
	'name'		    => lang('ct_school'),
	'version'		=> '0.0.1',
	'author'		=> 'admin',
    'weights'       => 100,
);
