<?php 
$school_model   = $this->model('school_model');
$children_model = $this->model('children_model');
            
$field = 'school_name_' . strtolower(get_short_language_name($current_user->language));            
$title = $record->$field;

if(isset($_GET['return_url']) && $_GET['return_url'] != '')
{
    $prev_page = rawurldecode($_GET['return_url']);
}
?>
<div class="admin-box">
    <h3><?php echo $title; ?></h3>
    
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal" autocomplete="off"'); ?>
    <input type="hidden" id="new_classes" name="new_classes" value=""/>
    <input type="hidden" id="new_grade_added" name="new_grade_added" value=""/>
    <input type="hidden" id="selected_tab" name="selected_tab" value="<?php echo $selected_tab; ?>"/>
    
    <div class="form-buttons">
        <?php echo anchor($prev_page, lang('ct_btn_back'), 'class="btn btn-inverse"'); ?>
        
        <?php if ($this->auth->has_permission('School.System.Delete') && isset($record)) : ?>
            <?php if(isset($record->active_status) && $record->active_status == STATUS_ACTIVE) { ?>
            <button type="submit" name="deactivate" class="btn"><?php echo lang('ct_btn_deactivate'); ?></button>
            <?php } else if(isset($record->active_status) && $record->active_status == STATUS_INACTIVE) { ?>
            <button type="submit" name="activate" class="btn"><?php echo lang('ct_btn_activate') ?></button>
            <?php } ?>
            
            <button type="submit" name="delete" class="btn" onclick="return confirm('<?php echo lang('msg_school_delete_one_confirm'); ?>')"><?php echo lang('ct_btn_delete'); ?></button>
        <?php endif; ?>
        
        <?php if(isset($exist_new_grade) && $exist_new_grade) : ?>
            <input type="submit" name="upgrade" class="btn btn-inverse" value="<?php echo  lang('ct_year_upgrade'); ?>" />&nbsp;
        <?php endif; ?>
    </div>
    <!--end form-buttons-->
    
    <ul class="nav nav-tabs" >
        <li class="active"><a href="#general-box" data-toggle="tab"><?php echo lang('ct_general'); ?></a></li>
        <li><a href="#current-class-box" data-toggle="tab"><?php echo lang('ct_live_grades'); ?></a></li>
        <li><a href="#old-class-box" data-toggle="tab"><?php echo lang('ct_graduated_grades'); ?></a></li>
    </ul>
    
    <div class="tab-content">
        <div class="tab-pane fade in active" id="general-box"> 
            <div class="control-group <?php echo form_error('general[school_name_cn]') ? 'error' : ''; ?>">
                <?php echo form_label(lang('ct_school_name').'('.lang('ct_chinese').')'.lang('bf_form_label_required'), 'school_name_cn', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="school_name_cn" type="text" name="general[school_name_cn]" maxlength="128" class="span5" value="<?php echo set_value('general[school_name_cn]', isset($record->school_name_cn) ? $record->school_name_cn : ''); ?>"  />
                    <span class="help-inline"><?php echo form_error('general[school_name_cn]'); ?></span>
                </div>
            </div>
            
            <div class="control-group <?php echo form_error('general[school_name_ko]') ? 'error' : ''; ?>">
                <?php echo form_label('('.lang('ct_korean').')'.lang('bf_form_label_required'), 'school_name_ko', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="school_name_ko" type="text" name="general[school_name_ko]" maxlength="128" class="span5" value="<?php echo set_value('general[school_name_ko]', isset($record->school_name_ko) ? $record->school_name_ko : ''); ?>"  />
                    <span class="help-inline"><?php echo form_error('general[school_name_ko]'); ?></span>
                </div>
            </div>
            
            <div class="control-group <?php echo form_error('general[school_name_en]') ? 'error' : ''; ?>">
                <?php echo form_label('('.lang('ct_english').')'.lang('bf_form_label_required'), 'school_name_en', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="school_name_en" type="text" name="general[school_name_en]" maxlength="128" class="span5" value="<?php echo set_value('general[school_name_en]', isset($record->school_name_en) ? $record->school_name_en : ''); ?>"  />
                    <span class="help-inline"><?php echo form_error('general[school_name_en]'); ?></span>
                </div>
            </div>

            <div class="control-group <?php echo form_error('general[school_region]') ? 'error' : ''; ?>">
                <?php echo form_label(lang('ct_city').lang('bf_form_label_required'), 'province', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <?php echo form_dropdown2('general[province]', $provinces, set_value('general[province]', isset($region['province']) ? $region['province'] : ''), 'class="auto-width" id="province"')?>        
                    <?php echo form_dropdown2('general[school_region]', $cities, set_value('general[school_region]', isset($record->region_uid) ? $record->region_uid : ''), 'class="auto-width" id="city"')?>        
                    <span class="help-inline"><?php echo form_error('general[school_region]'); ?></span>
                </div>
            </div>
            
            <div class="control-group <?php echo form_error('general[school_address]') ? 'error' : ''; ?>">
                <?php echo form_label(lang('ct_address'), 'school_address', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="school_name" type="text" name="general[school_address]" maxlength="100" class="span5" value="<?php echo set_value('school_address', isset($record->school_address) ? $record->school_address : ''); ?>"  />
                    <span class="help-inline"><?php echo form_error('general[school_address]'); ?></span>
                </div>
            </div>
            
            <div class="control-group <?php echo form_error('general[school_latitude]') ? 'error' : ''; ?> <?php echo form_error('general[school_latitude]') ? 'error' : ''; ?>">
                <?php echo form_label(lang('ct_latitude').lang('bf_form_label_required'), 'school_latitude', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="school_latitude" type="text" name="general[school_latitude]" maxlength="255" class="span5" value="<?php echo set_value('general[school_latitude]', isset($record->latitude) ? $record->latitude : '0'); ?>"  />
                    <span class="help-inline"><?php echo form_error('general[school_latitude]'); ?></span>
                </div>
            </div>
            
            <div class="control-group <?php echo form_error('general[school_longitude]') ? 'error' : ''; ?> <?php echo form_error('general[school_longitude]') ? 'error' : ''; ?>">
                <?php echo form_label(lang('ct_longitude').lang('bf_form_label_required'), 'school_longitude', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="school_longitude" type="text" name="general[school_longitude]" maxlength="255" class="span5" value="<?php echo set_value('general[school_longitude]', isset($record->longitude) ? $record->longitude : '0'); ?>"  />
                    <span class="help-inline"><?php echo form_error('general[school_longitude]'); ?></span>
                </div>
            </div>
            
            <?php $school_types = get_school_types(FALSE); ?>
            <?php echo form_dropdown('general[school_type]', $school_types, set_value('general[school_type]', isset($record->school_type) ? $record->school_type : ''), lang('ct_school_type').lang('bf_form_label_required'), 'class="auto-width"')?>
            
            <div class="form-actions">
                <?php echo anchor($prev_page, lang('ct_btn_back'), 'class="btn btn-inverse"'); ?>
                <button type="submit" name="save_general" class="btn btn-info"><?php echo  lang('ct_btn_save'); ?></button>
                <button type="submit" name="apply_general" class="btn"><?php echo  lang('ct_btn_apply'); ?></button>
            </div>
        </div> 
        <!--end general-box-->
        
        <div class="tab-pane fade in" id="current-class-box">
            <div class="control-group">
                <?php echo form_label('', '', array('class' => "control-label") ); ?>
                <div class='controls'>
                    <table class="table table-bordered" style="width:auto;">
                    <colgroup>
                        <col width="150"/>
                        <col width="150"/>
                        <col width="150"/>
                        <col width="150"/>
                        <col width="100"/>
                        <col width="250"/>
                    </colgroup>
                    <thead>
                        <tr>
                            <th class="text-center"><?php echo lang('ct_grade'); ?></th>
                            <th class="text-center"><?php echo lang('ct_entrance'); ?></th>
                            <th class="text-center"><?php echo lang('ct_classes'); ?></th>
                            <th class="text-center"><?php echo lang('ct_children'); ?></th>
                            <th class="text-center"></th>
                            <th class="text-center"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($grades as $grade) { ?>
                        <tr>
                            <td class="text-center"><?php echo sprintf(lang('ct_grade_with'), $grade->grade_num); ?></td>
                            <td class="text-center"><?php echo $grade->year_level; ?></td>
                            <td class="text-center"><?php echo $school_model->filter_grade($grade->grade_uuid)->filter_live_classes()->count_classes($record->school_uuid); ?></td>
                            <td class="text-center"><?php echo $children_model->with_class()->filter_school($record->school_uuid)->filter_grade($grade->grade_uuid)->count_all(); ?></td>
                            <td class="text-center"><?php echo anchor(SITE_AREA."/system/school/edit_grade/{$record->school_uuid}/{$grade->grade_uuid}", lang('ct_btn_view')); ?></td>
                            <td class="text-center">
                                <?php if($highes_grade == $grade->grade_num) { ?>
                                <input type="text" name="graduate_date" class="date" value="<?php echo date(FORMAT_DATE); ?>" style="width:100px;"/>
                                <button type="submit" name="apply_graduate" class="btn" value="<?php echo $grade->grade_uuid; ?>"><?php echo lang('ct_graduate'); ?></button>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                    </table>
                </div>
            </div>
            
            <?php if(!$exist_new_grade && $able_new_grade) : ?>
            <div class="control-group" style="padding:8px 0;">
                <label class="control-label noPadding noMargin">&nbsp;</label>
                <div class='controls'>
                    <a href="javascript:void(0);" id="add_new_grade"><?php echo lang('ct_btn_new_grade'); ?></a><br/>
                </div>
            </div>
            
            <div class="control-group" id="new_grade_box" style="display:none;">
                <label class="control-label"><?php echo sprintf(lang('ct_grade_with'), lang('ct_new')); ?></label>
                <div class='controls'>
                    <span class="inline right" style="width:90px;"><?php echo lang('ct_classes'); ?></span>&nbsp;&nbsp;<input type="text" class="span1 inline" name="new_grade[class_nums]" value="1"/>&nbsp;<?php echo lang('ct_class_unit'); ?>&nbsp;&nbsp;<br/><br/>
                    <span class="inline right" style="width:90px;"><?php echo lang('ct_entrance'); ?></span>&nbsp;&nbsp;<input type="text" class="span1 inline" name="new_grade[year_level]" value="<?php echo date('Y', time()); ?>"/><br/><br/>
                    <!--<span class="inline right" style="width:90px;">&nbsp;</span>&nbsp;&nbsp;<input type="submit" class="btn" name="" value="<?php echo lang('ct_add'); ?>"/>-->
                </div>
            </div>
            <?php endif; ?>
            
            <div class="form-actions">
                <?php echo anchor($prev_page, lang('ct_btn_back'), 'class="btn btn-inverse"'); ?>
                <button type="submit" id="save_new_grade" name="save_new_grade" class="btn btn-info" disabled="disabled"><?php echo  lang('ct_btn_save'); ?></button>
                <button type="submit" id="apply_new_grade" name="apply_new_grade" class="btn" disabled="disabled"><?php echo  lang('ct_btn_apply'); ?></button>
            </div>
        </div>
        <!--end current-class-box-->
        
        <div class="tab-pane fade in" id="old-class-box">   
        </div>
        <!--end old-class-box-->
    </div>
    <!--end tab-content-->
    <?php echo form_close(); ?>

</div>
<!--end admin-box-->

<div class="hidden">
    <label id="template_class" class="sub-label">
        <span><?php echo lang('ct_which_class'); ?></span>&nbsp;
        <input type="checkbox" name="year_class[][]" value=""/>
    </label>           
</div>
