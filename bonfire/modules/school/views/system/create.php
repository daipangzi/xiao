<?php 
$title = lang('msg_school_details');

if(isset($_GET['return_url']) && $_GET['return_url'] != '')
{
    $prev_page = rawurldecode($_GET['return_url']);
}
?>
<div class="admin-box">
    <h3><?php echo $title; ?></h3>
    
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal" autocomplete="off"'); ?>
    <input type="hidden" id="new_classes" name="new_classes" value=""/>
    <input type="hidden" id="new_grade_added" name="new_grade_added" value=""/>
    
    <div class="form-buttons">
        <?php echo anchor($prev_page, lang('ct_btn_back'), 'class="btn btn-inverse"'); ?>
        <button type="submit" name="save" class="btn btn-info"><?php echo  lang('ct_btn_save'); ?></button>
        <button type="submit" name="apply" class="btn"><?php echo  lang('ct_btn_apply'); ?></button>        
    </div>
    
    <div class="control-group <?php echo form_error('school_name') ? 'error' : ''; ?>">
        <?php echo form_label(lang('ct_school_name').lang('bf_form_label_required'), 'school_name', array('class' => "control-label") ); ?>
        <div class='controls'>
            <input id="school_name" type="text" name="school_name" maxlength="1218" class="span5" value="<?php echo set_value('school_name', isset($record['school_name']) ? $record['school_name'] : ''); ?>"  />
            <span class="help-inline"><?php echo form_error('school_name'); ?></span>
        </div>
    </div>

    <div class="control-group <?php echo form_error('school_region') ? 'error' : ''; ?>">
        <?php echo form_label(lang('ct_city').lang('bf_form_label_required'), 'province', array('class' => "control-label") ); ?>
        <div class='controls'>
            <?php echo form_dropdown2('province', $provinces, set_value('province', isset($region['province']) ? $region['province'] : ''), 'class="auto-width" id="province"')?>        
            <?php echo form_dropdown2('school_region', $cities, set_value('school_region', isset($record['region_uid']) ? $record['region_uid'] : ''), 'class="auto-width" id="city"')?>        
            <span class="help-inline"><?php echo form_error('school_region'); ?></span>
        </div>
    </div>
    
    <div class="control-group <?php echo form_error('school_address') ? 'error' : ''; ?>">
        <?php echo form_label(lang('ct_school_address'), 'school_address', array('class' => "control-label") ); ?>
        <div class='controls'>
            <input id="school_address" type="text" name="school_address" maxlength="100" class="span5" value="<?php echo set_value('school_address', isset($record['school_address']) ? $record['school_address'] : ''); ?>"  />
            <span class="help-inline"><?php echo form_error('school_address'); ?></span>
        </div>
    </div>
    
    <div class="control-group <?php echo form_error('school_latitude') ? 'error' : ''; ?> <?php echo form_error('school_latitude') ? 'error' : ''; ?>">
        <?php echo form_label(lang('ct_latitude').lang('bf_form_label_required'), 'school_latitude', array('class' => "control-label") ); ?>
        <div class='controls'>
            <input id="school_latitude" type="text" name="school_latitude" maxlength="255" class="span5" value="<?php echo set_value('school_latitude', isset($record['latitude']) ? $record['latitude'] : '0'); ?>"  />
            <span class="help-inline"><?php echo form_error('school_latitude'); ?></span>
        </div>
    </div>
    
    <div class="control-group <?php echo form_error('school_longitude') ? 'error' : ''; ?> <?php echo form_error('school_longitude') ? 'error' : ''; ?>">
        <?php echo form_label(lang('ct_longitude').lang('bf_form_label_required'), 'school_longitude', array('class' => "control-label") ); ?>
        <div class='controls'>
            <input id="school_longitude" type="text" name="school_longitude" maxlength="255" class="span5" value="<?php echo set_value('school_longitude', isset($record['longitude']) ? $record['longitude'] : '0'); ?>"  />
            <span class="help-inline"><?php echo form_error('school_longitude'); ?></span>
        </div>
    </div>
    
    <?php $school_types = get_school_types(); ?>
    <?php echo form_dropdown('school_type', $school_types, set_value('school_type', isset($record['school_type']) ? $record['school_type'] : ''), lang('ct_school_type').lang('bf_form_label_required'), 'class="auto-width" id="school_type"')?>
    
    <div class="control-group">
        <?php echo form_label('', '', array('class' => "control-label") ); ?>
        <div class='controls'>
            <?php $grades = config_item('school_grades'); ?>
            
            <?php 
                $i = 1; 
                $selected_type = isset($region['school_type']) ? $region['school_type'] : '';
            ?>
            <div id="grade_class" class="grade_class"><span class="help-block" style="margin-top:5px;"><?php echo lang('msg_input_grade_class_numers'); ?></span></div>
            <?php foreach($grades as $g) : ?>
            <div id="grade_class<?php echo $i; ?>" class="grade_class" style="<?php echo ($i==$selected_type)?"":"display:none;"; ?>">
                <?php for($j=1; $j<=$g; $j++) : ?>
                <div class="row-fluid">
                    <label for="grade-<?php echo $i; ?>-<?php echo $j; ?>" class="sub-label"><?php echo sprintf(lang('ct_grade_with'), $j); ?>:</label>
                    <input type="text" id="grade-<?php echo $i; ?>-<?php echo $j; ?>" class="grade-class span1 inline" name="grade_class[<?php echo $i; ?>][<?php echo $j; ?>]" value="1"/>&nbsp;<?php echo lang('ct_class_unit'); ?>
                </div>
                <?php endfor; ?>
            </div>
            <?php $i++; endforeach; ?>
        </div>
    </div>
    
    <div class="form-actions">
        <?php echo anchor($prev_page, lang('ct_btn_back'), 'class="btn btn-inverse"'); ?>
        <button type="submit" name="save" class="btn btn-info"><?php echo  lang('ct_btn_save'); ?></button>
        <button type="submit" name="apply" class="btn"><?php echo  lang('ct_btn_apply'); ?></button>
    </div>
    <?php echo form_close(); ?>

</div>

<div class="hidden">
    <label id="template_class" class="sub-label">
        <span><?php echo lang('ct_which_class'); ?></span>&nbsp;
        <input type="checkbox" name="year_class[][]" value=""/>
    </label>           
</div>
