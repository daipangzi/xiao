<?php
$teacher_model  = $this->model('teacher_model');    
$children_model = $this->model('children_model');    
$school_model = $this->model('school_model');    
$school_types = get_school_types();
//$field = 'school_name_' . strtolower(get_short_language_name($current_user->language));
$url = SITE_AREA.'/system/school?orderby=' . $params['orderby'] . '&amp;order=' . $params['order'];
?>  
<div class="admin-box"> 
    <h3><?php echo lang('ct_schools'); ?><small><?php echo $this->pagination->get_page_status();?></small></h3>
    <?php echo form_open($url); ?>
        <table class="table table-striped">
        <colgroup>
            <?php if ($this->auth->has_permission('School.System.Delete') && isset($records) && is_array($records) && count($records)) : ?>
            <col width="40"/>
            <?php endif;?>
            
            <col width=""/>
            <col width="300"/>
            <col width="120"/>
            <col width="100"/>
            <col width="100"/>
            <col width="80"/>
            <col width="80"/>
            <col width="80"/>
            <col width="90"/>
        </colgroup>
		<thead>
			<tr>
				<?php if ($this->auth->has_permission('School.System.Delete') && isset($records) && is_array($records) && count($records)) : ?>
				<th class="column-check"><input class="check-all" type="checkbox" /></th>
				<?php endif;?>
				
                <th class="<?php echo sort_classes($params['orderby'], "name", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/school?orderby=name&amp;order='.sort_direction($params['orderby'], "name", $params['order'])); ?>"><span><?php echo lang('ct_school_name'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "region", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/school?orderby=region&amp;order='.sort_direction($params['orderby'], "region", $params['order'])); ?>"><span><?php echo lang('ct_school_address'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "type", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/school?orderby=type&amp;order='.sort_direction($params['orderby'], "type", $params['order'])); ?>"><span><?php echo lang('ct_school_type'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "date", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/school?orderby=date&amp;order='.sort_direction($params['orderby'], "date", $params['order'])); ?>"><span><?php echo lang('ct_registered'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "status", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/system/school?orderby=status&amp;order='.sort_direction($params['orderby'], "status", $params['order'])); ?>"><span><?php echo lang('ct_status'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th><span><?php echo lang('ct_classes'); ?></span></th>
                <th><span><?php echo lang('ct_teachers'); ?></span></th>
                <th><span><?php echo lang('ct_children'); ?></span></th>
                <th><span><?php //echo lang('ct_actions'); ?></span></th>
			</tr>
            <tr class="search_row">
                <?php if ($this->auth->has_permission('School.System.Delete') && isset($records) && is_array($records) && count($records)) : ?>
                <td></td>
                <?php endif;?>
                
                <td><input type="text" name="search[name]" value="<?php echo isset($search['name']) ? $search['name'] : ''; ?>" style=""/></td>
                <td>
                    <?php echo form_dropdown2('search[province]', $provinces, set_value('province', isset($search['province']) ? $search['province'] : ''), 'class="auto-width" id="province"')?>        
                    <?php echo form_dropdown2('search[district]', $cities, set_value('district', isset($search['district']) ? $search['district'] : ''), 'class="auto-width" id="city"')?>        
                </td>
                <td>
                    <?php echo form_dropdown2('search[school_type]', $school_types, set_value('school_type', isset($search['school_type']) ? $search['school_type'] : ''), 'class="auto-width" id="school_type"')?>
                </td>
                <td>
                    <input type="text" name="search[from_date]" class="date" value="<?php echo isset($search['from_date']) ? $search['from_date'] : ''; ?>" style=""/><br/>
                    <input type="text" name="search[to_date]" class="date" value="<?php echo isset($search['to_date']) ? $search['to_date'] : ''; ?>" style=""/>    
                </td>
                <td>
                    <?php $options = array(''=>'', 1=>lang('bf_yes'), 2=>lang('bf_no')); ?>
                    <?php echo form_dropdown2('search[active_status]', $options, set_value('active_status', isset($search['active_status']) ? $search['active_status'] : ''), 'class="auto-width"')?>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <button type="submit" name="search_entries" class="btn" value="1" title="<?php echo lang('ct_btn_search'); ?>"><i class="icon-filter"></i></button>
                    <button type="submit" name="reset_search" class="btn" value="1" title="<?php echo lang('ct_btn_reset'); ?>"><i class="icon-refresh"></i></button>
                </td>
            </tr>
		</thead>
		<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<tfoot>
			<?php if ($this->auth->has_permission('School.System.Delete')) : ?>
			<tr>
				<td colspan="20">
					<input type="submit" name="delete" class="btn btn-danger select_one" value="<?php echo lang('ct_btn_delete') ?>" onclick="return confirm('<?php echo lang('msg_school_delete_list_confirm'); ?>')" disabled="disabled">
                    <input type="submit" name="activate" class="btn select_one" value="<?php echo lang('ct_btn_activate') ?>" disabled="disabled">
                    <input type="submit" name="deactivate" class="btn select_one" value="<?php echo lang('ct_btn_deactivate') ?>" disabled="disabled">
				</td>
			</tr>
			<?php endif;?>
		</tfoot>
		<?php endif; ?>
		<tbody>
		<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<?php foreach ($records as $record) : ?>
			<tr>
				<?php if ($this->auth->has_permission('School.System.Delete')) : ?>
				<td><input type="checkbox" name="checked[]" value="<?php echo $record->school_uuid ?>" /></td>
				<?php endif;?>
				
				<?php if ($this->auth->has_permission('School.System.Edit')) : ?>
				<td><?php echo anchor(SITE_AREA .'/system/school/edit/'.$record->school_uuid.'/'.md5($record->school_uuid), $record->school_name) ?></td>
				<?php else: ?>
				<td><?php echo $record->school_name ?></td>
				<?php endif; ?>
			
				<td><?php echo $provinces[substr($record->region_uid, 0, 2)]?>&nbsp;<?php echo $record->region_name?>&nbsp;<?php echo $record->school_address; ?></td>
				<td><?php echo $school_types[$record->school_type] ?></td>
				<td class="text-center"><?php echo format_date($record->reg_date) ?></td>
                <td>
                    <span class="label <?php echo $record->active_status==STATUS_ACTIVE?'label-success':'label-warning'; ?>">
                    <?php echo $record->active_status==STATUS_ACTIVE?lang('ct_active'):lang('ct_inactive'); ?>
                    </span>
                </td>
                <td class="text-center"><?php echo $school_model->filter_live_grades()->filter_live_classes()->count_classes($record->school_uuid); ?></td>
                <td class="text-center"><?php echo $teacher_model->filter_school($record->school_uuid)->count_all(); ?></td>
                <td class="text-center"><?php echo $children_model->with_class()->filter_school($record->school_uuid)->count_all(); ?></td>
                <td class="text-center">
                    <?php echo anchor(SITE_AREA .'/system/school/edit/'.$record->school_uuid.'/'.md5($record->school_uuid), '<i class="icon-pencil"></i>', 'title="'.lang('bf_action_edit').'"') ?>
                </td>
			</tr>
		<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="20"><?php echo lang('msg_school_no_records'); ?></td>
			</tr>
		<?php endif; ?>
		</tbody>
		</table>
	<?php echo form_close(); ?>
</div>
<?php echo $this->pagination->create_links(); ?>