<?php 
$user_types = array(''=>'&nbsp;', USER_TYPE_TEACHER=>lang('ct_teacher'), USER_TYPE_MASTER=>lang('ct_master'), USER_TYPE_CHILDREN=>lang('ct_children'), USER_TYPE_PARENT=>lang('ct_parent')); 
$devices = array('', DEVICE_ANDROID=>lang('ct_android'), DEVICE_IPHONE=>lang('ct_iphone'), DEVICE_PC=>lang('ct_pc'));
$url = SITE_AREA.'/reports/logins?orderby=' . $params['orderby'] . '&amp;order=' . $params['order'];
?>
                    
<div class="admin-box">
	<h3><?php echo lang('ct_logins'); ?><small><?php echo $this->pagination->get_page_status();?></small></h3>
	<?php echo form_open($url); ?>
		<table class="table table-striped">
        <colgroup> 
            <?php if(isset($records) && is_array($records) && count($records) && $this->auth->has_permission('Logins.Reports.Manage')) : ?>
            <col width="30"/>                                                                                                                
            <?php endif;?>   
            <col width="10"/>
            <col width="160"/>
            <col width="120"/>
            <col width="80"/>
            <col width="110"/>
            <col width="260"/>
            <col width="150"/>
            <col width="100"/>
            <col width=""/>
            <col width="100"/>
        </colgroup>
		<thead>
			<tr>
                <?php if(isset($records) && is_array($records) && count($records) && $this->auth->has_permission('Logins.Reports.Manage')) : ?>
                <th class="column-check"><input class="check-all" type="checkbox" /></th>
                <?php endif;?>
                <th></th>
				<th class="<?php echo sort_classes($params['orderby'], "userid", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/logins?orderby=userid&amp;order='.sort_direction($params['orderby'], "userid", $params['order'])); ?>"><span><?php echo lang('ct_user_id'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "name", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/logins?orderby=name&amp;order='.sort_direction($params['orderby'], "name", $params['order'])); ?>"><span><?php echo lang('ct_name'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "type", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/logins?orderby=type&amp;order='.sort_direction($params['orderby'], "type", $params['order'])); ?>"><span><?php echo lang('ct_user_type'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "device_type", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/logins?orderby=device_type&amp;order='.sort_direction($params['orderby'], "device_type", $params['order'])); ?>"><span><?php echo lang('ct_device_type'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "device_info", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/logins?orderby=device_info&amp;order='.sort_direction($params['orderby'], "device_info", $params['order'])); ?>"><span><?php echo lang('ct_device_info'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "date", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/logins?orderby=date&amp;order='.sort_direction($params['orderby'], "date", $params['order'])); ?>"><span><?php echo lang('ct_login_time'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "status", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/reports/logins?orderby=status&amp;order='.sort_direction($params['orderby'], "status", $params['order'])); ?>"><span><?php echo lang('ct_status'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th><?php echo lang('ct_login_remarks'); ?></th>
                <th></th>
			</tr>
            <tr class="search_row">
                <?php if(isset($records) && is_array($records) && count($records) && $this->auth->has_permission('Logins.Reports.Manage')) : ?>
                <td></td>
                <?php endif;?>
                
                <td></td>
                <td><input type="text" name="search[idnum]" value="<?php echo isset($search['idnum']) ? $search['idnum'] : ''; ?>" style=""/></td>
                <td><input type="text" name="search[name]" value="<?php echo isset($search['name']) ? $search['name'] : ''; ?>" style=""/></td>
                <td><?php echo form_dropdown2('search[type]', $user_types, set_value('type', isset($search['type']) ? $search['type'] : ''), 'class="auto-width" style="min-width:80px;"')?></td>
                <td><?php echo form_dropdown2('search[device_type]', $devices, set_value('device_type', isset($search['device_type']) ? $search['device_type'] : ''), 'class="auto-width" style="min-width:80px;"')?></td>
                <td><input type="text" name="search[device_info]" class="" value="<?php echo isset($search['device_info']) ? $search['device_info'] : ''; ?>" style=""/></td>
                <td>
                    <input type="text" name="search[login_from]" class="date" value="<?php echo isset($search['login_from']) ? $search['login_from'] : ''; ?>" style=""/><br/>
                    <input type="text" name="search[login_to]" class="date" value="<?php echo isset($search['login_to']) ? $search['login_to'] : ''; ?>" style=""/>    
                </td>
                <td>
                    <?php $options = array(''=>'&nbsp;', 1=>lang('ct_online'), 2=>lang('ct_logout')); ?>
                    <?php echo form_dropdown2('search[status]', $options, set_value('status', isset($search['status']) ? $search['status'] : ''), 'class="auto-width" style="min-width:80px;"')?>
                </td>
                <td></td>
                <td>
                    <button type="submit" name="search_entries" class="btn" value="1" title="<?php echo lang('ct_btn_search'); ?>"><i class="icon-filter"></i></button>
                    <button type="submit" name="reset_search" class="btn" value="1" title="<?php echo lang('ct_btn_reset'); ?>"><i class="icon-refresh"></i></button>
                </td>
            </tr>
		</thead>
        <?php if (isset($records) && is_array($records) && count($records) && $this->auth->has_permission('Logins.Reports.Manage')) : ?>
        <tfoot>
            <tr>
                <td colspan="20">
                    <input type="submit" name="delete" class="btn btn-danger select_one" value="<?php echo lang('ct_btn_delete') ?>" onclick="return confirm('<?php echo lang('msg_login_delete_list_confirm'); ?>')" disabled="disabled"/>
                    <input type="submit" name="logout" class="btn btn-inverse select_one" value="<?php echo lang('ct_logout') ?>" disabled="disabled"/>
                    <input type="submit" name="truncate" class="btn btn-warning" value="<?php echo lang('ct_btn_truncate') ?>" onclick="return confirm('<?php echo lang('msg_login_truncate_confirm'); ?>')"/>
                </td>
            </tr>
        </tfoot>
        <?php endif; ?>
		<tbody>
		<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<?php foreach ($records as $record) : ?>
			<tr>
                <?php if($this->auth->has_permission('Logins.Reports.Manage')) : ?>
                <td><input type="checkbox" name="checked[]" value="<?php echo $record->uid ?>" /></td>
                <?php endif;?>
                
                <td></td>
				<td><?php echo $record->user_id; ?></td>
                <td><?php echo $record->user_name; ?></td>
                <td><?php echo $user_types[$record->user_type]; ?></td>
                <td><?php echo isset($devices[$record->device_type])?$devices[$record->device_type]:lang('ct_unknown'); ?></td>
                <td><?php echo $record->device_info; ?></td>
                <td><?php echo format_time($record->login_date); ?></td>
                <td>
                <?php 
                    if($record->active_status == LOGIN_STATUS_ONLINE)
                    {
                        echo "<span class='label label-success'>" . lang('ct_online') . "</span>";
                    }
                    else
                    {
                        echo "<span class='label label-inverse'>" . lang('ct_logout') . "</span>";
                    }
                ?>
                </td>
                <td>
                    <?php 
                    $status = (int)$record->active_status;
                    if($status === LOGIN_STATUS_DUPLICATE)
                    {
                        echo lang('msg_duplicate_login');
                    }
                    else if($status === LOGIN_STATUS_YOU_UPDATED)
                    {
                        echo lang('msg_school_updated');
                    }
                    else if($status === LOGIN_STATUS_YOUR_CHILDREN_UPDATED)
                    {
                        echo lang('msg_parent_children_updated');
                    }
                    else if($status === LOGIN_STATUS_OUT_BY_ADMIN)
                    {
                        echo lang('msg_by_admin');
                    }
                    ?>
                </td>
                <td></td>
			</tr>
		<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="20"><?php echo lang('msg_login_no_records'); ?></td>
			</tr>
		<?php endif; ?>
		</tbody>
		</table>
	<?php echo form_close(); ?>
</div>
<?php echo $this->pagination->create_links(); ?>