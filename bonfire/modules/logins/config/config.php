<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');$config['module_config'] = array(
	'description'	=> 'Monitoring all user login information in our system',
	'name'          => lang('ct_logins'),
	'version'		=> '0.0.1',
	'author'		=> 'admin',
    'weights'       => 200,
);
