<?php 

// ------------------------------------------------------------------------
include_once "inc.php";

class send_verify_code extends MX_Controller
{
    public function __construct($language='english')
    {
        parent::__construct(); 
                
        //load lang
        $this->lang->load('mobile', $language);
        
        //load model
        $this->load->model('sms_model');
    }
    //--------------------------------------------------------------------
    
    /**
    * generate verify code and send it to phones
    * 
    * @param string $phones : sperated by comma
    * @param string $emails  : sperated by comma
    */
    public function send_code($phones='', $emails='', $sms_uid=-1)
    { 
        if($phones == -1 || $sms_uid == -1) 
        {
            return;   
        }
          
        $code = $this->sms_model->get_code($sms_uid);
        if($code === FALSE)
        {
            return;
        }
        
        //send sms here and get device_num
        $this->_send_sms($phones, $code, $sms_uid);
            
        //send sms to email(optional)
        if($emails != '' || valid_emails($emails))
        {
            $message = sprintf(lang('msg_sms_verify_code'), $code);
            $this->_send_sms_email($emails, $message);
        }
        //end
    }
    
        
    /**
    * send sms code to phone
    * 
    * @param string $phones: seperated by comma
    * @param string $text
    */
    private function _send_sms($phones, $text, $sms_uid)
    {
        if($phones == '' || $text == '') return;
        
        $this->load->library('smessage');
        
        $latest_device = $this->sms_model->get_latest_device();
        $sms    = $this->smessage;  
        $phones = array_unique(explode(',', $phones));
        foreach($phones as $phone)
        {
            if(trim($phone) == '') continue;

            $status = $sms->StartSend(trim($phone), $text, $sms->NextDeviceNumber($latest_device));
              
            usleep(SMS_DEVICE_DELAY_TIME);
        }        
        
        $last_used_device = is_numeric($sms->used_device)?$sms->used_device:0;
        $this->sms_model->update($sms_uid, array('device_num' => $last_used_device));
    }
    
    /**
    * send message by email
    * 
    * @param string $emails: seperated by comma
    * @param string $message
    */
    private function _send_sms_email($emails, $message)
    {
        if($emails == '' || $message == '') return;
        
        $this->load->library('emailer/emailer');
        
        $emails = array_unique(explode(',', $emails));
        foreach($emails as $email)
        {
            if($email == '') continue;
            
            $data = array
            (
                'to'       => $email,
                'subject'  => 'SMS Code',
                'message'  => $message,
            );
            
            $this->emailer->send($data);
        }
    }
}//end class

$language = isset($argv[1])?$argv[1]:'english';
$sms_uid  = isset($argv[1])?$argv[2]:-1;
$phones   = isset($argv[2])?$argv[3]:-1;
$emails   = isset($argv[3])?$argv[4]:-1;

$sender = new send_verify_code($language);
$sender->send_code($phones, $emails, $sms_uid);