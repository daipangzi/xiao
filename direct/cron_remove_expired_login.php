<?php 

// ------------------------------------------------------------------------
include_once "inc.php";
                                                            
class remove_expire_login extends MX_Controller
{
    public function execute()
    {        
        $this->load->model('log_login_model');
        
        $this->log_login_model->remove_expires();
    }
}//end class
         
$object = new remove_expire_login();                     
$object->execute();