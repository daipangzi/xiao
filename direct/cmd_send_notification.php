<?php 

// ------------------------------------------------------------------------
include_once "inc.php";
                                                            
class send_notification extends MX_Controller
{
    public function __construct($language='english')
    {
        parent::__construct(); 
        
        //load lang
        $this->lang->load('mobile', $language);  
    }
    //--------------------------------------------------------------------
    
    public function push($message_type, $channels, $sender_name)
    {        
        if(!is_array($channels) || empty($channels)) return;
           
        $message = '';
        switch($message_type)
        {
            case CHANNEL_TYPE_MESSAGE:
            default:
                $message = sprintf(lang('msg_new_message_arrived'), $sender_name);
                break;            
        }
                 
        send_push_notification($channels, $message);
    }
}//end class
    
$language     = isset($argv[1])?$argv[1]:'english';
$message_type = isset($argv[1])?$argv[2]:-1;
$channels     = explode(',', isset($argv[3])?$argv[3]:'');
$sender_name  = isset($argv[4])?$argv[4]:'';
     
$object = new send_notification($language);                     
$object->push($message_type, $channels, $sender_name);