<?php 

// ------------------------------------------------------------------------
include_once "inc.php";

class send_security extends MX_Controller
{
    public function __construct($language='english')
    {
        parent::__construct(); 
        
        //load lang
        $this->lang->load('mobile', $language);
       
        //load model
        $this->load->model('teacher_model');
        $this->load->model('children_model');
        $this->load->model('parent_model');
    }
    //--------------------------------------------------------------------
    
    /**
    * send password via email or phone
    * 
    * @param int    $user_type
    * @param string $user_id
    * @param bool   $via_phone
    * @param bool   $via_email
    */
    public function send_security_info($user_type, $user_id, $via_phone, $via_email)
    { 
        $user = $this->_user_row($user_id, $user_type);
        if(empty($user))
        {        
            return;
        }
                
        //send security info by sms
        if($via_phone == 1)
        {
            $sms_text = sprintf(lang('msg_send_security'), $user->user_passwd);
            $this->_send_by_sms($user->user_phone, $sms_text);
        }
            
        //send security info by email
        if($via_email == 1)
        {
            $email_text = sprintf(lang('msg_send_security'), $user->user_passwd);
            $this->_send_by_email($user->user_email, $email_text);
            //end
        }
    }
     
    /**
    * send by phone
    * 
    * @param string $phones: seperated by comma
    * @param string $text
    */
    private function _send_by_sms($phone, $text)
    {
        $this->load->library('smessage');
        $sms = $this->smessage;  
        
        if(trim($phone) == '' || !valid_phone($phone)) continue;

        $sms->StartSend(trim($phone), $text, $sms->NextDeviceNumber(0));
    }
    
    /**
    * send message by email
    * 
    * @param string $emails: seperated by comma
    * @param string $message
    */
    private function _send_by_email($email, $message)
    {
        $this->load->library('emailer/emailer');
        
        if($email == '' || !valid_email($email)) continue;
            
        $data = array
        (
            'to'       => $email,
            'subject'  => lang('msg_send_security_title'),
            'message'  => $message,
        );
        
        $this->emailer->send($data);
    }
    
    /**
    * get user row
    * 
    * @param int    $school_uid
    * @param string $user_id
    * @param int    $user_type
    */
    private function _user_row($user_id, $user_type)
    {
        $user_model = '';
        switch($user_type)
        {
            case USER_TYPE_TEACHER:
            case USER_TYPE_MASTER:
            default:
                $user_model = 'teacher_model';
                break;
            case USER_TYPE_CHILDREN:
                $user_model = 'children_model';
                break;
            case USER_TYPE_PARENT:
                $user_model = 'parent_model';
                break;
        }
        
        return $this->{$user_model}->find_by_userid($user_id);
    }
}//end class

//get params
$language  = isset($argv[1])?$argv[1]:'english';
$user_type = isset($argv[2])?$argv[2]:-1;
$user_id   = isset($argv[3])?$argv[3]:-1;
$via_phone = isset($argv[4])?$argv[4]:0;
$via_email = isset($argv[5])?$argv[5]:0;

if($user_type === -1 || $user_id === -1)
{
    return;
}
if($via_phone === 0 && $via_email === 0)
{
    return;
}   

$sender = new send_security($language);
$sender->send_security_info($user_type, $user_id, $via_phone, $via_email);